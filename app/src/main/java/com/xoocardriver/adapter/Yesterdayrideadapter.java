//package com.xoocardriver.adapter;
//
//import android.app.ProgressDialog;
//import android.content.Context;
//import android.graphics.Typeface;
//import android.support.v4.app.FragmentManager;
//import android.support.v7.widget.RecyclerView;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import com.google.android.gms.maps.GoogleMap;
//import com.google.android.gms.maps.model.LatLng;
//import com.xoocardriver.R;
//import com.xoocardriver.map.GeocodingLocation;
//import com.xoocardriver.model.Detail;
//import com.xoocardriver.model.Grossamount;
//import com.xoocardriver.model.Yesterdaydetaillist;
//
//import java.util.ArrayList;
//
///**
// * Created by abc on 19-07-2017.
// */
//
//public  class Yesterdayrideadapter extends RecyclerView.Adapter<Yesterdayrideadapter.PurchasedListViewHolder> {
//
//    public ArrayList<Yesterdaydetaillist> list;
//    //  public static ArrayList<Orderlist> orderlist;
//    Context context;
//    // MainActivity mainActivity;
//    //  private List<Orderlist> list;
//    //  public static DBHelper dbhelper;
//    private GoogleMap googleMap;
//    private ProgressDialog mProgressDialog;
//    private String classID, semID;
//    public static FragmentManager fragmentManager;
//    private boolean flag = false;
//    private String check;
//    private int pos;
//    private GeocodingLocation geoLocation;
//    private LatLng latLng;
//
//    // private ArrayList<SearchVehicleType> searchCabArrayList;
//    public Yesterdayrideadapter(Context context, ArrayList<Yesterdaydetaillist> list) {
//        this.context = context;
//        this.list = list;
//    }
//
//    @Override
//    public Yesterdayrideadapter.PurchasedListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View vh = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_daysummary, parent, false);
//        Yesterdayrideadapter.PurchasedListViewHolder ViewHolder = new Yesterdayrideadapter.PurchasedListViewHolder(vh);
//        return ViewHolder;
//    }
//
//    @Override
//    public void onBindViewHolder(Yesterdayrideadapter.PurchasedListViewHolder holder, final int position) {
//
//        if (holder instanceof Yesterdayrideadapter.PurchasedListViewHolder) {
//
//            holder.timeTV.setText(list.get(position).getVehicleno());
//            holder.vehicletypeTv.setText(list.get(position).getBin());
//            // holder.costTv.setText(list.get(position).getTotal());
//            int basefare = Integer.parseInt(list.get(position).getBasefare());
//            int ridecharge =Integer.parseInt(list.get(position).getRidecharge());
//            int perminutchargeaccordingly =Integer.parseInt(list.get(position).getPerminutchargeaccordingly());
//            int taxcharge =Integer.parseInt(list.get(position).getGst());
//            //int coupon_price =Integer.parseInt(list.get(position).getCouponprice());
//            int toll_tax =Integer.parseInt(list.get(position).getTolltax());
//            int total_price = basefare + taxcharge + toll_tax  +ridecharge+perminutchargeaccordingly;
//            String total_pricestring =String.valueOf(total_price);
//            holder.costTv.setText(total_pricestring);
//            holder.costlabelTv.setText(list.get(position).getRidedate());
//            // Log.d("inside adapter ",list.get(position).getPrice()+" ");
//        }
//    }
//
//    @Override
//    public int getItemCount() {
//        return list.size();
//    }
//
//    public class PurchasedListViewHolder extends RecyclerView.ViewHolder {
//        private LinearLayout headerLL, subHeaderLL;
//        private TextView timeTV, vehicletypeTv, costTv, costlabelTv, statuslabelTv, statusTv, sourceTv, destTv;
//        // MapView mMapView;
//        private RelativeLayout trip_statusRl, trip_cashRl;
//
//        public PurchasedListViewHolder(View itemView) {
//            super(itemView);
//            timeTV = (TextView) itemView.findViewById(R.id.vehiclenotextviewone);
//            vehicletypeTv =(TextView) itemView.findViewById(R.id.binno);
//            costTv =(TextView) itemView.findViewById(R.id.grossamtrupees);
//            costlabelTv =(TextView) itemView.findViewById(R.id.datetextviewone);
//        }
//    }
//}
