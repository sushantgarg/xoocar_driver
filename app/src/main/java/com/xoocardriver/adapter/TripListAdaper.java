package com.xoocardriver.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xoocardriver.R;
import com.xoocardriver.model.TripList;

import java.util.ArrayList;


/**
 * Created by mindz on 23/9/16.
 */
public class TripListAdaper extends RecyclerView.Adapter<TripListAdaper.MyRidesHolder>{

    // private ArrayList<CourseClass> courseClassesList;
    //  public static ArrayList<Orderlist> orderlist;
    Context context;
    // MainActivity mainActivity;
    //  private List<Orderlist> list;
    //  public static DBHelper dbhelper;
    private ProgressDialog mProgressDialog;
    private String classID, semID;
    public static FragmentManager fragmentManager;
    private boolean flag = false;
    private String check;
    private int pos;
    private ArrayList<TripList> schedulList;


    public TripListAdaper(Context context, ArrayList<TripList> schedulList)
    {
        this.context = context;
        this.schedulList=schedulList;

    }

    @Override
    public MyRidesHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View vh = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_trip_list_item, parent, false);
        MyRidesHolder ViewHolder = new MyRidesHolder(vh);
        return ViewHolder;
    }

    @Override
    public void onBindViewHolder(MyRidesHolder holder, final int position) {

        if(holder instanceof MyRidesHolder)
        {
            holder.timeTV.setText(schedulList.get(position).getActualTime());
            holder.costTv.setText(schedulList.get(position).getActualCost());

            Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
            holder.timeTV.setTypeface(face);
            holder.vehicletypeTv.setTypeface(face);
            holder.costTv.setTypeface(face);
            holder.costlabelTv.setTypeface(face);
        }
    }

    @Override
    public int getItemCount() {
        return schedulList.size();
    }

    public class MyRidesHolder extends RecyclerView.ViewHolder {
        private TextView timeTV,vehicletypeTv,costTv,costlabelTv;

        public  MyRidesHolder(View itemView) {
            super(itemView);
            timeTV = (TextView) itemView.findViewById(R.id.custom_item_date_time_tv);
            vehicletypeTv = (TextView) itemView.findViewById(R.id.custom_item_vehicle_tv);
            costTv = (TextView) itemView.findViewById(R.id.custom_item_cost_label_tv);
            costlabelTv = (TextView) itemView.findViewById(R.id.custom_item_cost_tv);
        }
    }
}
