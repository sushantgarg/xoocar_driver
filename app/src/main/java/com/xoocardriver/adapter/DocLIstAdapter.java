package com.xoocardriver.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.xoocardriver.R;

import java.util.ArrayList;

/**
 * Created by Praveen Kumar on 31/8/16.
 */

public class DocLIstAdapter extends BaseAdapter {
    private Context mMainActivity;
    private int videoColumnIndex;
    private String title;
    private String thumbPath;
    String res_name;

    private ArrayList<String> vehicle;

    public DocLIstAdapter(Context mMainActivity, ArrayList<String> vehicle) {
        this.mMainActivity = mMainActivity;
        this.vehicle = vehicle;
    }

    public int getCount() {
        return vehicle.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View listItemRow = null;
        listItemRow = LayoutInflater.from(mMainActivity).inflate(R.layout.custom_doc_item, parent, false);
        TextView txtTitle = (TextView) listItemRow.findViewById(R.id.custom_doc_tv);
        txtTitle.setText(vehicle.get(position));
        return listItemRow;
    }
}
