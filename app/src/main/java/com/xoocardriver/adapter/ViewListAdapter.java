package com.xoocardriver.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.xoocardriver.R;
import com.xoocardriver.helper.CircularImageView;
import com.xoocardriver.helper.Constant;
import com.xoocardriver.helper.CustomVolleyRequest;
import com.xoocardriver.model.View_Document;
import com.xoocardriver.view.fragment.ViewDocImage;

import java.util.ArrayList;


/**
 * Created by mindz on 23/9/16.
 */
public class ViewListAdapter extends RecyclerView.Adapter<ViewListAdapter.PurchasedListViewHolder>{

    public ArrayList<View_Document> list;
    private Context mainActivity;

    public ViewListAdapter(Context mainActivity, ArrayList<View_Document> list)
    {
        this.mainActivity = mainActivity;
        this.list=list;
    }

    @Override
    public PurchasedListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vh = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_doc_view_item, parent, false);
        return new PurchasedListViewHolder(vh);
    }

    @Override
    public void onBindViewHolder(PurchasedListViewHolder holder, final int position) {

        if(holder != null)
        {
            holder.viewnameTv.setText(list.get(position).getName());

            ImageLoader im = CustomVolleyRequest.getInstance(mainActivity).getImageLoader();
            holder.picIv.setImageUrl(Constant.VIEW_DOCUMENT_IMAGE_URL +list.get(position).getDocImage(), im);

            Typeface face = Typeface.createFromAsset(mainActivity.getAssets(), "fonts/OpenSans-Regular.ttf");
            holder.viewBtn.setTypeface(face);
            holder.viewnameTv.setTypeface(face);

            holder.viewBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent i=new Intent(mainActivity,ViewDocImage.class);
                    i.putExtra("url",Constant.VIEW_DOCUMENT_IMAGE_URL +list.get(position).getDocImage());
                    i.putExtra("name",list.get(position).getName());
                    mainActivity.startActivity(i);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class PurchasedListViewHolder extends RecyclerView.ViewHolder {
        private TextView viewnameTv;
        private TextView viewBtn;
        private CircularImageView picIv;

        PurchasedListViewHolder(View itemView) {
            super(itemView);
            viewBtn = (TextView) itemView.findViewById(R.id.custom_view_doc_image_btn);
            viewnameTv = (TextView) itemView.findViewById(R.id.custom_view_name_tv);
            picIv = (CircularImageView) itemView.findViewById(R.id.custom_view_doc_image_iv);
        }
    }
}
