package com.xoocardriver.adapter;


import android.app.ProgressDialog;
import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.xoocardriver.R;
import com.xoocardriver.map.GeocodingLocation;

import com.xoocardriver.model.Thirtydays;

import java.util.ArrayList;

public class Thirtydaysadapter extends RecyclerView.Adapter<Thirtydaysadapter.PurchasedListViewHolder> {

    public ArrayList<Thirtydays> list;
    //  public static ArrayList<Orderlist> orderlist;
    Context context;
    // MainActivity mainActivity;
    //  private List<Orderlist> list;
    //  public static DBHelper dbhelper;
    private GoogleMap googleMap;
    private ProgressDialog mProgressDialog;
    private String classID, semID;
    public static FragmentManager fragmentManager;
    private boolean flag = false;
    private String check;
    private int pos;

    private GeocodingLocation geoLocation;
    private LatLng latLng;

    // private ArrayList<SearchVehicleType> searchCabArrayList;
    public Thirtydaysadapter(Context context, ArrayList<Thirtydays> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public Thirtydaysadapter.PurchasedListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vh = LayoutInflater.from(parent.getContext()).inflate(R.layout.activitysevendays, parent, false);
        Thirtydaysadapter.PurchasedListViewHolder ViewHolder = new Thirtydaysadapter.PurchasedListViewHolder(vh);
        return ViewHolder;
    }

    @Override
    public void onBindViewHolder(Thirtydaysadapter.PurchasedListViewHolder holder, int position) {
        if (holder instanceof Thirtydaysadapter.PurchasedListViewHolder) {
            holder.timeTV.setText(list.get(position).getDate());
            holder.vehicletypeTv.setText(list.get(position).getTotalride());
            holder.costTv.setText(list.get(position).getCancelledride());
            String Totalprioce=list.get(position).getTotalprice();
            holder.costlabelTv.setText(list.get(position).getTotalprice());
            if (Totalprioce!=null){
                double costlabelTv1 = Double.parseDouble(String.valueOf(Totalprioce));
                double costlabelTv2 = Math.round(costlabelTv1*100)/100;
                Log.e("roundoff",""+costlabelTv2);
                holder.costlabelTv.setText(""+costlabelTv2);
            }
        }
    }






    @Override
    public int getItemCount() {
        return list.size();
    }

    public class PurchasedListViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout headerLL, subHeaderLL;
        private TextView timeTV, vehicletypeTv, costTv, costlabelTv, statuslabelTv, statusTv, sourceTv, destTv;
        // MapView mMapView;
        private RelativeLayout trip_statusRl, trip_cashRl;

        public PurchasedListViewHolder(View itemView) {
            super(itemView);
            timeTV = (TextView) itemView.findViewById(R.id.datetextdescription);
            vehicletypeTv =(TextView) itemView.findViewById(R.id.vehiclenotextdescription);
            costTv =(TextView) itemView.findViewById(R.id.binnotextdesription);
            costlabelTv =(TextView) itemView.findViewById(R.id.rideamttextdesription);
           /* if(costlabelTv!=null){
                double costlabelTv1 = Double.parseDouble(String.valueOf(costlabelTv));
                double costlabelTv2 = Math.round(costlabelTv1*100)/100;
                Log.e("roundoff",""+costlabelTv2);
                costlabelTv.setText(""+costlabelTv2);*/

        }
    }
}



