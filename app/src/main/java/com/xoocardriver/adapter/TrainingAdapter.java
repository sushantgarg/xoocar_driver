package com.xoocardriver.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xoocardriver.R;
import com.xoocardriver.RetroFit.GetTrainingVideos.GetTrainingVideoResponceData;

import java.util.List;

/**
 * Created by sushant on 9/8/17.
 */

public class TrainingAdapter extends RecyclerView.Adapter<TrainingAdapter.FollowerViewHolder> {

    private List<GetTrainingVideoResponceData> couponList;
    private Context context;

    public TrainingAdapter(List<GetTrainingVideoResponceData> couponList, Context ctx) {
        this.couponList =couponList;
        context = ctx;
    }

    @Override
    public FollowerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row_training, parent, false);
        return new FollowerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final FollowerViewHolder holder, final int position) {
        final GetTrainingVideoResponceData coupon = couponList.get(holder.getAdapterPosition());

        holder.trainingLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(coupon.getUrl())));
            }
        });

        holder.trainingText.setText(coupon.getDesc());

    }

    @Override
    public int getItemCount() {
        return couponList.size();
    }

    class FollowerViewHolder extends RecyclerView.ViewHolder {

        private TextView trainingText;
        private LinearLayout trainingLinear;


        FollowerViewHolder(View convertView) {
            super(convertView);

            trainingText = (TextView) convertView.findViewById(R.id.trainingText);
            trainingLinear = (LinearLayout) convertView.findViewById(R.id.trainingLinear);

        }

    }
}
