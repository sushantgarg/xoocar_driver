package com.xoocardriver.adapter;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.xoocardriver.R;
import com.xoocardriver.helper.FileDownloader;
import com.xoocardriver.helper.Way_bill;
import com.xoocardriver.model.Model;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;


/**
 * Created by mindz on 23/9/16.
 */
public class WayBillListAdapter extends RecyclerView.Adapter<WayBillListAdapter.PurchasedListViewHolder> {

    public ArrayList<Way_bill> list;
    private Context mainActivity;
    private ImageLoader im;
    private ProgressDialog progressDialog;

    public WayBillListAdapter(Context mainActivity, ArrayList<Way_bill> list) {
        this.mainActivity = mainActivity;
        this.list = list;
        progressDialog = new ProgressDialog(mainActivity);
    }

    @Override
    public PurchasedListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vh = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_way_bill_item, parent, false);
        return new PurchasedListViewHolder(vh);
    }

    @Override
    public void onBindViewHolder(PurchasedListViewHolder holder, final int position) {

        if (holder != null) {
            holder.sourceTv.setText(list.get(position).getSource());
            holder.destinationTv.setText(list.get(position).getDestination());

            Typeface face = Typeface.createFromAsset(mainActivity.getAssets(), "fonts/OpenSans-Regular.ttf");
            holder.sourceTv.setTypeface(face);
            holder.destinationTv.setTypeface(face);

            holder.download_docLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    DownloadFile downloadFile = new DownloadFile();
//                    downloadFile.execute(Constant.DOCUMENT_URL + list.get(position).getWayBill()
//                            , list.get(position).getWayBill());

                    String url = "http://velenesa.com/vele/index.php/"+list.get(position).getWayBill();
                    DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
                    request.setDescription("Bill pdf");
                    request.setTitle("Bill Summary");
// in order for this if to run, you must use the android 3.2 to compile your app
                    request.allowScanningByMediaScanner();
                    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                    request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, list.get(position).getWayBill()+".ext");

// get download service and enqueue file
                    DownloadManager manager = (DownloadManager) mainActivity.getSystemService(Context.DOWNLOAD_SERVICE);
                    manager.enqueue(request);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class PurchasedListViewHolder extends RecyclerView.ViewHolder {
        private TextView sourceTv;
        private TextView destinationTv;
        private LinearLayout download_docLL;

        public PurchasedListViewHolder(View itemView) {
            super(itemView);
            sourceTv = (TextView) itemView.findViewById(R.id.custom_way_bill_source_tv);
            destinationTv = (TextView) itemView.findViewById(R.id.custom_way_bill_destination_tv);
            download_docLL = (LinearLayout) itemView.findViewById(R.id.custom_way_download_ll);
        }
    }

    private class DownloadFile extends AsyncTask<String, String, String> {
        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.dismiss();
            Model.ShowToast(mainActivity, "Downloaded successfully, find the file in downloads of the phone");
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            progressDialog.setProgress(Integer.parseInt(values[0]));
        }

        @Override
        protected String doInBackground(String... strings) {

            String fileUrl = strings[0];   // -> http://maven.apache.org/maven-1.x/maven.pdf
            String fileName = strings[1].replace(" ", ""); // -> maven.pdf


            String fileName1 = fileName.replace("/", "");
            //  File pdfFile = new File("/storage/emulated/0/", fileName1);
            File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), fileName1);
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

            String path = "/storage/emulated/0/" + fileName1;
          /*  Log.d("path dkp",":"+fileName1);
            File targetFile = new File(path);*/
            Uri targetUri = Uri.fromFile(file);
            Log.d("get from file path dkp", ":" + targetUri);
            FileDownloader.downloadFile(fileUrl, file);
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                progressDialog.setMessage("Please wait downloading file...");
                progressDialog.setIndeterminate(true);
                progressDialog.setMax(100);
                progressDialog.setCancelable(false);
                progressDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
            //   progressDialog.show();
        }
    }
}
