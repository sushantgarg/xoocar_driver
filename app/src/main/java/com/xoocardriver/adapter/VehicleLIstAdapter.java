package com.xoocardriver.adapter;

import android.content.Context;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.xoocardriver.R;
import com.xoocardriver.model.VihicleListHome;

import java.util.ArrayList;

/**
 * Created by Praveen Kumar on 31/8/16.
 */

public class VehicleLIstAdapter extends BaseAdapter {
    protected Context mMainActivity;
    private int videoColumnIndex;
    private String title;
    private String thumbPath;
    String res_name;

    private ArrayList<VihicleListHome> vehicle;

    private String[] thumbColumns = {MediaStore.Video.Thumbnails.DATA, MediaStore.Video.Thumbnails.VIDEO_ID};

    public VehicleLIstAdapter(Context mMainActivity, ArrayList<VihicleListHome> vehicle) {
        this.mMainActivity = mMainActivity;
        this.vehicle = vehicle;
    }

    public int getCount() {
        return vehicle.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View listItemRow = null;
        listItemRow = LayoutInflater.from(mMainActivity).inflate(R.layout.custom_sp_item_city, parent, false);
        TextView txtTitle = (TextView) listItemRow.findViewById(R.id.custom_city_tv);
        TextView subtxtTitle = (TextView) listItemRow.findViewById(R.id.custom_city_no_tv);
        txtTitle.setText(vehicle.get(position).getVVehiclename());
        subtxtTitle.setText(vehicle.get(position).getVLicenceNo());
        return listItemRow;
    }
}
