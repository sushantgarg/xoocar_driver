package com.xoocardriver.adapter;

/**
 * Created by mindz on 19/1/17.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.xoocardriver.R;
import com.xoocardriver.helper.Constant;
import com.xoocardriver.model.Contact;
import com.xoocardriver.preference.PrefManager;
import com.xoocardriver.view.fragment.EmergencyContactFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class GetAllContactsAdapter extends RecyclerView.Adapter<GetAllContactsAdapter.ContactViewHolder> {

    private List<Contact> contactVOList;
    private ProgressDialog progressDialog;
    private Context mainActivity;
    private PrefManager prefManager;

    public GetAllContactsAdapter(Context mainActivity, List<Contact> contactVOList) {
        this.contactVOList = contactVOList;
        this.mainActivity = mainActivity;
        prefManager = new PrefManager(mainActivity);
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mainActivity).inflate(R.layout.custom_item_contact_details, null);
        ContactViewHolder contactViewHolder = new ContactViewHolder(view);
        return contactViewHolder;
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, final int position) {
        Contact contactVO = contactVOList.get(position);
        holder.tvContactName.setText(contactVO.getName());
        holder.tvPhoneNumber.setText(contactVO.getContactNo());
        if (contactVOList.get(position).getRStatus().equals("1")) {
            holder.onoffTBn.setChecked(true);
        } else {
            holder.onoffTBn.setChecked(false);
        }


        Typeface face = Typeface.createFromAsset(mainActivity.getAssets(), "fonts/OpenSans-Regular.ttf");
        holder.tvContactName.setTypeface(face);
        holder.tvPhoneNumber.setTypeface(face);
        holder.tvPhoneNumber.setTypeface(face);
        holder.tvcontactDetails.setTypeface(face);


        holder.deleteContactIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    progressDialog = new ProgressDialog(mainActivity);
                    progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
                    progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                    progressDialog.show();
                    progressDialog.setContentView(R.layout.progressbar);
                } catch (NullPointerException e) {
                }
                deleteContact(mainActivity, Constant.DELETE_EMERGENCY_CONTACT_URL, contactVOList.get(position).getId(), position);
            }
        });

        holder.onoffTBn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    try {
                        progressDialog = new ProgressDialog(mainActivity);
                        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
                        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                        progressDialog.show();
                        progressDialog.setContentView(R.layout.progressbar);
                    } catch (NullPointerException e) {
                    }
                    onOffContact(mainActivity, Constant.STATUS_EMERGENCY_CONTACT_URL, contactVOList.get(position).getId(), "1");
                } else {
                    try {

                        progressDialog = new ProgressDialog(mainActivity);
                        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
                        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                        progressDialog.show();
                        progressDialog.setContentView(R.layout.progressbar);

                    } catch (NullPointerException e) {
                    }
                    onOffContact(mainActivity, Constant.STATUS_EMERGENCY_CONTACT_URL, contactVOList.get(position).getId(), "0");
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return contactVOList.size();
    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder {

        ImageView deleteContactIv;
        TextView tvContactName;
        TextView tvcontactDetails;
        TextView tvPhoneNumber;
        ToggleButton onoffTBn;

        public ContactViewHolder(View itemView) {
            super(itemView);
            deleteContactIv = (ImageView) itemView.findViewById(R.id.custom_contact_item_delete_tv);
            tvContactName = (TextView) itemView.findViewById(R.id.custom_contact_item_name_tv);
            tvPhoneNumber = (TextView) itemView.findViewById(R.id.custom_contact_item_phone_tv);
            tvcontactDetails = (TextView) itemView.findViewById(R.id.custom_contact_item_details_tv);
            onoffTBn = (ToggleButton) itemView.findViewById(R.id.custom_contact_item_on_off_tv);
        }
    }

    private void delete(int pos) {
        contactVOList.remove(pos);
        notifyDataSetChanged();
        notifyItemRangeChanged(pos, contactVOList.size());

        if (contactVOList.size()==0)
        {
            EmergencyContactFragment.notfoundTv.setVisibility(View.VISIBLE);
        }
        else
        {
            EmergencyContactFragment.notfoundTv.setVisibility(View.GONE);
        }
    }

    public void deleteContact(Context context, String url, final String id, final int pos) {
        Log.d("delete contact:", " url = " + url + ":id:" + id);

        RequestQueue rq = Volley.newRequestQueue(context);
        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("delete contact res ", "= " + response);
                progressDialog.dismiss();

                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.has("Success")) {
                        delete(pos);
                        Toast.makeText(mainActivity,"Contact delete successfully", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                VolleyLog.d("Error", "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.d("error ocurred", "TimeoutError");
                    // Toast.makeText(context, "TimeoutError", Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    Log.d("error ocurred", "AuthFailureError");
                    //  Toast.makeText(context, "AuthFailureError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    Log.d("error ocurred", "ServerError");
                    // Toast.makeText(context, "ServerError", Toast.LENGTH_LONG).show();
                } else if (error instanceof NetworkError) {
                    Log.d("error ocurred", "NetworkError");
                    // Toast.makeText(context, "NetworkError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    Log.d("error ocurred", "ParseError");
                    //Toast.makeText(context, "ParseError", Toast.LENGTH_LONG).show();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                Log.d(" dkp uid", ":" + id);
                String uid = prefManager.getKeyUserId();
                String[] tag = {"uid", "act_mode"};
                String[] value = {id, "deleteemergencycontact"};

                for (int i = 0; i < tag.length; i++)
                    params.put(tag[i], value[i]);
                return checkParams(params);
            }
        };
        postReq.setRetryPolicy(new DefaultRetryPolicy(
                800000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(postReq);
    }

    public void onOffContact(Context context, String url, final String id, final String status) {
        Log.d("onOffContact contact:", " url = " + url + ":status:" + status + ":id:" + id);

        RequestQueue rq = Volley.newRequestQueue(context);
        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("onOffCon contact res ", "= " + response);
                progressDialog.dismiss();

                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    if (jsonObject.has("success")) {
                      //  Toast.makeText(mainActivity,"Contact delete successfully",Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                       // Toast.makeText(mainActivity,"Contact delete successfully",Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                VolleyLog.d("Error", "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.d("error ocurred", "TimeoutError");
                    // Toast.makeText(context, "TimeoutError", Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    Log.d("error ocurred", "AuthFailureError");
                    //  Toast.makeText(context, "AuthFailureError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    Log.d("error ocurred", "ServerError");
                    // Toast.makeText(context, "ServerError", Toast.LENGTH_LONG).show();
                } else if (error instanceof NetworkError) {
                    Log.d("error ocurred", "NetworkError");
                    // Toast.makeText(context, "NetworkError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    Log.d("error ocurred", "ParseError");
                    //Toast.makeText(context, "ParseError", Toast.LENGTH_LONG).show();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                Log.d(" dkp uid", ":" + id);
                String uid = prefManager.getKeyUserId();
                String[] tag = {"uid", "status"};
                String[] value = {id, status};

                for (int i = 0; i < tag.length; i++)
                    params.put(tag[i], value[i]);
                return checkParams(params);
            }
        };
        postReq.setRetryPolicy(new DefaultRetryPolicy(
                800000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(postReq);
    }

    private Map<String, String> checkParams(Map<String, String> map) {
        Iterator<Map.Entry<String, String>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> pairs = (Map.Entry<String, String>) it.next();
            if (pairs.getValue() == null) {
                map.put(pairs.getKey(), "");
            }
        }
        return map;
    }
}