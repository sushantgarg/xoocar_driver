//package com.xoocardriver.adapter;
//
//import android.content.Context;
//import android.graphics.Color;
//import android.graphics.Typeface;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.TextView;
//
//import com.android.volley.toolbox.ImageLoader;
//import com.xoocardriver.R;
//import com.xoocardriver.model.VehicleItem;
//import com.xoocardriver.view.activity.MainActivity;
//
//import java.util.ArrayList;
//
///**
// * Created by elite on 19/12/16.
// */
//
//public class SelectVehicleAdapter extends RecyclerView.Adapter<SelectVehicleAdapter.CustomViewHolder> {
//    private ImageLoader imageLoader;
//    private Typeface normal_custom_fonts, bold_custom_fonts;
//    private ArrayList<VehicleItem> vehicleItemArrayList;
//    View vh;
//
//    public SelectVehicleAdapter(Context mainActivity, ArrayList<VehicleItem> vehicleItemArrayList) {
//        this.vehicleItemArrayList=vehicleItemArrayList;
//        this.normal_custom_fonts = Typeface.createFromAsset(mainActivity.getAssets(), "fonts/OpenSans-Regular.ttf");
//        this.bold_custom_fonts = Typeface.createFromAsset(mainActivity.getAssets(), "fonts/OpenSans-Bold.ttf");
//    }
//
//    @Override
//    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        vh = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_select_vehicle_item, parent, false);
//        CustomViewHolder ViewHolder = new CustomViewHolder(vh);
//        return ViewHolder;
//    }
//
//    @Override
//    public void onBindViewHolder(CustomViewHolder holder, int position) {
//        holder.vehicle_num.setTypeface(normal_custom_fonts);
//        holder.vehicle_model.setTypeface(normal_custom_fonts);
//        holder.vehicle_type.setTypeface(normal_custom_fonts);
//
//        if(position %2 == 1)
//        {
//            // Set a background color for ListView regular row/item
//            vh.setBackgroundColor(Color.parseColor("#FFFFFF"));
//        }
//        else
//        {
//            // Set the background color for alternate row/item
//            vh.setBackgroundColor(Color.parseColor("#f0f0f0"));
//        }
//
//        holder.vehicle_num.setText(vehicleItemArrayList.get(position).getVehicle_num());
//        holder.vehicle_model.setText(vehicleItemArrayList.get(position).getVehicle_model());
//
//    }
//
//    @Override
//    public int getItemCount() {
//        return vehicleItemArrayList.size();
//    }
//
//    public class CustomViewHolder extends RecyclerView.ViewHolder {
//        private TextView vehicle_num, vehicle_model, vehicle_type;
//
//        public CustomViewHolder(View itemView) {
//            super(itemView);
//
//            vehicle_num = (TextView) itemView.findViewById(R.id.vehicle_no);
//            vehicle_model = (TextView) itemView.findViewById(R.id.vehicle_model);
//            vehicle_type= (TextView) itemView.findViewById(R.id.vehicle_type);
//        }
//    }
//}
//
