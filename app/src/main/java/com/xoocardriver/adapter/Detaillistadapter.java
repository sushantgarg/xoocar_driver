package com.xoocardriver.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xoocardriver.R;
import com.xoocardriver.RetroFit.GetEarning.Driverridedetail;
import com.xoocardriver.view.fragment.Tripsummaryactivitycash;

import java.util.List;

/**
 * Created by abc on 06-07-2017.
 */

public class Detaillistadapter extends RecyclerView.Adapter<Detaillistadapter.PurchasedListViewHolder> {

    public List<Driverridedetail> list;
    Context context;

    public Detaillistadapter(Context context, List<Driverridedetail> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public Detaillistadapter.PurchasedListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vh = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_data, parent, false);
        return new PurchasedListViewHolder(vh);
    }

    @Override
    public void onBindViewHolder( final PurchasedListViewHolder holder,final int position) {
        if (holder != null) {
            list.get(position).getOid();
            holder.vehicleNum.setText(list.get(position).getvLicenceNo());
            holder.binNum.setText(list.get(position).getCrnNo());

            holder.costTv.setText(list.get(position).getActualCost());
            holder.rideDate.setText(list.get(position).getRideDate());
        }

        assert holder != null;
        holder.dataLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i=new Intent(context,Tripsummaryactivitycash.class);
                i.putExtra("oid",list.get(holder.getAdapterPosition()).getOid());
                context.startActivity(i);

            }
        });

        Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
        assert holder != null;
        holder.vehicleNum.setTypeface(face);
        holder.binNum.setTypeface(face);
        holder.costTv.setTypeface(face);
        holder.rideDate.setTypeface(face);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    class PurchasedListViewHolder extends RecyclerView.ViewHolder {
        private TextView vehicleNum, binNum, costTv, rideDate;
        private LinearLayout dataLinear;
        // MapView mMapView;

        PurchasedListViewHolder(View itemView) {
            super(itemView);
            vehicleNum = (TextView) itemView.findViewById(R.id.vehiclenotextdescriptionone);
            binNum = (TextView) itemView.findViewById(R.id.binnotextdesriptionoone);
            costTv = (TextView) itemView.findViewById(R.id.rideamttextdesriptionone);
            rideDate = (TextView) itemView.findViewById(R.id.datetextdescriptionone);
            dataLinear = (LinearLayout) itemView.findViewById(R.id.dataLinear);
        }
     }
    }


