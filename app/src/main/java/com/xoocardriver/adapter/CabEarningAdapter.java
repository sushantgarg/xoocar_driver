package com.xoocardriver.adapter;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xoocardriver.R;
import com.xoocardriver.model.Model;
import com.xoocardriver.model.MyRides;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by mindz on 23/9/16.
 */
public class CabEarningAdapter extends RecyclerView.Adapter<CabEarningAdapter.MyRidesHolder>{
    Context context;
    private ArrayList<HashMap<String, String>> cabEarnigList;

    public CabEarningAdapter(Context context, ArrayList<HashMap<String, String>> cabEarnigList)
    {
        this.context = context;
        this.cabEarnigList=cabEarnigList;
    }

    @Override
    public MyRidesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vh = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_earing_item, parent, false);
        MyRidesHolder ViewHolder = new MyRidesHolder(vh);
        return ViewHolder;
    }

    @Override
    public void onBindViewHolder(MyRidesHolder holder, final int position) {

        if(holder instanceof MyRidesHolder)
        {
            holder.sourceaddressTv.setText(cabEarnigList.get(position).get("v_vehiclename"));
            holder.destinationaddressTv.setText(cabEarnigList.get(position).get("v_licence_no"));
            holder.earnigTv.setText(cabEarnigList.get(position).get("earing"));

            Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
            holder.sourceaddressTv.setTypeface(face);
            holder.earnigTv.setTypeface(face);
            holder.destinationaddressTv.setTypeface(face);
        }
    }

    @Override
    public int getItemCount() {
        return cabEarnigList.size();
    }

    public class MyRidesHolder extends RecyclerView.ViewHolder {
        private TextView destinationaddressTv,sourceaddressTv,earnigTv;

        public  MyRidesHolder(View itemView) {
            super(itemView);
            sourceaddressTv = (TextView) itemView.findViewById(R.id.custom_earnig_source_tv);
            destinationaddressTv = (TextView) itemView.findViewById(R.id.custom_earnig_destination_tv);
            earnigTv = (TextView) itemView.findViewById(R.id.custom_earnig_amount_tv);
        }
    }
}