package com.xoocardriver.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.xoocardriver.R;
import com.xoocardriver.helper.Constant;
import com.xoocardriver.helper.CustomVolleyRequest;
import com.xoocardriver.model.MyRides;

import java.util.ArrayList;

/**
 * Created by mindz on 23/9/16.
 */
public class RideEarningAdaper extends RecyclerView.Adapter<RideEarningAdaper.MyRidesHolder>{
    Context context;
    private ArrayList<MyRides>schedulList;

    public RideEarningAdaper(Context context, ArrayList<MyRides> schedulList)
    {
        this.context = context;
        this.schedulList=schedulList;

    }

    @Override
    public MyRidesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vh = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_earing_item, parent, false);
        MyRidesHolder ViewHolder = new MyRidesHolder(vh);
        return ViewHolder;
    }

    @Override
    public void onBindViewHolder(MyRidesHolder holder, final int position) {

        if(holder instanceof MyRidesHolder)
        {
            holder.sourceaddressTv.setText(schedulList.get(position).getSource());
            holder.destinationaddressTv.setText(schedulList.get(position).getDestination());
            holder.earnigTv.setText(schedulList.get(position).getEaring());

            Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
            holder.sourceaddressTv.setTypeface(face);
            holder.earnigTv.setTypeface(face);
            holder.destinationaddressTv.setTypeface(face);
        }
    }

    @Override
    public int getItemCount() {
        return schedulList.size();
    }

    public class MyRidesHolder extends RecyclerView.ViewHolder {
        private TextView destinationaddressTv,sourceaddressTv,earnigTv;

        public  MyRidesHolder(View itemView) {
            super(itemView);
            sourceaddressTv = (TextView) itemView.findViewById(R.id.custom_earnig_source_tv);
            destinationaddressTv = (TextView) itemView.findViewById(R.id.custom_earnig_destination_tv);
            earnigTv = (TextView) itemView.findViewById(R.id.custom_earnig_amount_tv);
        }
    }
}
