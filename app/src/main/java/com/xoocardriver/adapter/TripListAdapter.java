package com.xoocardriver.adapter;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.xoocardriver.R;
import com.xoocardriver.map.GeocodingLocation;
import com.xoocardriver.model.TripList;

import java.util.ArrayList;


/**
 * Created by mindz on 23/9/16.
 */
public class TripListAdapter extends RecyclerView.Adapter<TripListAdapter.PurchasedListViewHolder> {

    public ArrayList<TripList> list;
    //  public static ArrayList<Orderlist> orderlist;
    Context context;
    // MainActivity mainActivity;
    //  private List<Orderlist> list;
    //  public static DBHelper dbhelper;
    private GoogleMap googleMap;
    private ProgressDialog mProgressDialog;
    private String classID, semID;
    public static FragmentManager fragmentManager;
    private boolean flag = false;
    private String check;
    private int pos;
    private GeocodingLocation geoLocation;
    private LatLng latLng;

    // private ArrayList<SearchVehicleType> searchCabArrayList;
    public TripListAdapter(Context context, ArrayList<TripList> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public PurchasedListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vh = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_trip_list_item, parent, false);
        PurchasedListViewHolder ViewHolder = new PurchasedListViewHolder(vh);
        return ViewHolder;
    }

    @Override
    public void onBindViewHolder(PurchasedListViewHolder holder, final int position) {

        if (holder instanceof PurchasedListViewHolder) {
            holder.timeTV.setText(list.get(position).getDCreatedOn());
            holder.vehicletypeTv.setText(list.get(position).getVVehiclename());
            holder.sourceTv.setText(list.get(position).getSource());
            holder.destTv.setText(list.get(position).getDestination());

            String status = list.get(position).getBookingStatus().toString();
            if (status.equals("Completed")) {
                holder.trip_statusRl.setVisibility(View.GONE);
                holder.trip_cashRl.setVisibility(View.VISIBLE);
                holder.costTv.setText("₹" + list.get(position).getActualCost());
                holder.costlabelTv.setText(list.get(position).getPayMode());
            } else {
                holder.trip_statusRl.setVisibility(View.VISIBLE);
                holder.trip_cashRl.setVisibility(View.GONE);
                holder.statusTv.setText(list.get(position).getBookingStatus());
            }

            Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
            holder.timeTV.setTypeface(face);
            holder.vehicletypeTv.setTypeface(face);
            holder.costTv.setTypeface(face);
            holder.costlabelTv.setTypeface(face);
            holder.statusTv.setTypeface(face);
            //  holder.statuslabelTv.setTypeface(face);


           /* holder.mMapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap mMap) {
                    googleMap = mMap;

                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    try {
                        googleMap.setMyLocationEnabled(true);

                        latLng=geoLocation.getLocationFromAddress(context,list.get(position).getSource());

                        // For dropping a marker at a point on the Map 28.622864, 77.077613
                        if (googleMap!=null)
                        {
                            googleMap.clear();
                        }
                        LatLng sydney = new LatLng(latLng.latitude, latLng.longitude);
                        googleMap.addMarker(new MarkerOptions().position(sydney).title("Driver Location").snippet(list.get(position).getSource()).icon(BitmapDescriptorFactory.fromResource(R.drawable.green_location))).hideInfoWindow();

                        // For zooming automatically to the location of the marker
                    CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(8).build();
                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            });*/
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class PurchasedListViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout headerLL, subHeaderLL;
        private TextView timeTV, vehicletypeTv, costTv, costlabelTv, statuslabelTv, statusTv, sourceTv, destTv;
        // MapView mMapView;
        private RelativeLayout trip_statusRl, trip_cashRl;

        public PurchasedListViewHolder(View itemView) {
            super(itemView);
            timeTV = (TextView) itemView.findViewById(R.id.custom_item_date_time_tv);
            vehicletypeTv = (TextView) itemView.findViewById(R.id.custom_item_vehicle_tv);
            costTv = (TextView) itemView.findViewById(R.id.custom_item_cost_tv);
            costlabelTv = (TextView) itemView.findViewById(R.id.custom_item_cost_label_tv);
            sourceTv = (TextView) itemView.findViewById(R.id.custom_item_source_tv);
            destTv = (TextView) itemView.findViewById(R.id.custom_item_destination_tv);

            trip_statusRl = (RelativeLayout) itemView.findViewById(R.id.trip_status_rl);
            trip_cashRl = (RelativeLayout) itemView.findViewById(R.id.trip_cash_rl);
            statusTv = (TextView) itemView.findViewById(R.id.custom_item_status_tv);

            }
        }
    }

