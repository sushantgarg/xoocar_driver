package com.xoocardriver.realm;

import android.annotation.TargetApi;
import android.graphics.Path;
import android.location.Location;
import android.os.Build;
import android.provider.SyncStateContract;
import android.support.annotation.RequiresApi;

import com.xoocardriver.Constants;
import com.xoocardriver.helper.Constant;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by sushant on 21/7/17.
 */

public class RealmHandler {

    @TargetApi(Build.VERSION_CODES.KITKAT)

    public static Location getPathLastLocaion() {

        Location loc=new Location("gps");
        if(Realm.getDefaultInstance().where(PathLocations.class).findAll().size() > 0 ) {
            RealmResults<PathLocations> list=Realm.getDefaultInstance().where(PathLocations.class).findAll();
            PathLocations journey = list.last();
            loc.setLatitude(journey.getLatitute());
            loc.setLongitude(journey.getLongitude());
            loc.setAccuracy(journey.getAccuracy());
            loc.setBearing(journey.getCourse());
            loc.setSpeed(journey.getSpeed());
            loc.setTime(journey.getTimestamp());
        }

        return loc;

    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static void addLocationToPath(final PathLocations locationRealmPojo) {
        try(Realm realm=Realm.getDefaultInstance()) {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.insertOrUpdate(locationRealmPojo);
                }
            });
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static void clearPath() {
        try(Realm realm=Realm.getDefaultInstance()) {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                realm.delete(PathLocations.class);
                }
            });
        }
    }



    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static List<PathLocations> getUnsendPath() {
                  return Realm.getDefaultInstance().copyFromRealm(Realm.getDefaultInstance()
                          .where(PathLocations.class).equalTo("status", Constants.UNSEND).findAll());
        }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static void updatePathStatus(final List<PathLocations> pathJson) {
        try(Realm realm=Realm.getDefaultInstance()) {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    for (PathLocations loc :
                            pathJson) {
                        PathLocations pathLoc=realm.where(PathLocations.class).equalTo("uniqueId"
                                ,loc.getUniqueId()).findFirst();

                        if(pathLoc != null) pathLoc.setStatus(Constants.SEND);
                    }
                }
            });
        }
    }
}
