package com.xoocardriver;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.xoocardriver.RetroFit.GetTrainingVideos.GetTrainingVideoRequestFields;
import com.xoocardriver.RetroFit.GetTrainingVideos.GetTrainingVideoResponceFields;
import com.xoocardriver.RetroFit.RequestInterface;
import com.xoocardriver.adapter.TrainingAdapter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by sushant on 9/8/17.
 */

public class Training extends AppCompatActivity {
    private RecyclerView couponList;
    private Context ctx;
    private TrainingAdapter adapter;
    private LinearLayout noVidImg;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training);
        ctx=Training.this;
        couponList = (RecyclerView) findViewById(R.id.couponList);
        couponList.setLayoutManager(new LinearLayoutManager(ctx));
        noVidImg= (LinearLayout) findViewById(R.id.noVidImg);

        getTrainingVideos();
    }

    private void getTrainingVideos() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);

        Call<GetTrainingVideoResponceFields> call = request.getTrainingVideos("api/newcustomersapi/training/format/json/"
                , new GetTrainingVideoRequestFields("yt_training"));

        call.enqueue(new Callback<GetTrainingVideoResponceFields>() {
            @Override
            public void onResponse(Call<GetTrainingVideoResponceFields> call, retrofit2.Response<GetTrainingVideoResponceFields> response) {

                if( response.body() != null ) {
                    if( response.body().getResponseCode() == 400 ) {
                        if(response.body().getDataArray().size() > 0) {
                            adapter = new TrainingAdapter(response.body().getDataArray(), ctx);
                            couponList.setAdapter(adapter);
                            noVidImg.setVisibility(View.GONE);
                            couponList.setVisibility(View.VISIBLE);
                        } else {
                            noVidImg.setVisibility(View.VISIBLE);
                            couponList.setVisibility(View.GONE);
                        }
                    } else {
                        noVidImg.setVisibility(View.VISIBLE);
                        couponList.setVisibility(View.GONE);
                    }
                } else {
                    noVidImg.setVisibility(View.VISIBLE);
                    couponList.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<GetTrainingVideoResponceFields> call, Throwable t) {
            }
        });
    }
}
