package com.xoocardriver;

import java.util.UUID;

/**
 * Created by sushant on 21/7/17.
 */
public class UuidGenerator {

    public static String generateUuid() {
        // creating UUID
//        UUID uid = UUID.fromString("38400000-8cf0-11bd-b23e-10b96e4ef00d");

        // checking the value of random UUID
        return String.valueOf(UUID.randomUUID()).toUpperCase();
    }
}
