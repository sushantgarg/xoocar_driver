package com.xoocardriver.model;

/**
 * Created by elite on 19/12/16.
 */

public class VehicleItem {
    private String vehicle_num;
    private String vehicle_model;

    public void setVehicle_num(String vehicle_num) {
        this.vehicle_num = vehicle_num;
    }

    public String getVehicle_num() {
        return vehicle_num;
    }

    public void setVehicle_model(String vehicle_model) {
        this.vehicle_model = vehicle_model;
    }

    public String getVehicle_model() {
        return vehicle_model;
    }
}
