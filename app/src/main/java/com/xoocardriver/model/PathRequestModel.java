package com.xoocardriver.model;

import com.xoocardriver.realm.PathLocations;

import java.util.List;

import io.realm.RealmList;

/**
 * Created by sushant on 24/7/17.
 */

public class PathRequestModel {
    public void setLocations(List<PathLocations> locations) {
        this.locations = locations;
    }

    List<PathLocations> locations;
    String ride_id, d_id, d_latt, d_long;

    public PathRequestModel(List<PathLocations> locations, String ride_id, String d_id) {
        this.locations = locations;
        this.ride_id = ride_id;
        this.d_id = d_id;
    }

    public String getRide_id() {

        return ride_id;
    }

    public void setRide_id(String ride_id) {
        this.ride_id = ride_id;
    }

    public String getD_id() {
        return d_id;
    }

    public void setD_id(String d_id) {
        this.d_id = d_id;
    }

    public String getD_latt() {
        return d_latt;
    }

    public void setD_latt(String d_latt) {
        this.d_latt = d_latt;
    }

    public String getD_long() {
        return d_long;
    }

    public void setD_long(String d_long) {
        this.d_long = d_long;
    }

    public List<PathLocations> getLocations() {

        return locations;
    }

    public void setLocations(RealmList<PathLocations> locations) {
        this.locations = locations;
    }
}
