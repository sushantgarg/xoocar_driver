package com.xoocardriver.model;

/**
 * Created by mindz on 28/12/16.
 */

public class CabRateCard {
    private String id;
    private String name;
    private String capacity;
    private String dCreatedOn;
    private String nCreatedBy;
    private String dModifiedOn;
    private String nModifiedBy;
    private String bIsActive;
    private String nBusinessId;
    private String nAdminType;
    private String nStatus;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getDCreatedOn() {
        return dCreatedOn;
    }

    public void setDCreatedOn(String dCreatedOn) {
        this.dCreatedOn = dCreatedOn;
    }

    public String getNCreatedBy() {
        return nCreatedBy;
    }

    public void setNCreatedBy(String nCreatedBy) {
        this.nCreatedBy = nCreatedBy;
    }

    public String getDModifiedOn() {
        return dModifiedOn;
    }

    public void setDModifiedOn(String dModifiedOn) {
        this.dModifiedOn = dModifiedOn;
    }

    public String getNModifiedBy() {
        return nModifiedBy;
    }

    public void setNModifiedBy(String nModifiedBy) {
        this.nModifiedBy = nModifiedBy;
    }

    public String getBIsActive() {
        return bIsActive;
    }

    public void setBIsActive(String bIsActive) {
        this.bIsActive = bIsActive;
    }

    public String getNBusinessId() {
        return nBusinessId;
    }

    public void setNBusinessId(String nBusinessId) {
        this.nBusinessId = nBusinessId;
    }

    public String getNAdminType() {
        return nAdminType;
    }

    public void setNAdminType(String nAdminType) {
        this.nAdminType = nAdminType;
    }

    public String getNStatus() {
        return nStatus;
    }

    public void setNStatus(String nStatus) {
        this.nStatus = nStatus;
    }
}


