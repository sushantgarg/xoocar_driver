package com.xoocardriver.model;

/**
 * Created by abc on 04-07-2017.
 */

public class Grossamount {
    String ridedate;
    String brn;
    String price;
    String vehicle_mo;
    String total;
    String couponprice;
    String tolltax;
    String basefare;
    String ridecharge;
    String perminutchargeaccordingly;
    String waitcharge;
    String Gst;
    String net;

    public String getBasefare() {
        return basefare;
    }

    public void setBasefare(String basefare) {
        this.basefare = basefare;
    }

    public String getRidecharge() {
        return ridecharge;
    }

    public void setRidecharge(String ridecharge) {
        this.ridecharge = ridecharge;
    }

    public String getPerminutchargeaccordingly() {
        return perminutchargeaccordingly;
    }

    public void setPerminutchargeaccordingly(String perminutchargeaccordingly) {
        this.perminutchargeaccordingly = perminutchargeaccordingly;
    }

    public String getWaitcharge() {
        return waitcharge;
    }

    public void setWaitcharge(String waitcharge) {
        this.waitcharge = waitcharge;
    }

    public String getGst() {
        return Gst;
    }

    public void setGst(String gst) {
        Gst = gst;
    }

    public String getNet() {
        return net;
    }

    public void setNet(String net) {
        this.net = net;
    }

    public String getCouponprice() {
        return couponprice;
    }

    public void setCouponprice(String couponprice) {
        this.couponprice = couponprice;
    }

    public String getTolltax() {
        return tolltax;
    }

    public void setTolltax(String tolltax) {
        this.tolltax = tolltax;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getVehicle_mo() {
        return vehicle_mo;
    }

    public void setVehicle_mo(String vehicle_mo) {
        this.vehicle_mo = vehicle_mo;
    }

    public String getRidedate() {
        return ridedate;
    }

    public void setRidedate(String ridedate) {
        this.ridedate = ridedate;
    }

    public String getBrn() {
        return brn;
    }

    public void setBrn(String brn) {
        this.brn = brn;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }



    public class MyRidesHolder {
    }
}
