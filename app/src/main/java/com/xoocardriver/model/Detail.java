package com.xoocardriver.model;

/**
 * Created by abc on 06-07-2017.
 */

public class Detail {

    String ridedate;
    String bin;
    String rideprice;
    String vehicleno;
    String total;
    String couponprice;
    String tolltax;
    String basefare;
    String ridecharge;
    String perminutchargeaccordingly;
    String waitcharge;
    String Gst;
    String net;
    String rideid;

    public String getRideid() {
        return rideid;
    }

    public void setRideid(String rideid) {
        this.rideid = rideid;
    }

    public String getNet() {
        return net;
    }

    public void setNet(String net) {
        this.net = net;
    }

    public String getBasefare() {
        return basefare;
    }

    public void setBasefare(String basefare) {
        this.basefare = basefare;
    }

    public String getRidecharge() {
        return ridecharge;
    }

    public void setRidecharge(String ridecharge) {
        this.ridecharge = ridecharge;
    }

    public String getPerminutchargeaccordingly() {
        return perminutchargeaccordingly;
    }

    public void setPerminutchargeaccordingly(String perminutchargeaccordingly) {
        this.perminutchargeaccordingly = perminutchargeaccordingly;
    }

    public String getWaitcharge() {
        return waitcharge;
    }

    public void setWaitcharge(String waitcharge) {
        this.waitcharge = waitcharge;
    }

    public String getGst() {
        return Gst;
    }

    public void setGst(String gst) {
        Gst = gst;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getCouponprice() {
        return couponprice;
    }

    public void setCouponprice(String couponprice) {
        this.couponprice = couponprice;
    }

    public String getTolltax() {
        return tolltax;
    }

    public void setTolltax(String tolltax) {
        this.tolltax = tolltax;
    }

    public String getVehicleno() {
        return vehicleno;
    }

    public void setVehicleno(String vehicleno) {
        this.vehicleno = vehicleno;
    }

    public String getRidedate() {
        return ridedate;
    }

    public void setRidedate(String ridedate) {
        this.ridedate = ridedate;
    }

    public String getBin() {
        return bin;
    }

    public void setBin(String bin) {
        this.bin = bin;
    }

    public String getRideprice() {
        return rideprice;
    }

    public void setRideprice(String rideprice) {
        this.rideprice = rideprice;
    }


}

