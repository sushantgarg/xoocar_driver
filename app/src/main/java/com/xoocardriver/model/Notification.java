package com.xoocardriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/11/17.
 */

public class Notification {
    @SerializedName("Source")
    @Expose
    private String source;
    @SerializedName("detination")
    @Expose
    private String detination;
    @SerializedName("payment_type")
    @Expose
    private String paymentType;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("ride_id")
    @Expose
    private String rideId;

    private String c_latt,c_long;

    public String getC_latt() {
        return c_latt;
    }

    public void setC_latt(String c_latt) {
        this.c_latt = c_latt;
    }

    public String getC_long() {
        return c_long;
    }

    public void setC_long(String c_long) {
        this.c_long = c_long;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDetination() {
        return detination;
    }

    public void setDetination(String detination) {
        this.detination = detination;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRideId() {
        return rideId;
    }

    public void setRideId(String rideId) {
        this.rideId = rideId;
    }
}
