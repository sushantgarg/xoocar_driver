package com.xoocardriver.model;

/**
 * Created by abc on 19-07-2017.
 */

public class Thirtydays {
    String totalride, driverid, totalprice, date, cancelledride, completeride, shcedule;

    public String getTotalride() {
        return totalride;
    }

    public void setTotalride(String totalride) {
        this.totalride = totalride;
    }

    public String getDriverid() {
        return driverid;
    }

    public void setDriverid(String driverid) {
        this.driverid = driverid;
    }

    public String getTotalprice() {
        return totalprice;
    }

    public void setTotalprice(String totalprice) {
        this.totalprice = totalprice;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCancelledride() {
        return cancelledride;
    }

    public void setCancelledride(String cancelledride) {
        this.cancelledride = cancelledride;
    }

    public String getCompleteride() {
        return completeride;
    }

    public void setCompleteride(String completeride) {
        this.completeride = completeride;
    }

    public String getShcedule() {
        return shcedule;
    }

    public void setShcedule(String shcedule) {
        this.shcedule = shcedule;
    }
}