package com.xoocardriver.model;

/**
 * Created by mindz on 29/12/16.
 */

public class VihicleListHome {
    private String id;
    private String oId;
    private String vMaker;
    private String vModel;
    private String vYear;
    private String vCategory;
    private String vNoofseat;
    private String vAttachmentfor;
    private String kmAtthetimeofattach;
    private String vLicenceNo;
    private String vInteriorcolor;
    private String vExteriorcolor;
    private String vCarrier;
    private String vMusicsystem;
    private String vFireextengusher;
    private String vRadiometer;
    private Object dCreatedOn;
    private Object nCreatedBy;
    private Object dModifiedOn;
    private Object nModifiedBy;
    private Object nAdminType;
    private String vStatus;
    private String approveByAdmin;
    private String vVehiclename;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOId() {
        return oId;
    }

    public void setOId(String oId) {
        this.oId = oId;
    }

    public String getVMaker() {
        return vMaker;
    }

    public void setVMaker(String vMaker) {
        this.vMaker = vMaker;
    }

    public String getVModel() {
        return vModel;
    }

    public void setVModel(String vModel) {
        this.vModel = vModel;
    }

    public String getVYear() {
        return vYear;
    }

    public void setVYear(String vYear) {
        this.vYear = vYear;
    }

    public String getVCategory() {
        return vCategory;
    }

    public void setVCategory(String vCategory) {
        this.vCategory = vCategory;
    }

    public String getVNoofseat() {
        return vNoofseat;
    }

    public void setVNoofseat(String vNoofseat) {
        this.vNoofseat = vNoofseat;
    }

    public String getVAttachmentfor() {
        return vAttachmentfor;
    }

    public void setVAttachmentfor(String vAttachmentfor) {
        this.vAttachmentfor = vAttachmentfor;
    }

    public String getKmAtthetimeofattach() {
        return kmAtthetimeofattach;
    }

    public void setKmAtthetimeofattach(String kmAtthetimeofattach) {
        this.kmAtthetimeofattach = kmAtthetimeofattach;
    }

    public String getVLicenceNo() {
        return vLicenceNo;
    }

    public void setVLicenceNo(String vLicenceNo) {
        this.vLicenceNo = vLicenceNo;
    }

    public String getVInteriorcolor() {
        return vInteriorcolor;
    }

    public void setVInteriorcolor(String vInteriorcolor) {
        this.vInteriorcolor = vInteriorcolor;
    }

    public String getVExteriorcolor() {
        return vExteriorcolor;
    }

    public void setVExteriorcolor(String vExteriorcolor) {
        this.vExteriorcolor = vExteriorcolor;
    }

    public String getVCarrier() {
        return vCarrier;
    }

    public void setVCarrier(String vCarrier) {
        this.vCarrier = vCarrier;
    }

    public String getVMusicsystem() {
        return vMusicsystem;
    }

    public void setVMusicsystem(String vMusicsystem) {
        this.vMusicsystem = vMusicsystem;
    }

    public String getVFireextengusher() {
        return vFireextengusher;
    }

    public void setVFireextengusher(String vFireextengusher) {
        this.vFireextengusher = vFireextengusher;
    }

    public String getVRadiometer() {
        return vRadiometer;
    }

    public void setVRadiometer(String vRadiometer) {
        this.vRadiometer = vRadiometer;
    }

    public Object getDCreatedOn() {
        return dCreatedOn;
    }

    public void setDCreatedOn(Object dCreatedOn) {
        this.dCreatedOn = dCreatedOn;
    }

    public Object getNCreatedBy() {
        return nCreatedBy;
    }

    public void setNCreatedBy(Object nCreatedBy) {
        this.nCreatedBy = nCreatedBy;
    }

    public Object getDModifiedOn() {
        return dModifiedOn;
    }

    public void setDModifiedOn(Object dModifiedOn) {
        this.dModifiedOn = dModifiedOn;
    }

    public Object getNModifiedBy() {
        return nModifiedBy;
    }

    public void setNModifiedBy(Object nModifiedBy) {
        this.nModifiedBy = nModifiedBy;
    }

    public Object getNAdminType() {
        return nAdminType;
    }

    public void setNAdminType(Object nAdminType) {
        this.nAdminType = nAdminType;
    }

    public String getVStatus() {
        return vStatus;
    }

    public void setVStatus(String vStatus) {
        this.vStatus = vStatus;
    }

    public String getApproveByAdmin() {
        return approveByAdmin;
    }

    public void setApproveByAdmin(String approveByAdmin) {
        this.approveByAdmin = approveByAdmin;
    }

    public String getVVehiclename() {
        return vVehiclename;
    }

    public void setVVehiclename(String vVehiclename) {
        this.vVehiclename = vVehiclename;
    }

}
