package com.xoocardriver.model;

/**
 * Created by mindz on 15/12/16.
 */

public class MyRides {

    private String firstName;
    private String userEmail;
    private String contact;
    private String dname;
    private String driverPic;
    private String demail;
    private String dcontact;
    private String cabName;
    private String licenceNo;
    private String oid;
    private String driverId;
    private String vehicalId;
    private String customerId;
    private String crnNo;
    private String source;
    private String destination;
    private String estimatedCost;
    private String estimatedTime;
    private String pickupTime;
    private String diliverTime;
    private String actualCost;
    private String actualTime;
    private Object dCreatedOn;
    private Object nCreatedBy;
    private Object dModifiedOn;
    private Object nModifiedBy;
    private String bDeleted;
    private String bookingStatus;
    private String bCanceledBy;
    private String cancelledWhy;
    private Object nAdminType;
    private String vLicenceNo;
    private String vVehiclename;
    private String earing;
    private String actualKms;


    public String getActualKms() {
        return actualKms;
    }

    public void setActualKms(String actualKms) {
        this.actualKms = actualKms;
    }

    public String getVLicenceNo() {
        return vLicenceNo;
    }

    public void setVLicenceNo(String vLicenceNo) {
        this.vLicenceNo = vLicenceNo;
    }

    public String getVVehiclename() {
        return vVehiclename;
    }

    public void setVVehiclename(String vVehiclename) {
        this.vVehiclename = vVehiclename;
    }

    public String getEaring() {
        return earing;
    }

    public void setEaring(String earing) {
        this.earing = earing;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getDname() {
        return dname;
    }

    public void setDname(String dname) {
        this.dname = dname;
    }

    public String getDriverPic() {
        return driverPic;
    }

    public void setDriverPic(String driverPic) {
        this.driverPic = driverPic;
    }

    public String getDemail() {
        return demail;
    }

    public void setDemail(String demail) {
        this.demail = demail;
    }

    public String getDcontact() {
        return dcontact;
    }

    public void setDcontact(String dcontact) {
        this.dcontact = dcontact;
    }

    public String getCabName() {
        return cabName;
    }

    public void setCabName(String cabName) {
        this.cabName = cabName;
    }

    public String getLicenceNo() {
        return licenceNo;
    }

    public void setLicenceNo(String licenceNo) {
        this.licenceNo = licenceNo;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getVehicalId() {
        return vehicalId;
    }

    public void setVehicalId(String vehicalId) {
        this.vehicalId = vehicalId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCrnNo() {
        return crnNo;
    }

    public void setCrnNo(String crnNo) {
        this.crnNo = crnNo;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getEstimatedCost() {
        return estimatedCost;
    }

    public void setEstimatedCost(String estimatedCost) {
        this.estimatedCost = estimatedCost;
    }

    public String getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(String estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public String getPickupTime() {
        return pickupTime;
    }

    public void setPickupTime(String pickupTime) {
        this.pickupTime = pickupTime;
    }

    public String getDiliverTime() {
        return diliverTime;
    }

    public void setDiliverTime(String diliverTime) {
        this.diliverTime = diliverTime;
    }

    public String getActualCost() {
        return actualCost;
    }

    public void setActualCost(String actualCost) {
        this.actualCost = actualCost;
    }

    public String getActualTime() {
        return actualTime;
    }

    public void setActualTime(String actualTime) {
        this.actualTime = actualTime;
    }

    public Object getDCreatedOn() {
        return dCreatedOn;
    }

    public void setDCreatedOn(Object dCreatedOn) {
        this.dCreatedOn = dCreatedOn;
    }

    public Object getNCreatedBy() {
        return nCreatedBy;
    }

    public void setNCreatedBy(Object nCreatedBy) {
        this.nCreatedBy = nCreatedBy;
    }

    public Object getDModifiedOn() {
        return dModifiedOn;
    }

    public void setDModifiedOn(Object dModifiedOn) {
        this.dModifiedOn = dModifiedOn;
    }

    public Object getNModifiedBy() {
        return nModifiedBy;
    }

    public void setNModifiedBy(Object nModifiedBy) {
        this.nModifiedBy = nModifiedBy;
    }

    public String getBDeleted() {
        return bDeleted;
    }

    public void setBDeleted(String bDeleted) {
        this.bDeleted = bDeleted;
    }

    public String getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public String getBCanceledBy() {
        return bCanceledBy;
    }

    public void setBCanceledBy(String bCanceledBy) {
        this.bCanceledBy = bCanceledBy;
    }

    public String getCancelledWhy() {
        return cancelledWhy;
    }

    public void setCancelledWhy(String cancelledWhy) {
        this.cancelledWhy = cancelledWhy;
    }

    public Object getNAdminType() {
        return nAdminType;
    }

    public void setNAdminType(Object nAdminType) {
        this.nAdminType = nAdminType;
    }

}

