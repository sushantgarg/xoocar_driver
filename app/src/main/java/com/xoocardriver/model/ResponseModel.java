package com.xoocardriver.model;

/**
 * Created by sushant on 24/7/17.
 */

public class ResponseModel<GENERIC> {
    private String status;
    private String message;
    private GENERIC value;
    private GENERIC[] values;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public GENERIC getValue() {
        return value;
    }

    public void setValue(GENERIC value) {
        this.value = value;
    }

    public GENERIC[] getValues() {
        return values;
    }

    public void setValues(GENERIC[] values) {
        this.values = values;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}