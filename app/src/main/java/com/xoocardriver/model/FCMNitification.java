package com.xoocardriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mindz on 6/12/16.
 */


public class FCMNitification {

    private static String subtitle;
    private static String notificationType;
    private static Integer sound;
    private static String title;
    private static Integer vibrate;
    private static String tickerText;
    private static String driverId;
    private static String actualcost;
    private static String pay_status;
    /////////////////////////////
    @SerializedName("Source")
    @Expose
    private String source;
    @SerializedName("detination")
    @Expose
    private String detination;
    @SerializedName("payment_type")
    @Expose
    private String paymentType;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("ride_id")
    @Expose
    private String rideId;
    @SerializedName("a_driver_ids")
    @Expose
    private List<Object> aDriverIds = null;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDetination() {
        return detination;
    }

    public void setDetination(String detination) {
        this.detination = detination;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRideId() {
        return rideId;
    }

    public void setRideId(String rideId) {
        this.rideId = rideId;
    }

    public List<Object> getADriverIds() {
        return aDriverIds;
    }

    public void setADriverIds(List<Object> aDriverIds) {
        this.aDriverIds = aDriverIds;
    }



    /////////////////////////////


    public String getActualcost() {
        return actualcost;
    }


    public void setActualcost(String actualcost) {
        this.actualcost = actualcost;
    }


    public String getSubtitle() {
        return subtitle;
    }


    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }


    public String getNotificationType() {
        return notificationType;
    }

    /**
     * @param notificationType The notification_type
     */
    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    /**
     * @return The sound
     */
    public Integer getSound() {
        return sound;
    }

    /**
     * @param sound The sound
     */
    public void setSound(Integer sound) {
        this.sound = sound;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The vibrate
     */
    public Integer getVibrate() {
        return vibrate;
    }

    /**
     * @param vibrate The vibrate
     */
    public void setVibrate(Integer vibrate) {
        this.vibrate = vibrate;
    }

    /**
     * @return The message
     */

    /**
     * @return The detination
     */

    /**
     * @return The pay_status
     */
    public String getPay_status() {
        return pay_status;
    }

    /**
     * @param pay_status The payment_type
     */
    public void setPay_status(String pay_status) {
        this.pay_status = pay_status;
    }

    /**
     * @return The rideId
     */


    /**
     * @return The driverId
     */
    public String getDriverId() {
        return driverId;
    }

    /**
     * @param driverId The ride_id
     */
    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    /**
     * @return The tickerText
     */
    public String getTickerText() {
        return tickerText;
    }

    /**
     * @param tickerText The tickerText
     */
    public void setTickerText(String tickerText) {
        this.tickerText = tickerText;
    }
}
