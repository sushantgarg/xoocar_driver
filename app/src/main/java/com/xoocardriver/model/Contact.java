package com.xoocardriver.model;

/**
 * Created by mindz on 20/1/17.
 */

public class Contact {
    private String id;
    private String uid;
    private String name;
    private String contactNo;
    private String rStatus;
    private String rCreatedon;
    private Object rModifiedon;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getRStatus() {
        return rStatus;
    }

    public void setRStatus(String rStatus) {
        this.rStatus = rStatus;
    }

    public String getRCreatedon() {
        return rCreatedon;
    }

    public void setRCreatedon(String rCreatedon) {
        this.rCreatedon = rCreatedon;
    }

    public Object getRModifiedon() {
        return rModifiedon;
    }

    public void setRModifiedon(Object rModifiedon) {
        this.rModifiedon = rModifiedon;
    }
}

