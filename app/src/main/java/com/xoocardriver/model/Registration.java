package com.xoocardriver.model;

/**
 * Created by mindz on 1/12/16.
 */
public class Registration {
    private String uid;
    private String userType;
    private String oId;
    private String firstName;
    private Object lastName;
    private String userEmail;
    private String contact;
    private Object age;
    private String gender;
    private String dob;
    private String profilePic;
    private String regPassword;
    private String address;
    private String state;
    private String city;
    private String devicetype;
    private String devicetoken;
    private String createdOn;
    private String tStatus;

    /**
     *
     * @return
     * The uid
     */
    public String getUid() {
        return uid;
    }

    /**
     *
     * @param uid
     * The uid
     */
    public void setUid(String uid) {
        this.uid = uid;
    }

    /**
     *
     * @return
     * The userType
     */
    public String getUserType() {
        return userType;
    }

    /**
     *
     * @param userType
     * The user_type
     */
    public void setUserType(String userType) {
        this.userType = userType;
    }

    /**
     *
     * @return
     * The oId
     */
    public String getOId() {
        return oId;
    }

    /**
     *
     * @param oId
     * The o_id
     */
    public void setOId(String oId) {
        this.oId = oId;
    }

    /**
     *
     * @return
     * The firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     *
     * @param firstName
     * The first_name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     *
     * @return
     * The lastName
     */
    public Object getLastName() {
        return lastName;
    }

    /**
     *
     * @param lastName
     * The last_name
     */
    public void setLastName(Object lastName) {
        this.lastName = lastName;
    }

    /**
     *
     * @return
     * The userEmail
     */
    public String getUserEmail() {
        return userEmail;
    }

    /**
     *
     * @param userEmail
     * The user_email
     */
    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    /**
     *
     * @return
     * The contact
     */
    public String getContact() {
        return contact;
    }

    /**
     *
     * @param contact
     * The contact
     */
    public void setContact(String contact) {
        this.contact = contact;
    }

    /**
     *
     * @return
     * The age
     */
    public Object getAge() {
        return age;
    }

    /**
     *
     * @param age
     * The age
     */
    public void setAge(Object age) {
        this.age = age;
    }

    /**
     *
     * @return
     * The gender
     */
    public String getGender() {
        return gender;
    }

    /**
     *
     * @param gender
     * The gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     *
     * @return
     * The dob
     */
    public String getDob() {
        return dob;
    }

    /**
     *
     * @param dob
     * The dob
     */
    public void setDob(String dob) {
        this.dob = dob;
    }

    /**
     *
     * @return
     * The profilePic
     */
    public String getProfilePic() {
        return profilePic;
    }

    /**
     *
     * @param profilePic
     * The profile_pic
     */
    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    /**
     *
     * @return
     * The regPassword
     */
    public String getRegPassword() {
        return regPassword;
    }

    /**
     *
     * @param regPassword
     * The reg_password
     */
    public void setRegPassword(String regPassword) {
        this.regPassword = regPassword;
    }

    /**
     *
     * @return
     * The address
     */
    public String getAddress() {
        return address;
    }

    /**
     *
     * @param address
     * The address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     *
     * @return
     * The state
     */
    public String getState() {
        return state;
    }

    /**
     *
     * @param state
     * The state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     *
     * @return
     * The city
     */
    public String getCity() {
        return city;
    }

    /**
     *
     * @param city
     * The city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     *
     * @return
     * The devicetype
     */
    public String getDevicetype() {
        return devicetype;
    }

    /**
     *
     * @param devicetype
     * The devicetype
     */
    public void setDevicetype(String devicetype) {
        this.devicetype = devicetype;
    }

    /**
     *
     * @return
     * The devicetoken
     */
    public String getDevicetoken() {
        return devicetoken;
    }

    /**
     *
     * @param devicetoken
     * The devicetoken
     */
    public void setDevicetoken(String devicetoken) {
        this.devicetoken = devicetoken;
    }

    /**
     *
     * @return
     * The createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     *
     * @param createdOn
     * The created_on
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    /**
     *
     * @return
     * The tStatus
     */
    public String getTStatus() {
        return tStatus;
    }

    /**
     *
     * @param tStatus
     * The t_status
     */
    public void setTStatus(String tStatus) {
        this.tStatus = tStatus;
    }

}