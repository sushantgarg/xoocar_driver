package com.xoocardriver.model;

import com.xoocardriver.view.fragment.StartTrip;

/**
 * Created by mindz on 5/1/17.
 */

public class Trip_complete {

    private static  String total;
    private static  String source;
    private static  String destination;
    private static String basefare;
    private static String ridecharge;
    private static String waitcharge;
    private static String taxcharge;
    private static String conveniencecharge;
    private static String perminutchargeaccordingly;
    private static String pdflink;

    private static String actualKms;
    private static String actualTime;
    private static String payMode;
    private static String couponprice;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getCouponprice() {
        return couponprice;
    }

    public void setCouponprice(String couponprice) {
        this.couponprice = couponprice;
    }


    public void setSource(String source) {
        this.source = source;
    }

    public String getSource() {
        return source;
    }


    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDestination() {
        return destination;
    }


    public String getBasefare() {
        return basefare;
    }

    public void setBasefare(String basefare) {
        this.basefare = basefare;
    }

    public String getRidecharge() {
        return ridecharge;
    }

    public void setRidecharge(String ridecharge) {
        this.ridecharge = ridecharge;
    }

    public String getWaitcharge() {
        return waitcharge;
    }

    public void setWaitcharge(String waitcharge) {
        this.waitcharge = waitcharge;
    }

    public String getTaxcharge() {
        return taxcharge;
    }

    public void setTaxcharge(String taxcharge) {
        this.taxcharge = taxcharge;
    }

    public String getConveniencecharge() {
        return conveniencecharge;
    }

    public void setConveniencecharge(String conveniencecharge) {
        this.conveniencecharge = conveniencecharge;
    }

    public String getPerminutchargeaccordingly() {
        return perminutchargeaccordingly;
    }

    public void setPerminutchargeaccordingly(String perminutchargeaccordingly) {
        this.perminutchargeaccordingly = perminutchargeaccordingly;
    }

    public String getPdflink() {
        return pdflink;
    }

    public void setPdflink(String pdflink) {
        this.pdflink = pdflink;
    }


    public String getActualKms() {
        return actualKms;
    }

    public void setActualKms(String actualKms) {
        this.actualKms = actualKms;
    }

    public String getActualTime() {
        return actualTime;
    }

    public void setActualTime(String actualTime) {
        this.actualTime = actualTime;
    }

    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }


}