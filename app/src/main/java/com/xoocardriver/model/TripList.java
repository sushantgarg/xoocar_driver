package com.xoocardriver.model;

import java.util.List;

/**
 * Created by mindz on 3/1/17.
 */

public class TripList {


    private String oid;
    private String driverId;
    private String vehicalId;
    private String customerId;
    private String crnNo;
    private String source;
    private String destination;
    private String estimatedCost;
    private String estimatedTime;
    private String pickupTime;
    private String diliverTime;
    private String actualCost;
    private String actualTime;
    private String actualKms;
    private String isCoupon;
    private String couponId;
    private String couponDiscount;
    private String dCreatedOn;
    private Object nCreatedBy;
    private Object dModifiedOn;
    private Object nModifiedBy;
    private String bDeleted;
    private String bookingStatus;
    private String payMode;
    private String paySuccess;
    private String ridetype;
    private String wayBill;
    private String bCancelledBy;
    private String alter_driver;
    private String cancelledWhy;
    private Object nAdminType;
    private List<Object> rideSummary = null;
    private String vVehiclename;
    private List<Object> lattlong = null;

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getAlter_driver() {
        return alter_driver;
    }

    public void setAlter_driver(String alter_driver) {
        this.alter_driver = alter_driver;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getVehicalId() {
        return vehicalId;
    }

    public void setVehicalId(String vehicalId) {
        this.vehicalId = vehicalId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCrnNo() {
        return crnNo;
    }

    public void setCrnNo(String crnNo) {
        this.crnNo = crnNo;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getEstimatedCost() {
        return estimatedCost;
    }

    public void setEstimatedCost(String estimatedCost) {
        this.estimatedCost = estimatedCost;
    }

    public String getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(String estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public String getPickupTime() {
        return pickupTime;
    }

    public void setPickupTime(String pickupTime) {
        this.pickupTime = pickupTime;
    }

    public String getDiliverTime() {
        return diliverTime;
    }

    public void setDiliverTime(String diliverTime) {
        this.diliverTime = diliverTime;
    }

    public String getActualCost() {
        return actualCost;
    }

    public void setActualCost(String actualCost) {
        this.actualCost = actualCost;
    }

    public String getActualTime() {
        return actualTime;
    }

    public void setActualTime(String actualTime) {
        this.actualTime = actualTime;
    }

    public String getActualKms() {
        return actualKms;
    }

    public void setActualKms(String actualKms) {
        this.actualKms = actualKms;
    }

    public String getIsCoupon() {
        return isCoupon;
    }

    public void setIsCoupon(String isCoupon) {
        this.isCoupon = isCoupon;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public String getCouponDiscount() {
        return couponDiscount;
    }

    public void setCouponDiscount(String couponDiscount) {
        this.couponDiscount = couponDiscount;
    }

    public String getDCreatedOn() {
        return dCreatedOn;
    }

    public void setDCreatedOn(String dCreatedOn) {
        this.dCreatedOn = dCreatedOn;
    }

    public Object getNCreatedBy() {
        return nCreatedBy;
    }

    public void setNCreatedBy(Object nCreatedBy) {
        this.nCreatedBy = nCreatedBy;
    }

    public Object getDModifiedOn() {
        return dModifiedOn;
    }

    public void setDModifiedOn(Object dModifiedOn) {
        this.dModifiedOn = dModifiedOn;
    }

    public Object getNModifiedBy() {
        return nModifiedBy;
    }

    public void setNModifiedBy(Object nModifiedBy) {
        this.nModifiedBy = nModifiedBy;
    }

    public String getBDeleted() {
        return bDeleted;
    }

    public void setBDeleted(String bDeleted) {
        this.bDeleted = bDeleted;
    }

    public String getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    public String getPaySuccess() {
        return paySuccess;
    }

    public void setPaySuccess(String paySuccess) {
        this.paySuccess = paySuccess;
    }

    public String getRidetype() {
        return ridetype;
    }

    public void setRidetype(String ridetype) {
        this.ridetype = ridetype;
    }

    public String getWayBill() {
        return wayBill;
    }

    public void setWayBill(String wayBill) {
        this.wayBill = wayBill;
    }

    public String getBCancelledBy() {
        return bCancelledBy;
    }

    public void setBCancelledBy(String bCancelledBy) {
        this.bCancelledBy = bCancelledBy;
    }

    public String getCancelledWhy() {
        return cancelledWhy;
    }

    public void setCancelledWhy(String cancelledWhy) {
        this.cancelledWhy = cancelledWhy;
    }

    public Object getNAdminType() {
        return nAdminType;
    }

    public void setNAdminType(Object nAdminType) {
        this.nAdminType = nAdminType;
    }

    public List<Object> getRideSummary() {
        return rideSummary;
    }

    public void setRideSummary(List<Object> rideSummary) {
        this.rideSummary = rideSummary;
    }

    public String getVVehiclename() {
        return vVehiclename;
    }

    public void setVVehiclename(String vVehiclename) {
        this.vVehiclename = vVehiclename;
    }

    public List<Object> getLattlong() {
        return lattlong;
    }

    public void setLattlong(List<Object> lattlong) {
        this.lattlong = lattlong;
    }

}


