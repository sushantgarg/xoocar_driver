package com.xoocardriver.model;

import java.util.List;

/**
 * Created by mindz on 8/4/17.
 */

public class SnapToRoadModel {

    private Float latitude;
    private Float longitude;

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }


}