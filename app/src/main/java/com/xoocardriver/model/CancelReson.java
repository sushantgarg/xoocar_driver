package com.xoocardriver.model;

/**
 * Created by mindz on 10/12/16.
 */


public class CancelReson {

    private String id;
    private String whyCancelled;
    private String cancelledBy;
    private String dCreatedOn;
    private String nCreatedBy;
    private String dModifiedOn;
    private String nModifiedBy;
    private String bIsActive;
    private String sStatus;
    private String nAdminType;

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The whyCancelled
     */
    public String getWhyCancelled() {
        return whyCancelled;
    }

    /**
     *
     * @param whyCancelled
     * The why_cancelled
     */
    public void setWhyCancelled(String whyCancelled) {
        this.whyCancelled = whyCancelled;
    }

    /**
     *
     * @return
     * The cancelledBy
     */
    public String getCancelledBy() {
        return cancelledBy;
    }

    /**
     *
     * @param cancelledBy
     * The cancelled_by
     */
    public void setCancelledBy(String cancelledBy) {
        this.cancelledBy = cancelledBy;
    }

    /**
     *
     * @return
     * The dCreatedOn
     */
    public String getDCreatedOn() {
        return dCreatedOn;
    }

    /**
     *
     * @param dCreatedOn
     * The d_CreatedOn
     */
    public void setDCreatedOn(String dCreatedOn) {
        this.dCreatedOn = dCreatedOn;
    }

    /**
     *
     * @return
     * The nCreatedBy
     */
    public String getNCreatedBy() {
        return nCreatedBy;
    }

    /**
     *
     * @param nCreatedBy
     * The n_CreatedBy
     */
    public void setNCreatedBy(String nCreatedBy) {
        this.nCreatedBy = nCreatedBy;
    }

    /**
     *
     * @return
     * The dModifiedOn
     */
    public String getDModifiedOn() {
        return dModifiedOn;
    }

    /**
     *
     * @param dModifiedOn
     * The d_ModifiedOn
     */
    public void setDModifiedOn(String dModifiedOn) {
        this.dModifiedOn = dModifiedOn;
    }

    /**
     *
     * @return
     * The nModifiedBy
     */
    public String getNModifiedBy() {
        return nModifiedBy;
    }

    /**
     *
     * @param nModifiedBy
     * The n_ModifiedBy
     */
    public void setNModifiedBy(String nModifiedBy) {
        this.nModifiedBy = nModifiedBy;
    }

    /**
     *
     * @return
     * The bIsActive
     */
    public String getBIsActive() {
        return bIsActive;
    }

    /**
     *
     * @param bIsActive
     * The b_IsActive
     */
    public void setBIsActive(String bIsActive) {
        this.bIsActive = bIsActive;
    }

    /**
     *
     * @return
     * The sStatus
     */
    public String getSStatus() {
        return sStatus;
    }

    /**
     *
     * @param sStatus
     * The s_status
     */
    public void setSStatus(String sStatus) {
        this.sStatus = sStatus;
    }

    /**
     *
     * @return
     * The nAdminType
     */
    public String getNAdminType() {
        return nAdminType;
    }

    /**
     *
     * @param nAdminType
     * The n_AdminType
     */
    public void setNAdminType(String nAdminType) {
        this.nAdminType = nAdminType;
    }

}


