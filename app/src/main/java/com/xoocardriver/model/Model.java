package com.xoocardriver.model;

import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by mindz on 6/10/16.
 */
public class Model {

    public static void ShowToast(Context activity, String str) {
        if (str != null)
            Toast.makeText(activity, str, Toast.LENGTH_LONG).show();
    }

    public static JSONObject getJsonObject(JSONObject jObject, String title) {
        try {
            return jObject.getJSONObject(title);
        } catch (Exception ignored) {
        }
        return new JSONObject();
    }

    public static JSONObject getObject(String str) {
        try {
            return new JSONObject(str);
        } catch (Exception ignored) {
        }
        return null;
    }

    public static JSONObject getObject(JSONArray jSon, int index) {
        try {
            return jSon.getJSONObject(index);
        } catch (Exception ignored) {

        }
        return null;
    }

    public static JSONArray getArray(String str) {
        try {
            return new JSONArray(str);
        } catch (Exception ignored) {
        }
        return null;
    }

    public static JSONArray getArray(JSONObject object, String tag) {

        try {
            return object.getJSONArray(tag);
        } catch (Exception ignored) {
        }
        return null;
    }

    public static JSONArray getArrays(JSONObject object, String key) {
        try {
            return object.getJSONArray(key);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getString(JSONObject jObject, String title) {
        try {
            return jObject.getString(title);
        } catch (Exception ignored) {
        }
        return null;
    }

//    public static String getJSONStringData(String url) {
//        StringBuilder builder;
//        builder = new StringBuilder();
//        HttpClient client = new DefaultHttpClient();
//        HttpGet httpGet = new HttpGet(url);
//        try {
//            HttpResponse response = client.execute(httpGet);
//            HttpEntity entity = response.getEntity();
//            InputStream content = entity.getContent();
//            BufferedReader reader = new BufferedReader(new InputStreamReader(
//                    content));
//            String line;
//            while ((line = reader.readLine()) != null) {
//                builder.append(line);
//            }
//        } catch (Exception ignored) {    }
//
//        return builder.toString();
//    }

    public static int getInt(JSONObject jObject, String title) {
        try {
            return jObject.getInt(title);
        } catch (Exception ignored) {
        }
        return 0;
    }

    public static String getString(JSONArray jsonArray, int index) {
        try {
            return jsonArray.getString(index);
        } catch (Exception ignored) {
        }
        return null;
    }

    public static float getFloat(JSONObject jObject, String title) {
        try {
            return (float) jObject.getDouble(title);
        } catch (Exception ignored) {
        }
        return 0;
    }

    @SuppressLint("SimpleDateFormat")
    public static String getDate(String date) {
        Date dates;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            dates = format.parse(date);
            if (String.valueOf(dates).length() > 16)
                date = String.valueOf(dates).substring(0, 16);
            else
                date = dates.toString();
        } catch (Exception ignored) {
        }
        return date;
    }



}
