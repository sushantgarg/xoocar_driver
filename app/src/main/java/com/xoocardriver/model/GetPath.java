package com.xoocardriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 24/7/17.
 */

public class GetPath {
    @SerializedName("fields")
    @Expose
    private PathRequestModel fields;

    /**
     * No args constructor for use in serialization
     *
     */
    public GetPath() {
    }

    /**
     *
     * @param fields
     */
    public GetPath(PathRequestModel fields) {
        super();
        this.fields = fields;
    }

    public PathRequestModel getFields() {
        return fields;
    }

    public void setFields(PathRequestModel fields) {
        this.fields = fields;
    }
}
