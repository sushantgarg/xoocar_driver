package com.xoocardriver.model;

/**
 * Created by mindz on 4/1/17.
 */
class RideSummary {
    private Float total;
    private String basefare;
    private Float ridecharge;
    private String waitcharge;
    private Float taxcharge;
    private String conveniencecharge;
    private Float perminutchargeaccordingly;

    public Float getTotal() {
        return total;
    }

    public void setTotal(Float total) {
        this.total = total;
    }

    public String getBasefare() {
        return basefare;
    }

    public void setBasefare(String basefare) {
        this.basefare = basefare;
    }

    public Float getRidecharge() {
        return ridecharge;
    }

    public void setRidecharge(Float ridecharge) {
        this.ridecharge = ridecharge;
    }

    public String getWaitcharge() {
        return waitcharge;
    }

    public void setWaitcharge(String waitcharge) {
        this.waitcharge = waitcharge;
    }

    public Float getTaxcharge() {
        return taxcharge;
    }

    public void setTaxcharge(Float taxcharge) {
        this.taxcharge = taxcharge;
    }

    public String getConveniencecharge() {
        return conveniencecharge;
    }

    public void setConveniencecharge(String conveniencecharge) {
        this.conveniencecharge = conveniencecharge;
    }

    public Float getPerminutchargeaccordingly() {
        return perminutchargeaccordingly;
    }

    public void setPerminutchargeaccordingly(Float perminutchargeaccordingly) {
        this.perminutchargeaccordingly = perminutchargeaccordingly;
    }
}
