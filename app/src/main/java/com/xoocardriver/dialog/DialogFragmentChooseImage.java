//package com.xoocardriver.dialog;
//
//import android.annotation.SuppressLint;
//import android.app.Dialog;
//import android.graphics.Color;
//import android.graphics.drawable.ColorDrawable;
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.v4.app.DialogFragment;
//import android.view.View;
//import android.view.Window;
//import android.view.WindowManager;
//import android.widget.LinearLayout;
//
//import com.xoocardriver.R;
//import com.xoocardriver.helper.SecondPageFragmentListener;
//import com.xoocardriver.view.activity.MainActivity;
//
//
///**
// * Created by mindz on 8/12/16.
// */
//
//@SuppressLint("ValidFragment")
//public class DialogFragmentChooseImage extends DialogFragment {
//
//
//    MainActivity mainActivity;
//
//    String choose_option;
//
//   SecondPageFragmentListener secondPageFragmentListener;
//
//    LinearLayout layout_camera_dialog, layout_gallery_dialog, layout_cancel_dialog;
//
//    @SuppressLint("ValidFragment")
//    public DialogFragmentChooseImage(MainActivity mainActivity, SecondPageFragmentListener secondPageFragmentListener) {
//        this.mainActivity = mainActivity;
//        this.secondPageFragmentListener = secondPageFragmentListener;
//    }
//
//    @NonNull
//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//
//        final Dialog dialog = new Dialog(getActivity());
//        dialog.setCanceledOnTouchOutside(false);
//        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        dialog.setContentView(R.layout.layout_choose_image);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        intiView(dialog);
//        listnerView();
//        return dialog;
//    }
//
//    public void intiView(Dialog dialog) {
//        layout_camera_dialog = (LinearLayout) dialog.findViewById(R.id.layout_camera_dialog);
//        layout_gallery_dialog = (LinearLayout) dialog.findViewById(R.id.layout_gallery_dialog);
//        layout_cancel_dialog = (LinearLayout) dialog.findViewById(R.id.layout_cancel_dialog);
//    }
//
//    public void listnerView() {
//
//        layout_camera_dialog.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dismiss();
//                choose_option = "camera";
//                secondPageFragmentListener.callImageChooser(choose_option);
//            }
//        });
//
//        layout_gallery_dialog.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dismiss();
//                choose_option = "gallery";
//                secondPageFragmentListener.callImageChooser(choose_option);
//            }
//        });
//
//        layout_cancel_dialog.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dismiss();
//            }
//        });
//    }
//
//
//}
