//package com.xoocardriver.dialog;
//
//
//import android.app.Dialog;
//import android.graphics.Bitmap;
//import android.graphics.Color;
//import android.graphics.drawable.ColorDrawable;
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.v4.app.DialogFragment;
//import android.support.v4.app.Fragment;
//import android.util.Log;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.Window;
//import android.view.WindowManager;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//
//import com.xoocardriver.R;
//import com.xoocardriver.helper.SetPreviewImageListnerUploadDocument;
//import com.xoocardriver.view.activity.MainActivity;
//
//
///**
// * A simple {@link Fragment} subclass.
// */
//public class DialogFragmentUploadDocumentPreview extends DialogFragment {
//
//    SetPreviewImageListnerUploadDocument setPreviewImageListner;
//
//    ImageView network_image_view_user_profile;
//
//    LinearLayout layout_cancel_preview, layout_preview_image;
//
//    MainActivity mainActivity;
//
//    Bitmap image;
//
//    public DialogFragmentUploadDocumentPreview(MainActivity mainActivity, Bitmap image, SetPreviewImageListnerUploadDocument setPreviewImageListner) {
//        this.mainActivity = mainActivity;
//        this.setPreviewImageListner = setPreviewImageListner;
//        this.image = image;
//        Log.d("image", "image " + image);
//    }
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setStyle(DialogFragment.STYLE_NORMAL, R.style.MY_DIALOG);
//    }
//
//
//    @NonNull
//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        final Dialog dialog = new Dialog(getActivity());
//        dialog.setCanceledOnTouchOutside(false);
//        setStyle(DialogFragment.STYLE_NORMAL, R.style.MY_DIALOG);
//        dialog.setContentView(R.layout.fragment_dialog_fragment_upload_document_picture_preview);
//        Window window = dialog.getWindow();
//        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));
//        intiView(dialog);
//        listnerView();
//        return dialog;
//    }
//
//    public void intiView(Dialog dialog) {
//        network_image_view_user_profile = (ImageView) dialog.findViewById(R.id.network_image_view_user_profile);
//        network_image_view_user_profile.setImageBitmap(image);
//        layout_cancel_preview = (LinearLayout) dialog.findViewById(R.id.layout_cancel_preview);
//        layout_preview_image = (LinearLayout) dialog.findViewById(R.id.layout_preview_image);
//    }
//
//    public void listnerView() {
//        layout_cancel_preview.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dismiss();
//            }
//        });
//        layout_preview_image.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d("meet","meet");
//                setPreviewImageListner.setImage(image);
//                dismiss();
//            }
//        });
//
//    }
//
//    @Override
//    public void onStart() {
//        super.onStart();
//        Dialog d = getDialog();
//        if (d != null) {
//            int width = ViewGroup.LayoutParams.MATCH_PARENT;
//            int height = ViewGroup.LayoutParams.MATCH_PARENT;
//            d.getWindow().setLayout(width, height);
//        }
//    }
//
//}
