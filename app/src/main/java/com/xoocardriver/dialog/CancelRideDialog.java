//package com.xoocardriver.dialog;
//
//import android.app.Dialog;
//import android.content.Context;
//import android.graphics.Color;
//import android.graphics.drawable.ColorDrawable;
//import android.os.Bundle;
//import android.support.v4.app.DialogFragment;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.Window;
//import android.view.WindowManager;
//import android.widget.LinearLayout;
//import android.widget.RadioButton;
//import android.widget.RadioGroup;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.android.volley.AuthFailureError;
//import com.android.volley.DefaultRetryPolicy;
//import com.android.volley.NetworkError;
//import com.android.volley.NoConnectionError;
//import com.android.volley.ParseError;
//import com.android.volley.Request;
//import com.android.volley.RequestQueue;
//import com.android.volley.Response;
//import com.android.volley.ServerError;
//import com.android.volley.TimeoutError;
//import com.android.volley.VolleyError;
//import com.android.volley.VolleyLog;
//import com.android.volley.toolbox.StringRequest;
//import com.android.volley.toolbox.Volley;
//import com.xoocardriver.R;
//import com.xoocardriver.helper.Constant;
//import com.xoocardriver.model.Model;
//import com.xoocardriver.preference.PrefManager;
//import com.xoocardriver.view.activity.MainActivity;
//import com.xoocardriver.view.fragment.OfflineOnline;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * Created by mindz on 30/11/16.
// */
//
//public class CancelRideDialog extends DialogFragment {
//    private LinearLayout mLinearLayout;
//    private RadioButton[] rb;
//    private int i;
//    private RadioGroup rg;
//    private ArrayList<String> id_list, name_list;
//    private String reson_name;
//    private String reson_id = "";
//    private String rider_id;
//    private String payment_type, adriverid, destination, source;
//    private MainActivity mainActivity;
//    private PrefManager prefManager;
//
//    public CancelRideDialog(MainActivity mainActivity, String adriverid, String rider_id, String payment_type, String source, String destination) {
//        this.rider_id = rider_id;
//        this.mainActivity = mainActivity;
//        this.payment_type = payment_type;
//        this.adriverid = adriverid;
//        this.source = source;
//        this.destination = destination;
//    }
//
//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        final Dialog dialog = new Dialog(mainActivity);
//        prefManager = new PrefManager(mainActivity);
//        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        dialog.setContentView(R.layout.custom_cancel_reson);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        dialog.show();
//        TextView cancelTv = (TextView) dialog.findViewById(R.id.book_confirm_custom_cancel_tv);
//        mLinearLayout = (LinearLayout) dialog.findViewById(R.id.linear1);
//        rg = new RadioGroup(mainActivity);
//
//        getCancelBooking(Constant.BOOK_CANCEL_GET_REQUEST_URL);
//
//        cancelTv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (rg.getCheckedRadioButtonId() != -1) {
//                    int id = rg.getCheckedRadioButtonId();
//                    View radioButton = rg.findViewById(id);
//                    int radioId = rg.indexOfChild(radioButton);
//                    RadioButton btn = (RadioButton) rg.getChildAt(radioId);
//                    String selection = (String) btn.getText();
//                    for (int i = 0; i < name_list.size(); i++) {
//                        String name = name_list.get(i);
//                        if (name.equals(selection)) {
//                            reson_name = name_list.get(i).toString();
//                            reson_id = id_list.get(i).toString();
//                        }
//                    }
//                }
//                if (reson_name != null && !reson_name.isEmpty() && !reson_name.equals("null")) {
//                    postCancelBooking(Constant.BOOK_CANCEL_POST_REQUEST_URL);
//                } else {
//                    Toast.makeText(mainActivity, "Please select reason", Toast.LENGTH_LONG).show();
//                }
//                dismiss();
//            }
//        });
//
//        return dialog;
//    }
//
//    public void ShowDialog() {
//        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.MATCH_PARENT,
//                LinearLayout.LayoutParams.MATCH_PARENT);
//        final Dialog imageDialog = new Dialog(mainActivity);
//        imageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        imageDialog.setCancelable(true);
//        imageDialog.setCanceledOnTouchOutside(true);
//
//        mainActivity.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
//                WindowManager.LayoutParams.MATCH_PARENT);
//
//        LayoutInflater inflater = (LayoutInflater) mainActivity
//                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        View layout = inflater.inflate(R.layout.custom_cancel_show, null);
//        TextView mTextView = (TextView) layout.findViewById(R.id.custom_messge_tv);
//        TextView mTextView1 = (TextView) layout.findViewById(R.id.book_show_custom_cancel_tv);
//        mTextView.setText("Your booking has been cancelled successfully.");
//
//        mTextView1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View arg0) {
//                prefManager.setRideIdCheck("0");
//                String driver_id = prefManager.getKeyUserId();
//                mainActivity.getSupportFragmentManager().beginTransaction()
//                        .replace(R.id.frame_layout, new OfflineOnline(mainActivity, driver_id), "OfflineOnline").commit();
//                imageDialog.dismiss();
//            }
//        });
//        layout.setLayoutParams(param);
//        imageDialog.setContentView(layout, param);
//        imageDialog.show();
//    }
//
//    public void getCancelBooking(String url) {
//        RequestQueue rq = Volley.newRequestQueue(getContext());
//        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.d("cancel reson response ", "= " + response);
//                JSONArray jsonArray = Model.getArray(response);
//                id_list = new ArrayList<>();
//                name_list = new ArrayList<>();
//                rb = new RadioButton[jsonArray.length()];
//                View view = new View(mainActivity);
//                view.setMinimumHeight(1);
//                view.setBackgroundColor(Integer.parseInt(String.valueOf(R.color.black_color)));
//                rg.setOrientation(RadioGroup.VERTICAL);
//                for (i = 0; i < jsonArray.length(); i++) {
//                    try {
//                        JSONObject jsonObject = jsonArray.getJSONObject(i);
//                        id_list.add(Model.getString(jsonObject, "id"));
//                        name_list.add(Model.getString(jsonObject, "why_cancelled"));
//                        rb[i] = new RadioButton(mainActivity);
//                        rg.addView(rb[i]);
//                        rb[i].setText(jsonObject.getString("why_cancelled"));
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//                mLinearLayout.addView(rg);
//            }
//            // }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                VolleyLog.d("Error", "Error: " + error.getMessage());
//                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
//                    Log.d("error ocurred", "TimeoutError");
//                    // Toast.makeText(context, "TimeoutError", Toast.LENGTH_LONG).show();
//                }
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> params = new HashMap<>();
//                String[] tag = {"cancelledby"};
//                String[] value = {"driver"};
//                for (int i = 0; i < tag.length; i++)
//                    params.put(tag[i], value[i]);
//                return checkParams(params);
//            }
//        };
//        postReq.setRetryPolicy(new DefaultRetryPolicy(
//                800000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        rq.add(postReq);
//    }
//
//    public void postCancelBooking(String url) {
//        RequestQueue rq = Volley.newRequestQueue(getContext());
//        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.d("cancel POST response ", "= " + response);
//                ShowDialog();
//            }
//            // }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                VolleyLog.d("Error", "Error: " + error.getMessage());
//                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
//                    Log.d("error ocurred", "TimeoutError");
//                     Toast.makeText(getActivity().getApplicationContext(), "Internet not available, Please try again", Toast.LENGTH_LONG).show();
//                } else{
//                    ShowDialog();
//                }
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> params = new HashMap<>();
//                String driver_id = prefManager.getKeyUserId();
//                String str="";
//                String[] tag = {"adriverid", "driverid", "rideid", "source", "destination", "paymode", "whycancel"};
//                String[] value = {str, driver_id, rider_id, source, destination, payment_type, reson_id};
//                for (int i = 0; i < tag.length; i++)
//                    params.put(tag[i], value[i]);
//                return checkParams(params);
//            }
//        };
//        postReq.setRetryPolicy(new DefaultRetryPolicy(
//                800000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        rq.add(postReq);
//    }
//
//    private Map<String, String> checkParams(Map<String, String> map) {
//        for (Map.Entry<String, String> pairs : map.entrySet()) {
//            if (pairs.getValue() == null) {
//                map.put(pairs.getKey(), "");
//            }
//        }
//        return map;
//    }
//}