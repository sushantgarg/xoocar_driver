package com.xoocardriver.fcm;


import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.xoocardriver.preference.PrefManager;
import com.xoocardriver.preference.PrefManagerNotification;

/**
 * Created by Belal on 5/27/2016.
 */


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";
    public static final String REGISTRATION_SUCCESS = "RegistrationSuccess";
    public PrefManagerNotification prefManager;


    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        prefManager=new PrefManagerNotification(getBaseContext());
        prefManager.setRegistrationId(refreshedToken);
        PrefManager prefManager=new PrefManager(this);
        prefManager.setFcmToken(refreshedToken);
    }

    private void sendRegistrationToServer(String token) {
    }
}