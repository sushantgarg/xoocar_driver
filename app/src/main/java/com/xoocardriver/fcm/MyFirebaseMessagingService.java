package com.xoocardriver.fcm;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.xoocardriver.R;
import com.xoocardriver.helper.Constant;
import com.xoocardriver.model.Notification;
import com.xoocardriver.preference.PrefManager;
import com.xoocardriver.view.fragment.Home;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by Belal on 5/27/2016.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";
    PrefManager prefManager;

    @Override
    public void onMessageReceived( RemoteMessage remoteMessage ) {
        prefManager = new PrefManager(getApplicationContext());

        String notification_type = remoteMessage.getData().get("notification_type");
        switch (notification_type) {
            case "1":
                boolean flag ;
                String alarm_check = prefManager.getCheckBookingAlarm();
                flag = alarm_check == null;

                prefManager.setCheckBookingAlarm(notification_type);
                Notification notifications=new Gson().fromJson(remoteMessage.getData().get("data_array"),Notification.class);
                prefManager.setRideIdCheck(notifications.getRideId());
                prefManager.saveRideDetailsJson(remoteMessage.getData().get("data_array"));

                if( flag ) {
                    sendNotification("New Booking", notifications.getMessage(), notification_type);
                } else {
                    onfortunatallyCancel(Constant.REJECT_RIDE, notifications.getRideId());
                }
                break;

            case "15":
                sendNotification("New Booking", "Ride cancelled", notification_type);
                break;

            case "31":
                sendNotification( remoteMessage.getData().get("title"), remoteMessage.getData().get("message"),notification_type);
                break;
        }
    }

    private void sendNotification(String title, String subtitle, String notification_type) {

        Intent intent = new Intent(this, Home.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.xoocar_logo_svg)
                .setContentTitle(title)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(subtitle))
                .setColor(Color.parseColor("#000000"))
                .setContentText(subtitle)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());


        Intent launchIntent = getApplicationContext().getPackageManager().getLaunchIntentForPackage(getApplicationContext().getPackageName());
        launchIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(launchIntent);

    }

    public void onfortunatallyCancel(String url, final String ride_id) {
        Log.d("rejectRide:", " url = " + url + " ride_id" + ride_id);
        RequestQueue rq = Volley.newRequestQueue(this);
        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.has("uid")) {
                        Log.d("rejectRide  ", " = Booking cancel successfully " + response);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error", "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.d("error ocurred", "TimeoutError");
                    // Toast.makeText(context, "TimeoutError", Toast.LENGTH_LONG).show();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                String[] tag = {"rideid","driverId"};
                String[] value = {ride_id,prefManager.getKeyUserId()};
                for (int i = 0; i < tag.length; i++)
                    params.put(tag[i], value[i]);
                return checkParams(params);
            }
        };
        postReq.setRetryPolicy(new DefaultRetryPolicy(
                800000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(postReq);
    }

    private Map<String, String> checkParams(Map<String, String> map) {
        for (Map.Entry<String, String> pairs : map.entrySet()) {
            if (pairs.getValue() == null) {
                map.put(pairs.getKey(), "");
            }
        }
        return map;
    }
}