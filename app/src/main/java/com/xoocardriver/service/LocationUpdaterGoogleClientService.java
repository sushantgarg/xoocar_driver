package com.xoocardriver.service;

import android.Manifest;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.icu.text.SimpleDateFormat;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.LocationRequest;
import com.xoocardriver.CommonMethods;
import com.xoocardriver.Constants;
import com.xoocardriver.FireBase.FirebaseManager;
import com.xoocardriver.R;
import com.xoocardriver.RetroFit.RequestInterface;
import com.xoocardriver.RetroFit.UpdateDriverPosRequestData;
import com.xoocardriver.RetroFit.UpdateDriverPosRequestFields;
import com.xoocardriver.RetroFit.UpdateDriverPosResponceFields;
import com.xoocardriver.UuidGenerator;
import com.xoocardriver.preference.PrefManager;
import com.xoocardriver.realm.PathLocations;
import com.xoocardriver.realm.RealmHandler;
import com.xoocardriver.view.fragment.OfflineOnline;

import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by mindz on 11/11/16.
 */
public class LocationUpdaterGoogleClientService extends Service {

    IBinder mBinder = new LocalBinder();
    PrefManager prefManager;
    Handler handler = new Handler();
    RequestQueue rq;
    FirebaseManager firebaseManager = FirebaseManager.getInstance();
    private String ride_id;
    private Location currentLocation;
    private LocationManager locationManager;
    private LocationListener locationListener;
    private long lastUpdatedTime;
    Runnable runnable = new Runnable() {
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        public void run() {
            doPost();
            handler.postDelayed(runnable, 30 * 1000);
        }
    };

    private void doPost() {
        ride_id = prefManager.getRideIdCheck();
        Location currentLocTemp = null;

        if (currentLocation == null) {
            currentLocTemp = new Location("gps");
            currentLocTemp.setLatitude(prefManager.getCurrentLat());
            currentLocTemp.setLongitude(prefManager.getCurrentLng());
        }


        if (currentLocation != null) {
            firebaseManager.updatePosition(prefManager.getKeyUserId(),currentLocation);
            firebaseManager.addGeoFireLoc(prefManager.getKeyUserId(), currentLocation);
        } else {
//                        update sess manager loc
            firebaseManager.updatePosition(prefManager.getKeyUserId(),currentLocTemp);
            firebaseManager.addGeoFireLoc(prefManager.getKeyUserId(), currentLocTemp);

        }

        if (!ride_id.equals("0") && prefManager.isRideRunning()) {
            CommonMethods.sendPathToServer(ride_id, prefManager.getKeyUserId());

            if (currentLocation != null) {
                registerRequest(currentLocation);
            } else {
                registerRequest(currentLocTemp);
//                update sess manager loc
            }
        } else {
            if (currentLocation != null) {
                registerRequest(currentLocation);
            } else {
//                update sess manager loc
                registerRequest(currentLocTemp);
            }
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        prefManager = new PrefManager(getApplicationContext());
        ride_id = prefManager.getRideIdCheck();
        rq = Volley.newRequestQueue(this);
        Log.d("ride_id", ":" + prefManager.getRideIdCheck());

        LocationRequest mLocationRequest = LocationRequest.create();
        // Use high accuracy
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        // Set the update interval to 5 seconds
        mLocationRequest.setInterval(30000);
        // Set the fastest update interval to 1 second
        mLocationRequest.setFastestInterval(25 * 1000);

        lastUpdatedTime = System.currentTimeMillis();

        builtNotification();

        /*
         * Create a new location client, using the enclosing class to
         * handle callbacks.
         */
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        if (locationManager == null)
            requestLocatioUpdates();

        handler.postDelayed(runnable, 10 * 1000);
        return START_STICKY;
    }

    /*
     * Create a new location client, using the enclosing class to
     * handle callbacks.
     */

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public String getTime() {
        SimpleDateFormat mDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return mDateFormat.format(new Date());
    }

    @Override
    public void onDestroy() {
        // Turn off the request flag
        if (locationManager != null)
            locationManager.removeUpdates(locationListener);
        locationManager = null;
        handler.removeCallbacks(runnable);
        super.onDestroy();
    }

    public void registerRequest(Location loc) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);

        long timeSincePreviousUpdate = System.currentTimeMillis() - lastUpdatedTime;
        Log.d("aaaa-timeDiff", "" + timeSincePreviousUpdate);

        Call<UpdateDriverPosResponceFields> call = request.updatePosition("api/driverappapi/adddriverlocfire/format/json/"
                , new UpdateDriverPosRequestFields(new UpdateDriverPosRequestData(prefManager.getKeyUserId()
                        , "" + loc.getLatitude()
                        , "" + loc.getLongitude()
                        , "" + loc.getBearing()
                        , prefManager.getSessionId()
                        , String.valueOf(timeSincePreviousUpdate)))
        );

        call.enqueue(new Callback<UpdateDriverPosResponceFields>() {
            @Override
            public void onResponse(Call<UpdateDriverPosResponceFields> call, retrofit2.Response<UpdateDriverPosResponceFields> response) {

                lastUpdatedTime = System.currentTimeMillis();

                if ( response.body() != null ) {

                    if ( response.body().getStatus() == 1 ) {
                        if ( !prefManager.lastCancelledRideId().equals(response.body().getDataArray().getOid() )
                                && prefManager.getRideIdCheck().equals("0")) {

                            prefManager.setRideIdCheck(response.body().getDataArray().getOid());

                            Intent launchIntent = getApplicationContext().getPackageManager().getLaunchIntentForPackage(getApplicationContext().getPackageName());
                            launchIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(launchIntent);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<UpdateDriverPosResponceFields> call, Throwable t) {
                Log.d("aaaa", "error");
            }
        });

//        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                try {
//                    JSONObject jsonObject = new JSONObject(response);
//                    if (jsonObject.has("status")) {
//                        String status = jsonObject.getString("status");
//                        if (status.equals("1")) {
//
//                            Intent intent = new Intent();
//                            intent.setAction(Intent.ACTION_MAIN);
//                            intent.addCategory(Intent.CATEGORY_LAUNCHER);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            ComponentName cn = new ComponentName(ctx, MainActivity.class);
//                            intent.setComponent(cn);
//                        }
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                VolleyLog.d("Error", "Error: " + error.getMessage());
//                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
//                    Log.d("error ocurred", "TimeoutError");
//                    // Toast.makeText(context, "TimeoutError", Toast.LENGTH_LONG).show();
//                }
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> params = new HashMap<>();
//                String driver_id = prefManager.getKeyUserId();
//                Log.d("driver id upadate", ":" + driver_id);
//                String[] tag = { "act_mode", "d_id", "d_latt", "d_long","course" };
//                String[] value = { "driver_loc", driver_id, lat, longi,course };
//
//                for (int i = 0; i < tag.length; i++)
//                    params.put(tag[i], value[i]);
////                return params;
//                return checkParams(params);
//            }
//        };
//        postReq.setRetryPolicy(new DefaultRetryPolicy(
//                800000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        rq.add(postReq);
    }

    /*
     * Called by Location Services when the request to connect the
     * client finishes successfully. At this point, you can
     * request the current location or start periodic updates
     */

    /*
 * Called by Location Services if the connection to the
 * location client drops because of an error.
 */

    private void builtNotification() {
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder builder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.xoocar_logo_svg)
                .setAutoCancel(false)
                .setVibrate(new long[]{0, 200})
                .setSound(alarmSound)
                .setOngoing(true)
                .setColor(Color.parseColor("#000000"))
                .setContentTitle("Driver")
                .setContentText("Service is active");

        Intent notificationIntent = new Intent(this, OfflineOnline.class);
        notificationIntent.setFlags(Intent.FLAG_RECEIVER_FOREGROUND);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);

        // Add as notification
        Notification notification = builder.build();
        startForeground(7300, notification);
    }

    private void requestLocatioUpdates() {
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationListener = new MyLocationListener();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this
                , Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5 * 1000, 10, locationListener);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 50 * 1000, 20, locationListener);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void updateDecider(Location loc) {

        if (significantChange(loc)) {

            PathLocations locationRealmPojo = new PathLocations();
            locationRealmPojo.setLatitute(loc.getLatitude());
            locationRealmPojo.setLongitude(loc.getLongitude());
            locationRealmPojo.setTimestamp(loc.getTime());
            locationRealmPojo.setAccuracy(loc.getAccuracy());
            locationRealmPojo.setCourse(loc.getBearing());
            locationRealmPojo.setSpeed(loc.getSpeed());
            locationRealmPojo.setUniqueId(UuidGenerator.generateUuid());
            locationRealmPojo.setStatus(Constants.UNSEND);
            RealmHandler.addLocationToPath(locationRealmPojo);
        }
    }

    private boolean significantChange(Location location) {
        //Distance is in meters.
        boolean isSignificant = false;
        if (RealmHandler.getPathLastLocaion().getLatitude() != 0) {
            double distance = location.distanceTo(RealmHandler.getPathLastLocaion());
            long interval = location.getTime() - RealmHandler.getPathLastLocaion().getTime();
            if (location.getProvider().equals("gps")) {
                if (location.getAccuracy() < 80 && distance > 16.1f && location.getSpeed() > 0.9f && interval > 2500)
                    isSignificant = true;
            } else {
                isSignificant = false;
            }
        } else {
            isSignificant = true;
        }
        return isSignificant;
    }

    private class LocalBinder extends Binder {
        public LocationUpdaterGoogleClientService getServerInstance() {
            return LocationUpdaterGoogleClientService.this;
        }
    }

    private class MyLocationListener implements LocationListener {

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        public void onLocationChanged(Location location) {
            if (location.getProvider().equals("gps")) {
                ride_id = prefManager.getRideIdCheck();
                prefManager.setCurrentLat(location.getLatitude());
                prefManager.setCurrentLng(location.getLongitude());

                currentLocation = location;

                firebaseManager.updatePosition(prefManager.getKeyUserId(), currentLocation);
                firebaseManager.addGeoFireLoc(prefManager.getKeyUserId(), currentLocation);

                if (!ride_id.equals("0") && prefManager.isRideRunning()) {
                    updateDecider(location);
                }
            }
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    }

//    private void updateOrder(  final String ride_id, final List<PathLocations> pathLocations) {
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(Constant.base)
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//        RequestInterface request = retrofit.create(RequestInterface.class);
//
//        Call<ResponseModel<ResponceValues>> call = request.getOrders("api/driverappapi/routemap/format/json/" ,new GetPath(new PathRequestModel(pathLocations,ride_id,prefManager.getKeyUserId())));
//        call.enqueue(new Callback<ResponseModel<ResponceValues>>() {
//
//            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
//            @Override
//            public void onResponse(Call<ResponseModel<ResponceValues>> call, retrofit2.Response<ResponseModel<ResponceValues>> response) {
//            Log.d("aaaa","Success");
//                RealmHandler.updatePathStatus(pathLocations);
//            }
//
//            @Override
//            public void onFailure(Call<ResponseModel<ResponceValues>> call, Throwable t) {
//                if (t != null)
//                    Log.d("Error","Message:" ,t);
//            }
//        });
//    }

}