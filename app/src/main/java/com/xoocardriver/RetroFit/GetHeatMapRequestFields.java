package com.xoocardriver.RetroFit;

/**
 * Created by sushant on 9/4/17.
 */

public class GetHeatMapRequestFields {
    private String fields;

    public String getFields() {
        return fields;
    }

    public void setFields(String fields) {
        this.fields = fields;
    }
}
