package com.xoocardriver.RetroFit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 9/1/17.
 */

public class IncentiveDetailRequestData {
    public IncentiveDetailRequestData(String driverid) {
        this.driverid = driverid;
    }

    public String getDriverid() {

        return driverid;
    }

    public void setDriverid(String driverid) {
        this.driverid = driverid;
    }

    @SerializedName("driverid")
    @Expose
    private String driverid;
}
