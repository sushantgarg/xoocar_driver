package com.xoocardriver.RetroFit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 9/4/17.
 */

public class GetHeatMapResponceData {

    public String getcLatt() {
        return cLatt;
    }

    public void setcLatt(String cLatt) {
        this.cLatt = cLatt;
    }

    public String getcLong() {
        return cLong;
    }

    public void setcLong(String cLong) {
        this.cLong = cLong;
    }

    public String getdCreatedOn() {
        return dCreatedOn;
    }

    public void setdCreatedOn(String dCreatedOn) {
        this.dCreatedOn = dCreatedOn;
    }

    @SerializedName("c_latt")
    @Expose

    private String cLatt;
    @SerializedName("c_long")
    @Expose
    private String cLong;
    @SerializedName("d_CreatedOn")
    @Expose
    private String dCreatedOn;

}
