package com.xoocardriver.RetroFit.GetEarning;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 9/11/17.
 */

public class Earning {


    @SerializedName("totalcomplt")
    @Expose
    private String totalcomplt;
    @SerializedName("totalcancl")
    @Expose
    private String totalcancl;
    @SerializedName("totalbooking")
    @Expose
    private String totalbooking;
    @SerializedName("wallet")
    @Expose
    private String wallet;
    @SerializedName("cash")
    @Expose
    private String cash;
    @SerializedName("netamt")
    @Expose
    private String netamt;
    @SerializedName("toll")
    @Expose
    private String toll;
    @SerializedName("taxcharge")
    @Expose
    private String taxcharge;
    @SerializedName("incentive")
    @Expose
    private String incentive;
    @SerializedName("login_hrs")
    @Expose
    private String loginHrs;
    @SerializedName("driverearning")
    @Expose
    private String driverearning;

    public String getBankdiposit() {
        return bankdiposit;
    }

    public void setBankdiposit(String bankdiposit) {
        this.bankdiposit = bankdiposit;
    }

    private String bankdiposit;

    public String getTotalcomplt() {
        return totalcomplt;
    }

    public void setTotalcomplt(String totalcomplt) {
        this.totalcomplt = totalcomplt;
    }

    public String getTotalcancl() {
        return totalcancl;
    }

    public void setTotalcancl(String totalcancl) {
        this.totalcancl = totalcancl;
    }

    public String getTotalbooking() {
        return totalbooking;
    }

    public void setTotalbooking(String totalbooking) {
        this.totalbooking = totalbooking;
    }

    public String getWallet() {
        return wallet;
    }

    public void setWallet(String wallet) {
        this.wallet = wallet;
    }

    public String getCash() {
        return cash;
    }

    public void setCash(String cash) {
        this.cash = cash;
    }

    public String getNetamt() {
        return netamt;
    }

    public void setNetamt(String netamt) {
        this.netamt = netamt;
    }

    public String getToll() {
        return toll;
    }

    public void setToll(String toll) {
        this.toll = toll;
    }

    public String getTaxcharge() {
        return taxcharge;
    }

    public void setTaxcharge(String taxcharge) {
        this.taxcharge = taxcharge;
    }

    public String getIncentive() {
        return incentive;
    }

    public void setIncentive(String incentive) {
        this.incentive = incentive;
    }

    public String getLoginHrs() {
        return loginHrs;
    }

    public void setLoginHrs(String loginHrs) {
        this.loginHrs = loginHrs;
    }

    public String getDriverearning() {
        return driverearning;
    }

    public void setDriverearning(String driverearning) {
        this.driverearning = driverearning;
    }
}
