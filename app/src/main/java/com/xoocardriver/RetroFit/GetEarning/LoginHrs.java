package com.xoocardriver.RetroFit.GetEarning;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 9/11/17.
 */

class LoginHrs {
    public Object getDriverId() {
        return driverId;
    }

    public void setDriverId(Object driverId) {
        this.driverId = driverId;
    }

    public Object getOnlineTime() {
        return onlineTime;
    }

    public void setOnlineTime(Object onlineTime) {
        this.onlineTime = onlineTime;
    }

    public Object getLastLoginLogout() {
        return lastLoginLogout;
    }

    public void setLastLoginLogout(Object lastLoginLogout) {
        this.lastLoginLogout = lastLoginLogout;
    }

    @SerializedName("driver_id")
    @Expose
    private Object driverId;
    @SerializedName("online_time")
    @Expose
    private Object onlineTime;
    @SerializedName("last_login_logout")
    @Expose
    private Object lastLoginLogout;
}
