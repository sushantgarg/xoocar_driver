package com.xoocardriver.RetroFit.GetEarning;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 9/11/17.
 */

public class GetEarningRequestData {
    @SerializedName("act_mode")
    @Expose
    private String actMode;
    @SerializedName("driver_id")
    @Expose
    private String driverId;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("end_date")
    @Expose
    private String endDate;

    public GetEarningRequestData(String actMode, String driverId, String startDate, String endDate) {
        this.actMode = actMode;
        this.driverId = driverId;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public String getActMode() {

        return actMode;
    }

    public void setActMode(String actMode) {
        this.actMode = actMode;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
