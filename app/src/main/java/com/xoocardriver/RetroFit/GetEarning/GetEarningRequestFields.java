package com.xoocardriver.RetroFit.GetEarning;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 9/11/17.
 */

public class GetEarningRequestFields {
    public GetEarningRequestFields(GetEarningRequestData fields) {
        this.fields = fields;
    }

    public GetEarningRequestData getFields() {

        return fields;
    }

    public void setFields(GetEarningRequestData fields) {
        this.fields = fields;
    }

    @SerializedName("fields")
    @Expose
    private GetEarningRequestData fields;
}
