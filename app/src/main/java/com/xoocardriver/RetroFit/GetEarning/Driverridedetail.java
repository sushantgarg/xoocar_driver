package com.xoocardriver.RetroFit.GetEarning;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 9/11/17.
 */

public class Driverridedetail {


    @SerializedName("ride_date")
    @Expose
    private String rideDate;
    @SerializedName("v_licence_no")
    @Expose
    private String vLicenceNo;
    @SerializedName("crn_no")
    @Expose
    private String crnNo;
    @SerializedName("actual_cost")
    @Expose
    private String actualCost;

    private String oid;

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getRideDate() {
        return rideDate;
    }

    public void setRideDate(String rideDate) {
        this.rideDate = rideDate;
    }

    public String getvLicenceNo() {
        return vLicenceNo;
    }

    public void setvLicenceNo(String vLicenceNo) {
        this.vLicenceNo = vLicenceNo;
    }

    public String getCrnNo() {
        return crnNo;
    }

    public void setCrnNo(String crnNo) {
        this.crnNo = crnNo;
    }

    public String getActualCost() {
        return actualCost;
    }

    public void setActualCost(String actualCost) {
        this.actualCost = actualCost;
    }
}
