package com.xoocardriver.RetroFit.GetEarning;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by sushant on 9/11/17.
 */

public class GetEarningResponceData {

    @SerializedName("earning")
    @Expose
    private Earning earning;
    @SerializedName("login_hrs")
    @Expose
    private LoginHrs loginHrs;

    public Earning getEarning() {
        return earning;
    }

    public void setEarning(Earning earning) {
        this.earning = earning;
    }

    public LoginHrs getLoginHrs() {
        return loginHrs;
    }

    public void setLoginHrs(LoginHrs loginHrs) {
        this.loginHrs = loginHrs;
    }

    public List<Driverridedetail> getDriverridedetail() {
        return driverridedetail;
    }

    public void setDriverridedetail(List<Driverridedetail> driverridedetail) {
        this.driverridedetail = driverridedetail;
    }

    @SerializedName("driverridedetail")
    @Expose
    private List<Driverridedetail> driverridedetail = null;
}
