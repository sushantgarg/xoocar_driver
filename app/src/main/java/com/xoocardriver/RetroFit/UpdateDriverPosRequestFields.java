package com.xoocardriver.RetroFit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/14/17.
 */

public class UpdateDriverPosRequestFields {

    public UpdateDriverPosRequestFields(UpdateDriverPosRequestData fields) {
        this.fields = fields;
    }

    public UpdateDriverPosRequestData getFields() {

        return fields;
    }

    public void setFields(UpdateDriverPosRequestData fields) {
        this.fields = fields;
    }

    @SerializedName("fields")
    @Expose
    private UpdateDriverPosRequestData fields;
}
