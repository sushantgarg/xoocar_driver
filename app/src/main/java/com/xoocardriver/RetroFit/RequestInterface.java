package com.xoocardriver.RetroFit;

import com.xoocardriver.RetroFit.GetEarning.GetEarningRequestFields;
import com.xoocardriver.RetroFit.GetEarning.GetEarningResponceFields;
import com.xoocardriver.RetroFit.GetTrainingVideos.GetTrainingVideoRequestFields;
import com.xoocardriver.RetroFit.GetTrainingVideos.GetTrainingVideoResponceFields;
import com.xoocardriver.RetroFit.InviteDriver.InviteDriverRequestFields;
import com.xoocardriver.RetroFit.InviteDriver.InviteDriverResponceFields;
import com.xoocardriver.RetroFit.TripDetailBillSummary.TripDetailRequestFields;
import com.xoocardriver.RetroFit.TripDetailBillSummary.TripDetailResponceFields;
import com.xoocardriver.model.GetPath;
import com.xoocardriver.model.ResponceValues;
import com.xoocardriver.model.ResponseModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Url;

/**
 * Created by sushant on 24/7/17.
 */

public interface RequestInterface {

    @POST()
    Call<ResponseModel<ResponceValues>> getOrders(@Url String url, @Body GetPath orders);

    @POST()
    Call<AcceptRideResponce> acceptRide(@Url String url, @Body RequestFieldsAcceptRide orders);

    @POST()
    Call<RejectRideResponce> rejectRide(@Url String url, @Body RequestFieldsAcceptRide orders);

    @POST()
    Call<UpdateDriverPosResponceFields> updatePosition(@Url String url, @Body UpdateDriverPosRequestFields orders);

    @POST()
    Call<CancelRideScheduledResponceFields> CancelRideScheduledFields(@Url String url, @Body CancelRideScheduledFields orders);

    @POST()
    Call<IncentiveDetailResponceFields> getIncentiveDetail(@Url String url, @Body IncentiveDetailRequestFields orders);

    @POST()
    Call<GetHeatMapResponceFields> getHeatMap(@Url String url, @Body GetHeatMapRequestFields orders);

    @POST()
    Call<InviteDriverResponceFields> sendInviteToDriver(@Url String url, @Body InviteDriverRequestFields orders);

    @POST()
    Call<GetTrainingVideoResponceFields> getTrainingVideos(@Url String url, @Body GetTrainingVideoRequestFields orders);

    @POST()
    Call<GetEarningResponceFields> getEarning(@Url String url, @Body GetEarningRequestFields orders);


    @POST()
    Call<TripDetailResponceFields> getTripDetail(@Url String url, @Body TripDetailRequestFields orders);
}
