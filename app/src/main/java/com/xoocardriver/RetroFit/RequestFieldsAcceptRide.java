package com.xoocardriver.RetroFit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/12/17.
 */

public class RequestFieldsAcceptRide {
    @SerializedName("fields")
    @Expose
    private RequestDataAcceptRequest fields;

    public RequestFieldsAcceptRide(RequestDataAcceptRequest fields) {
        this.fields = fields;
    }

    public RequestDataAcceptRequest getFields() {
        return fields;
    }

    public void setFields(RequestDataAcceptRequest fields) {
        this.fields = fields;
    }
}
