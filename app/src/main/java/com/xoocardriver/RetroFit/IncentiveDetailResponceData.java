package com.xoocardriver.RetroFit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 9/4/17.
 */

public class IncentiveDetailResponceData {
    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getOnreferral() {
        return onreferral;
    }

    public void setOnreferral(String onreferral) {
        this.onreferral = onreferral;
    }

    public String getOnride() {
        return onride;
    }

    public void setOnride(String onride) {
        this.onride = onride;
    }

    @SerializedName("balance")
    @Expose
    private String balance;
    @SerializedName("onreferral")
    @Expose
    private String onreferral;
    @SerializedName("onride")
    @Expose
    private String onride;
}
