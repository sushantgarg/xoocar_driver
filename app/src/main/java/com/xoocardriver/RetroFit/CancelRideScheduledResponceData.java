package com.xoocardriver.RetroFit;

/**
 * Created by sushant on 8/14/17.
 */

class CancelRideScheduledResponceData {
    String rideid,c_id,c_first_name,source,destination,crn_no;

    public CancelRideScheduledResponceData(String rideid, String c_id, String c_first_name, String source, String destination, String crn_no) {
        this.rideid = rideid;
        this.c_id = c_id;
        this.c_first_name = c_first_name;
        this.source = source;
        this.destination = destination;
        this.crn_no = crn_no;
    }

    public String getRideid() {

        return rideid;
    }

    public void setRideid(String rideid) {
        this.rideid = rideid;
    }

    public String getC_id() {
        return c_id;
    }

    public void setC_id(String c_id) {
        this.c_id = c_id;
    }

    public String getC_first_name() {
        return c_first_name;
    }

    public void setC_first_name(String c_first_name) {
        this.c_first_name = c_first_name;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getCrn_no() {
        return crn_no;
    }

    public void setCrn_no(String crn_no) {
        this.crn_no = crn_no;
    }
}
