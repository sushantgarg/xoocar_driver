package com.xoocardriver.RetroFit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/14/17.
 */

public class UpdateDriverPosRequestData {
    private final String sessionid;
    private final String difference;
    @SerializedName("d_id")
    @Expose
    private String dId;
    @SerializedName("d_latt")
    @Expose
    private String dLatt;
    @SerializedName("d_long")
    @Expose
    private String dLong;
    @SerializedName("d_course")
    @Expose
    private String dCourse;

    public UpdateDriverPosRequestData(String dId, String dLatt, String dLong, String dCourse, String sessionId, String timeDiff) {
        this.dId = dId;
        this.dLatt = dLatt;
        this.dLong = dLong;
        this.dCourse = dCourse;
        this.sessionid = sessionId;
        this.difference = timeDiff;
    }

    public String getSessionid() {
        return sessionid;
    }

    public String getDifference() {
        return difference;
    }

    public String getdId() {

        return dId;
    }

    public void setdId(String dId) {
        this.dId = dId;
    }

    public String getdLatt() {
        return dLatt;
    }

    public void setdLatt(String dLatt) {
        this.dLatt = dLatt;
    }

    public String getdLong() {
        return dLong;
    }

    public void setdLong(String dLong) {
        this.dLong = dLong;
    }

    public String getdCourse() {
        return dCourse;
    }

    public void setdCourse(String dCourse) {
        this.dCourse = dCourse;
    }
}
