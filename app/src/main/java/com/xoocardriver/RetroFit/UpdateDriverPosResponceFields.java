package com.xoocardriver.RetroFit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/14/17.
 */

public class UpdateDriverPosResponceFields< T > {
    @SerializedName("response_code")
    @Expose
    private Integer responseCode;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("mesagges")
    @Expose
    private String mesagges;

    public UpdateDriverPosResponceFields(Integer responseCode, Integer status, String mesagges, UpdateDriverPosResponceData dataArray) {
        this.responseCode = responseCode;
        this.status = status;
        this.mesagges = mesagges;
        this.dataArray = dataArray;
    }

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMesagges() {
        return mesagges;
    }

    public void setMesagges(String mesagges) {
        this.mesagges = mesagges;
    }

    public UpdateDriverPosResponceData getDataArray() {
        return dataArray;
    }

    public void setDataArray(UpdateDriverPosResponceData dataArray) {
        this.dataArray = dataArray;
    }

    @SerializedName("data_array")
    @Expose
    private UpdateDriverPosResponceData dataArray;
}
