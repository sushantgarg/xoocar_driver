package com.xoocardriver.RetroFit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 9/1/17.
 */

public class IncentiveDetailRequestFields {
    @SerializedName("fields")
    @Expose
    private IncentiveDetailRequestData fields;

    public IncentiveDetailRequestFields(IncentiveDetailRequestData fields) {
        this.fields = fields;
    }

    public IncentiveDetailRequestData getFields() {

        return fields;
    }

    public void setFields(IncentiveDetailRequestData fields) {
        this.fields = fields;
    }
}
