package com.xoocardriver.RetroFit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/14/17.
 */

public class CancelRideScheduledFields {
    @SerializedName("fields")
    @Expose
    private CancelRideScheduledRequestData fields;

    public CancelRideScheduledFields(CancelRideScheduledRequestData fields) {
        this.fields = fields;
    }

    public CancelRideScheduledRequestData getFields() {

        return fields;
    }

    public void setFields(CancelRideScheduledRequestData fields) {
        this.fields = fields;
    }
}
