package com.xoocardriver.RetroFit.TripDetailBillSummary;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 9/13/17.
 */

public class TripDetailRequestData {
    public String getRideid() {
        return rideid;
    }

    public void setRideid(String rideid) {
        this.rideid = rideid;
    }

    public TripDetailRequestData(String rideid) {

        this.rideid = rideid;
    }

    @SerializedName("rideid")
    @Expose

    private String rideid;
}
