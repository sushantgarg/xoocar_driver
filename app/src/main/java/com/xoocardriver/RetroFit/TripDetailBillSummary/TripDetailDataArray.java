package com.xoocardriver.RetroFit.TripDetailBillSummary;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 9/13/17.
 */

public class TripDetailDataArray {

    public TripDetailResponceData getDriverridedetail() {
        return driverridedetail;
    }

    public void setDriverridedetail(TripDetailResponceData driverridedetail) {
        this.driverridedetail = driverridedetail;
    }

    @SerializedName("driverridedetail")
    @Expose
    private TripDetailResponceData driverridedetail;
}
