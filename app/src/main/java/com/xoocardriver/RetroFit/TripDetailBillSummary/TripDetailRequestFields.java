package com.xoocardriver.RetroFit.TripDetailBillSummary;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 9/13/17.
 */

public class TripDetailRequestFields {
    public TripDetailRequestFields(TripDetailRequestData fields) {
        this.fields = fields;
    }

    public TripDetailRequestData getFields() {

        return fields;
    }

    public void setFields(TripDetailRequestData fields) {
        this.fields = fields;
    }

    @SerializedName("fields")
    @Expose
    private TripDetailRequestData fields;
}
