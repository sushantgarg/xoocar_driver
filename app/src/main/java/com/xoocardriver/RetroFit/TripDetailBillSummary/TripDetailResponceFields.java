package com.xoocardriver.RetroFit.TripDetailBillSummary;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 9/13/17.
 */

public class TripDetailResponceFields {

    @SerializedName("response_code")
    @Expose
    private Integer responseCode;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("mesagges")
    @Expose
    private String mesagges;

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMesagges() {
        return mesagges;
    }

    public void setMesagges(String mesagges) {
        this.mesagges = mesagges;
    }

    public TripDetailDataArray getDataArray() {
        return dataArray;
    }

    public void setDataArray(TripDetailDataArray dataArray) {
        this.dataArray = dataArray;
    }

    @SerializedName("data_array")
    @Expose
    private TripDetailDataArray dataArray;
}
