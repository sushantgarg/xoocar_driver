package com.xoocardriver.RetroFit.TripDetailBillSummary;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 9/13/17.
 */

public class TripDetailResponceData {


    @SerializedName("cash_paid")
    @Expose
    private String cashPaid;
    @SerializedName("actual_kms")
    @Expose
    private String actualKms;
    @SerializedName("crn_no")
    @Expose
    private String crnNo;
    @SerializedName("source")
    @Expose
    private String source;
    @SerializedName("destination")
    @Expose
    private String destination;
    @SerializedName("wallet_paid")
    @Expose
    private String walletPaid;
    @SerializedName("actual_time")
    @Expose
    private String actualTime;
    @SerializedName("d_CreatedOn")
    @Expose
    private String dCreatedOn;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("ride_id")
    @Expose
    private String rideId;
    @SerializedName("gross_amt")
    @Expose
    private String grossAmt;
    @SerializedName("base_fare")
    @Expose
    private String baseFare;
    @SerializedName("ride_charge")
    @Expose
    private String rideCharge;
    @SerializedName("wait_charge")
    @Expose
    private String waitCharge;
    @SerializedName("tax_charge")
    @Expose
    private String taxCharge;
    @SerializedName("coupon_price")
    @Expose
    private String couponPrice;
    @SerializedName("convenience_charge")
    @Expose
    private String convenienceCharge;
    @SerializedName("perminute_charge_accordingly")
    @Expose
    private String perminuteChargeAccordingly;
    @SerializedName("tolltax")
    @Expose
    private String tolltax;
    @SerializedName("driver_earning")
    @Expose
    private String driverEarning;
    @SerializedName("pay_mode")
    @Expose
    private String payMode;
    @SerializedName("net_amt")
    @Expose
    private String netAmt;
    @SerializedName("summery_created_on")
    @Expose
    private String summeryCreatedOn;
    @SerializedName("summery_modified_on")
    @Expose
    private String summeryModifiedOn;
    @SerializedName("driverearning")
    @Expose
    private String driverearning;
    @SerializedName("ridefare")
    @Expose
    private Integer ridefare;
    @SerializedName("xootocollect")
    @Expose
    private Double xootocollect;

    public String getCashPaid() {
        return cashPaid;
    }

    public void setCashPaid(String cashPaid) {
        this.cashPaid = cashPaid;
    }

    public String getActualKms() {
        return actualKms;
    }

    public void setActualKms(String actualKms) {
        this.actualKms = actualKms;
    }

    public String getCrnNo() {
        return crnNo;
    }

    public void setCrnNo(String crnNo) {
        this.crnNo = crnNo;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getWalletPaid() {
        return walletPaid;
    }

    public void setWalletPaid(String walletPaid) {
        this.walletPaid = walletPaid;
    }

    public String getActualTime() {
        return actualTime;
    }

    public void setActualTime(String actualTime) {
        this.actualTime = actualTime;
    }

    public String getdCreatedOn() {
        return dCreatedOn;
    }

    public void setdCreatedOn(String dCreatedOn) {
        this.dCreatedOn = dCreatedOn;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRideId() {
        return rideId;
    }

    public void setRideId(String rideId) {
        this.rideId = rideId;
    }

    public String getGrossAmt() {
        return grossAmt;
    }

    public void setGrossAmt(String grossAmt) {
        this.grossAmt = grossAmt;
    }

    public String getBaseFare() {
        return baseFare;
    }

    public void setBaseFare(String baseFare) {
        this.baseFare = baseFare;
    }

    public String getRideCharge() {
        return rideCharge;
    }

    public void setRideCharge(String rideCharge) {
        this.rideCharge = rideCharge;
    }

    public String getWaitCharge() {
        return waitCharge;
    }

    public void setWaitCharge(String waitCharge) {
        this.waitCharge = waitCharge;
    }

    public String getTaxCharge() {
        return taxCharge;
    }

    public void setTaxCharge(String taxCharge) {
        this.taxCharge = taxCharge;
    }

    public String getCouponPrice() {
        return couponPrice;
    }

    public void setCouponPrice(String couponPrice) {
        this.couponPrice = couponPrice;
    }

    public String getConvenienceCharge() {
        return convenienceCharge;
    }

    public void setConvenienceCharge(String convenienceCharge) {
        this.convenienceCharge = convenienceCharge;
    }

    public String getPerminuteChargeAccordingly() {
        return perminuteChargeAccordingly;
    }

    public void setPerminuteChargeAccordingly(String perminuteChargeAccordingly) {
        this.perminuteChargeAccordingly = perminuteChargeAccordingly;
    }

    public String getTolltax() {
        return tolltax;
    }

    public void setTolltax(String tolltax) {
        this.tolltax = tolltax;
    }

    public String getDriverEarning() {
        return driverEarning;
    }

    public void setDriverEarning(String driverEarning) {
        this.driverEarning = driverEarning;
    }

    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    public String getNetAmt() {
        return netAmt;
    }

    public void setNetAmt(String netAmt) {
        this.netAmt = netAmt;
    }

    public String getSummeryCreatedOn() {
        return summeryCreatedOn;
    }

    public void setSummeryCreatedOn(String summeryCreatedOn) {
        this.summeryCreatedOn = summeryCreatedOn;
    }

    public String getSummeryModifiedOn() {
        return summeryModifiedOn;
    }

    public void setSummeryModifiedOn(String summeryModifiedOn) {
        this.summeryModifiedOn = summeryModifiedOn;
    }

    public String getDriverearning() {
        return driverearning;
    }

    public void setDriverearning(String driverearning) {
        this.driverearning = driverearning;
    }

    public Integer getRidefare() {
        return ridefare;
    }

    public void setRidefare(Integer ridefare) {
        this.ridefare = ridefare;
    }

    public Double getXootocollect() {
        return xootocollect;
    }

    public void setXootocollect(Double xootocollect) {
        this.xootocollect = xootocollect;
    }
}
