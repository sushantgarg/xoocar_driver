package com.xoocardriver.RetroFit;

/**
 * Created by sushant on 8/14/17.
 */

public class CancelRideScheduledRequestData {
    private String driverid,rideid,whycancel;

    public String getDriverid() {
        return driverid;
    }

    public void setDriverid(String driverid) {
        this.driverid = driverid;
    }

    public CancelRideScheduledRequestData(String driverid, String rideid, String whycancel) {
        this.driverid = driverid;
        this.rideid = rideid;
        this.whycancel = whycancel;
    }

    public String getRideid() {
        return rideid;

    }

    public void setRideid(String rideid) {
        this.rideid = rideid;
    }

    public String getWhycancel() {
        return whycancel;
    }

    public void setWhycancel(String whycancel) {
        this.whycancel = whycancel;
    }
}
