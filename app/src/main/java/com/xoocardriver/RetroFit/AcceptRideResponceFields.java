package com.xoocardriver.RetroFit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/12/17.
 */

public class AcceptRideResponceFields {


    @SerializedName("rideid")
    @Expose
    private String rideid;

    public String getRideid() {
        return rideid;
    }

    public void setRideid(String rideid) {
        this.rideid = rideid;
    }

    public String getcId() {
        return cId;
    }

    public void setcId(String cId) {
        this.cId = cId;
    }

    public String getcFirstName() {
        return cFirstName;
    }

    public void setcFirstName(String cFirstName) {
        this.cFirstName = cFirstName;
    }

    public String getcContact() {
        return cContact;
    }

    public void setcContact(String cContact) {
        this.cContact = cContact;
    }

    public String getcProfilepic() {
        return cProfilepic;
    }

    public void setcProfilepic(String cProfilepic) {
        this.cProfilepic = cProfilepic;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getCrnNo() {
        return crnNo;
    }

    public void setCrnNo(String crnNo) {
        this.crnNo = crnNo;
    }

    public String getcDevicetype() {
        return cDevicetype;
    }

    public void setcDevicetype(String cDevicetype) {
        this.cDevicetype = cDevicetype;
    }

    public String getcDevicetoken() {
        return cDevicetoken;
    }

    public void setcDevicetoken(String cDevicetoken) {
        this.cDevicetoken = cDevicetoken;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getvLicenceNo() {
        return vLicenceNo;
    }

    public void setvLicenceNo(String vLicenceNo) {
        this.vLicenceNo = vLicenceNo;
    }

    public String getcLatt() {
        return cLatt;
    }

    public void setcLatt(String cLatt) {
        this.cLatt = cLatt;
    }

    public String getcLong() {
        return cLong;
    }

    public void setcLong(String cLong) {
        this.cLong = cLong;
    }

    public String getdLatt() {
        return dLatt;
    }

    public void setdLatt(String dLatt) {
        this.dLatt = dLatt;
    }

    public String getdLong() {
        return dLong;
    }

    public void setdLong(String dLong) {
        this.dLong = dLong;
    }

    public String getcContacno() {
        return cContacno;
    }

    public void setcContacno(String cContacno) {
        this.cContacno = cContacno;
    }

    public String getcDestLat() {
        return cDestLat;
    }

    public void setcDestLat(String cDestLat) {
        this.cDestLat = cDestLat;
    }

    public String getcDestLng() {
        return cDestLng;
    }

    public void setcDestLng(String cDestLng) {
        this.cDestLng = cDestLng;
    }

    @SerializedName("c_id")
    @Expose

    private String cId;
    @SerializedName("c_first_name")
    @Expose
    private String cFirstName;
    @SerializedName("c_contact")
    @Expose
    private String cContact;
    @SerializedName("c_profilepic")
    @Expose
    private String cProfilepic;
    @SerializedName("source")
    @Expose
    private String source;
    @SerializedName("destination")
    @Expose
    private String destination;
    @SerializedName("crn_no")
    @Expose
    private String crnNo;
    @SerializedName("c_devicetype")
    @Expose
    private String cDevicetype;
    @SerializedName("c_devicetoken")
    @Expose
    private String cDevicetoken;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("contact")
    @Expose
    private String contact;
    @SerializedName("v_licence_no")
    @Expose
    private String vLicenceNo;
    @SerializedName("c_latt")
    @Expose
    private String cLatt;
    @SerializedName("c_long")
    @Expose
    private String cLong;
    @SerializedName("d_latt")
    @Expose
    private String dLatt;
    @SerializedName("d_long")
    @Expose
    private String dLong;
    @SerializedName("c_contacno")
    @Expose
    private String cContacno;
    @SerializedName("c_dest_lat")
    @Expose
    private String cDestLat;
    @SerializedName("c_dest_lng")
    @Expose
    private String cDestLng;
}
