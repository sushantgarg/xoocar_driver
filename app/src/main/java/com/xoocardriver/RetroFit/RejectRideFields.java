package com.xoocardriver.RetroFit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/13/17.
 */

public class RejectRideFields {

    @SerializedName("rideid")
    @Expose
    private String rideid;
    @SerializedName("c_devicetype")
    @Expose
    private String cDevicetype;
    @SerializedName("c_devicetoken")
    @Expose
    private String cDevicetoken;
    @SerializedName("driver_id")
    @Expose
    private String driverId;

    public RejectRideFields(String rideid, String cDevicetype, String cDevicetoken, String driverId) {
        this.rideid = rideid;
        this.cDevicetype = cDevicetype;
        this.cDevicetoken = cDevicetoken;
        this.driverId = driverId;
    }

    public String getRideid() {
        return rideid;
    }

    public void setRideid(String rideid) {
        this.rideid = rideid;
    }

    public String getcDevicetype() {
        return cDevicetype;
    }

    public void setcDevicetype(String cDevicetype) {
        this.cDevicetype = cDevicetype;
    }

    public String getcDevicetoken() {
        return cDevicetoken;
    }

    public void setcDevicetoken(String cDevicetoken) {
        this.cDevicetoken = cDevicetoken;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }
}
