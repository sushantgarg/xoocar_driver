package com.xoocardriver.RetroFit.GetTrainingVideos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 9/8/17.
 */

public class GetTrainingVideoRequestFields {
    public GetTrainingVideoRequestFields(String fields) {
        this.fields = fields;
    }

    public String getFields() {

        return fields;
    }

    public void setFields(String fields) {
        this.fields = fields;
    }

    @SerializedName("fields")
    @Expose
    private String fields;
}
