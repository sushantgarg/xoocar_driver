package com.xoocardriver.RetroFit.InviteDriver;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 9/7/17.
 */

public class InviteDriverRequestFields {
    public InviteDriverRequestFields(InviteDriverRequestData fields) {
        this.fields = fields;
    }

    public InviteDriverRequestData getFields() {

        return fields;
    }

    public void setFields(InviteDriverRequestData fields) {
        this.fields = fields;
    }

    @SerializedName("fields")
    @Expose
    private InviteDriverRequestData fields;
}
