package com.xoocardriver.RetroFit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sushant on 8/14/17.
 */

public class UpdateDriverPosResponceData<T> {


    @SerializedName("oid")
    @Expose
    private String oid;
    @SerializedName("source")
    @Expose
    private String source;
    @SerializedName("destination")
    @Expose
    private String destination;
    @SerializedName("pay_mode")
    @Expose
    private String payMode;
    @SerializedName("alter_driver")
    @Expose
    private String alterDriver;

    public UpdateDriverPosResponceData(String oid, String source, String destination, String payMode, String alterDriver) {
        this.oid = oid;
        this.source = source;
        this.destination = destination;
        this.payMode = payMode;
        this.alterDriver = alterDriver;
    }

    public String getOid() {

        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    public String getAlterDriver() {
        return alterDriver;
    }

    public void setAlterDriver(String alterDriver) {
        this.alterDriver = alterDriver;
    }
}
