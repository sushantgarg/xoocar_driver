package com.xoocardriver;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.xoocardriver.RetroFit.IncentiveDetailRequestData;
import com.xoocardriver.RetroFit.IncentiveDetailRequestFields;
import com.xoocardriver.RetroFit.IncentiveDetailResponceFields;
import com.xoocardriver.RetroFit.RequestInterface;
import com.xoocardriver.preference.PrefManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by sushant on 8/31/17.
 */

public class IncentiveActivity extends AppCompatActivity {
    private TextView incentiveText;
    private TextView incentiveTextDesc;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incentive);
        PrefManager prefManager = new PrefManager(this);

        incentiveText= (TextView) findViewById(R.id.incentiveText);
        incentiveTextDesc= (TextView) findViewById(R.id.incentiveTextDesc);

        getIncentiveDetail(prefManager.getKeyUserId());
    }

    private void getIncentiveDetail( String keyUserId ) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);
        Call<IncentiveDetailResponceFields> call = request.getIncentiveDetail("api/driverappapi/getdriverincentive/format/json/"
                , new IncentiveDetailRequestFields(new IncentiveDetailRequestData(keyUserId)));

        call.enqueue(new Callback<IncentiveDetailResponceFields>() {
            @Override
            public void onResponse(Call<IncentiveDetailResponceFields> call, retrofit2.Response<IncentiveDetailResponceFields> response) {

                if (response.body() != null) {
                    incentiveText.setText(getString(R.string.rupees_symbol)+" "+response.body().getDataArray().getBalance());
//                    incentiveTextDesc.setText(response.body().getDataArray().getOnreferral());
                }
            }

            @Override
            public void onFailure(Call<IncentiveDetailResponceFields> call, Throwable t) {
            }
        });
    }
}
