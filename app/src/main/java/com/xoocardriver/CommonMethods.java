package com.xoocardriver;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.xoocardriver.RetroFit.RequestInterface;
import com.xoocardriver.helper.Constant;
import com.xoocardriver.model.GetPath;
import com.xoocardriver.model.PathRequestModel;
import com.xoocardriver.model.ResponceValues;
import com.xoocardriver.model.ResponseModel;
import com.xoocardriver.realm.PathLocations;
import com.xoocardriver.realm.RealmHandler;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by sushant on 21/7/17.
 */

public class CommonMethods {

    public static boolean isServiceRunning(Context ctx, Class<?> serviceClass) {

        ActivityManager manager = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static String convertDateUnixToStringFormat(long timestamp) {

        String timeStamp = String.valueOf(timestamp);
        String upToNCharacters = String.valueOf(timestamp);

        if ( timeStamp.length() > 10 )
            upToNCharacters = timeStamp.substring(0, 10);

        long unixSeconds = Long.parseLong(upToNCharacters);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = new Date(unixSeconds * 1000L);
        sdf.setTimeZone(TimeZone.getDefault());
        return sdf.format(date);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static void sendPathToServer(String ride_id,String driverId) {
        List<PathLocations> locationses= RealmHandler.getUnsendPath();

        if( locationses.size() > 0 ) {
            if( locationses.size()> 50 ) {
                updateOrder( ride_id,locationses.subList(0,49),driverId);
            } else {
                updateOrder( ride_id,locationses,driverId);
            }
        }
    }

    private static void updateOrder(  final String ride_id, final List<PathLocations> pathLocations, String driverId) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.base)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);

        Call<ResponseModel<ResponceValues>> call = request.getOrders("api/driverappapi/routemap/format/json/" ,new GetPath(new PathRequestModel(pathLocations,ride_id,driverId)));
        call.enqueue(new Callback<ResponseModel<ResponceValues>>() {

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<ResponseModel<ResponceValues>> call, retrofit2.Response<ResponseModel<ResponceValues>> response) {
                Log.d("aaaa","Success");
                RealmHandler.updatePathStatus(pathLocations);
            }

            @Override
            public void onFailure(Call<ResponseModel<ResponceValues>> call, Throwable t) {
                if (t != null)
                    Log.d("Error","Message:" ,t);
            }
        });
    }

}
