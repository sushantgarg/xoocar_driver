package com.xoocardriver;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.xoocardriver.RetroFit.InviteDriver.InviteDriverRequestData;
import com.xoocardriver.RetroFit.InviteDriver.InviteDriverRequestFields;
import com.xoocardriver.RetroFit.InviteDriver.InviteDriverResponceFields;
import com.xoocardriver.RetroFit.RejectRideResponce;
import com.xoocardriver.RetroFit.RequestDataAcceptRequest;
import com.xoocardriver.RetroFit.RequestFieldsAcceptRide;
import com.xoocardriver.RetroFit.RequestInterface;
import com.xoocardriver.helper.Validator;
import com.xoocardriver.preference.PrefManager;
import com.xoocardriver.view.fragment.OfflineOnline;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by sushant on 9/7/17.
 */

public class InviteDriver extends AppCompatActivity {
    private LinearLayout addContacts;
    private EditText phoneNum;
    private Button invite, share;
    private PrefManager prefManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_driver);

        prefManager=new PrefManager(InviteDriver.this);

        phoneNum= (EditText) findViewById(R.id.invite_driver_vehicle_edt);
        invite= (Button) findViewById(R.id.invite_driver_btn);
        share= (Button) findViewById(R.id.share_driver_btn);
        addContacts =(LinearLayout)findViewById(R.id.add_contact_driver_from_phone_rl);

        addContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                    startActivityForResult(contactPickerIntent, Constants.PICK_CONTACT);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Validator.mobValid(phoneNum, InviteDriver.this)) {
                    sendInviteToDriver(phoneNum.getText().toString());
                } else {
                    phoneNum.requestFocus();
                    phoneNum.setError("Invalid Number");
                    //  mMobileTv.setError("Please enter valid mobile");
                }
            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String c_first_name=prefManager.getKeyUserName();
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,"Hurrey! "+c_first_name+" ne aapko refer kiya hai-diye gaye link ko click kar Xoocar ke saath jude aur jaada kamaye."+" https://goo.gl/SQG3kv");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });
    }

    private void sendInviteToDriver(String s) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);

        Call<InviteDriverResponceFields> call = request.sendInviteToDriver("api/driverappapi/sendreffertodriver/format/json/"
                ,new InviteDriverRequestFields(new InviteDriverRequestData(s,prefManager.getKeyUserId())));

        call.enqueue(new Callback<InviteDriverResponceFields>() {
            @Override
            public void onResponse(Call<InviteDriverResponceFields> call
                    , retrofit2.Response<InviteDriverResponceFields> response) {

                if( response.body() != null ) {
                    if( response.body().getResponseCode() == 400 ){
                        phoneNum.setText("");
                        Toast.makeText(InviteDriver.this, "Invite send successfully", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(InviteDriver.this, "Error sending invite", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(InviteDriver.this, "Error sending invite", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<InviteDriverResponceFields> call, Throwable t) {
            }
        });
    }

    @Override
    protected void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        switch (reqCode) {
            case (Constants.PICK_CONTACT):
                contactPicked(data);
        }
    }

    private void contactPicked(Intent data) {
        Cursor cursor = null;
        try {
            String phoneNo = null ;
            String name = null;
            // getData() method will have the Content Uri of the selected contact
            Uri uri = data.getData();
            //Query the content uri
            cursor = getContentResolver().query(uri, null, null, null, null);
            cursor.moveToFirst();
            // column index of the phone number
            int  phoneIndex =cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            // column index of the contact name
            int  nameIndex =cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            phoneNo = cursor.getString(phoneIndex);
            name = cursor.getString(nameIndex);
            phoneNo=phoneNo.replace(" ","");
            phoneNo=phoneNo.replace("+","");
            phoneNum.setText(phoneNo);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
