package com.xoocardriver.preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class PrefManagerNotification {
    public static final String OTP = "otp";
    public static final String OID = "o_id";
    public static final String RIDE_ID = "ride_id";
    public static final String PROFILE_PIC = "profile_pic";
    public static final String CITY = "city";
    public static final String VEHICLE_ID = "vehicle_id";
    public static final String D_STATUS = "d_status";
    public static final String KEY_USER_ID = "uid";
    public static final String KEY_USER_NAME = "first_name";
    public static final String KEY_USER_EMAIL = "user_email";
    public static final String KEY_USER_PASSWORD = "reg_password";
    public static final String KEY_USER_STATUS = "t_status";
    public static final String KEY_USER_CONTACT = "contact";
    private static final String KEY_USER_CREATEDON = "created_on";
    private static final String KEY_USER_DELETE = "isdelete";
    private static final String KEY_FCM_REGISTRATION_ID = "fcm_registration_id";
    private static final String NOTIFICATION = "notification";

    // Shared preferences file name
    private static final String PREF_NAME = "XoocarDriver_noi";
    //    {"login":{"name":"Praveen","r_id":"6","r_regtype":"Student","r_username":"pra123","r_email":"pavankmr111@gmail.com","status":1}}
    private static final String KEY_DEVICE_ID = "deviceid";
    private static final String KEY_IS_LOGGED_IN = "isLoggedIn";
    // Shared Preferences
    private SharedPreferences pref;
    // Editor for Shared preferences
    private SharedPreferences.Editor editor;
    // Context
    private Context _context;
    // Shared pref mode
    private int PRIVATE_MODE = 0;

    private static final String BANK_LOGO = "bkf_logo";



    public void createLogin(String id, String username, String useremail, String password, String status,String usercontact,String createdon,String isdelete) {

        Log.d("user id pref",":"+id);
        editor.putString(KEY_USER_ID, id);
        editor.putString(KEY_USER_NAME, username);
        editor.putString(KEY_USER_EMAIL, useremail);
        editor.putString(KEY_USER_PASSWORD, password);
        editor.putString(KEY_USER_STATUS, status);
        editor.putString(KEY_USER_CONTACT, usercontact);
        editor.putString(KEY_USER_CREATEDON, createdon);
        editor.putString(KEY_USER_DELETE, isdelete);
        editor.commit();
    }


    public String getOtp() {
        return pref.getString(OTP, null);
    }

    public void setOtp(String otp) {
        editor.putString(OTP, otp);
        editor.commit();
    }

    public String getOid() {
        return pref.getString(OID, null);
    }

    public void setOid(String o_id) {
        editor.putString(OID, o_id);
        editor.commit();
    }


    public String getRideId() {
        return pref.getString(RIDE_ID, null);
    }

    public void setRideId(String ride_id) {
        editor.putString(RIDE_ID, ride_id);
        editor.commit();
    }

    public String getProfilePic() {
        return pref.getString(PROFILE_PIC, null);
    }

    public void setProfilePic(String profile_pic) {
        editor.putString(PROFILE_PIC, profile_pic);
        editor.commit();
    }

    public String getCity() {
        return pref.getString(CITY, null);
    }

    public void setCity(String city) {
        editor.putString(CITY, city);
        editor.commit();
    }



    public String getVehicleId() {
        return pref.getString(VEHICLE_ID, null);
    }

    public void setVehicleId(String vehicle_id) {
        editor.putString(VEHICLE_ID, vehicle_id);
        editor.commit();
    }

    public String getDriverStatus() {
        return pref.getString(D_STATUS, null);
    }

    public void setDriverStatus(String d_status) {
        editor.putString(D_STATUS, d_status);
        editor.commit();
    }


    public void setKeyUserId(String dur_id) {
        editor.putString(KEY_USER_ID,dur_id );
        editor.commit();
    }
    public String getKeyUserId() {
        return pref.getString(KEY_USER_ID, null);
    }

    public void setRegistrationId(String fcm_registration_id) {
        editor.putString(KEY_FCM_REGISTRATION_ID,fcm_registration_id );
        editor.commit();
    }
    public String getKeyFcmRegistrationId() {
        return pref.getString(KEY_FCM_REGISTRATION_ID, null);
    }


    public void setKeyUserContact(String dur_id) {
        editor.putString(KEY_USER_CONTACT,dur_id );
        editor.commit();
    }
    public String getKeyUserContact() {
        return pref.getString(KEY_USER_CONTACT, null);
    }

    public void setNotification(String notification) {
        editor.putString(NOTIFICATION,notification );
        editor.commit();
    }
    public String getNotification() {
        return pref.getString(NOTIFICATION, null);
    }


    public void setDeviceId(String deviceId) {
        editor.putString(KEY_DEVICE_ID, deviceId);
        editor.commit();
    }

    public String getDeviceId() {
        return pref.getString(KEY_DEVICE_ID, null);
    }

    public void setKeyUserName(String username) {
        editor.putString(KEY_USER_NAME, username);
        editor.commit();
    }

    public String getKeyUserName() {
        return pref.getString(KEY_USER_NAME, null);
    }

    public void setKeyUserEmail(String userEmail) {
        editor.putString(KEY_USER_EMAIL, userEmail);
        editor.commit();
    }

    public String getKeyUserEmail() {
        return pref.getString(KEY_USER_EMAIL, null);
    }

    public void setKeyUserPassword(String userPassword) {
        editor.putString(KEY_USER_PASSWORD, userPassword);
        editor.commit();
    }

    public String getKeyUserPassword() {
        return pref.getString(KEY_USER_PASSWORD, null);
    }


    /*public void setUserContact(String userContact) {
        Log.d("user_contacttttttt", ""+userContact);
        editor.putString(USER_CONTACT, userContact);
        editor.commit();
    }

    public String getUserContact() {
        return pref.getString(USER_CONTACT, null);
    }*/

    public static String getKeyUserCreatedon() {
        return KEY_USER_CREATEDON;
    }

    public static String getKeyUserDelete() {
        return KEY_USER_DELETE;
    }



    public static String getKeyIsLoggedIn() {
        return KEY_IS_LOGGED_IN;
    }

    public static String getPrefName() {
        return PREF_NAME;
    }

    public static String getKeyUserStatus() {
        return KEY_USER_STATUS;
    }

    public String getBankLogo() {
        return pref.getString(BANK_LOGO, null);
    }

    public void setBankLogo(String bu_id) {
        editor.putString(BANK_LOGO,bu_id );
        editor.commit();
    }

    public PrefManagerNotification(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }


    public void clearSession() {
        editor.clear();
        editor.commit();
        Log.d("TAG", "Deleted all user info from shared preference");
    }

}
