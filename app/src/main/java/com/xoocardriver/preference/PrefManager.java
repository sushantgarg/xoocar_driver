package com.xoocardriver.preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import retrofit2.http.PUT;

public class PrefManager {
    public static final String OTP = "otp";
    public static final String OTP_MOBILE = "otp_mobile";
    public static final String RIDE_ID_CHECK = "ride_id_check";
    public static final String OID = "o_id";
    public static final String RIDE_ID = "ride_id";
    public static final String PROFILE_PIC = "profile_pic";
    public static final String CITY = "city";
    public static final String VEHICLE_ID = "vehicle_id";
    public static final String D_STATUS = "d_status";
    public static final String KEY_USER_ID = "uid";
    public static final String KEY_USER_NAME = "first_name";
    public static final String KEY_USER_EMAIL = "user_email";
    public static final String KEY_USER_PASSWORD = "reg_password";
    public static final String KEY_USER_STATUS = "t_status";
    public static final String KEY_USER_CONTACT = "contact";
    public static final String DURATION = "duration";
    public static final String OTP_alarm = "otp_alarm";
    private static final String KEY_USER_CREATEDON = "created_on";
    private static final String KEY_USER_DELETE = "isdelete";
    private static final String KEY_FCM_REGISTRATION_ID = "fcm_registration_id";
    private static final String NOTIFICATION = "notification";
    // Shared preferences file name
    private static final String PREF_NAME = "XoocarDriver";
    //    {"login":{"name":"Praveen","r_id":"6","r_regtype":"Student","r_username":"pra123","r_email":"pavankmr111@gmail.com","status":1}}
    private static final String KEY_DEVICE_ID = "deviceid";
    private static final String KEY_IS_LOGGED_IN = "isLoggedIn";
    private static final String ISRIDERUNNING ="isriderunning" ;
    private static final String RIDEDETAILS ="ridedetails" ;
    private static final String BANK_LOGO = "bkf_logo";
    // Shared Preferences
    private SharedPreferences pref;
    // Editor for Shared preferences
    private SharedPreferences.Editor editor;
    // Context
    private Context _context;
    // Shared pref mode
    private int PRIVATE_MODE = 0;
    private String CURRENTLAT="currentlat";
    private String CURRENTLNG="currentlng";


    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public static String getKeyUserCreatedon() {
        return KEY_USER_CREATEDON;
    }

    public static String getKeyUserDelete() {
        return KEY_USER_DELETE;
    }

    public static String getKeyIsLoggedIn() {
        return KEY_IS_LOGGED_IN;
    }

    public static String getPrefName() {
        return PREF_NAME;
    }

    public static String getKeyUserStatus() {
        return KEY_USER_STATUS;
    }

    public void createLogin(String id, String username, String useremail, String password, String status, String usercontact, String createdon, String isdelete) {

        Log.d("user id pref", ":" + id);
        editor.putString(KEY_USER_ID, id);
        editor.putString(KEY_USER_NAME, username);
        editor.putString(KEY_USER_EMAIL, useremail);
        editor.putString(KEY_USER_PASSWORD, password);
        editor.putString(KEY_USER_STATUS, status);
        editor.putString(KEY_USER_CONTACT, usercontact);
        editor.putString(KEY_USER_CREATEDON, createdon);
        editor.putString(KEY_USER_DELETE, isdelete);
        editor.commit();
    }

    public String getCheckBookingAlarm() {
        return pref.getString(OTP_alarm, null);
    }

    public void setCheckBookingAlarm(String otp_alarm) {
        editor.putString(OTP_alarm, otp_alarm);
        editor.commit();
    }

    public String getOtpMobile() {
        return pref.getString(OTP_MOBILE, null);
    }

    public void setOtpMobile(String otp_mobile) {
        editor.putString(OTP_MOBILE, otp_mobile);
        editor.commit();
    }

    public String getOtp() {
        return pref.getString(OTP, null);
    }

    public void setOtp(String otp) {
        editor.putString(OTP, otp);
        editor.commit();
    }

    public String getRideIdCheck() {
        return pref.getString(RIDE_ID_CHECK, "0");
    }

    public void setRideIdCheck(String ride_id_check) {
        editor.putString(RIDE_ID_CHECK, ride_id_check);
        editor.commit();
    }

    public String getOid() {
        return pref.getString(OID, null);
    }

    public void setOid(String o_id) {
        editor.putString(OID, o_id);
        editor.commit();
    }

    public String getRideId() {
        return pref.getString(RIDE_ID, null);
    }

    public void setRideId(String ride_id) {
        editor.putString(RIDE_ID, ride_id);
        editor.commit();
    }

    public String getDuration() {
        return pref.getString(DURATION, null);
    }

    public void setDuration(String duration) {
        editor.putString(DURATION, duration);
        editor.commit();
    }

    public String getProfilePic() {
        return pref.getString(PROFILE_PIC, null);
    }

    public void setProfilePic(String profile_pic) {
        editor.putString(PROFILE_PIC, profile_pic);
        editor.commit();
    }

    public String getCity() {
        return pref.getString(CITY, null);
    }

    public void setCity(String city) {
        editor.putString(CITY, city);
        editor.commit();
    }

    public String getVehicleId() {
        return pref.getString(VEHICLE_ID, null);
    }

    public void setVehicleId(String vehicle_id) {
        editor.putString(VEHICLE_ID, vehicle_id);
        editor.commit();
    }

    public String getDriverOnlineOfflineStatus() {
        return pref.getString(D_STATUS, "");
    }

    public void setDriverOnlineOfflineStatus(String d_status) {
        editor.putString(D_STATUS, d_status);
        editor.commit();
    }

    public String getKeyUserId() {
        return pref.getString(KEY_USER_ID, null);
    }

    public void setKeyUserId(String dur_id) {
        editor.putString(KEY_USER_ID, dur_id);
        editor.commit();
    }

    public String getKeyUserContact() {
        return pref.getString(KEY_USER_CONTACT, null);
    }

    public void setKeyUserContact(String dur_id) {
        editor.putString(KEY_USER_CONTACT, dur_id);
        editor.commit();
    }

    public String getDeviceId() {
        return pref.getString(KEY_DEVICE_ID, null);
    }

    public void setDeviceId(String deviceId) {
        editor.putString(KEY_DEVICE_ID, deviceId);
        editor.commit();
    }

    /*public void setUserContact(String userContact) {
        Log.d("user_contacttttttt", ""+userContact);
        editor.putString(USER_CONTACT, userContact);
        editor.commit();
    }
    public String getUserContact() {
        return pref.getString(USER_CONTACT, null);
    }*/

    public String getKeyUserName() {
        return pref.getString(KEY_USER_NAME, null);
    }

    public void setKeyUserName(String username) {
        editor.putString(KEY_USER_NAME, username);
        editor.commit();
    }

    public String getKeyUserEmail() {
        return pref.getString(KEY_USER_EMAIL, null);
    }

    public void setKeyUserEmail(String userEmail) {
        editor.putString(KEY_USER_EMAIL, userEmail);
        editor.commit();
    }

    public String getKeyUserPassword() {
        return pref.getString(KEY_USER_PASSWORD, null);
    }

    public void setKeyUserPassword(String userPassword) {
        editor.putString(KEY_USER_PASSWORD, userPassword);
        editor.commit();
    }

    public String getBankLogo() {
        return pref.getString(BANK_LOGO, null);
    }

    public void setBankLogo(String bu_id) {
        editor.putString(BANK_LOGO, bu_id);
        editor.commit();
    }

    public void clearSession() {
        String fcmToken=getFcmToken();
        editor.clear();
        editor.commit();
        setFcmToken(fcmToken);
    }

    public double getCurrentLat() {
        return Double.parseDouble(pref.getString(CURRENTLAT, "0"));
    }

    public void setCurrentLat(double lat){
        editor.putString(CURRENTLAT, String.valueOf(lat));
        editor.commit();
    }

    public double getCurrentLng() {
        return Double.parseDouble(pref.getString(CURRENTLNG, "0"));
    }

    public void setCurrentLng(double lng) {
        editor.putString(CURRENTLNG, String.valueOf(lng));
        editor.commit();
    }

    public void setIsRideRunning(boolean b) {
        editor.putBoolean(ISRIDERUNNING,b);
        editor.commit();
    }

    public boolean isRideRunning(){
        return pref.getBoolean(ISRIDERUNNING,false);
    }

    public void saveRideDetailsJson(String data_array) {
        editor.putString(RIDEDETAILS,data_array);
        editor.commit();
    }

    public String getRideDetails(){
        return pref.getString(RIDEDETAILS,"");
    }

    public void setLastCancelledRideId(String ride_id) {
        editor.putString("LASTCANCELLEDRIDEID",ride_id);
        editor.commit();
    }

    public String lastCancelledRideId(){
        return pref.getString("LASTCANCELLEDRIDEID","0");
    }

    public String getFcmToken() {
        return pref.getString("FCMTOKEN", "");
    }

    public void setFcmToken(String refreshedToken) {
        editor.putString("FCMTOKEN",refreshedToken);
        editor.commit();
    }

    public void setVehicleNum(String vLicenceNo) {
        editor.putString("VEHICLENUM",vLicenceNo);
        editor.commit();
    }

    public String getVehicleNumber(){
        return pref.getString("VEHICLENUM","");
    }

    public String getVehicleModel() {
        return pref.getString("VEHICLEMODEL", "");
    }

    public void setVehicleModel(String vVehiclename) {
        editor.putString("VEHICLEMODEL",vVehiclename);
        editor.commit();
    }

    public String getVehicleCategory() {
        return pref.getString("VEHICLECATEGORY", "");
    }

    public void setVehicleCategory(String vCategory) {
        editor.putString("VEHICLECATEGORY",vCategory);
        editor.commit();
    }

    public String getSessionId() {
        return pref.getString("SESSIONID", "0");
    }

    public void setSessionId(String sessionId) {
        editor.putString("SESSIONID", sessionId);
        editor.commit();
    }
}