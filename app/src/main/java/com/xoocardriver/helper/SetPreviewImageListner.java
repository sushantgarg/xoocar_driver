package com.xoocardriver.helper;

import android.graphics.Bitmap;

/**
 * Created by mindz on 23/12/16.
 */

public interface SetPreviewImageListner {
    public void setImage(Bitmap image);
}
