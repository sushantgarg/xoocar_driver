package com.xoocardriver.helper;

import android.content.Context;
import android.widget.EditText;
import android.widget.Toast;


import com.xoocardriver.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressWarnings("ALL")
public class Validator {
    // Email Pattern
      private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

   /* private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";*/

    private static final String NAME_PATTERN = "^[A-Z a-z]*$";

    public static boolean validateEmail(String email) {
        if (email.length()>0)
        {
            Pattern pattern = Pattern.compile(EMAIL_PATTERN);
            Matcher matcher = pattern.matcher(email);
            return matcher.matches();
        }
        return true;
    }

    public static boolean namevalidate(String name) {
        Pattern pattern = Pattern.compile(NAME_PATTERN);
        Matcher matcher = pattern.matcher(name);
        return !name.startsWith(" ") && matcher.matches();
    }


    public static boolean nameValid(EditText editText, Context mMain) {
        if (editText.getText().toString().length() == 0) {
            editText.setError("Enter full name");
            return false;
        } else if (editText.getText().toString().length() < 3) {
            editText.setError("Name must be at least 4 characters");
            return false;
        } else if (!(namevalidate(editText.getText().toString()))) {
            editText.setError("Name must be at least 4 characters");
            return false;
        } else {
            editText.setError(null);
            return true;

        }
    }

    public static boolean mobValid(EditText editText, Context mMain) {
        String value = editText.getText().toString();
        if (editText.getText().toString().length() == 0) {
            editText.setError("Enter mobile number");
            return false;
        } else if (editText.getText().toString().length() < 10 || editText.getText().toString().length() > 10) {
            editText.setError("Mobile must be 10 digit");
            return false;
        } else if (value.startsWith("0") || value.startsWith("1") || value.startsWith("2") || value.startsWith("3") || value.startsWith("4") || value.startsWith("5") || value.startsWith("6")) {
            editText.setError("Mobile must be valid 10 digit");
            return false;
        } else {
            editText.setError(null);
            return true;
        }

    }

    public static boolean mobValid1(EditText editText, Context mMain) {
        String value = editText.getText().toString();
        if (editText.getText().toString().length() == 0) {
           /* editText.setError(mMain.getResources().getString(
                    R.string.mobilehint));*/
            Toast.makeText(mMain, R.string.mobile_no, Toast.LENGTH_LONG).show();
            return false;
        } else if (editText.getText().toString().length() < 10) {
          /*  editText.setError(mMain.getResources().getString(
                    R.string.enter_valid_mobileno));*/
            Toast.makeText(mMain,R.string.mobile_no, Toast.LENGTH_LONG).show();
            return false;
        } else if (value.startsWith("0") || value.startsWith("1") || value.startsWith("2") || value.startsWith("3") || value.startsWith("4") || value.startsWith("5") || value.startsWith("6")) {
           /* editText.setError(mMain.getResources().getString(
                    R.string.enter_valid_mobileno));*/
            Toast.makeText(mMain,R.string.mobile_no, Toast.LENGTH_LONG).show();
            return false;
        } else {
            editText.setError(null);
            return true;
        }
    }

    public static boolean validPassword(EditText editText, Context mMainActivity) {
        if (editText.getText().toString().length() == 0) {
//            editText.setError(mMainActivity.getResources().getString(
//                    R.string.passhint));
            editText.setError("Please enter password");
            return false;
        } else if (editText.getText().toString().length() > 0 && editText.getText().toString().length() < 6) {
//            editText.setError(mMainActivity.getResources().getString(
//                    R.string.passhint));
            editText.setError("Password must be at least 6 characters");
            return false;
        } else if (editText.getText().toString().length() < 6 && editText.getText().toString().length() > 8) {
            editText.setError("Password must be at least 6 characters");
            return false;
        } else {
            editText.setError(null);
            return true;
        }
    }

    public static boolean validEmail(EditText editText, Context mMainActivity) {
        if (editText.getText().toString().length() == 0) {
//            editText.setError(mMainActivity.getResources().getString(
//                    R.string.passhint));
            editText.setError("Please enter email");
            return false;
        } else {
            editText.setError(null);
            return true;
        }
    }

}

/*class AlphaNumericInputFilter implements InputFilter {
    public CharSequence filter(CharSequence source, int start, int end,
                               Spanned dest, int dstart, int dend) {

        // Only keep characters that are alphanumeric
        StringBuilder builder = new StringBuilder();
        for (int i = start; i < end; i++) {
            char c = source.charAt(i);
            if (Character.isLetterOrDigit(c)) {
                builder.append(c);
            }
        }
        // If all characters are valid, return null, otherwise only return the filtered characters
        boolean allCharactersValid = (builder.length() == end - start);
        return allCharactersValid ? null : builder.toString();
    }*/
//}
