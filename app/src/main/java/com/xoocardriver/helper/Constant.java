package com.xoocardriver.helper;


/**
 * Created by Praveen Kumar on 24/8/16.
 */

public class Constant {

    /*--------------------------------------------Constant-----------------------------------------------*/
    public static String flag="0";
    // Milliseconds per second
    private static final int MILLISECONDS_PER_SECOND = 1000;
    // Update frequency in seconds
    private static final int UPDATE_INTERVAL_IN_SECONDS = 60;
    // Update frequency in milliseconds
    public static final long UPDATE_INTERVAL = MILLISECONDS_PER_SECOND * UPDATE_INTERVAL_IN_SECONDS;
    // The fastest update frequency, in seconds
    private static final int FASTEST_INTERVAL_IN_SECONDS = 60;
    // A fast frequency ceiling in milliseconds
    public static final long FASTEST_INTERVAL = MILLISECONDS_PER_SECOND * FASTEST_INTERVAL_IN_SECONDS;
    // Stores the lat / long pairs in a text file
    public static final String LOCATION_FILE = "sdcard/location.txt";
    // Stores the connect / disconnect data in a text file
    public static final String LOG_FILE = "sdcard/log.txt";

    public static final String RUNNING = "runningInBackground"; // Recording data in background

    public static final String APP_PACKAGE_NAME = "com.blackcj.locationtracker";

    /**
     * Suppress default constructor for noninstantiability
     */


    public static final String google_direction_purchage_key = "AIzaSyAhME7SQnSieMr4w9VPGgi0o6fmN0XKbqg";

    /*-----------------------------------------------BASE URL---------------------------------------------------*/

    //   public static final String base = "http://115.124.98.243/~xoocar/";
//    public static final String base = "http://www.xoocar.com/";
    public static final String base = "http://velenesa.com/vele/index.php/";

    // public static final String base = "http://192.168.1.30/xoocar/";

     /*-----------------------------------------------IMAGE FULL URL---------------------------------------------------*/

    public static final String PROFILE_IMAGE_URL = base + "assets/profileimage/";

    /*-----------------------------------------------DOCUMENT FULL URL---------------------------------------------------*/

    public static final String DOCUMENT_URL = base;/* "http://115.124.98.243/~xoocar/";*/

     /*-----------------------------------------------VIEW DOCUMENT FULL URL---------------------------------------------------*/

    public static final String VIEW_DOCUMENT_IMAGE_URL = base + "assets/upload/cab_doc/";


    /*-----------------------------------------------FULL URL---------------------------------------------------*/

    public static final String CHANGE_PASSWORD_POST_REQUEST_URL = base + "api/customersapi/changepwd/format/json";//customer_id,image


    /*----------------------------------------------Cab search api---------------------------------------------*/
    public static final String SEARCH_GET_URL = base + "api/searchapi/getcab/format/json/";


    public static final String SEARCH_VEHICLE_POST_REQUEST_URL = base + "api/searchapi/vehicaltype/format/json/";
    public static final String LOGIN_POST_REQUEST_URL = base + "api/driversapi/driverlogin/format/json/";
    public static final String REGISTRATION_POST_REQUEST_URL = base + "api/loginapi/reguser/format/json/";
    public static final String OTP_POST_REQUEST_URL = base + "api/loginapi/checkotp/format/json/";
    public static final String RESET_PASS_POST_REQUEST_URL = base + "api/loginapi/forget_password/format/json/";
    public static final String BOOK_CONFIRMATION_POST_REQUEST_URL = base + "api/driversapi/booking/format/json/";

    public static final String UPDAT_DRIVER_LOCATION_POST_REQUEST_URL = base + "api/driversapi/driverlocnew/format/json/";
    public static final String JEJECT_NOTIFICATION_POST_REQUEST_URL = base + "api/searchapi/aletrnatedriver/format/json/";
    public static final String ACCEPT_NOTIFICATION_POST_REQUEST_URL = base + "api/processapi/confirmbydriver/format/json/";

    public static final String BOOK_CANCEL_GET_REQUEST_URL = base + "api/driversapi/cancelledreasons/format/json/";
    public static final String BOOK_CANCEL_POST_REQUEST_URL = base + "api/searchapi/aletrnatedriver/format/json/";

    public static final String GET_PROFILE_POST_REQUEST_URL = base + "api/driversapi/fetchrdriverdetail/format/json";
    public static final String UPDATGE_PROFILE_POST_REQUEST_URL = base + "api/driversapi/updatedriverdetails/format/json";
    public static final String ALL_VEHICLE_POST_REQUEST_URL = base + "api/driversapi/vehicalfordriver/format/json/";

   // public static final String TRIP_POST_REQUEST_URL = base + "api/searchapi/triphistory/format/json/";
    public static final String TRIP_POST_REQUEST_URL = base + "api/searchapi/triphistoryfire/format/json/";

    public static final String VIEW_DOC_POST_REQUEST_URL = base + "api/customersapi/viewdriverdocs/format/json";//driver_id:136
    public static final String DOCUMENT_VIEW_POST_REQUEST_URL = base + "api/customersapi/documentview/format/json";//city_id:1
    public static final String ADD_DOCUMENT_POST_REQUEST_URL = base + "api/customersapi/documentadd/format/json"; //driver_id,image,doctype: doc type id, number,expiry_date


    public static final String VIEW_DRIVER_DOC_POST_REQUEST_URL = base + "api/customersapi/viewdriverdocs/format/json";//driver_id:136

    public static final String WAY_BILL_POST_REQUEST_URL = base + "api/searchapi/waybill/format/json/";//driver_id:136

    public static final String UPLOAD_PROFILE_PIC_POST_REQUEST_URL = base + "api/customersapi/uploadprofilepic/format/json";//customer_id,image


    /*---------------------------------------------driver order flow api -----------------------------------------------*/

    public static final String PICKUP_POST_REQUEST_URL = base + "api/driversapi/pickup/format/json/";//city_id", "cat_id","ride_id"
    public static final String COMPLETE_TRIP_POST_REQUEST_URL = base + "api/searchapi/completetrip/format/json/";//DRIVER_ID cat_id:7

    public static final String START_TRIP_POST_REQUEST_URL = base + "api/searchapi/starttrip/format/json/";//driverid:107rideid:43
    public static final String GET_RATINGS_POST_REQUEST_URL = base + "api/driversapi/fetchratings/format/json";//driver_id:107
    public static final String POST_RATINGS_POST_REQUEST_URL = base + "api/driversapi/insertrating/format/json";//driver_id:107

    public static final String PICK_PARALLELPOST_REQUEST_URL = base + "api/rideapi/confirmbydriver/format/json/";//driver_id:107
    public static final String START_PARALLELPOST_REQUEST_URL = base + "api/rideapi/starttripnew/format/json/";//driver_id:107
    public static final String COMPLETE_PARALLELPOST_REQUEST_URL = base + "api/rideapi/completetrip/format/json/";//driver_id:107


    public static final String UPDATE_ROOT_LAT_LONG_POST_REQUEST_URL = base + "api/driversapi/routemap/format/json/";//d_id:11ride_id:12d_latt:121212.121d_long:121212.1212
    public static final String UPDATE_ROOT_LAT_LONG_POST_REQUEST_URL_NEW_APP = base + "api/driversapi/routemapnewapp/format/json/";//d_id:11ride_id:12d_latt:121212.121d_long:121212.1212

    /*-----------------------------------------------------------Cab status---------------------------------------------------------------*/
    public static final String DRIVER_STATUS_POST_REQUEST_URL = base + "api/driversapi/driverstatus/format/json/";


    /*--------------------share contact---------------------*/

    public static final String ALL_CITY_POST_REQUEST_URL = base + "api/driversapi/getallcity/format/json/";

    public static final String EARNING_POST_REQUEST_URL = base + "api/searchapi/getearning/format/json/";
    public static final String SHARE_REFER_CONTACT_POST_REQUEST_URL = base + "api/searchapi/sendreffer/format/json/";

     /*-------------------------------------------------------Emergency contact apis------------------------------------------------------------*/

    public static final String ADD_EMERGENCY_CONTACT_URL = base + "api/customersapi/emergencycontact/format/json/";
    public static final String GET_EMERGENCY_CONTACT_URL = base + "api/customersapi/getemergencycontact/format/json/";
    public static final String STATUS_EMERGENCY_CONTACT_URL = base + "api/customersapi/statusemergencycontact/format/json/";
    public static final String DELETE_EMERGENCY_CONTACT_URL = base + "api/customersapi/getemergencycontact/format/json/";

     /*-------------------------------------------------------SOS apis------------------------------------------------------------*/

    public static final String SOS_POST_REQUEST_URL = base + "api/customersapi/customersos/format/json/";//uid:7status:0

    /*----------------------------------------------My rides api---------------------------------------------------*/
    public static final String MYRIDES_POST_REQUEST_URL = base + "api/customersapi/customerridehistory/format/c/";
    public static final String HISTORY_POST_REQUEST_URL = base + "api/customersapi/customerridehistoryRow/format/json/";

    /*---------------------------------------------------------------------------------------------------------------------------*/


     /*-------------------------------------------------------Unfortunatally cancel apis------------------------------------------------------------*/

    public static final String REJECT_RIDE = base + "api/searchapi/cancelbeforeschedule/format/json/";

     /*-------------------------------------------------------Unfortunatally cancel apis------------------------------------------------------------*/

    public static final String CEHCK_RATING_POST_URL = base + "api/driversapi/checkrating/format/json/";

    /*----------------------------------------------My rides api---------------------------------------------------*/
    public static final String EARNING_DETAILS_POST_REQUEST = base + "api/searchapi/getearning/format/json/";

    /*-------------------------------------------------------Share link url------------------------------------------------------------*/
    public static final String SHARE_LINK = base + "webapp/webapp/mapdraw/";

    /*---------------------------------------------------------------Get cab location -------------------------------------------------------------------*/
    public static final String GET_SHARE_AMOUNT = base + "api/walletapi/getrupp/format/json/";

       /*--------------------------------------------------------Forgot password------------------------------------------------------------*/

    public static final String NEW_CHECK_OTP_POST_REQUEST_URL = base + "api/loginapi/checkdriverotp/format/json/";

    public static final String EARNING_DATEWISE_AMOUNT= base+"api/mobileapp/appdashboard/format/json";
}
