package com.xoocardriver.helper;

/**
 * Created by mindz on 4/1/17.
 */

public class Way_bill {
    private String oid;
    private String driverId;
    private String vehicalId;
    private String customerId;
    private String crnNo;
    private String source;
    private String destination;
    private String estimatedCost;
    private String estimatedTime;
    private String pickupTime;
    private String diliverTime;
    private String actualCost;
    private String actualTime;
    private String actualKms;
    private String dCreatedOn;
    private Object nCreatedBy;
    private String dModifiedOn;
    private Object nModifiedBy;
    private String bDeleted;
    private String bookingStatus;
    private String paySuccess;
    private String wayBill;
    private String bCancelledBy;
    private String cancelledWhy;
    private Object nAdminType;

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getVehicalId() {
        return vehicalId;
    }

    public void setVehicalId(String vehicalId) {
        this.vehicalId = vehicalId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCrnNo() {
        return crnNo;
    }

    public void setCrnNo(String crnNo) {
        this.crnNo = crnNo;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getEstimatedCost() {
        return estimatedCost;
    }

    public void setEstimatedCost(String estimatedCost) {
        this.estimatedCost = estimatedCost;
    }

    public String getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(String estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public String getPickupTime() {
        return pickupTime;
    }

    public void setPickupTime(String pickupTime) {
        this.pickupTime = pickupTime;
    }

    public String getDiliverTime() {
        return diliverTime;
    }

    public void setDiliverTime(String diliverTime) {
        this.diliverTime = diliverTime;
    }

    public String getActualCost() {
        return actualCost;
    }

    public void setActualCost(String actualCost) {
        this.actualCost = actualCost;
    }

    public String getActualTime() {
        return actualTime;
    }

    public void setActualTime(String actualTime) {
        this.actualTime = actualTime;
    }

    public String getActualKms() {
        return actualKms;
    }

    public void setActualKms(String actualKms) {
        this.actualKms = actualKms;
    }

    public String getDCreatedOn() {
        return dCreatedOn;
    }

    public void setDCreatedOn(String dCreatedOn) {
        this.dCreatedOn = dCreatedOn;
    }

    public Object getNCreatedBy() {
        return nCreatedBy;
    }

    public void setNCreatedBy(Object nCreatedBy) {
        this.nCreatedBy = nCreatedBy;
    }

    public String getDModifiedOn() {
        return dModifiedOn;
    }

    public void setDModifiedOn(String dModifiedOn) {
        this.dModifiedOn = dModifiedOn;
    }

    public Object getNModifiedBy() {
        return nModifiedBy;
    }

    public void setNModifiedBy(Object nModifiedBy) {
        this.nModifiedBy = nModifiedBy;
    }

    public String getBDeleted() {
        return bDeleted;
    }

    public void setBDeleted(String bDeleted) {
        this.bDeleted = bDeleted;
    }

    public String getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public String getPaySuccess() {
        return paySuccess;
    }

    public void setPaySuccess(String paySuccess) {
        this.paySuccess = paySuccess;
    }

    public String getWayBill() {
        return wayBill;
    }

    public void setWayBill(String wayBill) {
        this.wayBill = wayBill;
    }

    public String getBCancelledBy() {
        return bCancelledBy;
    }

    public void setBCancelledBy(String bCancelledBy) {
        this.bCancelledBy = bCancelledBy;
    }

    public String getCancelledWhy() {
        return cancelledWhy;
    }

    public void setCancelledWhy(String cancelledWhy) {
        this.cancelledWhy = cancelledWhy;
    }

    public Object getNAdminType() {
        return nAdminType;
    }

    public void setNAdminType(Object nAdminType) {
        this.nAdminType = nAdminType;
    }

}
