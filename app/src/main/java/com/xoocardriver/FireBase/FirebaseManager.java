package com.xoocardriver.FireBase;

import android.location.Location;
import android.util.Log;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.xoocardriver.CommonMethods;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sushant on 9/5/17.
 */

public class FirebaseManager {
    private DatabaseReference rootReference;

    private DatabaseReference tokenReferenceLeader;
    private ChildEventListener followerReferenceInLeaderListener;

    private volatile static FirebaseManager instance;
    private Object geoFireUsers;

    public static FirebaseManager getInstance() {
        if(instance == null) {
            synchronized (FirebaseManager.class) {
                instance = new FirebaseManager();
            }
        }
        return instance;
    }

    private FirebaseManager() {
        // Constructor hidden because this is a singleton
        rootReference= FirebaseDatabase.getInstance().getReference().child("Drivers");
    }

    public void addDriver(String userId, String name, String carModel, String vehicleNumber,
                          String cityId, Location location, String vehicleCategory) {

        DatabaseReference locationsReference = rootReference.child(userId);
        Map<String, Object> map = new HashMap<>();
        map.put("driverId",userId);
        map.put("driverName",name);
        map.put("model",carModel );
        map.put("vehicleNumber",vehicleNumber);
        map.put("cityId",cityId);
        map.put("status",1);
        map.put("time", CommonMethods.convertDateUnixToStringFormat(System.currentTimeMillis()));
        map.put("category", vehicleCategory);

        Map<String, Object> loc = new HashMap<>();
        loc.put("latitude",location.getLatitude());
        loc.put("longitude",location.getLongitude());
        loc.put("provider",location.getProvider());
        loc.put("course",location.getBearing());
        loc.put("speed",location.getSpeed());
        loc.put("accuracy",location.hasAccuracy());
        loc.put("timestamp",location.getTime());

        map.put("location",loc);

        locationsReference.setValue(map);
    }

    public void updatePosition(String driverId,Location location ){
        DatabaseReference locationReference=rootReference.child(driverId);
        Map<String,Object> map=new HashMap<>();

        Map<String, Object> loc = new HashMap<>();
        loc.put("latitude",location.getLatitude());
        loc.put("longitude",location.getLongitude());
        loc.put("provider",location.getProvider());
        loc.put("course",location.getBearing());
        loc.put("speed",location.getSpeed());
        loc.put("accuracy",location.hasAccuracy());
        loc.put("timestamp",location.getTime());

        map.put( "location",loc );
        map.put("time",CommonMethods.convertDateUnixToStringFormat(System.currentTimeMillis()));
        locationReference.updateChildren(map);
    }

    public void removeDriver(String keyUserId) {
        rootReference.child(keyUserId).removeValue();
        FirebaseDatabase.getInstance().getReference().child("geo_fire").child(keyUserId).removeValue();
    }

    public void addGeoFireLoc(String keyUserId, Location loc) {
//        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("https://console.firebase.google.com/u/2/project/xoocardriver-160012/database/data/Drivers");
        GeoFire geoFire = new GeoFire(FirebaseDatabase.getInstance().getReference().child("geo_fire"));
        geoFire.setLocation( keyUserId, new GeoLocation( loc.getLatitude(), loc.getLongitude() ));
    }

    public void updateStatus( String driverId, int i ) {
        DatabaseReference locationReference=rootReference.child(driverId);
        Map<String,Object> map=new HashMap<>();

        map.put("status",i);
        locationReference.updateChildren(map);
    }

}
