//package com.xoocardriver.view.fragment;
//
//
//import android.app.ProgressDialog;
//import android.os.Bundle;
//import android.os.Parcelable;
//import android.support.v4.app.Fragment;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.TextView;
//
//import com.android.volley.AuthFailureError;
//import com.android.volley.DefaultRetryPolicy;
//import com.android.volley.NetworkError;
//import com.android.volley.NoConnectionError;
//import com.android.volley.ParseError;
//import com.android.volley.Request;
//import com.android.volley.RequestQueue;
//import com.android.volley.Response;
//import com.android.volley.ServerError;
//import com.android.volley.TimeoutError;
//import com.android.volley.VolleyError;
//import com.android.volley.VolleyLog;
//import com.android.volley.toolbox.StringRequest;
//import com.android.volley.toolbox.Volley;
//import com.google.android.gms.analytics.Tracker;
//import com.xoocardriver.GoogleAnalytic.AnalyticsApplication;
//import com.xoocardriver.R;
//import com.xoocardriver.adapter.Yesterdayrideadapter;
//import com.xoocardriver.model.Model;
//import com.xoocardriver.model.Yesterdaydetaillist;
//import com.xoocardriver.preference.PrefManager;
//import com.xoocardriver.view.activity.MainActivity;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.Map;
//
///**
// * Created by abc on 17-07-2017.
// */
//
//public class Yesterdayridedata extends Fragment {
//    private MainActivity mainActivity;
//    private ProgressDialog progressDialog;
//    private PrefManager prefManager;
//    Calendar myCalendar;
//    int total_price = 0;
//    private TextView booking;
//    private TextView bookincounted;
//    private RecyclerView recyclerView;
//    private ArrayList<Yesterdaydetaillist> listarray;
//
//
//
//    private int start_limit;
//
//    private Tracker mTracker;
//
//
//    public Yesterdayridedata(MainActivity mainActivity) {
//        this.mainActivity = mainActivity;
//        MainActivity.toolbarText.setText("Trip Earning");
//    }
//
//    public Yesterdayridedata() {
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.yesterdaydetail, container, false);
//        initView(view);
//
//        try {
//            progressDialog = new ProgressDialog(mainActivity);
//            progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
//            progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
//            progressDialog.show();
//            progressDialog.setContentView(R.layout.progressbar);
//        } catch (NullPointerException e) {
//            e.printStackTrace();
//        }
//
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd ");
//        Calendar cal = Calendar.getInstance();
//        cal.add(Calendar.DAY_OF_YEAR,-1);
//        System.out.println(sdf.format(cal.getTime()));
//        String date_from = sdf.format(cal.getTime());
//        String driver_id = prefManager.getKeyUserId();
//            getdetail(mainActivity, date_from,"",driver_id);
//        start_limit = 0;
//        AnalyticsApplication application = (AnalyticsApplication) mainActivity.getApplication();
//        mTracker = application.getDefaultTracker();
//        return view;
//    }
//
//
//
//    public void initView(View view){
//        prefManager = new PrefManager(mainActivity);
//        recyclerView = (RecyclerView) view.findViewById(R.id.trip_list_rl_yesterday);
//        recyclerView.setHasFixedSize(true);
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mainActivity,LinearLayoutManager.VERTICAL,false);
//        recyclerView.setLayoutManager(linearLayoutManager);
//    }
//    public void getdetail(MainActivity context,  final String date_from, String url, final String driver_id) {
//        RequestQueue rq = Volley.newRequestQueue(context);
//        final StringRequest postReq = new StringRequest(Request.Method.POST, "http://xoocar.com/api/mobileapp/appdashboard/format/json", new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.d(" eeeee response ", "= " + response);
//                progressDialog.dismiss();
//                //list_arr.clear();
//                try {
//                    JSONObject json=new JSONObject(response);
//                    JSONArray jsonArray = json.getJSONArray("app_ride_details");
//                    int total_price  = 0;
//                    listarray=new ArrayList<>();
//                    for (int i = 0; i < jsonArray.length(); i++) {
//                        JSONObject jsonObject = Model.getObject(jsonArray, i);
//                        Yesterdaydetaillist list = new Yesterdaydetaillist();
//                        list.setRidedate(jsonObject.getString("ride_date"));
//                        list.setBin(jsonObject.getString("brn"));
//                        list.setRideprice(jsonObject.getString("price"));
//                        list.setVehicleno(jsonObject.getString("v_licence_no"));
//                        JSONObject ride_summary_obj=Model.getJsonObject(jsonObject, "ride_summary");
//                        Log.d("ddddd ttt ", "= " + ride_summary_obj.getString("total"));
//
//                        list.setTotal(ride_summary_obj.getString("total"));
//                        list.setCouponprice(ride_summary_obj.getString("couponprice"));
//                        list.setTolltax(ride_summary_obj.getString("tolltax"));
//                        list.setBasefare(ride_summary_obj.getString("basefare"));
//                        list.setRidecharge(ride_summary_obj.getString("ridecharge"));
//                        list.setPerminutchargeaccordingly(ride_summary_obj.getString("perminutchargeaccordingly"));
//                        list.setWaitcharge(ride_summary_obj.getString("waitcharge"));
//                        list.setGst(ride_summary_obj.getString("Goods & Service Tax"));
//                        list.setNet(ride_summary_obj.getString("net"));
//                        listarray.add(list);
//                        Parcelable recyclerViewState;
//                        recyclerViewState = recyclerView.getLayoutManager().onSaveInstanceState();
//                        recyclerView.getLayoutManager().onRestoreInstanceState(recyclerViewState);
//                        Log.d("ddddd list_arr ", "= " + listarray.size());
//                        Yesterdayrideadapter yesterday=new Yesterdayrideadapter(mainActivity, listarray);
//                        recyclerView.setAdapter(yesterday);
//                    }
//                }
//                catch (JSONException e) {
//                    e.printStackTrace();
//
//                }
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                progressDialog.dismiss();
//                Log.d("dkp 1 error ocurred", "TimeoutError");
//                VolleyLog.d("Error", "Error: " + error.getMessage());
//                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
//                    Log.d("error ocurred", "TimeoutError");
//
//                } else if (error instanceof AuthFailureError) {
//                    Log.d("error ocurred", "AuthFailureError");
//
//                } else if (error instanceof ServerError) {
//                    Log.d("error ocurred", "ServerError");
//
//                } else if (error instanceof NetworkError) {
//                    Log.d("error ocurred", "NetworkError");
//                } else if (error instanceof ParseError) {
//                    Log.d("error ocurred", "ParseError");
//
//                }
//            }
//        }) {
//
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> params = new HashMap<>();
//                String[] tag = {"driver_id","date_from"};
//                String[] value = {driver_id,date_from};
//                Log.e("driver_idpp",""+driver_id);
//                Log.e("date_from98pp",""+date_from);
//                for (int i = 0; i < tag.length; i++)
//                    params.put(tag[i], value[i]);
//                return checkParams(params);
//            }
//        };
//        postReq.setRetryPolicy(new DefaultRetryPolicy(
//                800000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        rq.add(postReq);
//    }
//    private Map<String, String> checkParams(Map<String, String> map) {
//        Iterator<Map.Entry<String, String>> it = map.entrySet().iterator();
//        while (it.hasNext()) {
//            Map.Entry<String, String> pairs = (Map.Entry<String, String>) it.next();
//            if (pairs.getValue() == null) {
//                map.put(pairs.getKey(), "");
//            }
//        }
//        return map;
//    }
//}
//
//
//
//
//
//
