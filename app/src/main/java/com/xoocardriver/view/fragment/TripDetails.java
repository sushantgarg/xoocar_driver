package com.xoocardriver.view.fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.xoocardriver.R;
import com.xoocardriver.helper.Constant;
import com.xoocardriver.helper.MyApplication;
import com.xoocardriver.map.GeocodingLocation;
import com.xoocardriver.map.MapDirectionsParser;
import com.xoocardriver.preference.PrefManager;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class TripDetails extends AppCompatActivity implements GoogleMap.OnCameraIdleListener, GoogleMap.OnCameraMoveStartedListener,
        GoogleMap.OnCameraMoveListener,
        GoogleMap.OnCameraMoveCanceledListener, OnMapReadyCallback, View.OnClickListener, GoogleMap.OnMarkerDragListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private Context mainActivity;

    private GoogleMap googleMap;
    private View myLocationButton;
    private int ChangeAddressState = 1;
    //    private GPSTracker location;
    private ArrayList<LatLng> points = null;
    private ArrayList<LatLng> cabDraw = null;
    private Polyline mPolyline = null;
    private LatLng source_LatLong;
    private LatLng destination_LatLong;

    private String source_address;
    private String detination_address;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_trip_details);

        Intent i = getIntent();

        mainActivity = TripDetails.this;
        String cost = i.getStringExtra("cost");
        String duration = i.getStringExtra("duration");
        String distance = i.getStringExtra("distance");
        String source = i.getStringExtra("source");
        String destination = i.getStringExtra("destination");

        FragmentManager fragmentManager = getSupportFragmentManager();

        SupportMapFragment mMapFragment = (SupportMapFragment) fragmentManager.findFragmentById(R.id.trip_receiving_location_map);

        if (mMapFragment == null) {
            mMapFragment = SupportMapFragment.newInstance();
            fragmentManager.beginTransaction().replace(R.id.trip_receiving_location_map, mMapFragment).commit();
        }

        mMapFragment.getMapAsync(this);


        TextView costTV = (TextView) findViewById(R.id.trip_total_tv);
        TextView durationTv = (TextView) findViewById(R.id.trip_duration_tv);
        TextView distanceTv = (TextView) findViewById(R.id.trip_distance_tv);
        TextView detinationTv = (TextView) findViewById(R.id.trip_details_destination_tv);
        TextView sourceTv = (TextView) findViewById(R.id.trip_detail_source_tv);
        TextView costlabelTV = (TextView) findViewById(R.id.trip_total_label_tv);
        TextView durationlabelTv = (TextView) findViewById(R.id.trip_duration_label_tv);
        TextView distancelabelTv = (TextView) findViewById(R.id.trip_ditance_label_tv);

        costTV.setText("₹ " + cost);
        durationTv.setText(duration + " min");
        distanceTv.setText(distance + " km");
        sourceTv.setText(source);
        detinationTv.setText(destination);

        try {
            if (!source.equals("") || !source.equals("null") || !source.isEmpty()) {
                source_LatLong = GeocodingLocation.getLocationFromAddress(mainActivity, source);
            }
            if (!destination.equals("") || !destination.equals("null") || !destination.isEmpty()) {
                destination_LatLong = GeocodingLocation.getLocationFromAddress(mainActivity, destination);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }

        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
        costTV.setTypeface(face);
        durationTv.setTypeface(face);
        distanceTv.setTypeface(face);
        costlabelTV.setTypeface(face);
        durationlabelTv.setTypeface(face);
        distancelabelTv.setTypeface(face);
        sourceTv.setTypeface(face);
        detinationTv.setTypeface(face);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void addMarks1(GoogleMap googleMap, String pickup, String drop) {
        try {
            if (googleMap != null)
                googleMap.clear();
            if (destination_LatLong != null) {
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(source_LatLong);
                markerOptions.title("Pickup");
                markerOptions.snippet(pickup);
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.green_location));
                googleMap.addMarker(markerOptions);
                markerOptions.position(destination_LatLong);
                markerOptions.title("Drop");
                markerOptions.snippet(drop);
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.red_location));
                googleMap.addMarker(markerOptions);
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(source_LatLong, 17);
                googleMap.animateCamera(cameraUpdate);
                traceMe();

            } else {
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(source_LatLong);
                markerOptions.title("Pickup");
                markerOptions.snippet(pickup);
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.green_location));
                googleMap.addMarker(markerOptions);

                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(source_LatLong, 17);
                googleMap.animateCamera(cameraUpdate);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void traceMe() {
        String srcParam = "";
        String destParam = "";
        try {
            srcParam = source_LatLong.latitude + "," + source_LatLong.longitude;
            destParam = destination_LatLong.latitude + "," + destination_LatLong.longitude;
        } catch (Throwable e) {
            e.printStackTrace();
        }

        String url = "https://maps.googleapis.com/maps/api/directions/json?origin=" + srcParam + "&destination=" + destParam + "&sensor=false&units=metric&mode=driving&key=" + Constant.google_direction_purchage_key;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("dkp lat long", ":" + response);
                        MapDirectionsParser parser = new MapDirectionsParser(mainActivity);
                        List<List<HashMap<String, String>>> routes = parser.parse(response);

                        for (int i = 0; i < routes.size(); i++) {
                            points = new ArrayList<>();

                            List<HashMap<String, String>> path = routes.get(i);

                            Log.d("dkp lat long", ":" + path);
                            for (int j = 0; j < path.size(); j++) {
                                HashMap<String, String> point = path.get(j);
                                double lat = Double.parseDouble(point.get("lat"));
                                double lng = Double.parseDouble(point.get("lng"));
                                LatLng position = new LatLng(lat, lng);
                                points.add(position);
                            }
                        }
                        Log.d("dkp points lat long", ":" + points);
                        drawPoints(points, googleMap);

                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            Log.d("error ocurred", "TimeoutError");
                        }
                    }
                });
        MyApplication.getInstance().addToRequestQueue(jsonObjectRequest, "jreq");
    }

    private void drawPoints(ArrayList<LatLng> points, GoogleMap mMaps) {
        if (points == null) {
            return;
        }
        ArrayList<LatLng> traceOfMe = points;
        PolylineOptions polylineOpt = new PolylineOptions();
        for (LatLng latlng : traceOfMe) {
            polylineOpt.add(latlng);
        }
        polylineOpt.color(Color.BLACK);
        if (mPolyline != null) {
            mPolyline.remove();
            mPolyline = null;
        }
        if (mMaps != null) {
            mPolyline = mMaps.addPolyline(polylineOpt);
        } else {
        }
        if (mPolyline != null)
            mPolyline.setWidth(7);


    }


    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onCameraIdle() {

    }

    @Override
    public void onCameraMoveCanceled() {

    }

    @Override
    public void onCameraMove() {

    }

    @Override
    public void onCameraMoveStarted(int i) {

    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {

    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        try {
            this.googleMap = googleMap;
            googleMap.setOnCameraIdleListener(this);
            googleMap.setOnCameraMoveStartedListener(this);
            googleMap.setOnCameraMoveListener(this);
            googleMap.setOnCameraMoveCanceledListener(this);
            googleMap.getUiSettings().setZoomControlsEnabled(false);
            googleMap.getUiSettings().setMyLocationButtonEnabled(false);
            googleMap.getUiSettings().setCompassEnabled(true);
            googleMap.getUiSettings().setRotateGesturesEnabled(true);
            googleMap.getUiSettings().setZoomGesturesEnabled(true);

            if (ActivityCompat.checkSelfPermission(mainActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mainActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            googleMap.setMyLocationEnabled(true);

            if (myLocationButton != null && myLocationButton.getLayoutParams() instanceof RelativeLayout.LayoutParams) {
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) myLocationButton.getLayoutParams();
                params.addRule(RelativeLayout.CENTER_VERTICAL);
                params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 0);
                params.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
                final int margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10,
                        getResources().getDisplayMetrics());
                myLocationButton.setLayoutParams(params);
            }

            double latitude = new PrefManager(getApplicationContext()).getCurrentLat();
            double longitude = new PrefManager(getApplicationContext()).getCurrentLng();

            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(latitude, longitude)).zoom(10)
                    .build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            addMarks1(googleMap, source_address, detination_address);

            googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    marker.hideInfoWindow();
                    return true;
                }
            });
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
