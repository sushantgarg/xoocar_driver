package com.xoocardriver.view.fragment;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.xoocardriver.R;
import com.xoocardriver.helper.Constant;
import com.xoocardriver.model.Model;
import com.xoocardriver.model.Registration;
import com.xoocardriver.preference.PrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class OtpFragment extends AppCompatActivity {

    private Button verifyBtn;

    public static EditText otp1Edt, otp2Edt, otp3Edt, otp4Edt;
    private ImageView mbackIv;
    private String   mobile;
    private String otp1;
    private String otp2;
    private String otp3;
    private String otp4;
    private PrefManager prefManager;
    private TextView mResendOtpTv;
    private ProgressDialog progressDialog;
    private Context ctx;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_otp);
        ctx=OtpFragment.this;

        Intent i=getIntent();
        mobile = i.getStringExtra("mobile");

        verifyBtn = (Button) findViewById(R.id.verify_btn);
        otp1Edt = (EditText) findViewById(R.id.otp_h1_edt);
        otp2Edt = (EditText) findViewById(R.id.otp_h2_edt);
        otp3Edt = (EditText) findViewById(R.id.otp_h3_edt);
        otp4Edt = (EditText) findViewById(R.id.otp_h4_edt);

        TextView mheaderTv = (TextView) findViewById(R.id.otp_header_tv);
        TextView mheadingTv = (TextView) findViewById(R.id.otp_heading_tv);
        mResendOtpTv = (TextView) findViewById(R.id.otp_resend_otp_tv);
        prefManager = new PrefManager(ctx);

        mbackIv = (ImageView) findViewById(R.id.otp_back_iv);

        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
        verifyBtn.setTypeface(face);
        otp1Edt.setTypeface(face);
        otp2Edt.setTypeface(face);
        otp3Edt.setTypeface(face);
        otp4Edt.setTypeface(face);
        mheaderTv.setTypeface(face);
        mheadingTv.setTypeface(face);
        mResendOtpTv.setTypeface(face);

        listenerView();
        otp1Edt.requestFocus();
        edit_Text_Focus_Listner();
    }

    private void listenerView() {
        verifyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTextFromEditText();
                String otp = otp1 + otp2 + otp3 + otp4;
                String pref_otp = prefManager.getOtp();
                Log.d("otp", ":" + otp);
                Log.d("pref_otp", ":" + pref_otp);
                if (otp.equals(pref_otp)) {
                    Toast.makeText(ctx, "OTP successfully verified", Toast.LENGTH_LONG).show();
                    try {
                        progressDialog = new ProgressDialog(ctx);
                        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
                        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                        progressDialog.show();
                        progressDialog.setContentView(R.layout.progressbar);
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                    LoginRequest(Constant.LOGIN_POST_REQUEST_URL, "", mobile);
                } else {
                    Toast.makeText(ctx, "Otp is not verify successfully", Toast.LENGTH_LONG).show();
                }
            }
        });

        mResendOtpTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    progressDialog = new ProgressDialog(ctx);
                    progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
                    progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                    progressDialog.show();
                    progressDialog.setContentView(R.layout.progressbar);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                otpRequest( Constant.NEW_CHECK_OTP_POST_REQUEST_URL, mobile);
            }
        });

        mbackIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Intent i=new Intent(OtpFragment.this,SignIn.class);
                startActivity(i);
            }
        });
    }

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(ctx).registerReceiver(receiver, new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(ctx).unregisterReceiver(receiver);
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
                final String message = intent.getStringExtra("otp");
                Log.d("message1", "message1 " + message);

                Log.d("otp", ":tttttttttt" + message);
                char arr[] = message.toCharArray();

                otp1Edt.setText("" + arr[0]);
                otp2Edt.setText("" + arr[1]);
                otp3Edt.setText("" + arr[2]);
                otp4Edt.setText("" + arr[3]);

                getTextFromEditText();
                String otp = otp1 + otp2 + otp3 + otp4;
                String pref_otp = prefManager.getOtp();
                Log.d("otp", ":" + otp);
                Log.d("pref_otp", ":" + pref_otp);
                if (otp.equals(pref_otp)) {
                    Toast.makeText(ctx, "OTP successfully verified", Toast.LENGTH_LONG).show();
                    try {
                        progressDialog = new ProgressDialog(ctx);
                        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
                        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                        progressDialog.show();
                        progressDialog.setContentView(R.layout.progressbar);
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                    LoginRequest(Constant.LOGIN_POST_REQUEST_URL, "", mobile);
                } else {
                    Toast.makeText(ctx, "Otp is not verify successfully", Toast.LENGTH_LONG).show();
                }
            }
        }
    };

    public void getTextFromEditText() {
        otp1 = otp1Edt.getText().toString();
        otp2 = otp2Edt.getText().toString();
        otp3 = otp3Edt.getText().toString();
        otp4 = otp4Edt.getText().toString();
    }

    public void LoginRequest(String url, final String pass, final String mobile) {
        RequestQueue rq = Volley.newRequestQueue(ctx);
        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("login response ", "= " + response);
                progressDialog.dismiss();
                try {
                    JSONArray jsonArray = Model.getArray(response);
                    Registration reg = new Registration();
                    assert jsonArray != null;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = Model.getObject(jsonArray, i);
                        assert jsonObject != null;
                        if (jsonObject.has("message")) {
                            String status = jsonObject.getString("message");
                            if (status.equals("Invalid")) {
                                Toast.makeText(ctx, "Driver Not Approved", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            // Log.d("login ", "= " + jsonObject.toString());
                            reg.setFirstName(Model.getString(jsonObject, "first_name"));
                            reg.setLastName(Model.getString(jsonObject, "last_name"));
                            reg.setUid(Model.getString(jsonObject, "uid"));
                            reg.setUserEmail(Model.getString(jsonObject, "user_email"));
                            reg.setAddress(Model.getString(jsonObject,"address"));
                            reg.setAge(Model.getString(jsonObject, "age"));
                            reg.setUserType(Model.getString(jsonObject, "user_type"));
                            reg.setContact(Model.getString(jsonObject, "contact"));
                            reg.setDevicetoken(Model.getString(jsonObject, "devicetoken"));
                            reg.setRegPassword(Model.getString(jsonObject, "reg_password"));
                            reg.setProfilePic(Model.getString(jsonObject, "profile_pic"));
                            reg.setOId(Model.getString(jsonObject, "o_id"));
                            reg.setDevicetype(Model.getString(jsonObject, "devicetype"));
                            String f_name = Model.getString(jsonObject, "first_name");
                            String l_name = Model.getString(jsonObject, "last_name");
                            String fullname = "";
                            if (f_name != null && l_name != null) {
                                fullname = f_name + l_name;
                            } else if (l_name != null) {
                                fullname = l_name;
                            }
                            prefManager.setKeyUserName(fullname);
                            prefManager.setKeyUserEmail(reg.getUserEmail());
                            prefManager.setKeyUserId(Model.getString(jsonObject, "uid"));
                            prefManager.setKeyUserContact(reg.getContact());
                            prefManager.setKeyUserPassword(reg.getRegPassword());
                            prefManager.setOid(Model.getString(jsonObject, "o_id"));
                            prefManager.setCity(Model.getString(jsonObject, "city"));

                            Intent intent=new Intent(OtpFragment.this,Home.class);
                            startActivity(intent);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                VolleyLog.d("Error", "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.d("error ocurred", "TimeoutError");
                    // Toast.makeText(context, "TimeoutError", Toast.LENGTH_LONG).show();
                }
            }
        }) {
            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                }
                return volleyError;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                String fcm_reg_id = prefManager.getFcmToken();
                Log.d("fcm_reg_id dkkkkkkkk", ":" + fcm_reg_id);
                String[] tag = {"contact", "password", "dtype", "dtoken", "usertype"};
                String[] value = {mobile, pass, "Android", fcm_reg_id, "3"};
                for (int i = 0; i < tag.length; i++)
                    params.put(tag[i], value[i]);
                return checkParams(params);
            }
        };
        postReq.setRetryPolicy(new DefaultRetryPolicy(
                7000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(postReq);
    }

    public void otpRequest( String url, final String mobile) {
        Log.d("registration:", " url = " + url + "mobile = " + mobile);

        RequestQueue rq = Volley.newRequestQueue(ctx);
        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.d("otp response ", "= " + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.has("otp")) {
                        String otp = jsonObject.getString("otp");
                        prefManager.setOtp(otp);
                        Toast.makeText(ctx, "Resend otp successfully", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(ctx, "Server Not Responding", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                VolleyLog.d("Error", "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.d("error ocurred", "TimeoutError");
                    // Toast.makeText(context, "TimeoutError", Toast.LENGTH_LONG).show();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                Log.d(" dkp mobile", ":" + mobile);
                String[] tag = {"contactno"};
                String[] value = {mobile};

                for (int i = 0; i < tag.length; i++)
                    params.put(tag[i], value[i]);

                return checkParams(params);
            }
        };
        postReq.setRetryPolicy(new DefaultRetryPolicy(
                8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(postReq);
    }

    private Map<String, String> checkParams(Map<String, String> map) {
        for (Map.Entry<String, String> pairs : map.entrySet()) {
            if (pairs.getValue() == null) {
                map.put(pairs.getKey(), "");
            }
        }
        return map;
    }

    public void edit_Text_Focus_Listner() {
        otp1Edt.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (otp1Edt.getText().toString().length() == 1) {
                    otp2Edt.requestFocus();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
        });

        otp2Edt.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (otp2Edt.getText().toString().length() == 1) {
                    otp3Edt.requestFocus();
                } else if (otp2Edt.getText().toString().length() == 0) {
                    otp1Edt.requestFocus();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
        });

        otp3Edt.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (otp3Edt.getText().toString().length() == 1) {
                    otp4Edt.requestFocus();
                } else if (otp3Edt.getText().toString().length() == 0) {
                    otp2Edt.requestFocus();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
        });


        otp4Edt.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (otp4Edt.getText().toString().length() == 0) {
                    otp3Edt.requestFocus();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

        });
    }
}