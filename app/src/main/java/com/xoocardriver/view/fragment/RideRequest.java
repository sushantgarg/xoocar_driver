package com.xoocardriver.view.fragment;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.xoocardriver.FireBase.FirebaseManager;
import com.xoocardriver.R;
import com.xoocardriver.RetroFit.AcceptRideResponce;
import com.xoocardriver.RetroFit.AcceptRideResponceFields;
import com.xoocardriver.RetroFit.RejectRideResponce;
import com.xoocardriver.RetroFit.RequestDataAcceptRequest;
import com.xoocardriver.RetroFit.RequestFieldsAcceptRide;
import com.xoocardriver.RetroFit.RequestInterface;
import com.xoocardriver.helper.Constant;
import com.xoocardriver.helper.MyApplication;
import com.xoocardriver.map.GeocodingLocation;
import com.xoocardriver.map.MapDirectionsParser;
import com.xoocardriver.preference.PrefManager;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by elite on 20/12/16.
 */

public class RideRequest extends AppCompatActivity implements  OnMapReadyCallback, View.OnClickListener {
    private MediaPlayer mPlayer;
    private LinearLayout accept, reject;
    private GoogleMap googleMap;
    private ArrayList<LatLng> points = null;
    private Polyline mPolyline = null;
    private LatLng source_LatLong;
    private LatLng destination_LatLong;
    private String source_address;
    private String detination_address;

    private LatLng driver_LatLong;
    private ProgressDialog progressDialog;
    private PrefManager prefManager;
    private TextView timerTv;
    private LinearLayout timerLayout;
    private CountDownTimer timercontDown;
    private Vibrator v;
    private Context ctx;

    private Dialog dialog;
    private String bookingType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_ride_request);
        ctx=RideRequest.this;

        FragmentManager fragmentManager = getSupportFragmentManager();

        SupportMapFragment mMapFragment = (SupportMapFragment) fragmentManager.findFragmentById(R.id.location_map);

        if ( mMapFragment == null ) {
            mMapFragment = SupportMapFragment.newInstance();
            fragmentManager.beginTransaction().replace(R.id.location_map, mMapFragment).commit();
        }

        mMapFragment.getMapAsync(this);

        Intent i=getIntent();
        source_address=i.getStringExtra("c_source");
        detination_address=i.getStringExtra("c_destination");
        String cLat = i.getStringExtra("cLat");
        String cLng = i.getStringExtra("cLng");
        String dLat = i.getStringExtra("dLat");
        String dLng = i.getStringExtra("dLng");
        String estimatedCost = i.getStringExtra("est");
        bookingType=i.getStringExtra("type");
        String dateOfTravel = i.getStringExtra("date");

        accept = (LinearLayout) findViewById(R.id.accept);
        reject = (LinearLayout) findViewById(R.id.reject);
        LinearLayout intercityLayout = (LinearLayout) findViewById(R.id.intercityLayout);
        TextView pick_up_text = (TextView) findViewById(R.id.ride_request_source_h_tv);
        TextView pickupTv = (TextView) findViewById(R.id.ride_request_source_tv);
        TextView drop_up_text = (TextView) findViewById(R.id.ride_request_destination_h_tv);
        TextView dropTv = (TextView) findViewById(R.id.ride_request_destination_tv);
        TextView acceptTv = (TextView) findViewById(R.id.ride_request_accept_tv);
        TextView rejectTv = (TextView) findViewById(R.id.ride_request_reject_tv);
        timerTv = (TextView) findViewById(R.id.ride_request_timer_tv);

        TextView estCost = (TextView) findViewById(R.id.estText);
        TextView dateText = (TextView) findViewById(R.id.dateText);
        TextView typeText = (TextView) findViewById(R.id.typeText);

        timerLayout = (LinearLayout) findViewById(R.id.ride_request_timer_ll);
        v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);


        prefManager = new PrefManager(ctx);

        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
        pick_up_text.setTypeface(face);
        pickupTv.setTypeface(face);
        drop_up_text.setTypeface(face);
        dropTv.setTypeface(face);
        acceptTv.setTypeface(face);
        rejectTv.setTypeface(face);

        pickupTv.setText(source_address);
        dropTv.setText(detination_address);

        source_LatLong = new LatLng(Double.parseDouble(cLat),Double.parseDouble(cLng));
        destination_LatLong = new LatLng(Double.parseDouble(dLat),Double.parseDouble(dLng));

        if( timercontDown != null )
            timercontDown.cancel();

        if( bookingType.equals("1") ) {
            intercityLayout.setVisibility(View.VISIBLE);
            estCost.setText(estimatedCost);
            dateText.setText(dateOfTravel);
            typeText.setText("Intercity");
        }

        timeShow();
        stopMusic();
        startMusic();

        listenerView();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void timeShow() {
        try {
            timercontDown = new CountDownTimer(40000, 1000) {

                public void onTick(long millisUntilFinished) {
                    String time = "" + millisUntilFinished / 1000;
                    timerTv.setText("" + millisUntilFinished / 1000);
                    if (time.equals("15")) {
                        timerLayout.setBackgroundResource(R.drawable.solid_circle_ouline_border_offline);
                        timerTv.setTextColor(Color.parseColor("#ffffff"));
                    }
                }

                public void onFinish() {
                    timerTv.setText("0");
                    stopMusic();

                    if(bookingType.equals("1")){

                        rejectRide( prefManager.getRideIdCheck());

                        prefManager.setLastCancelledRideId(prefManager.getRideIdCheck());
                        prefManager.setRideIdCheck("0");

                        Intent i=new Intent(RideRequest.this,OfflineOnline.class);
                        startActivity(i);

                    } else {

                        prefManager.setLastCancelledRideId(prefManager.getRideIdCheck());
                        prefManager.setRideIdCheck("0");

                        Intent i=new Intent(RideRequest.this,OfflineOnline.class);
                        startActivity(i);
                    }


                }
            }.start();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void listenerView() {
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                progressDialog = new ProgressDialog(ctx);
                progressDialog.setMessage("loading");
                progressDialog.show();
                acceptRequest( prefManager.getRideIdCheck());
                stopMusic();
                //  timer="1";
                timercontDown.cancel();

            }
        });

        reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopMusic();
                timercontDown.cancel();
                showStopCastingDialog();
            }
        });

        timerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ride_id = prefManager.getRideIdCheck();
                try {
                    progressDialog = new ProgressDialog(ctx);
                    progressDialog.setMessage("loading");
                    progressDialog.show();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                stopMusic();
                //  timer="1";
                timercontDown.cancel();
                acceptRequest( ride_id);
            }
        });
    }

    public void startMusic() {
        if( mPlayer == null ) {
            if(bookingType.equals("1")){
                mPlayer = MediaPlayer.create(ctx, R.raw.songs);
            }else{
                mPlayer = MediaPlayer.create(ctx, R.raw.songs);
            }
            mPlayer.setLooping(true); // Set looping
            mPlayer.setVolume(100, 100);
            mPlayer.start();
            Constant.flag = "1";

            long[] pattern = {0, 100, 1000};

// The '0' here means to repeat indefinitely
// '0' is actually the index at which the pattern keeps repeating from (the start)
// To repeat the pattern from any other point, you could increase the index, e.g. '1'
            v.vibrate(pattern, 0);

        }else{
            stopMusic();
        }
    }

    public void stopMusic() {
        if (mPlayer != null) {
            if (mPlayer.isPlaying()) {
                mPlayer.stop();
                mPlayer.release();
                prefManager.setCheckBookingAlarm(null);
                mPlayer = null;
                Constant.flag = "0";

                if(v != null && v.hasVibrator()) {
                    v.cancel();
                }
            }
        }
    }


    private void addMarks1( GoogleMap googleMap, String pickup, String drop ) {
        try {
            googleMap.clear();
            driver_LatLong = new LatLng(prefManager.getCurrentLat(), prefManager.getCurrentLng());
            String driver = GeocodingLocation.getCompleteAddressString(ctx, driver_LatLong.latitude, driver_LatLong.longitude);

            if (destination_LatLong != null) {
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(source_LatLong);
                markerOptions.title("Pickup");
                markerOptions.snippet(pickup);
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.green_location));
                googleMap.addMarker(markerOptions);

                markerOptions.position(driver_LatLong);
                markerOptions.title("Driver");
                markerOptions.snippet(driver);
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.driver_black));
                googleMap.addMarker(markerOptions);

                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(source_LatLong, 17);
                googleMap.animateCamera(cameraUpdate);
                traceMe();

            } else {
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(source_LatLong);
                markerOptions.title("Pickup");
                markerOptions.snippet(pickup);
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.green_location));
                googleMap.addMarker(markerOptions);

                markerOptions.position(driver_LatLong);
                markerOptions.title("Driver");
                markerOptions.snippet(driver);
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.driver_black));
                googleMap.addMarker(markerOptions);

                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(source_LatLong, 17);
                googleMap.animateCamera(cameraUpdate);
                traceMe();
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        //super.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    public void rejectRide( final String ride_id) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://www.xoocar.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);

        Call<RejectRideResponce> call = request.rejectRide("api/processapi/cancelbeforeschedule/format/json/"
                , new RequestFieldsAcceptRide(new RequestDataAcceptRequest(ride_id,prefManager.getKeyUserId())));

        call.enqueue(new Callback<RejectRideResponce>() {
            @Override
            public void onResponse(Call<RejectRideResponce> call, retrofit2.Response<RejectRideResponce> response) {

                if( response.body() != null ) {

                    if( dialog != null && dialog.isShowing() ) {
                        dialog.dismiss();
                        finish();
                    }
                }
            }

            @Override
            public void onFailure(Call<RejectRideResponce> call, Throwable t) {
            }
        });
    }

    public void acceptRequest( final String ride_id) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);

        Call<AcceptRideResponce> call = request.acceptRide("api/processapi/confirmbydrivernew/format/json/"
                , new RequestFieldsAcceptRide(new RequestDataAcceptRequest(ride_id,prefManager.getKeyUserId())));

        call.enqueue(new Callback<AcceptRideResponce>() {
            @Override
            public void onResponse(Call<AcceptRideResponce> call, retrofit2.Response<AcceptRideResponce> response) {

                if( response.body() != null ) {
                   AcceptRideResponceFields responce=response.body().getDataArray();

                    prefManager.setRideId(responce.getRideid());
                    if(progressDialog != null && progressDialog.isShowing()){
                        progressDialog.dismiss();
                    }
                    FirebaseManager.getInstance().updateStatus(prefManager.getKeyUserId(),2);

                    Intent i=new Intent(ctx,PickUpReceiving.class);

                    i.putExtra("rideid",responce.getRideid());
                    i.putExtra("c_id",responce.getcId());
                    i.putExtra("c_first_name",responce.getcFirstName());
                    i.putExtra("c_destination",responce.getDestination());
                    i.putExtra("c_profile_pic",responce.getcProfilepic());
                    i.putExtra("d_latt", responce.getdLatt());
                    i.putExtra("d_long",responce.getdLong());
                    i.putExtra("city_id",prefManager.getCity());
                    i.putExtra("contact",responce.getcContact());
                    i.putExtra("first_name",responce.getFirstName());
                    i.putExtra("c_source",responce.getSource());
                    i.putExtra("last_name","");
                    i.putExtra("c_lat",responce.getcLatt());
                    i.putExtra("c_lng",responce.getcLong());
                    i.putExtra("c_dest_lat",responce.getcDestLat());
                    i.putExtra("c_dest_lng",responce.getcDestLng());
                    startActivity(i);

                } else {
                    Intent i=new Intent(ctx,CancelRideFragment.class);
                    i.putExtra("type", "other");
                    startActivity(i);
                }
            }

            @Override
            public void onFailure(Call<AcceptRideResponce> call, Throwable t) {
            }
        });
    }

    private void traceMe() {
        String srcParam;
        String destParam;
        srcParam = source_LatLong.latitude + "," + source_LatLong.longitude;
        destParam = driver_LatLong.latitude + "," + driver_LatLong.longitude;

        String url = "https://maps.googleapis.com/maps/api/directions/json?origin=" + srcParam + "&destination=" + destParam + "&sensor=false&units=metric&mode=driving&key=" + Constant.google_direction_purchage_key;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("dkp lat long", ":" + response);
                        MapDirectionsParser parser = new MapDirectionsParser(ctx);
                        List<List<HashMap<String, String>>> routes = parser.parse(response);
                        for (int i = 0; i < routes.size(); i++) {
                            points = new ArrayList<LatLng>();
                            // lineOptions = new PolylineOptions();
                            // Fetching i-th route
                            List<HashMap<String, String>> path = routes.get(i);
                            // Fetching all the points in i-th route
                            Log.d("dkp lat long", ":" + path);
                            for (int j = 0; j < path.size(); j++) {
                                HashMap<String, String> point = path.get(j);
                                double lat = Double.parseDouble(point.get("lat"));
                                double lng = Double.parseDouble(point.get("lng"));
                                LatLng position = new LatLng(lat, lng);
                                points.add(position);
                            }
                        }
                        Log.d("dkp points lat long", ":" + points);
                        drawPoints(points, googleMap);
                        //  PD.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            Log.d("error ocurred", "TimeoutError");
                           // Toast.makeText(getActivity(), "TimeoutError", Toast.LENGTH_LONG).show();
                        }
                    }
                });
        MyApplication.getInstance().addToRequestQueue(jsonObjectRequest, "jreq");
    }

    private void drawPoints(ArrayList<LatLng> points, GoogleMap mMaps) {
        if (points == null) {
            return;
        }
        PolylineOptions polylineOpt = new PolylineOptions();
        for (LatLng latlng : points) {
            polylineOpt.add(latlng);
        }
        polylineOpt.color(Color.BLACK);
        if (mPolyline != null) {
            mPolyline.remove();
            mPolyline = null;
        }
        if (mMaps != null) {
            mPolyline = mMaps.addPolyline(polylineOpt);
        }
        if (mPolyline != null)
            mPolyline.setWidth(7);
        //mFooterLayout.setVisibility(View.GONE);
    }


    @Override
    public void onPause() {
        super.onPause();
        // mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        stopMusic();
//        communicator.communicateToMain("open");

        if(timercontDown != null )
            timercontDown.cancel();

        super.onDestroy();
        // mMapView.onDestroy();
    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
        // mMapView.onLowMemory();
    }

    @Override
    public void onClick(View v) {
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        Log.d("onMapReady", ":" + "onMapReady");
        try {
            this.googleMap = googleMap;

            listenerView();
            googleMap.getUiSettings().setZoomControlsEnabled(false);
            googleMap.getUiSettings().setMyLocationButtonEnabled(false);
            googleMap.getUiSettings().setCompassEnabled(true);
            googleMap.getUiSettings().setRotateGesturesEnabled(true);
            googleMap.getUiSettings().setZoomGesturesEnabled(true);

            if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_FINE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ctx
                    , Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            googleMap.setMyLocationEnabled(true);

            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(prefManager.getCurrentLat(), prefManager.getCurrentLng())).zoom(17)
                    .build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            addMarks1(googleMap, source_address, detination_address);
            googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    marker.hideInfoWindow();
                    return true;
                }
            });
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void showStopCastingDialog() {

        dialog = new Dialog(ctx);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.custom_cancel_reson);

        final RadioButton rb=((RadioButton) dialog.findViewById(R.id.cancelReasonCb));

        dialog.findViewById(R.id.book_confirm_custom_cancel_tv).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if ( rb.isChecked() ) {
//                            Todo :call cancel ride
                            if( ! bookingType.equals("1") ){
                                rejectRide(prefManager.getRideIdCheck());
                            }
                            prefManager.setRideIdCheck("0");
                            Intent i=new Intent(ctx,OfflineOnline.class);
                            startActivity(i);

                        } else {
                            Toast.makeText(ctx, "Check reason first", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

        dialog.show();
    }

    @Override
    public void onBackPressed() {

    }
}
