package com.xoocardriver.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xoocardriver.R;

public class CollectCash extends AppCompatActivity {
    private Button done;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_collect_cash);
        Context ctx = CollectCash.this;

        Intent i=getIntent();
        String total = i.getStringExtra("total");
        String check_payment = i.getStringExtra("check_payment");

        TextView amount1 = (TextView) findViewById(R.id.amount);
        TextView total_amount = (TextView) findViewById(R.id.total_amount);
        done = (Button) findViewById(R.id.done);
        LinearLayout collect_hole_ll = (LinearLayout) findViewById(R.id.collect_hole_ll);
        TextView cash_label_tv = (TextView) findViewById(R.id.collect_xoocar_wallet_label_tv);
        TextView charge_label_tv = (TextView) findViewById(R.id.collect_promo_wallet_label_tv);
        TextView cash_tv = (TextView) findViewById(R.id.collect_xoocar_wallet_tv);
        TextView charge_tv = (TextView) findViewById(R.id.collect_promo_wallet_tv);
        TextView textViewCashMessage = (TextView) findViewById(R.id.textView2);

        Typeface normal_fonts = Typeface.createFromAsset(ctx.getAssets(), "fonts/OpenSans-Regular.ttf");
        Typeface bold_fonts = Typeface.createFromAsset(ctx.getAssets(), "fonts/OpenSans-Bold.ttf");

        amount1.setTypeface(normal_fonts);
        total_amount.setTypeface(normal_fonts);
        done.setTypeface(normal_fonts);
        cash_label_tv.setTypeface(bold_fonts);
        charge_label_tv.setTypeface(bold_fonts);
        cash_tv.setTypeface(bold_fonts);
        charge_tv.setTypeface(bold_fonts);

        Log.d("dkp", "check_payment:" + check_payment);

        switch (check_payment) {
            case "cash": {
//              done.setVisibility(View.GONE);
                textViewCashMessage.setVisibility(View.VISIBLE);
                amount1.setText("Collect Cash");
                amount1.setTextColor(Color.parseColor("#f15549"));
                String amount = getResources().getString(R.string.rupees_symbol) + total;
                total_amount.setText(amount);
                total_amount.setVisibility(View.VISIBLE);
                collect_hole_ll.setVisibility(View.GONE);
                break;
            }
            case "wallet": {
                //by wallet payment
//                done.setVisibility(View.VISIBLE);
                textViewCashMessage.setVisibility(View.GONE);
                amount1.setText("Payment paid successfully");
                amount1.setTextColor(Color.parseColor("#ff0A8815"));
                total_amount.setText("0");
                String amount = getResources().getString(R.string.rupees_symbol) + total;
                charge_tv.setText(amount);
                total_amount.setVisibility(View.GONE);
                collect_hole_ll.setVisibility(View.VISIBLE);
                break;
            }   case "Wallet": {
                //by wallet payment
//                done.setVisibility(View.VISIBLE);
                textViewCashMessage.setVisibility(View.GONE);
                amount1.setText("Payment paid successfully");
                amount1.setTextColor(Color.parseColor("#ff0A8815"));
                total_amount.setText("0");
                String amount = getResources().getString(R.string.rupees_symbol) + total;
                charge_tv.setText(amount);
                total_amount.setVisibility(View.GONE);
                collect_hole_ll.setVisibility(View.VISIBLE);
                break;
            }

            case "1": {
                //by cash payment
//                done.setVisibility(View.VISIBLE);
                textViewCashMessage.setVisibility(View.VISIBLE);
                amount1.setText("Payment receive in Cash");
                amount1.setTextColor(Color.parseColor("#f15549"));
                String amount = getResources().getString(R.string.rupees_symbol) + total;
                total_amount.setText(amount);
                total_amount.setVisibility(View.VISIBLE);
                collect_hole_ll.setVisibility(View.GONE);
                break;
            }
        }

        listenerView();
    }

    public void listenerView() {
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i=new Intent(CollectCash.this,Rating.class);
                startActivity(i);
            }
        });
    }

    @Override
    public void onBackPressed() {

    }
}