package com.xoocardriver.view.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.xoocardriver.R;
import com.xoocardriver.adapter.TripListAdapter;
import com.xoocardriver.helper.Constant;
import com.xoocardriver.helper.RecyclerItemClickListener;
import com.xoocardriver.model.Model;
import com.xoocardriver.model.Notification;
import com.xoocardriver.preference.PrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TripList extends AppCompatActivity {

    private RecyclerView recyclerView;
    private TripListAdapter triplistAdapter;
    private PrefManager prefManager;
    private ProgressDialog progressDialog;
    private ArrayList<com.xoocardriver.model.TripList> list_arr;
    private TextView not_found_tv;
    private String c_contact;
    private Handler handler;
    private int start_limit;
    private ProgressBar progressBar;
    private Context ctx;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_trip_list);
        ctx=TripList.this;
        prefManager = new PrefManager(ctx);

        not_found_tv = (TextView)     findViewById(R.id.trip_list_not_found_tv);
        progressBar = (ProgressBar)   findViewById(R.id.progressBar);


        recyclerView = (RecyclerView) findViewById(R.id.trip_list_rl2);
        recyclerView.setLayoutManager(new LinearLayoutManager(ctx));

        Typeface face = Typeface.createFromAsset( getAssets(), "fonts/OpenSans-Regular.ttf");
        not_found_tv.setTypeface(face);

        list_arr = new ArrayList<>();
        String driver_id = prefManager.getKeyUserId();
        getTripList(Constant.TRIP_POST_REQUEST_URL, driver_id, "" + start_limit);
        listenerView();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void listenerView() {
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(ctx, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        String booking_status = list_arr.get(position).getBookingStatus();
                        switch (booking_status) {
                            case "Running":
                                try {
                                    progressDialog = new ProgressDialog(ctx);
                                    progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
                                    progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                                    progressDialog.show();
                                    progressDialog.setContentView(R.layout.progressbar);
                                } catch (NullPointerException e) {
                                    e.printStackTrace();
                                }
                                prefManager.setRideId(list_arr.get(position).getOid());
                                parrallelstartApi(ctx, Constant.START_PARALLELPOST_REQUEST_URL, list_arr.get(position).getOid());
                                break;

                            case "Completed":
                            case "Cancelled":

                                        Intent i=new Intent(TripList.this,TripDetails.class);
                                        i.putExtra("cost",list_arr.get(position).getActualCost());
                                        i.putExtra("duration",list_arr.get(position).getActualTime());
                                        i.putExtra("distance",list_arr.get(position).getActualKms());
                                        i.putExtra("source",list_arr.get(position).getSource());
                                        i.putExtra("destination",list_arr.get(position).getDestination());
                                        startActivity(i);

                                break;

                            case "Schedule": {
                                try {
                                    progressDialog = new ProgressDialog(ctx);
                                    progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
                                    progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                                    progressDialog.show();
                                    progressDialog.setContentView(R.layout.progressbar);
                                } catch (NullPointerException e) {
                                    e.printStackTrace();
                                }
                                prefManager.setRideId(list_arr.get(position).getOid());

                                parrallelpickupApi(ctx, Constant.PICK_PARALLELPOST_REQUEST_URL, list_arr.get(position).getOid());
                                break;
                            }
                            case "Pending": {
                                parrallelstartApi(ctx, Constant.START_PARALLELPOST_REQUEST_URL, list_arr.get(position).getOid());
                                break;
                            }
                        }
                    }
                })
        );
    }


    public void parrallelstartApi(Context context, String url, final String rideid) {
        Log.d("p start flow:", " url = " + url + ": rideid :" + rideid);
        RequestQueue rq = Volley.newRequestQueue(context);
        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("p start  response ", "= " + response);
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String c_first_name = jsonObject.getString("c_first_name");
                    String c_source = jsonObject.getString("source");
                    String c_destination = jsonObject.getString("destination");
                    String c_id = jsonObject.getString("c_id");
                    String d_latt = jsonObject.getString("d_latt");
                    String d_long = jsonObject.getString("d_long");
                    String city_id = jsonObject.getString("city");
                    String rideid = jsonObject.getString("rideid");
                    String booking_status = jsonObject.getString("booking_status");
                    String c_destLat=jsonObject.getString("c_dest_lat");
                    String c_destLng=jsonObject.getString("c_dest_lng");
                    String cLat=jsonObject.getString("c_latt");
                    String cLng=jsonObject.getString("c_long");



                    Notification fcmNitification=new Notification();
                    fcmNitification.setRideId(rideid);
                    fcmNitification.setSource(c_source);
                    fcmNitification.setDetination(c_destination);

                    prefManager.saveRideDetailsJson(new Gson().toJson(fcmNitification));


                    switch (booking_status) {
                        case "Pending":
//                        call timer
                            prefManager.setRideIdCheck(rideid);

                            String bookingType = jsonObject.optString("ridetype");
                            String estimate = jsonObject.optString("estimated_cost");
                            String date = jsonObject.optString("creat");

                            Intent i=new Intent(TripList.this,RideRequest.class);
                            i.putExtra("c_source",c_source);
                            i.putExtra("c_destination",c_destination);
                            i.putExtra("cLat",cLat);
                            i.putExtra("cLng",cLng);
                            i.putExtra("dLat",c_destLat);
                            i.putExtra("dLng",c_destLng);
                            i.putExtra("type",bookingType);
                            i.putExtra("est",estimate);
                            i.putExtra("date",date);
                            startActivity(i);

                            break;

                        case "Schedule":
//                        call pickup fragment

                            String first_name = jsonObject.getString("first_name");
                            String last_name = jsonObject.getString("last_name");
                            String profile_pic = jsonObject.getString("c_profilepic");
                            c_contact = jsonObject.getString("c_contact");

                            prefManager.setRideIdCheck(rideid);

                            Intent i2=new Intent(TripList.this,PickUpReceiving.class);

                            i2.putExtra("rideid",rideid);
                            i2.putExtra("c_id",c_id);
                            i2.putExtra("c_first_name",c_first_name);
                            i2.putExtra("c_destination",c_destination);
                            i2.putExtra("c_profile_pic",profile_pic);
                            i2.putExtra("d_latt",d_latt);
                            i2.putExtra("d_long",d_long);
                            i2.putExtra("city_id",city_id);
                            i2.putExtra("contact",c_contact);
                            i2.putExtra("first_name",first_name);
                            i2.putExtra("c_source",c_source);
                            i2.putExtra("last_name",last_name);
                            i2.putExtra("c_lat",cLat);
                            i2.putExtra("c_lng",cLng);
                            i2.putExtra("c_dest_lat",c_destLat);
                            i2.putExtra("c_dest_lng",c_destLng);
                            startActivity(i2);

                            break;

                        case "Running":
                            prefManager.setRideIdCheck(rideid);

                            Intent i1=new Intent(TripList.this,CompleteTrip.class);

                            i1.putExtra("rideid",rideid);
                            i1.putExtra("c_first_name",c_first_name);
                            i1.putExtra("c_source",c_source);
                            i1.putExtra("c_destination",c_destination);
                            i1.putExtra("c_lat",cLat);
                            i1.putExtra("c_lng",cLng);
                            i1.putExtra("c_dest_lat",c_destLat);
                            i1.putExtra("c_dest_lng",c_destLng);
                            startActivity(i1);

                            break;

                        case "Cancelled":
                                Intent intent=new Intent(TripList.this,CancelRideFragment.class);
                            intent.putExtra("type", "cancel");
                                startActivity(intent);
                            break;

                        default:
                            prefManager.setRideIdCheck("0");
                            Intent intent2=new Intent(TripList.this,OfflineOnline.class);
                            startActivity(intent2);
                            break;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.e("dkp error ocurred", "Error");
                VolleyLog.d("Error", "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("error ocurred", "TimeoutError");
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                String[] tag = { "rideid", "d_id" };
                String[] value = { rideid, prefManager.getKeyUserId() };
                for ( int i = 0; i < tag.length; i++ )
                    params.put(tag[i], value[i]);
                return checkParams(params);
            }
        };

        postReq.setRetryPolicy(new DefaultRetryPolicy(
                800000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(postReq);
    }

    public void parrallelpickupApi(Context context, String url, final String rideid) {
        Log.d("p pickup flow:", " url = " + url + ": rideid :" + rideid);
        RequestQueue rq = Volley.newRequestQueue(context);
        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("p pickup  response ", "= " + response);
                progressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String c_first_name = jsonObject.getString("c_first_name");
                    String first_name = Model.getString(jsonObject, "first_name");
                    String last_name = Model.getString(jsonObject, "last_name");
                    String c_source = jsonObject.getString("source");
                    String c_destination = jsonObject.getString("destination");
                    String c_id = jsonObject.getString("c_id");
                    String d_latt = jsonObject.getString("d_latt");
                    String d_long = jsonObject.getString("d_long");
                    String city_id = jsonObject.getString("city");
                    String rideid = jsonObject.getString("rideid");
                    String profile_pic = jsonObject.getString("c_profilepic");

                    String c_destLat=jsonObject.getString("c_dest_lat");
                    String c_destLng=jsonObject.getString("c_dest_lng");
                    String cLat=jsonObject.getString("c_latt");
                    String cLng=jsonObject.getString("c_long");
                    c_contact = Model.getString(jsonObject, "c_contact");


                    Intent i=new Intent(TripList.this,PickUpReceiving.class);

                    i.putExtra("rideid",rideid);
                    i.putExtra("c_id",c_id);
                    i.putExtra("c_first_name",c_first_name);
                    i.putExtra("c_destination",c_destination);
                    i.putExtra("c_profile_pic",profile_pic);
                    i.putExtra("d_latt",d_latt);
                    i.putExtra("d_long",d_long);
                    i.putExtra("city_id",city_id);
                    i.putExtra("contact",c_contact);
                    i.putExtra("first_name",first_name);
                    i.putExtra("c_source",c_source);
                    i.putExtra("last_name",last_name);
                    i.putExtra("c_lat",cLat);
                    i.putExtra("c_lng",cLng);
                    i.putExtra("c_dest_lat",c_destLat);
                    i.putExtra("c_dest_lng",c_destLng);
                    startActivity(i);


//                    getFragmentManager().beginTransaction().
//                            replace(R.id.frame_layout, new PickUpReceiving(rideid, c_id, c_first_name, c_source, c_destination
//                                    , profile_pic, d_latt, d_long, city_id, c_contact
//                                    , first_name, last_name,cLat,cLng,c_destLat,c_destLng), "pick_up_receivinhg").commitAllowingStateLoss();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.e("dkp error ocurred", "Error");
                VolleyLog.d("Error", "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("error ocurred", "TimeoutError");
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                String[] tag = {"rideid"};
                String[] value = {rideid};

                for (int i = 0; i < tag.length; i++)
                    params.put(tag[i], value[i]);
                return checkParams(params);
            }
        };

        postReq.setRetryPolicy(new DefaultRetryPolicy(
                800000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(postReq);
    }

    public void getTripList(String url, final String driver_id, final String startlimit) {
        Log.d("trip list ", " url = " + url + " driver_id=" + driver_id);
        RequestQueue rq = Volley.newRequestQueue(ctx);
        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("trip list response ", "= " + response);

                if (!response.contains("Something Went Wrong")) {
                    try {
                        JSONArray jsonArray = Model.getArray(response);
                        assert jsonArray != null;
                        // list_arr = new ArrayList<>();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = Model.getObject(jsonArray, i);
                            com.xoocardriver.model.TripList list = new com.xoocardriver.model.TripList();
                            list.setActualCost(Model.getString(jsonObject, "actual_cost"));
                            list.setActualKms(Model.getString(jsonObject, "actual_kms"));
                            list.setActualTime(Model.getString(jsonObject, "actual_time"));
                            list.setEstimatedCost(Model.getString(jsonObject, "estimated_cost"));
                            list.setEstimatedTime(Model.getString(jsonObject, "estimated_time"));
                            list.setBookingStatus(Model.getString(jsonObject, "booking_status"));
                            list.setCustomerId(Model.getString(jsonObject, "customer_id"));
                            list.setDriverId(Model.getString(jsonObject, "driver_id"));
                            list.setOid(Model.getString(jsonObject, "oid"));
                            list.setVehicalId(Model.getString(jsonObject, "vehical_id"));
                            list.setSource(Model.getString(jsonObject, "source"));
                            list.setDestination(Model.getString(jsonObject, "destination"));
                            list.setAlter_driver(Model.getString(jsonObject, "alter_driver"));
                            list.setDCreatedOn(Model.getString(jsonObject, "d_CreatedOn"));
                            list.setVVehiclename(Model.getString(jsonObject, "v_vehiclename"));
                            list.setPayMode(Model.getString(jsonObject, "pay_mode"));
                            list_arr.add(list);
                        }

                        triplistAdapter = new TripListAdapter(ctx, list_arr);
                        recyclerView.setAdapter(triplistAdapter);
                        not_found_tv.setVisibility(View.GONE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                        not_found_tv.setVisibility(View.VISIBLE);
                        not_found_tv.setText("No Trips available");

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (startlimit.equals("0")) {
                    if(progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                }
                Log.d("dkp ocurred", "TimeoutError");
                VolleyLog.d("Error", "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.d("error ocurred", "TimeoutError");
                    // Toast.makeText(context, "TimeoutError", Toast.LENGTH_LONG).show();
                }
            }
        }) {
            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                }
                return volleyError;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                String[] tag = {"driver_id", "startlimit"};
                String[] value = {driver_id, startlimit};

                for (int i = 0; i < tag.length; i++)
                    params.put(tag[i], value[i]);
                return checkParams(params);
            }
        };
        postReq.setRetryPolicy(new DefaultRetryPolicy(
                1000000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(postReq);
    }

    private Map<String, String> checkParams(Map<String, String> map) {
        for (Map.Entry<String, String> pairs : map.entrySet()) {
            if (pairs.getValue() == null) {
                map.put(pairs.getKey(), "");
            }
        }
        return map;
    }

}
