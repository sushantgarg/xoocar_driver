package com.xoocardriver.view.fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.xoocardriver.CommonMethods;
import com.xoocardriver.R;
import com.xoocardriver.helper.Constant;
import com.xoocardriver.helper.MyApplication;
import com.xoocardriver.map.GeocodingLocation;
import com.xoocardriver.map.MapDirectionsParser;
import com.xoocardriver.model.Model;
import com.xoocardriver.model.Trip_complete;
import com.xoocardriver.preference.PrefManager;
import com.xoocardriver.realm.RealmHandler;
import com.xoocardriver.service.LocationUpdaterGoogleClientService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by elite on 21/12/16.
 */

public class CompleteTrip extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener {
    private LinearLayout complete_trip_layout;
    private GoogleMap googleMap;
    private ArrayList<LatLng> points = null;
    private Polyline mPolyline = null;

    public ProgressDialog progressDialog;
    private PrefManager prefManager;
    private String rideid;
    private String c_source;
    private String c_destination;
    private LatLng source_LatLong;
    private LatLng destination_LatLong;
    private LinearLayout sosLL;
    private LinearLayout navigate_btn;

    private Context ctx;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_complete_trip);
        ctx=CompleteTrip.this;

        Intent i=getIntent();
        rideid =        i.getStringExtra("rideid");
        String c_first_name = i.getStringExtra("c_first_name");
        c_source =      i.getStringExtra("c_source");
        c_destination = i.getStringExtra("c_destination");
        String c_lat = i.getStringExtra("c_lat");
        String c_lng = i.getStringExtra("c_lng");
        String c_dest_lat = i.getStringExtra("c_dest_lat");
        String c_dest_lng = i.getStringExtra("c_dest_lng");

        if( ! CommonMethods.isServiceRunning(ctx,LocationUpdaterGoogleClientService.class) )
            ctx.startService(new Intent(ctx,LocationUpdaterGoogleClientService.class));

        FragmentManager fragmentManager = getSupportFragmentManager();

        SupportMapFragment mMapFragment = (SupportMapFragment) fragmentManager.findFragmentById(R.id.complete_trip_location_map);

        if ( mMapFragment == null ) {
            mMapFragment = SupportMapFragment.newInstance();
            fragmentManager.beginTransaction().replace(R.id.complete_trip_location_map, mMapFragment).commit();
        }

        mMapFragment.getMapAsync(this);

        sosLL = (LinearLayout)             findViewById(R.id.complete_tripe_sos_ll);
        TextView pick_up_text = (TextView) findViewById(R.id.complete_tripe_source_tv);
        TextView pick_up_label_text = (TextView) findViewById(R.id.complete_tripe_source_label_tv);
        TextView complete_trip_text = (TextView) findViewById(R.id.complete_tripe_complete_tripe_tv);
        TextView drop_text = (TextView) findViewById(R.id.complete_tripe_drop_tv);
        TextView drop_lable_text = (TextView) findViewById(R.id.complete_tripe_drop_label_tv);
        TextView nameTv = (TextView) findViewById(R.id.complete_tripe_name_tv);
        complete_trip_layout = (LinearLayout) findViewById(R.id.complete_trip_layout);
        navigate_btn = (LinearLayout) findViewById(R.id.complete_trip_navigate_map);

        Typeface normal_fonts = Typeface.createFromAsset(ctx.getAssets(), "fonts/OpenSans-Regular.ttf");
        pick_up_text.setTypeface(normal_fonts);
        pick_up_label_text.setTypeface(normal_fonts);
        complete_trip_text.setTypeface(normal_fonts);
        drop_text.setTypeface(normal_fonts);
        drop_lable_text.setTypeface(normal_fonts);
        nameTv.setTypeface(normal_fonts);

        prefManager = new PrefManager(ctx);

        // location = new GPSTracker(getActivity());

        pick_up_text.setText(c_source);
        drop_text.setText(c_destination);
        nameTv.setText(c_first_name);
        rideid = prefManager.getRideId();

        source_LatLong = new LatLng(Double.parseDouble(c_lat),Double.parseDouble(c_lng));
        destination_LatLong = new LatLng(Double.parseDouble(c_dest_lat),Double.parseDouble(c_dest_lng));
        listenerView();
    }

    @Override
    public void onResume() {
        super.onResume();

//        try {
//            final InputMethodManager imm = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
//            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
//        } catch (Throwable e) {
//            e.printStackTrace();
//        }
    }


    public void listenerView() {
        navigate_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (source_LatLong != null && destination_LatLong != null) {
                    final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?" + "saddr=" + source_LatLong.latitude + "," + source_LatLong.longitude + "&daddr=" + destination_LatLong.latitude + "," + destination_LatLong.longitude));
                    intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                    startActivity(intent);
                } else {
                    Toast.makeText(ctx, "Ask customer for drop location", Toast.LENGTH_SHORT).show();
                }
            }
        });

        sosLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String address = GeocodingLocation.getCompleteAddressString(ctx, prefManager.getCurrentLat(), prefManager.getCurrentLng());

                try {
                    progressDialog = new ProgressDialog(ctx);
                    progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
                    progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                    progressDialog.show();
                    progressDialog.setContentView(R.layout.progressbar);
                } catch (Throwable e) {
                    e.printStackTrace();
                }
                sosApiRequest(ctx, Constant.SOS_POST_REQUEST_URL, rideid, address);
                String phone = "100";
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                startActivity(intent);
            }
        });

        complete_trip_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog = new ProgressDialog(ctx);
                progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
                progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                progressDialog.show();
                progressDialog.setContentView(R.layout.progressbar);
                showDialog();
            }
        });
    }

    public void showDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(ctx).create();
        alertDialog.setTitle("Alert!");
        alertDialog.setMessage("Are you sure you want to complete this ride.");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
                new DialogInterface.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    public void onClick(DialogInterface dialog, int which) {

                        if( RealmHandler.getUnsendPath().size() > 0 ) {
                            Toast.makeText(ctx, "Uploading path please wait ...", Toast.LENGTH_LONG).show();
                            String ride_id=prefManager.getRideIdCheck();

                            if ( ! ride_id.equals("0")  && prefManager.isRideRunning()) {
                                CommonMethods.sendPathToServer(ride_id,prefManager.getKeyUserId());
                            }
                            if(progressDialog!=null && progressDialog.isShowing())
                                progressDialog.dismiss();
                        } else {
                            completeTripRequest( ctx, Constant.COMPLETE_TRIP_POST_REQUEST_URL, prefManager.getCity(), rideid);
                        }

//                        askForPermission1(Manifest.permission.ACCESS_FINE_LOCATION, LOCATION, "oncreate");
                        dialog.dismiss();// use dismiss to cancel alert dialog
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if(progressDialog!= null && progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                    }
                });
        alertDialog.show();
    }

    public void sosApiRequest(Context context, String url, final String rideid, final String address_location) {
        Log.d("sosApiRequest:", " url = " + url + "rideid +" + rideid + ":address_location:" + address_location);
        RequestQueue rq = Volley.newRequestQueue(context);
        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("sosApiRequest response", "= " + response);
                progressDialog.dismiss();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    if (jsonArray.length() > 0) {
                         Toast.makeText(ctx, "Alert message send to added emergency contact", Toast.LENGTH_SHORT).show();
                    } else {
                        // Toast.makeText(mainActivity, "Please firstly add emergency contact", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.e("dkp error ocurred", "Error");
                VolleyLog.d("Error", "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("error ocurred", "TimeoutError");
                    // Toast.makeText(context, "TimeoutError", Toast.LENGTH_LONG).show();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                String userId = prefManager.getKeyUserId();
                Log.e("dkp userId", ":" + userId);
                String[] tag = {"location", "rideid", "userid"};
                String[] value = {address_location, rideid, userId};

                for (int i = 0; i < tag.length; i++)
                    params.put(tag[i], value[i]);
                return checkParams(params);
            }
        };

        postReq.setRetryPolicy(new DefaultRetryPolicy(
                800000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(postReq);
    }

    public void completeTripRequest(final Context context, String url, final String city_id, final String rideid) {

        RequestQueue rq = Volley.newRequestQueue(context);
        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(String response) {
                Log.d("complete trip response", "= " + response);
                progressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Trip_complete trip_complete = new Trip_complete();
                    String total = Model.getString(jsonObject, "total");
                    String basefare = Model.getString(jsonObject, "basefare");
                    String ridecharge = Model.getString(jsonObject, "ridecharge");
                    String taxcharge = Model.getString(jsonObject, "taxcharge");
                    String waitcharge = Model.getString(jsonObject, "waitcharge");
                    String conveniencecharge = Model.getString(jsonObject, "conveniencecharge");
                    String pdflink = Model.getString(jsonObject, "pdflink");

                    String actual_kms = Model.getString(jsonObject, "actual_kms");
                    String actual_time = Model.getString(jsonObject, "actual_time");
                    String pay_mode = Model.getString(jsonObject, "pay_mode");
                    String pay_status = Model.getString(jsonObject, "pay_status");
                    String couponprice = Model.getString(jsonObject, "couponprice");


                    if (total == null || total.equals("null") || total.equals("") || total.isEmpty()) {
                        total="0";
                    }

                    if (couponprice == null || couponprice.equals("null") || couponprice.equals("") || couponprice.isEmpty()) {
                        couponprice="0";
                    }

                    int final_total= Integer.parseInt(total);
                    trip_complete.setBasefare(basefare);
                    trip_complete.setTotal(""+final_total);
                    trip_complete.setRidecharge(ridecharge);
                    trip_complete.setConveniencecharge(conveniencecharge);
                    trip_complete.setPdflink(pdflink);
                    trip_complete.setTaxcharge(taxcharge);
                    trip_complete.setWaitcharge(waitcharge);
                    trip_complete.setSource(c_source);
                    trip_complete.setDestination(c_destination);
                    trip_complete.setActualKms(actual_kms);
                    trip_complete.setActualTime(actual_time);
                    trip_complete.setPayMode(pay_mode);
                    trip_complete.setCouponprice(couponprice);

                    prefManager.setRideIdCheck("0");
                    prefManager.setRideId(rideid);
                    prefManager.setIsRideRunning(false);
                    RealmHandler.clearPath();

                    assert pay_status != null;

                    Intent i=new Intent(context,TripSummary.class);
                    i.putExtra("total",trip_complete.getTotal());
                    i.putExtra("c_source",trip_complete.getSource());
                    i.putExtra("atual_time",actual_time);
                    i.putExtra("distance_km",actual_kms);
                    i.putExtra("payment_mode",pay_mode);
                    i.putExtra("c_destination",trip_complete.getDestination());
                    i.putExtra("couponprice",trip_complete.getCouponprice());
                    startActivity(i);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.e("dkp error ocurred", "Error");
                VolleyLog.d("Error", "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("error ocurred", "TimeoutError");
                    // Toast.makeText(context, "TimeoutError", Toast.LENGTH_LONG).show();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                String cat_id = prefManager.getVehicleId();
                String driver_id = prefManager.getKeyUserId();
                String[] tag = {"city_id", "rideid", "cat_id", "driver_id", "destination_add","d_lat","d_lng"};
                String[] value = { city_id, rideid, cat_id, driver_id, c_destination
                        , String.valueOf(prefManager.getCurrentLat()), String.valueOf(prefManager.getCurrentLng())};

                for (int i = 0; i < tag.length; i++)
                    params.put(tag[i], value[i]);
                return checkParams(params);
            }
        };

        postReq.setRetryPolicy(new DefaultRetryPolicy(
                800000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(postReq);
    }

    private Map<String, String> checkParams(Map<String, String> map) {
        for (Map.Entry<String, String> pairs : map.entrySet()) {
            if (pairs.getValue() == null) {
                map.put(pairs.getKey(), "");
            }
        }
        return map;
    }

    private void addMarks1(GoogleMap googleMap, String pickup, String drop) {
        try {
            googleMap.clear();
            //Log.d("dkp destination lat long", ":" + destination_LatLong);
            LatLng driver_LatLong = new LatLng(prefManager.getCurrentLat(), prefManager.getCurrentLng());
            String destination_add = GeocodingLocation.getCompleteAddressString(ctx, prefManager.getCurrentLat(), prefManager.getCurrentLng());

            if (destination_LatLong != null) {
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(source_LatLong);
                markerOptions.title("Pickup");
                markerOptions.snippet(pickup);
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.green_location));
                googleMap.addMarker(markerOptions);
                markerOptions.position(destination_LatLong);
                markerOptions.title("Drop");
                markerOptions.snippet(drop);
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.red_location));
                googleMap.addMarker(markerOptions);

                markerOptions.position(driver_LatLong);
                markerOptions.title("Driver");
                markerOptions.snippet(destination_add);
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.driver_black));
                googleMap.addMarker(markerOptions);

                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(source_LatLong, 17);
                googleMap.animateCamera(cameraUpdate);
                traceMe();

            } else {
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(source_LatLong);
                markerOptions.title("Pickup");
                markerOptions.snippet(pickup);
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.green_location));
                googleMap.addMarker(markerOptions);

                markerOptions.position(driver_LatLong);
                markerOptions.title("Driver");
                markerOptions.snippet(destination_add);
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.driver_black));
                googleMap.addMarker(markerOptions);

                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(source_LatLong, 17);
                googleMap.animateCamera(cameraUpdate);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void traceMe() {
        String srcParam = "";
        String destParam = "";
        // if (MainActivity.key == 1) {
        try {
            srcParam = source_LatLong.latitude + "," + source_LatLong.longitude;
            destParam = destination_LatLong.latitude + "," + destination_LatLong.longitude;
        } catch (Throwable e) {
            e.printStackTrace();
        }
        Log.d("drop lat pp1", ":" + destParam);
       /* } else {
            srcParam = fromPosition.latitude + "," + fromPosition.longitude;
            destParam = toPosition.latitude + "," + toPosition.longitude;
            Log.d("drop lat pp2", ":" + destParam);
        }*/

        String modes[] = {"driving", "walking", "bicycling", "transit"};
        String url = "https://maps.googleapis.com/maps/api/directions/json?origin=" + srcParam + "&destination=" + destParam + "&sensor=false&units=metric&mode=driving&key=" + Constant.google_direction_purchage_key;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("dkp lat long", ":" + response);
                        MapDirectionsParser parser = new MapDirectionsParser(ctx);
                        List<List<HashMap<String, String>>> routes = parser.parse(response);

                        for (int i = 0; i < routes.size(); i++) {
                            points = new ArrayList<LatLng>();
                            // lineOptions = new PolylineOptions();
                            // Fetching i-th route
                            List<HashMap<String, String>> path = routes.get(i);
                            // Fetching all the points in i-th route
                            Log.d("dkp lat long", ":" + path);
                            for (int j = 0; j < path.size(); j++) {
                                HashMap<String, String> point = path.get(j);
                                double lat = Double.parseDouble(point.get("lat"));
                                double lng = Double.parseDouble(point.get("lng"));
                                LatLng position = new LatLng(lat, lng);
                                points.add(position);
                            }
                        }
                        Log.d("dkp points lat long", ":" + points);
                        drawPoints(points, googleMap);
                        //  PD.dismiss();
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            Log.d("error ocurred", "TimeoutError");
                            Toast.makeText(ctx, "TimeoutError", Toast.LENGTH_LONG).show();
                        }
                    }
                });
        MyApplication.getInstance().addToRequestQueue(jsonObjectRequest, "jreq");
    }

    private void drawPoints(ArrayList<LatLng> points, GoogleMap mMaps) {
        if (points == null) {
            return;
        }
        PolylineOptions polylineOpt = new PolylineOptions();
        for (LatLng latlng : points) {
            polylineOpt.add(latlng);
        }
        polylineOpt.color(Color.BLACK);
        if (mPolyline != null) {
            mPolyline.remove();
            mPolyline = null;
        }
        if (mMaps != null) {
            mPolyline = mMaps.addPolyline(polylineOpt);
        }
        if (mPolyline != null)
            mPolyline.setWidth(7);

        //mFooterLayout.setVisibility(View.GONE);
    }


    @Override
    public void onPause() {
        super.onPause();
        // mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // mMapView.onDestroy();0./80
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        // mMapView.onLowMemory();
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        try {
            this.googleMap = googleMap;

            googleMap.getUiSettings().setZoomControlsEnabled(false);
            googleMap.getUiSettings().setMyLocationButtonEnabled(false);
            googleMap.getUiSettings().setCompassEnabled(true);
            googleMap.getUiSettings().setRotateGesturesEnabled(true);
            googleMap.getUiSettings().setZoomGesturesEnabled(true);

            if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }

            googleMap.setMyLocationEnabled(true);
            addMarker();

//            buildGoogleApiClient();
//            mGoogleApiClient.connect();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void addMarker() {
        addMarks1(googleMap, c_source, c_destination);

        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(prefManager.getCurrentLat(), prefManager.getCurrentLng())).zoom(17)
                .build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    @Override
    public void onBackPressed() {

    }
}

