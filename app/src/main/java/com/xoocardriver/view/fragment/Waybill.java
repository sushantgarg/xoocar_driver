package com.xoocardriver.view.fragment;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.xoocardriver.R;
import com.xoocardriver.adapter.WayBillListAdapter;
import com.xoocardriver.helper.CircularImageView;
import com.xoocardriver.helper.Constant;
import com.xoocardriver.helper.CustomVolleyRequest;
import com.xoocardriver.helper.Way_bill;
import com.xoocardriver.model.Model;
import com.xoocardriver.preference.PrefManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

public class Waybill extends AppCompatActivity {
    private ProgressDialog progressDialog;
    private String driver_id;
    private RecyclerView recyclerView;
    private ArrayList<Way_bill> list_arr;

    private WayBillListAdapter waybilllistAdapter;
    private TextView not_found_tv;
    private View editProfileLL;
    private TextView endDateTv;
    private Calendar myCalendar;
    private LinearLayout requestFocustLL;
    private Context ctx;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_waybill);
        ctx=Waybill.this;

        PrefManager prefManager = new PrefManager(ctx);
        editProfileLL = (LinearLayout) findViewById(R.id.waibill_edit_profile_ll);
        not_found_tv = (TextView) findViewById(R.id.way_bill_not_available_tv);
        TextView nameTv = (TextView) findViewById(R.id.way_bill_name_tv);
        TextView headingTv = (TextView) findViewById(R.id.way_bill_heading_tv);
        recyclerView = (RecyclerView) findViewById(R.id.way_bill_rl);
        CircularImageView picProfileIv = (CircularImageView) findViewById(R.id.way_bill_iv);
        endDateTv = (TextView) findViewById(R.id.way_bill_end_date_tv);
        requestFocustLL=(LinearLayout)findViewById(R.id.collapsing_toolbar);
        recyclerView.setLayoutManager(new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false));
        String image = prefManager.getProfilePic();
        String name = prefManager.getKeyUserName();
        myCalendar = Calendar.getInstance();
        nameTv.setText(name);
        ImageLoader im = CustomVolleyRequest.getInstance(ctx).getImageLoader();
        picProfileIv.setImageUrl(Constant.PROFILE_IMAGE_URL + image, im);
        Typeface face = Typeface.createFromAsset(ctx.getAssets(), "fonts/OpenSans-Regular.ttf");
        nameTv.setTypeface(face);
        headingTv.setTypeface(face);
        not_found_tv.setTypeface(face);
        requestFocustLL.requestFocus();

        listenerView();
        try {
            progressDialog = new ProgressDialog(ctx);
            progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            progressDialog.show();
            progressDialog.setContentView(R.layout.progressbar);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        driver_id = prefManager.getKeyUserId();
        Calendar c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c.getTime());
        Log.d("current_date", ":" + formattedDate);
        getDocument(ctx, Constant.WAY_BILL_POST_REQUEST_URL, driver_id, formattedDate);

    }


    private void listenerView() {
        editProfileLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(Waybill.this,EditProfile.class);
                startActivity(i);
            }
        });
        endDateTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(ctx, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    public void initView(View view) {

    }

    final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateEnd();
        }
    };

    private void updateEnd() {
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        String e_date = sdf.format(myCalendar.getTime());
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String todayDate = df.format(c.getTime());
        Log.d("current_date", ":" + todayDate);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date fdate = null;
        try {
            fdate = dateFormat.parse(e_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date cdate = null;
        try {
            cdate = dateFormat.parse(todayDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (fdate.before(cdate) || fdate.equals(cdate)) {
            endDateTv.setText(sdf.format(myCalendar.getTime()));
            try {
                progressDialog = new ProgressDialog(ctx);
                progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
                progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                progressDialog.show();
                progressDialog.setContentView(R.layout.progressbar);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
           requestFocustLL.requestFocus();
            getDocument(ctx, Constant.WAY_BILL_POST_REQUEST_URL, driver_id, e_date);
        } else {
            Toast.makeText(ctx, "Please select before or equal to current date", Toast.LENGTH_SHORT).show();
        }
    }

    public void getDocument(Context context, String url, final String driver_id, final String endDate) {
        Log.d("WAY BILL document:", " url = " + url + ":driver_id:" + driver_id + ":endDate:" + endDate);
        RequestQueue rq = Volley.newRequestQueue(context);
        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("WAY BILL response ", "= " + response);
                progressDialog.dismiss();
                list_arr = new ArrayList<>();
                waybilllistAdapter = new WayBillListAdapter(ctx, list_arr);
                if (!response.contains("Something Went Wrong")) {
                    try {
                        JSONArray jsonArray = Model.getArray(response);
                        assert jsonArray != null;
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = Model.getObject(jsonArray, i);
                            Way_bill list = new Way_bill();
                            list.setOid(Model.getString(jsonObject, "oid"));
                            list.setDriverId(Model.getString(jsonObject, "driver_id"));
                            list.setVehicalId(Model.getString(jsonObject, "vehical_id"));
                            list.setCustomerId(Model.getString(jsonObject, "customer_id"));
                            list.setSource(Model.getString(jsonObject, "source"));
                            list.setDestination(Model.getString(jsonObject, "destination"));
                            list.setWayBill(Model.getString(jsonObject, "way_bill"));
                            list_arr.add(list);
                        }
                        recyclerView.setAdapter(waybilllistAdapter);
                        not_found_tv.setVisibility(View.GONE);
                        requestFocustLL.requestFocus();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    if (list_arr != null) {
                        list_arr.clear();
                    }
                    recyclerView.removeAllViews();
                    waybilllistAdapter.notifyDataSetChanged();
                    not_found_tv.setVisibility(View.VISIBLE);
                    not_found_tv.setText("No Waybill available");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.e("dkp error ocurred", "Error");
                VolleyLog.d("Error", "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("error ocurred", "TimeoutError");
                } else if (error instanceof AuthFailureError) {
                    Log.e("error ocurred", "AuthFailureError");
                } else if (error instanceof ServerError) {
                    Log.e("error ocurred", "ServerError");

                } else if (error instanceof NetworkError) {
                    Log.e("error ocurred", "NetworkError");

                } else if (error instanceof ParseError) {
                    Log.e("error ocurred", "ParseError");

                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                String[] tag = {"driver_id", "enddate"};
                String[] value = {driver_id, endDate};
                for (int i = 0; i < tag.length; i++)
                    params.put(tag[i], value[i]);
                return checkParams(params);
            }
        };
        postReq.setRetryPolicy(new DefaultRetryPolicy(
                800000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(postReq);
    }
    private Map<String, String> checkParams(Map<String, String> map) {
        Iterator<Map.Entry<String, String>> it = map.entrySet().iterator();
        while (it.hasNext()) {

            Map.Entry<String, String> pairs = (Map.Entry<String, String>) it.next();
            if (pairs.getValue() == null) {
                map.put(pairs.getKey(), "");
            }
        }
        return map;
    }
}
