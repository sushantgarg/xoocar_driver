package com.xoocardriver.view.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.maps.android.heatmaps.HeatmapTileProvider;
import com.google.maps.android.heatmaps.WeightedLatLng;
import com.xoocardriver.CommonMethods;
import com.xoocardriver.FireBase.FirebaseManager;
import com.xoocardriver.IncentiveActivity;
import com.xoocardriver.InviteDriver;
import com.xoocardriver.R;
import com.xoocardriver.RetroFit.GetHeatMapRequestFields;
import com.xoocardriver.RetroFit.GetHeatMapResponceData;
import com.xoocardriver.RetroFit.GetHeatMapResponceFields;
import com.xoocardriver.RetroFit.RequestInterface;
import com.xoocardriver.RetroFit.UpdateDriverPosRequestData;
import com.xoocardriver.RetroFit.UpdateDriverPosRequestFields;
import com.xoocardriver.RetroFit.UpdateDriverPosResponceFields;
import com.xoocardriver.Training;
import com.xoocardriver.helper.Constant;
import com.xoocardriver.model.Model;
import com.xoocardriver.preference.PrefManager;
import com.xoocardriver.service.LocationUpdaterGoogleClientService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class OfflineOnline extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener ,LocationListener {

    private GoogleMap googleMap;
    private double latitude = 0, longitude = 0;
    private PrefManager prefManager;
    private ProgressBar progressBar;
    private LinearLayout heatMapDesc;
    private int flage_count = 0;
    private GoogleApiClient mGoogleApiClient;
    private Context ctx;
    private FirebaseManager firebaseManager;
    private DrawerLayout navigationDrawer;

    private LinearLayout document_sub_category;
    private ImageView expand_or_collapse;
    private int click_time = 0;       // how and hide document sub tag
    private ImageView onlineOfflineLL;
    String currentVersion = "6.4";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_offline_online);
        ctx = OfflineOnline.this;
        prefManager = new PrefManager(ctx);

        FragmentManager fragmentManager = getSupportFragmentManager();

        SupportMapFragment mMapFragment = (SupportMapFragment) fragmentManager.findFragmentById(R.id.online_offline_location_map);

        if (mMapFragment == null) {
            mMapFragment = SupportMapFragment.newInstance();
            fragmentManager.beginTransaction().replace(R.id.online_offline_location_map, mMapFragment).commit();
        }

        mMapFragment.getMapAsync(this);

        buildGoogleApiClient();
        firebaseManager = FirebaseManager.getInstance();

        LinearLayout zoomRl = (LinearLayout) findViewById(R.id.home_zoom_navigate_map);
        onlineOfflineLL = (ImageView) findViewById(R.id.offlineToggel);
        heatMapDesc = (LinearLayout) findViewById(R.id.heatMapDesc);
        ImageView heatMapImage = (ImageView) findViewById(R.id.imageView8);
        ImageView inviteCustomerImage = (ImageView) findViewById(R.id.imageView7);
        ImageView inviteDriverImage = (ImageView) findViewById(R.id.imageView6);
        ImageView trainingImage = (ImageView) findViewById(R.id.imageView9);

//        drawer layout
        ImageView menuIcon = (ImageView) findViewById(R.id.menuIcon);
        navigationDrawer = (DrawerLayout) findViewById(R.id.navigationDrawer);

        LinearLayout invite_fireds_tab = (LinearLayout) findViewById(R.id.menu_invite_friends_ll);
        LinearLayout menu_invite_driver_ll = (LinearLayout) findViewById(R.id.menu_invite_driver_ll);
        LinearLayout document_tab = (LinearLayout) findViewById(R.id.document_tab);
        LinearLayout profileLL = (LinearLayout) findViewById(R.id.menu_profile_ll);
        LinearLayout emerContactLL = (LinearLayout) findViewById(R.id.menu_emergency_contact_ll);
        LinearLayout incentivelayout = (LinearLayout) findViewById(R.id.menu_incentive_ll);
        LinearLayout uploadDocumentLL = (LinearLayout) findViewById(R.id.menu_upload_document_ll);
        LinearLayout viewdocumentLL = (LinearLayout) findViewById(R.id.menu_view_document_ll);
        LinearLayout about = (LinearLayout) findViewById(R.id.about);
        LinearLayout policyLL = (LinearLayout) findViewById(R.id.menu_policy_ll);
        LinearLayout logoutLL = (LinearLayout) findViewById(R.id.logout_ll);
        LinearLayout helpLL = (LinearLayout) findViewById(R.id.menu_help_ll);
        LinearLayout termConditionLL = (LinearLayout) findViewById(R.id.menu_term_condition_ll);
        LinearLayout earningLL = (LinearLayout) findViewById(R.id.menu_earnings_ll);
        LinearLayout currentTripLL = (LinearLayout) findViewById(R.id.menu_trip_history_ll);
        LinearLayout waybillLL = (LinearLayout) findViewById(R.id.menu_way_bill_ll);
        LinearLayout menu_training_ll = (LinearLayout) findViewById(R.id.menu_training_ll);
        document_sub_category = (LinearLayout) findViewById(R.id.document_sub_category);
        expand_or_collapse = (ImageView) findViewById(R.id.expand_or_collapse);



        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        LocationManager locationManager = (LocationManager) ctx.getSystemService(LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            showDialogToSettings();
        }

        if (!CommonMethods.isServiceRunning(ctx, LocationUpdaterGoogleClientService.class))
            ctx.startService(new Intent(ctx, LocationUpdaterGoogleClientService.class));

        Location mLastLocation = new Location("gps");
        mLastLocation.setLatitude(prefManager.getCurrentLat());
        mLastLocation.setLongitude(prefManager.getCurrentLng());

        firebaseManager.addDriver(prefManager.getKeyUserId(), prefManager.getKeyUserName(), prefManager.getVehicleModel(), prefManager.getVehicleNumber()
                , prefManager.getCity(), mLastLocation, prefManager.getVehicleCategory());
        firebaseManager.addGeoFireLoc(prefManager.getKeyUserId(), mLastLocation);

        onlineOfflineLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    String ride_check = prefManager.getRideIdCheck();
                    if (ride_check == null || ride_check.equals("0")) {
                        progressBar.setVisibility(View.VISIBLE);
                        postDriverStatus(ctx, Constant.DRIVER_STATUS_POST_REQUEST_URL, prefManager.getKeyUserId(), "0",0);
                    } else {
                        Toast.makeText(ctx, "Sorry! First complete trip", Toast.LENGTH_SHORT).show();
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });

        zoomRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                latitude = prefManager.getCurrentLat();
                longitude = prefManager.getCurrentLng();

                if (googleMap != null) {
                    if (flage_count == 0) {
                        flage_count = 1;
                        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(latitude, longitude)).zoom(10)
                                .build();
                        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    } else if (flage_count == 1) {
                        flage_count = 0;
                        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                        googleMap.setBuildingsEnabled(true);
                        // displayGeofences();
                        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(latitude, longitude)).zoom(10)
                                .build();
                        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    }
                }
            }
        });

        menuIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigationDrawer.openDrawer(GravityCompat.START);
            }
        });
//
//        inviteCustomerImage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(ctx, InviteFriends.class);
//                startActivity(i);
//            }
//        });
//
//        trainingImage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(ctx, Training.class);
//                startActivity(i);
//            }
//        });
//
//        inviteDriverImage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(ctx, InviteDriver.class);
//                startActivity(i);
//            }
//        });

        heatMapImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (heatMapDesc.getVisibility() == View.VISIBLE) {
                    heatMapDesc.setVisibility(View.GONE);
                } else {
                    heatMapDesc.setVisibility(View.VISIBLE);
                }
            }
        });

        ////////Drawer items

//        invite_fireds_tab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (navigationDrawer.isDrawerOpen(GravityCompat.START)) {
//                    navigationDrawer.closeDrawer(GravityCompat.START);
//                }
//                Intent i=new Intent(OfflineOnline.this,InviteFriends.class);
//                startActivity(i);
//            }
//        });
//
//        menu_invite_driver_ll.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i=new Intent(OfflineOnline.this, InviteDriver.class);
//                startActivity(i);
//            }
//        });
//
//
//        document_tab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (click_time == 0) {
//                    document_sub_category.setVisibility(View.VISIBLE);
//                    click_time = 1;
//                    expand_or_collapse.setImageResource(R.mipmap.collapse);
//                } else {
//                    document_sub_category.setVisibility(View.GONE);
//                    click_time = 0;
//                    expand_or_collapse.setImageResource(R.mipmap.expand);
//                }
//            }
//        });
//
//        profileLL.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View view) {
//                if (navigationDrawer.isDrawerOpen(GravityCompat.START)) {
//                    navigationDrawer.closeDrawer(GravityCompat.START);
//                }
//                Intent i=new Intent(OfflineOnline.this,Profile.class);
//                startActivity(i);
//            }
//        });
//
//        emerContactLL.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (navigationDrawer.isDrawerOpen(GravityCompat.START)) {
//                    navigationDrawer.closeDrawer(GravityCompat.START);
//                }
//
//                Intent i=new Intent(OfflineOnline.this,EmergencyContactFragment.class);
//                startActivity(i);
//            }
//        });
//
//        incentivelayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i=new Intent(OfflineOnline.this, IncentiveActivity.class);
//                startActivity(i);
//            }
//        });
//
//        uploadDocumentLL.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (navigationDrawer.isDrawerOpen(GravityCompat.START)) {
//                    navigationDrawer.closeDrawer(GravityCompat.START);
//                }
//                Intent i=new Intent(OfflineOnline.this,UploadDocument.class);
//                startActivity(i);
//            }
//        });
//
//        viewdocumentLL.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (navigationDrawer.isDrawerOpen(GravityCompat.START)) {
//                    navigationDrawer.closeDrawer(GravityCompat.START);
//                }
//                Intent i=new Intent(OfflineOnline.this,ViewDocument.class);
//                startActivity(i);
//            }
//        });
//
//        about.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (navigationDrawer.isDrawerOpen(GravityCompat.START)) {
//                    navigationDrawer.closeDrawer(GravityCompat.START);
//                }
//                Intent i=new Intent(OfflineOnline.this,AboutUsFragment.class);
//                i.putExtra("url","http://www.xoocar.com/webapp/webapp/aboutus");
//                startActivity(i);
//
//            }
//        });
//        policyLL.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (navigationDrawer.isDrawerOpen(GravityCompat.START)) {
//                    navigationDrawer.closeDrawer(GravityCompat.START);
//                }
//
//                Intent i=new Intent(OfflineOnline.this,AboutUsFragment.class);
//                i.putExtra("url","http://www.xoocar.com/webapp/webapp/policies");
//                startActivity(i);
//
//            }
//        });
//
//        helpLL.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (navigationDrawer.isDrawerOpen(GravityCompat.START)) {
//                    navigationDrawer.closeDrawer(GravityCompat.START);
//                }
//                Intent i=new Intent(OfflineOnline.this,AboutUsFragment.class);
//                i.putExtra("url","http://www.xoocar.com/webapp/webapp/contactus");
//                startActivity(i);
//
//            }
//        });
//
//        termConditionLL.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (navigationDrawer.isDrawerOpen(GravityCompat.START)) {
//                    navigationDrawer.closeDrawer(GravityCompat.START);
//                }
//                Intent i=new Intent(OfflineOnline.this,AboutUsFragment.class);
//                i.putExtra("url","http://www.xoocar.com/webapp/webapp/terms");
//                startActivity(i);
//
//            }
//        });
//
//        currentTripLL.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                if (!isRingerActive) {
//                    if (navigationDrawer.isDrawerOpen(GravityCompat.START)) {
//                        navigationDrawer.closeDrawer(GravityCompat.START);
//                    }
//                    Intent i=new Intent(OfflineOnline.this,TripList.class);
//                    startActivity(i);
////                } else {
////                    Toast.makeText(OfflineOnline.this, "Not allowed during ringer", Toast.LENGTH_SHORT).show();
////                }
//            }
//        });
//
//        earningLL.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (navigationDrawer.isDrawerOpen(GravityCompat.START)) {
//                    navigationDrawer.closeDrawer(GravityCompat.START);
//                }
//                Intent i=new Intent(OfflineOnline.this,TripEarning.class);
//                startActivity(i);
//            }
//        });
//
//        waybillLL.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (navigationDrawer.isDrawerOpen(GravityCompat.START)) {
//                    navigationDrawer.closeDrawer(GravityCompat.START);
//                }
//                Intent i=new Intent(OfflineOnline.this,Waybill.class);
//                startActivity(i);
//            }
//        });

        logoutLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (navigationDrawer.isDrawerOpen(GravityCompat.START)) {
                    navigationDrawer.closeDrawer(GravityCompat.START);
                }

                String ride_check = prefManager.getRideIdCheck();
                if (ride_check == null || ride_check.equals("0")) {
                    try {
                        postDriverStatus(OfflineOnline.this, Constant.DRIVER_STATUS_POST_REQUEST_URL, prefManager.getKeyUserId(), "0",1);
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(OfflineOnline.this, "Sorry! First complete trip", Toast.LENGTH_SHORT).show();
                }
            }
        });

//        menu_training_ll.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                Intent i=new Intent( OfflineOnline.this, Training.class );
//                startActivity(i);
//            }
//        });

        new GetVersionCode().execute();

    }


    public void showDialogToSettings() {
        // Build the alert dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setTitle("Location not available");
        builder.setMessage("Please enable GPS for accurate path");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // Show location settings when the user
                // acknowledges the alert dialog
                enableLoc();
            }
        });
        builder.setNegativeButton("CANCEL",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // Show location settings when the user
                        // acknowledges the alert dialog
                        dialogInterface.dismiss();
                    }
                });
        Dialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();

    }

    private void enableLoc() {

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
// Show the dialog by calling startResolutionForResult(),
// and check the result in onActivityResult().
                            status.startResolutionForResult(OfflineOnline.this, 101);
                        } catch (IntentSender.SendIntentException e) {
// Ignore the error.
                        }
                        break;
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        prefManager.setCheckBookingAlarm(null);
        String ride_id = prefManager.getRideIdCheck();

            if( ! ride_id.equals("0") ) {
                parrallelstartApi( Constant.START_PARALLELPOST_REQUEST_URL, ride_id);
            }
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onClick(View v) {
    }

    public void parrallelstartApi( String url, final String rideid) {
        RequestQueue rq = Volley.newRequestQueue(OfflineOnline.this);
        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String c_first_name = jsonObject.getString("c_first_name");      ///9719039102
                    String c_source = jsonObject.getString("source");
                    String c_destination = jsonObject.getString("destination");
                    String c_id = jsonObject.getString("c_id");
                    String d_latt = jsonObject.getString("d_latt");
                    String d_long = jsonObject.getString("d_long");
                    String c_destLat=jsonObject.getString("c_dest_lat");
                    String c_destLng=jsonObject.getString("c_dest_lng");
                    String cLat=jsonObject.getString("c_latt");
                    String cLng=jsonObject.getString("c_long");
                    String city_id = jsonObject.getString("city");
                    String rideid = jsonObject.getString("rideid");
                    String booking_status = jsonObject.getString("booking_status");

                    switch (booking_status) {
                        case "Pending":
//                        call timer
                            String bookingType=jsonObject.optString("ridetype");
                            String estimate=jsonObject.optString("estimated_cost");
                            String date = jsonObject.optString("creat");
//                            String date=jsonObject.optString("created_on");

                                Intent i=new Intent(OfflineOnline.this,RideRequest.class);
                                i.putExtra("c_source",c_source);
                                i.putExtra("c_destination",c_destination);
                                i.putExtra("cLat",cLat);
                                i.putExtra("cLng",cLng);
                                i.putExtra("dLat",c_destLat);
                                i.putExtra("dLng",c_destLng);
                                i.putExtra("type",bookingType);
                                i.putExtra("est",estimate);
                                i.putExtra("date",date);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(i);

                            break;

                        case "Schedule":
//                        call pickup fragment
                            String first_name = jsonObject.getString( "first_name");
                            String last_name = jsonObject.getString( "last_name");
                            String profile_pic = jsonObject.getString("c_profilepic");
                            String c_contact = jsonObject.getString("c_contact");

                                Intent i2=new Intent(OfflineOnline.this,PickUpReceiving.class);
                                i2.putExtra("rideid",rideid);
                                i2.putExtra("c_id",c_id);
                                i2.putExtra("c_first_name",c_first_name);
                                i2.putExtra("c_destination",c_destination);
                                i2.putExtra("c_profile_pic",profile_pic);
                                i2.putExtra("d_latt",d_latt);
                                i2.putExtra("d_long",d_long);
                                i2.putExtra("city_id",city_id);
                                i2.putExtra("contact",c_contact);
                                i2.putExtra("first_name",first_name);
                                i2.putExtra("c_source",c_source);
                                i2.putExtra("last_name",last_name);
                                i2.putExtra("c_lat",cLat);
                                i2.putExtra("c_lng",cLng);
                                i2.putExtra("c_dest_lat",c_destLat);
                                i2.putExtra("c_dest_lng",c_destLng);
                                startActivity(i2);
                            break;

                        case "Running":
                            if( ! isFinishing()) {

                                Intent i1=new Intent(OfflineOnline.this,CompleteTrip.class);

                                i1.putExtra("rideid",rideid);
                                i1.putExtra("c_first_name",c_first_name);
                                i1.putExtra("c_source",c_source);
                                i1.putExtra("c_destination",c_destination);
                                i1.putExtra("c_lat",cLat);
                                i1.putExtra("c_lng",cLng);
                                i1.putExtra("c_dest_lat",c_destLat);
                                i1.putExtra("c_dest_lng",c_destLng);
                                startActivity(i1);

                            }
                            break;
                        case "Cancelled":
                                Intent i1=new Intent(OfflineOnline.this,CancelRideFragment.class);
                            i1.putExtra("type", "cancel");
                                startActivity(i1);

                            break;
                        default:
                            prefManager.setRideIdCheck("0");
                            break;
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("aaaa","error");
                prefManager.setRideIdCheck("0");
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                String[] tag = {"rideid","d_id"};//866
                String[] value = {rideid,prefManager.getKeyUserId()};

                for (int i = 0; i < tag.length; i++)
                    params.put(tag[i], value[i]);
                return checkParams(params);
            }
        };
        postReq.setRetryPolicy(new DefaultRetryPolicy(
                7*1000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(postReq);
    }


    protected synchronized void buildGoogleApiClient() {

        mGoogleApiClient = new GoogleApiClient.Builder(ctx)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        LocationRequest mLocationRequest;
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        mLocationRequest.setFastestInterval(2 * 60 * 1000);

        if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ctx,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {

        try {
            this.googleMap = googleMap;

            googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            ctx, R.raw.style_json));

            mGoogleApiClient.connect();

            this.googleMap.clear();
            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(latitude, longitude)).zoom(10)
                    .build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    marker.hideInfoWindow();
                    return true;
                }
            });

            googleMap.getUiSettings().setZoomControlsEnabled(false);
            googleMap.getUiSettings().setMyLocationButtonEnabled(false);
            googleMap.getUiSettings().setCompassEnabled(true);

            if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            googleMap.setMyLocationEnabled(true);

            addHeatMap();


        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void postDriverStatus(Context context, String url, final String driverid, final String status,final int source) {
//        Log.d("all vehicle:", " url = " + url + " driverid :" + driverid + " status:" + status);
        RequestQueue rq = Volley.newRequestQueue(context);
        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (progressBar != null)
                    progressBar.setVisibility(View.GONE);
                Log.d("post driver status ", "= " + response);
                JSONArray jsonArray = Model.getArray(response);
                try {
                    assert jsonArray != null;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String d_current_status = jsonObject.getString("d_current_status");
                        onlineOfflineLL.setImageResource(R.drawable.off_duty);

                        if (CommonMethods.isServiceRunning(ctx, LocationUpdaterGoogleClientService.class))
                            ctx.stopService(new Intent(ctx, LocationUpdaterGoogleClientService.class));

                        if ( d_current_status.equals("0") ) {
                            if(source==1) {
                                firebaseManager.removeDriver(prefManager.getKeyUserId());
                                prefManager.setKeyUserId(null);
                                prefManager.setDriverOnlineOfflineStatus("0");

                                Intent intent=new Intent(ctx,SignIn.class);
                                startActivity(intent);
                            } else {
                                firebaseManager.removeDriver(prefManager.getKeyUserId());
                                prefManager.setDriverOnlineOfflineStatus("0");

                                Intent intent=new Intent(ctx,Home.class);
                                startActivity(intent);
                            }

                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Log.e("dkp error ocurred", "Error");
                VolleyLog.d("Error", "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("error ocurred", "TimeoutError");
                    // Toast.makeText(context, "TimeoutError", Toast.LENGTH_LONG).show();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                String[] tag = {"act_mode", "d_id", "status"};
                String[] value = {"driver_status", driverid, status};

                for (int i = 0; i < tag.length; i++)
                    params.put(tag[i], value[i]);
                return checkParams(params);
            }
        };

        postReq.setRetryPolicy(new DefaultRetryPolicy(
                8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(postReq);
    }

    private Map<String, String> checkParams(Map<String, String> map) {
        for (Map.Entry<String, String> pairs : map.entrySet()) {
            if (pairs.getValue() == null) {
                map.put(pairs.getKey(), "");
            }
        }
        return map;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ActivityCompat.checkSelfPermission(ctx, permissions[0]) != PackageManager.PERMISSION_GRANTED) {
//            finish();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onLocationChanged(Location mLastLocation) {
        if (mLastLocation != null) {
            prefManager.setCurrentLat(mLastLocation.getLatitude());
            prefManager.setCurrentLng(mLastLocation.getLongitude());

            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                    mLastLocation.getLatitude(), mLastLocation.getLongitude()), 10));

            if (prefManager.getRideIdCheck().equals("0"))
                updateDriverLocation(mLastLocation);

            firebaseManager.addDriver(prefManager.getKeyUserId(), prefManager.getKeyUserName()
                    , prefManager.getVehicleModel(), prefManager.getVehicleNumber(), prefManager.getCity(), mLastLocation, prefManager.getVehicleCategory());
            firebaseManager.addGeoFireLoc(prefManager.getKeyUserId(), mLastLocation);
//            firebaseManager.getGeoFireUsers(mLastLocation);


            if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    public void updateDriverLocation(Location loc) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);

        Call<UpdateDriverPosResponceFields> call = request.updatePosition("api/driverappapi/adddriverlocfire/format/json/"
                , new UpdateDriverPosRequestFields(new UpdateDriverPosRequestData(prefManager.getKeyUserId()
                        , "" + loc.getLatitude()
                        , "" + loc.getLongitude()
                        , "" + loc.getBearing()
                        , prefManager.getSessionId()
                        , "30000"))
        );

        call.enqueue(new Callback<UpdateDriverPosResponceFields>() {
            @Override
            public void onResponse(Call<UpdateDriverPosResponceFields> call, retrofit2.Response<UpdateDriverPosResponceFields> response) {

                if (response.body() != null) {
                    if (response.body().getStatus() == 1) {

                        prefManager.setRideIdCheck(response.body().getDataArray().getOid());
                        parrallelstartApi( Constant.START_PARALLELPOST_REQUEST_URL, prefManager.getRideIdCheck() );

                    }
                }
            }

            @Override
            public void onFailure(Call<UpdateDriverPosResponceFields> call, Throwable t) {
            }

        });
    }

    private void addHeatMap() {

        final List<WeightedLatLng> list = new ArrayList<>();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);

        Call<GetHeatMapResponceFields> call = request.getHeatMap("api/driverappapi/getheatmap/format/json/"
                , new GetHeatMapRequestFields());

        call.enqueue(new Callback<GetHeatMapResponceFields>() {
            @Override
            public void onResponse(Call<GetHeatMapResponceFields> call, retrofit2.Response<GetHeatMapResponceFields> response) {

                if (response.body() != null) {
                    List<GetHeatMapResponceData> requests = response.body().getDataArray();
                    for (GetHeatMapResponceData reqData :
                            requests) {
                        if (!reqData.getcLatt().equals("0")) {
                            WeightedLatLng area = new WeightedLatLng(new LatLng(Double.parseDouble(reqData.getcLatt())
                                    , Double.parseDouble(reqData.getcLong())), 1);
                            list.add(area);
                        }
                    }

                    if (list.size() > 0) {
                        HeatmapTileProvider provider = new HeatmapTileProvider.Builder().radius(35).opacity(0.8).weightedData(list).build();
                        googleMap.addTileOverlay(new TileOverlayOptions().tileProvider(provider));
                    }
                }
            }

            @Override
            public void onFailure(Call<GetHeatMapResponceFields> call, Throwable t) {
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public class GetVersionCode extends AsyncTask<Void, String, String> {
        @Override
        protected String doInBackground(Void... voids) {

            String newVersion = "";
            try {
                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + ctx.getPackageName() + "&hl=it")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div[itemprop=softwareVersion]")
                        .first()
                        .ownText();
                return newVersion;
            } catch (Exception e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(String onlineVersion) {
            super.onPostExecute(onlineVersion);
            if (onlineVersion != null && !onlineVersion.isEmpty()) {
                if (Float.valueOf(currentVersion) < Float.valueOf(onlineVersion)) {
                    ShowDialog((Activity) ctx, getString(R.string.version_upgrade_msg));

                }
                Log.d("update", "Current version " + currentVersion + "playstore version " + onlineVersion);
            }
        }

        void ShowDialog(Activity mMainActivity, String msg) {
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            final Dialog imageDialog = new Dialog(mMainActivity);
            imageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            imageDialog.setCancelable(false);
            imageDialog.setCanceledOnTouchOutside(false);
            imageDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            LayoutInflater inflater = (LayoutInflater) mMainActivity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.layout_update_dialoge, null);
            TextView mTextView = (TextView) layout.findViewById(R.id.id_network_ok);
            TextView mTextmsg = (TextView) layout.findViewById(R.id.id_network_not);
            mTextmsg.setText(msg);
            mTextView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {

                    postDriverStatus(ctx, Constant.DRIVER_STATUS_POST_REQUEST_URL, prefManager.getKeyUserId(), "0",0);
                    Intent i=new Intent(OfflineOnline.this,Home.class);
                    ctx.startActivity(i);

//                    Intent in = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.xoocardriver&hl=en"));
//                    startActivity(in);
                    imageDialog.dismiss();
                }
            });
            layout.setLayoutParams(param);
            imageDialog.setContentView(layout, param);
            imageDialog.show();
        }


    }

}
