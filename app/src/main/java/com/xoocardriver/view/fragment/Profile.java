package com.xoocardriver.view.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.xoocardriver.R;
import com.xoocardriver.helper.CircularImageView;
import com.xoocardriver.helper.Constant;
import com.xoocardriver.helper.CustomVolleyRequest;
import com.xoocardriver.model.Model;
import com.xoocardriver.preference.PrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class Profile extends AppCompatActivity {

    private LinearLayout editProfileLL;
    private ProgressDialog progressDialog;
    private TextView nameTv;
    private TextView emailTv;
    private TextView totalRideTv;
    private TextView distanceTv;
    private TextView mobileTv;
    private TextView addressTv;
    private PrefManager prefManager;
    private CircularImageView picProfileIv;
    private ImageLoader im;
    private Context ctx;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_profile);
        ctx=Profile.this;

        editProfileLL = (LinearLayout) findViewById(R.id.profile_edit_profile_ll);
        nameTv = (TextView) findViewById(R.id.profile_user_name_tv);
        TextView emailLabelTv = (TextView) findViewById(R.id.profile_user_email_label_tv);
        emailTv = (TextView) findViewById(R.id.profile_user_email_tv);
        TextView passwordLabelTv = (TextView) findViewById(R.id.profile_user_password_label_tv);
        TextView passwordTv = (TextView) findViewById(R.id.profile_user_password_tv);
        TextView totalRideLabelTv = (TextView) findViewById(R.id.profile_user_total_ride_label_tv);
        totalRideTv = (TextView) findViewById(R.id.profile_user_total_ride_tv);
        TextView distanceLabelTv = (TextView) findViewById(R.id.profile_user_ditance_km_label_tv);
        distanceTv = (TextView) findViewById(R.id.profile_user_ditance_km_tv);
        TextView mobileLabelTv = (TextView) findViewById(R.id.profile_user_mobile_label_tv);
        mobileTv = (TextView) findViewById(R.id.profile_user_mobile_tv);
        TextView addressLabelTv = (TextView) findViewById(R.id.profile_user_address_label_tv);
        addressTv = (TextView) findViewById(R.id.profile_user_address_tv);

        picProfileIv=(CircularImageView)findViewById(R.id.profile_user_pic_iv);
        prefManager=new PrefManager(ctx);

        Typeface face = Typeface.createFromAsset(ctx.getAssets(), "fonts/OpenSans-Regular.ttf");
        nameTv.setTypeface(face);
        emailLabelTv.setTypeface(face);
        emailTv.setTypeface(face);

        passwordLabelTv.setTypeface(face);
        passwordTv.setTypeface(face);
        totalRideLabelTv.setTypeface(face);
        totalRideTv.setTypeface(face);
        distanceLabelTv.setTypeface(face);
        distanceTv.setTypeface(face);
        mobileLabelTv.setTypeface(face);
        mobileTv.setTypeface(face);
        addressLabelTv.setTypeface(face);
        addressTv.setTypeface(face);

        listenerView();

        try {
            progressDialog = new ProgressDialog(ctx);
            progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            progressDialog.show();
            progressDialog.setContentView(R.layout.progressbar);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        String driver_id = prefManager.getKeyUserId();
        getProfileDetails(ctx, Constant.GET_PROFILE_POST_REQUEST_URL, driver_id);
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    private void listenerView() {
        editProfileLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(ctx,EditProfile.class);
                startActivity(i);
            }
        });

    }


    public void getProfileDetails(Context context, String url, final String did) {
        Log.d("get profile:", " url = " + url);

        RequestQueue rq = Volley.newRequestQueue(context);
        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("get profile response ", "= " + response);
                progressDialog.dismiss();
                JSONArray jsonArray = Model.getArray(response);
                try {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String first_name=Model.getString(jsonObject,"first_name");
                        String last_name=Model.getString(jsonObject,"last_name");
                        String user_email=Model.getString(jsonObject,"user_email");
                        String address=Model.getString(jsonObject,"address");
                        String dob=Model.getString(jsonObject,"dob");
                        String gender=Model.getString(jsonObject,"gender");
                        String contact=Model.getString(jsonObject,"contact");
                        String age=Model.getString(jsonObject,"age");
                        String trips=Model.getString(jsonObject,"trips");
                        String kms=Model.getString(jsonObject,"kms");

                        nameTv.setText(first_name+" "+last_name);
                        emailTv.setText(user_email);
                        mobileTv.setText(contact);

                        totalRideTv.setText(trips);
                        distanceTv.setText(kms+" km");

                        String image = Model.getString(jsonObject, "profile_pic");

                        im = CustomVolleyRequest.getInstance(ctx).getImageLoader();

                        addressTv.setText(address);

                        im = CustomVolleyRequest.getInstance(ctx).getImageLoader();
                        picProfileIv.setImageUrl(Constant.PROFILE_IMAGE_URL +image, im);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.e("dkp error ocurred", "Error");
                VolleyLog.d("Error", "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("error ocurred", "TimeoutError");
                } else if (error instanceof AuthFailureError) {
                    Log.e("error ocurred", "AuthFailureError");

                } else if (error instanceof ServerError) {
                    Log.e("error ocurred", "ServerError");
                } else if (error instanceof NetworkError) {
                    Log.e("error ocurred", "NetworkError");

                } else if (error instanceof ParseError) {
                    Log.e("error ocurred", "ParseError");

                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                String[] tag = {"driver_id"};
                String[] value = {did};

                for (int i = 0; i < tag.length; i++)
                    params.put(tag[i], value[i]);

                return checkParams(params);
            }
        };

        postReq.setRetryPolicy(new DefaultRetryPolicy(
                800000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(postReq);
    }

    private Map<String, String> checkParams(Map<String, String> map) {
        Iterator<Map.Entry<String, String>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> pairs = (Map.Entry<String, String>) it.next();
            if (pairs.getValue() == null) {
                map.put(pairs.getKey(), "");
            }
        }
        return map;
    }
}
