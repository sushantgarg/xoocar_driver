package com.xoocardriver.view.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.PersistableBundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.theartofdev.edmodo.cropper.CropImage;
import com.xoocardriver.R;
import com.xoocardriver.helper.Constant;
import com.xoocardriver.helper.MyApplication;
import com.xoocardriver.helper.SecondPageFragmentListener;
import com.xoocardriver.helper.SetPreviewImageListner;
import com.xoocardriver.model.Model;
import com.xoocardriver.preference.PrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.HashMap;
import java.util.Map;


public class EditProfile extends AppCompatActivity implements SecondPageFragmentListener, SetPreviewImageListner {

    private ProgressDialog progressDialog;
    private EditText addressTv;
    private PrefManager prefManager;
    private String driver_id;
    private Button updatBtn;
    private EditText fnameTv;
    private EditText lnameTv;
    private String lname;
    private String fname;
    private String address_text;
    private NetworkImageView editPicIv;
    public ImageLoader mImageLoadered;
    private static final int SELECT_PICTURE = 100;
    SecondPageFragmentListener secondPageFragmentListener;
    SetPreviewImageListner setPreviewImageListner;
    private Bitmap imageBitmap=null;private LinearLayout uploadPicLL;
    private Uri outPutfileUri;
    private int TAKE_PIC=1;
    private Context ctx;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_edit_profile);
        ctx=EditProfile.this;

        secondPageFragmentListener = this;
        setPreviewImageListner = this;

        updatBtn = (Button) findViewById(R.id.edit_profile_user_update_btn);
        fnameTv = (EditText) findViewById(R.id.edit_profile_user_first_name_tv);
        TextView fnameLabelTv = (TextView) findViewById(R.id.edit_profile_user_first_name_label_tv);
        lnameTv = (EditText) findViewById(R.id.edit_profile_user_last_name__tv);
        TextView lnameLabelTv = (TextView) findViewById(R.id.edit_profile_user_last_name_label_tv);
        uploadPicLL=(LinearLayout)findViewById(R.id.edit_prof_imageview_user_profile);
        TextView addressLabelTv = (TextView) findViewById(R.id.edit_profile_user_address_label_tv);
        addressTv = (EditText) findViewById(R.id.edit_profile_user_address_tv);
        editPicIv = (NetworkImageView) findViewById(R.id.edit_profile_iv);
        prefManager=new PrefManager(ctx);
        Typeface face = Typeface.createFromAsset(ctx.getAssets(), "fonts/OpenSans-Regular.ttf");
        fnameTv.setTypeface(face);
        fnameLabelTv.setTypeface(face);
        lnameTv.setTypeface(face);
        lnameLabelTv.setTypeface(face);
        addressLabelTv.setTypeface(face);
        addressTv.setTypeface(face);

        listenerView();

        try {
            progressDialog = new ProgressDialog(ctx);
            progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            progressDialog.show();
            progressDialog.setContentView(R.layout.progressbar);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        driver_id=prefManager.getKeyUserId();
        getProfileDetails(ctx, Constant.GET_PROFILE_POST_REQUEST_URL,driver_id);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void getTextFromEditText() {
        lname = lnameTv.getText().toString();
        fname = fnameTv.getText().toString();
        address_text = addressTv.getText().toString();
    }

    private void listenerView() {
        updatBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    progressDialog = new ProgressDialog(ctx);
                    progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
                    progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                    progressDialog.show();
                    progressDialog.setContentView(R.layout.progressbar);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                driver_id=prefManager.getKeyUserId();
                getTextFromEditText();
                postProfileDetails(ctx, Constant.UPDATGE_PROFILE_POST_REQUEST_URL,driver_id,lname,fname,"","",address_text);
            }
        });

        uploadPicLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                DialogFragmentChooseImage dialogFragmentChooseImage = new DialogFragmentChooseImage(mainActivity, secondPageFragmentListener);
//                dialogFragmentChooseImage.show( mainActivity.getSupportFragmentManager(), "");
            }
        });

    }

    private void initView(View view) {

    }

    public void getProfileDetails(Context context, String url, final String did) {
        Log.d("get profile:", " url = " + url);

        RequestQueue rq = Volley.newRequestQueue(context);
        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("get profile response ", "= " + response);
                progressDialog.dismiss();
                JSONArray jsonArray = Model.getArray(response);
                try {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String first_name=Model.getString(jsonObject,"first_name");
                        String last_name=Model.getString(jsonObject,"last_name");
                        String address=Model.getString(jsonObject,"address");
                        fnameTv.setText(first_name);
                        lnameTv.setText(last_name);
                        addressTv.setText(address);
                        String image = Model.getString(jsonObject, "profile_pic");
                        mImageLoadered = MyApplication.getInstance().getImageLoader();
                        editPicIv.setImageUrl(Constant.PROFILE_IMAGE_URL +image, mImageLoadered);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.e("dkp error ocurred", "Error");
                VolleyLog.d("Error", "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("error ocurred", "TimeoutError");
                    // Toast.makeText(context, "TimeoutError", Toast.LENGTH_LONG).show();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                String[] tag = {"driver_id"};
                String[] value = {did};

                for (int i = 0; i < tag.length; i++)
                    params.put(tag[i], value[i]);

                return checkParams(params);
            }
        };

        postReq.setRetryPolicy(new DefaultRetryPolicy(
                800000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(postReq);
    }

    public void ShowDialog(Context mMainActivity,String message_test) {
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        final Dialog imageDialog = new Dialog(mMainActivity);
        imageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        imageDialog.setCancelable(true);
        imageDialog.setCanceledOnTouchOutside(true);

        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);

        LayoutInflater inflater = (LayoutInflater) mMainActivity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.confirm_dailog_success_show, null);
        TextView message = (TextView) layout.findViewById(R.id.dialog_success_message_tv);
        TextView ok = (TextView) layout.findViewById(R.id.dialog_ok_tv);
        message.setText(message_test);
        ok.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                imageDialog.dismiss();
            }
        });
        layout.setLayoutParams(param);
        imageDialog.setContentView(layout, param);
        imageDialog.show();
    }

    public void uploadImage(Context context, String url, final String Imagebase64,final String driver_id) {
        Log.d("upload image:", " url = " + url+":driver_id:"+driver_id+":Imagebase64:"+Imagebase64);

        RequestQueue rq = Volley.newRequestQueue(context);
        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("upload image response ", "= " + response);
                progressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.has("response")) {
                        String status = jsonObject.getString("response");
                        if (status.equals("1")) {
                            Toast.makeText(ctx, "Pic upload successfully", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(ctx, "Pic not upload successfully", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(ctx, "Change not password successfully", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.e("dkp error ocurred", "Error");
                VolleyLog.d("Error", "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("error ocurred", "TimeoutError");
                    // Toast.makeText(context, "TimeoutError", Toast.LENGTH_LONG).show();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                String[] tag = {"customer_id","image"};
                String[] value = {driver_id,Imagebase64};

                for (int i = 0; i < tag.length; i++)
                    params.put(tag[i], value[i]);

                return checkParams(params);
            }
        };

        postReq.setRetryPolicy(new DefaultRetryPolicy(
                800000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(postReq);
    }

    public void postProfileDetails(final Context context, String url, final String did, final String lname, final String fname, final String age, final String dob_text, final String address_text) {
        Log.d("get profile:", " url = " + url);

        RequestQueue rq = Volley.newRequestQueue(context);
        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("post profile response ", "= " + response);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.has("Success"))
                    {
                       // Toast.makeText(context,"Your request update successfully",Toast.LENGTH_SHORT).show();
                        ShowDialog(context,"Your request update successfully");
                    }
                    else
                    {
                        ShowDialog(context,"Your request not update successfully");
                      //  Toast.makeText(context,"Your request not update successfully",Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.e("dkp error ocurred", "Error");
                VolleyLog.d("Error", "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("error ocurred", "TimeoutError");
                    // Toast.makeText(context, "TimeoutError", Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    Log.e("error ocurred", "AuthFailureError");
                    //  Toast.makeText(context, "AuthFailureError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    Log.e("error ocurred", "ServerError");
                    // Toast.makeText(context, "ServerError", Toast.LENGTH_LONG).show();
                } else if (error instanceof NetworkError) {
                    Log.e("error ocurred", "NetworkError");
                    // Toast.makeText(context, "NetworkError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    Log.e("error ocurred", "ParseError");
                    //Toast.makeText(context, "ParseError", Toast.LENGTH_LONG).show();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                String[] tag = {"driver_id","f_name","l_name","age","dob","address"};
                String[] value = {did,fname,lname,age,dob_text,address_text};

                for (int i = 0; i < tag.length; i++)
                    params.put(tag[i], value[i]);
                return checkParams(params);
            }
        };

        postReq.setRetryPolicy(new DefaultRetryPolicy(
                800000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(postReq);
    }
    private Map<String, String> checkParams(Map<String, String> map) {
        for (Map.Entry<String, String> pairs : map.entrySet()) {
            if (pairs.getValue() == null) {
                map.put(pairs.getKey(), "");
            }
        }
        return map;
    }

    public String getStringImage(Bitmap bmp)
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        return Base64.encodeToString(imageBytes, Base64.DEFAULT);
    }

    @Override
    public void callImageChooser(String choose_option) {
        if (choose_option.equals("camera")) {
            takeImageFromCamera();
        } else if (choose_option.equals("gallery")) {
           // chooseFromGallery();
        }
    }

    @Override
    public void setImage(Bitmap image) {
        Log.d("meet1", "meet1");
        imageBitmap=image;
        if (imageBitmap!=null)
        {
            String  image_base64=getStringImage(imageBitmap);
            try {
                progressDialog = new ProgressDialog(ctx);
                progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
                progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                progressDialog.show();
                progressDialog.setContentView(R.layout.progressbar);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            driver_id=prefManager.getKeyUserId();
            uploadImage(ctx, Constant.UPLOAD_PROFILE_PIC_POST_REQUEST_URL, image_base64, driver_id);
        }
        else
            {
            Toast.makeText(ctx, "Please Select Image", Toast.LENGTH_SHORT).show();
        }
        editPicIv.setImageBitmap(image);
    }

    public void takeImageFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = new File(Environment.getExternalStorageDirectory(), "Profile_Picture.jpg");
        outPutfileUri = Uri.fromFile(file);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, outPutfileUri);
        startActivityForResult(intent, TAKE_PIC);
    }

    public void choosefromgallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == TAKE_PIC && resultCode == RESULT_OK) {
            if (null != outPutfileUri) {
                crop_Method(outPutfileUri);
            }
        }

        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                Uri selectedImageUri = data.getData();
                if (null != selectedImageUri) {
                    crop_Method(selectedImageUri);
                }
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), resultUri);
                    if (bitmap != null) {
                        Drawable d = new BitmapDrawable(getResources(), bitmap);
                        imageBitmap=bitmap;
                        if (imageBitmap!=null)
                        {
                            String  image_base64=getStringImage(imageBitmap);
                            try {
                                progressDialog = new ProgressDialog(ctx);
                                progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
                                progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                                progressDialog.show();
                                progressDialog.setContentView(R.layout.progressbar);
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }
                            driver_id=prefManager.getKeyUserId();
                            uploadImage(ctx, Constant.UPLOAD_PROFILE_PIC_POST_REQUEST_URL, image_base64, driver_id);
                        }
                        else
                        {
                            Toast.makeText(ctx, "Please Select Image", Toast.LENGTH_SHORT).show();
                        }
                        editPicIv.setImageBitmap(bitmap);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    public void crop_Method(Uri imageUri) {
        CropImage.activity(imageUri).start(EditProfile.this);
    }
}