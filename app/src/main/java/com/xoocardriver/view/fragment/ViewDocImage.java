package com.xoocardriver.view.fragment;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.xoocardriver.R;
import com.xoocardriver.helper.MyApplication;


public class ViewDocImage extends AppCompatActivity {

    private ImageView crossIv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_view_doc_image);

        Intent i=getIntent();
        String url = i.getStringExtra("url");
        String name = i.getStringExtra("name");

        TextView nameTv = (TextView) findViewById(R.id.view_doc_header_text_full_iv);
        crossIv=(ImageView)findViewById(R.id.view_doc_image_full_cross_iv) ;
        NetworkImageView networkImageView = (NetworkImageView) findViewById(R.id.view_doc_image_full_iv);
        nameTv.setText(name);
        ImageLoader im = MyApplication.getInstance().getImageLoader();
        networkImageView.setImageUrl(url, im);

        Typeface face = Typeface.createFromAsset( getAssets(), "fonts/OpenSans-Regular.ttf");
        nameTv.setTypeface(face);        listenerView();
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    private void listenerView() {
        crossIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewDocImage.super.onBackPressed();
            }
        });
    }

}
