package com.xoocardriver.view.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.xoocardriver.IncentiveActivity;
import com.xoocardriver.InviteDriver;
import com.xoocardriver.R;
import com.xoocardriver.Training;
import com.xoocardriver.adapter.VehicleLIstAdapter;
import com.xoocardriver.helper.Constant;
import com.xoocardriver.helper.CustomVolleyRequest;
import com.xoocardriver.model.Model;
import com.xoocardriver.model.VihicleListHome;
import com.xoocardriver.preference.PrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by elite on 19/12/16.
 */

public class Home extends AppCompatActivity {
    private ImageView go_online;
    private Spinner vehicleSp;
    private ProgressDialog progressDialog;
    private ArrayList<VihicleListHome> vehicleList;
    private ArrayList<String> vehicleName;
    private ArrayList<String> vehicelId;
    private String vehicel_id = "0";
    private PrefManager prefManager;
    private String driver_id;
    private String v_id = "0";
    private ImageLoader im;
    private Context ctx;

    private ImageView menuIcon;
    private DrawerLayout navigationDrawer;

    //    drawer layout
    private LinearLayout invite_fireds_tab;
    private LinearLayout menu_invite_driver_ll;
    private LinearLayout document_tab;
    private LinearLayout menu_training_ll;
    private LinearLayout waybillLL;
    private LinearLayout currentTripLL;
    private LinearLayout earningLL;
    private LinearLayout termConditionLL;
    private LinearLayout helpLL;
    private LinearLayout logoutLL;
    private LinearLayout policyLL;
    private LinearLayout about;
    private LinearLayout viewdocumentLL;
    private LinearLayout uploadDocumentLL;
    private LinearLayout incentivelayout;
    private LinearLayout emerContactLL;
    private LinearLayout profileLL;
    private LinearLayout document_sub_category;
    private ImageView expand_or_collapse;
    private int click_time = 0;       // how and hide document sub tag
    String currentVersion = "6.4";
    private String vehicleNum;
    private String selectedModel;
    private String selectedVehCategory;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_home);
        ctx=Home.this;
        prefManager = new PrefManager(ctx);

        go_online = (ImageView) findViewById(R.id.go_online);
        vehicleSp = (Spinner) findViewById(R.id.vehicle_sp);
        TextView earnUpLabelTv = (TextView) findViewById(R.id.select_vehicle_earn_up_tv);

//        drawer layout
        menuIcon = (ImageView) findViewById(R.id.menuIconHome);
        navigationDrawer = (DrawerLayout) findViewById(R.id.navigationDrawerHome);

        invite_fireds_tab = (LinearLayout) findViewById(R.id.menu_invite_friends_ll);
        menu_invite_driver_ll = (LinearLayout) findViewById(R.id.menu_invite_driver_ll);
        document_tab = (LinearLayout) findViewById(R.id.document_tab);
        profileLL = (LinearLayout) findViewById(R.id.menu_profile_ll);
        emerContactLL = (LinearLayout) findViewById(R.id.menu_emergency_contact_ll);
        incentivelayout = (LinearLayout) findViewById(R.id.menu_incentive_ll);
        uploadDocumentLL = (LinearLayout) findViewById(R.id.menu_upload_document_ll);
        viewdocumentLL = (LinearLayout) findViewById(R.id.menu_view_document_ll);
        about = (LinearLayout) findViewById(R.id.about);
        policyLL = (LinearLayout) findViewById(R.id.menu_policy_ll);
        logoutLL = (LinearLayout) findViewById(R.id.logout_ll);
        helpLL = (LinearLayout) findViewById(R.id.menu_help_ll);
        termConditionLL = (LinearLayout) findViewById(R.id.menu_term_condition_ll);
        earningLL = (LinearLayout) findViewById(R.id.menu_earnings_ll);
        currentTripLL = (LinearLayout) findViewById(R.id.menu_trip_history_ll);
        waybillLL = (LinearLayout) findViewById(R.id.menu_way_bill_ll);
        menu_training_ll = (LinearLayout) findViewById(R.id.menu_training_ll);
        document_sub_category = (LinearLayout) findViewById(R.id.document_sub_category);
        expand_or_collapse = (ImageView) findViewById(R.id.expand_or_collapse);


        Typeface face = Typeface.createFromAsset( getAssets(), "fonts/OpenSans-Regular.ttf");
        earnUpLabelTv.setTypeface(face);

        listenerView();

        try {
            progressDialog = new ProgressDialog( ctx);
            progressDialog.setMessage("loading");
            progressDialog.show();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        if( prefManager.getDriverOnlineOfflineStatus().equals("1") ){
            Intent i=new Intent(Home.this,OfflineOnline.class);
            startActivity(i);
        }

        driver_id = prefManager.getKeyUserId();
        String oid = prefManager.getOid();
        getAllVehicle(ctx, Constant.ALL_VEHICLE_POST_REQUEST_URL, oid);

        getProfileDetails(ctx, Constant.GET_PROFILE_POST_REQUEST_URL, driver_id);

    }

    @Override
    public void onResume() {
        super.onResume();
        new GetVersionCode().execute();
    }

    public void initView(View view) {
    }

    public void listenerView() {

        menuIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigationDrawer.openDrawer(GravityCompat.START);

            }
        });

        go_online.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!v_id.equals("0")) {
                    go_online.setImageResource(R.drawable.on_duty);
                    progressDialog = new ProgressDialog(ctx);
                    progressDialog.setMessage("loading");
                    progressDialog.show();

                    prefManager.setVehicleId(vehicel_id);
                    prefManager.setVehicleNum(vehicleNum);
                    prefManager.setVehicleModel(selectedModel);
                    prefManager.setVehicleCategory(selectedVehCategory);
                    changeOnlineStatus(ctx, Constant.DRIVER_STATUS_POST_REQUEST_URL, driver_id, "1", v_id);
                }
            }
        });

        vehicleSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                vehicel_id = vehicleList.get(position).getVCategory();
                v_id = vehicleList.get(position).getId();
                vehicleNum=vehicleList.get(position).getVLicenceNo();

                selectedModel=vehicleList.get(position).getVVehiclename();
                selectedVehCategory=vehicleList.get(position).getVCategory();

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        invite_fireds_tab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (navigationDrawer.isDrawerOpen(GravityCompat.START)) {
                    navigationDrawer.closeDrawer(GravityCompat.START);
                }
                Intent i=new Intent(Home.this,InviteFriends.class);
                startActivity(i);
            }
        });

        menu_invite_driver_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(Home.this, InviteDriver.class);
                startActivity(i);
            }
        });


//        document_tab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (click_time == 0) {
//                    document_sub_category.setVisibility(View.VISIBLE);
//                    click_time = 1;
//                    expand_or_collapse.setImageResource(R.mipmap.collapse);
//                } else {
//                    document_sub_category.setVisibility(View.GONE);
//                    click_time = 0;
//                    expand_or_collapse.setImageResource(R.mipmap.expand);
//                }
//            }
//        });
//
//        profileLL.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View view) {
//                if (navigationDrawer.isDrawerOpen(GravityCompat.START)) {
//                    navigationDrawer.closeDrawer(GravityCompat.START);
//                }
//                Intent i=new Intent(Home.this,Profile.class);
//                startActivity(i);
//            }
//        });
//
//        emerContactLL.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (navigationDrawer.isDrawerOpen(GravityCompat.START)) {
//                    navigationDrawer.closeDrawer(GravityCompat.START);
//                }
//
//                Intent i=new Intent(Home.this,EmergencyContactFragment.class);
//                startActivity(i);
//            }
//        });
//
//        incentivelayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i=new Intent(Home.this, IncentiveActivity.class);
//                startActivity(i);
//            }
//        });
//
//        uploadDocumentLL.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (navigationDrawer.isDrawerOpen(GravityCompat.START)) {
//                    navigationDrawer.closeDrawer(GravityCompat.START);
//                }
//                Intent i=new Intent(Home.this,UploadDocument.class);
//                startActivity(i);
//            }
//        });
//
//        viewdocumentLL.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (navigationDrawer.isDrawerOpen(GravityCompat.START)) {
//                    navigationDrawer.closeDrawer(GravityCompat.START);
//                }
//                Intent i=new Intent(Home.this,ViewDocument.class);
//                startActivity(i);
//            }
//        });
//
//        about.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (navigationDrawer.isDrawerOpen(GravityCompat.START)) {
//                    navigationDrawer.closeDrawer(GravityCompat.START);
//                }
//                Intent i=new Intent(Home.this,AboutUsFragment.class);
//                i.putExtra("url","http://www.xoocar.com/webapp/webapp/aboutus");
//                startActivity(i);
//
//            }
//        });
//        policyLL.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (navigationDrawer.isDrawerOpen(GravityCompat.START)) {
//                    navigationDrawer.closeDrawer(GravityCompat.START);
//                }
//
//                Intent i=new Intent(Home.this,AboutUsFragment.class);
//                i.putExtra("url","http://www.xoocar.com/webapp/webapp/policies");
//                startActivity(i);
//
//            }
//        });
//
//        helpLL.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (navigationDrawer.isDrawerOpen(GravityCompat.START)) {
//                    navigationDrawer.closeDrawer(GravityCompat.START);
//                }
//                Intent i=new Intent(Home.this,AboutUsFragment.class);
//                i.putExtra("url","http://www.xoocar.com/webapp/webapp/contactus");
//                startActivity(i);
//
//            }
//        });
//
//        termConditionLL.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (navigationDrawer.isDrawerOpen(GravityCompat.START)) {
//                    navigationDrawer.closeDrawer(GravityCompat.START);
//                }
//                Intent i=new Intent(Home.this,AboutUsFragment.class);
//                i.putExtra("url","http://www.xoocar.com/webapp/webapp/terms");
//                startActivity(i);
//
//            }
//        });
//
//        currentTripLL.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                if (!isRingerActive) {
//                if (navigationDrawer.isDrawerOpen(GravityCompat.START)) {
//                    navigationDrawer.closeDrawer(GravityCompat.START);
//                }
//                Intent i=new Intent(Home.this,TripList.class);
//                startActivity(i);
////                } else {
////                    Toast.makeText(Home.this, "Not allowed during ringer", Toast.LENGTH_SHORT).show();
////                }
//            }
//        });
//
//        earningLL.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (navigationDrawer.isDrawerOpen(GravityCompat.START)) {
//                    navigationDrawer.closeDrawer(GravityCompat.START);
//                }
//                Intent i=new Intent(Home.this,TripEarning.class);
//                startActivity(i);
//            }
//        });
//
//        waybillLL.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (navigationDrawer.isDrawerOpen(GravityCompat.START)) {
//                    navigationDrawer.closeDrawer(GravityCompat.START);
//                }
//                Intent i=new Intent(Home.this,Waybill.class);
//                startActivity(i);
//
//            }
//        });

        logoutLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (navigationDrawer.isDrawerOpen(GravityCompat.START)) {
                    navigationDrawer.closeDrawer(GravityCompat.START);
                }
                prefManager.setKeyUserId(null);
                Intent i=new Intent(ctx,SignIn.class);
                startActivity(i);

            }
        });

//        menu_training_ll.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                Intent i=new Intent( Home.this, Training.class );
//                startActivity(i);
//            }
//        });
    }

    @Override
    public void onDestroy() {
        if(progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();

        super.onDestroy();
    }

    public void getProfileDetails(Context context, String url, final String did) {
        Log.d("get profile:", " url = " + url);
        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("get profile response ", "= " + response);
                JSONArray jsonArray = Model.getArray(response);
                try {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String first_name = Model.getString(jsonObject, "first_name");
                        String last_name = Model.getString(jsonObject, "last_name");
                        String image = Model.getString(jsonObject, "profile_pic");
                        prefManager.setProfilePic(image);
                        prefManager.setKeyUserName(first_name + " " + last_name);
                        im = CustomVolleyRequest.getInstance(ctx).getImageLoader();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("dkp error ocurred", "Error");
                VolleyLog.d("Error", "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("error ocurred", "TimeoutError");
                    // Toast.makeText(context, "TimeoutError", Toast.LENGTH_LONG).show();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                String[] tag = {"driver_id"};
                String[] value = {did};
                for (int i = 0; i < tag.length; i++)
                    params.put(tag[i], value[i]);
                return checkParams(params);
            }
        };
        postReq.setRetryPolicy(new DefaultRetryPolicy(
                8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        CustomVolleyRequest.getInstance(ctx).addToRequestQueue(postReq, "profile_request");
    }

    public void changeOnlineStatus(Context context, String url, final String driverid, String status, final String vehicel_id) {
        Log.d("post vehicle:", " url = " + url + " driverid :" + driverid + " status:" + status + " vehicel_id :" + vehicel_id);
        RequestQueue rq = Volley.newRequestQueue(context);
        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("post driver status ", "= " + response);
                if( progressDialog!= null && progressDialog.isShowing() )
                progressDialog.dismiss();

                JSONArray jsonArray = Model.getArray(response);
                try {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String d_current_status = jsonObject.getString("d_current_status");
                        String sessionId = jsonObject.getString("sessionid");
                        prefManager.setSessionId(sessionId);
                        if (d_current_status.equals("1")) {

                            prefManager.setDriverOnlineOfflineStatus("1");
                            Intent intent=new Intent(ctx,OfflineOnline.class);
                            startActivity(intent);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if( progressDialog!= null && progressDialog.isShowing() )
                progressDialog.dismiss();
                Log.e("dkp error occurred", "Error");
                VolleyLog.d("Error", "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("error occurred", "TimeoutError");
                    // Toast.makeText(context, "TimeoutError", Toast.LENGTH_LONG).show();
                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                String[] tag = {"act_mode", "d_id", "status", "vechile_id", "dtoken", "dtype"};
                String[] value = {"driver_status", driverid, "1", vehicel_id,prefManager.getFcmToken(),"Android"};

                for (int i = 0; i < tag.length; i++)
                    params.put(tag[i], value[i]);

                return checkParams(params);
            }
        };

        postReq.setRetryPolicy(new DefaultRetryPolicy(
                800000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(postReq);
    }


    public void getAllVehicle(Context context, String url, final String oid) {
        Log.d("all vehicle:", " url = " + url + ":oid:" + oid);

        RequestQueue rq = Volley.newRequestQueue(context);
        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("all vehicle response ", "= " + response);
                if(progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();
                JSONArray jsonArray = Model.getArray(response);

                try {
                    vehicleList = new ArrayList<>();
                    vehicleName = new ArrayList<>();
                    vehicelId = new ArrayList<>();
                    vehicleName.add("Select vehicle type");
                    vehicelId.add("0");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        VihicleListHome veVihicleListHome = new VihicleListHome();
                        veVihicleListHome.setVVehiclename(jsonObject.getString("v_vehiclename"));
                        veVihicleListHome.setId(jsonObject.getString("Id"));
                        veVihicleListHome.setOId(jsonObject.getString("o_id"));
                        veVihicleListHome.setVLicenceNo(jsonObject.getString("v_licence_no"));
                        veVihicleListHome.setVCategory(jsonObject.getString("v_category"));
                        vehicleName.add(jsonObject.getString("v_vehiclename"));
                        vehicelId.add(jsonObject.getString("Id"));
                        vehicleList.add(veVihicleListHome);
                    }

                    Log.d("name vehicelId", "sizes :" + vehicelId.size());
                    Log.d("name vehicleName", "sizes :" + vehicleName.size());

                    VehicleLIstAdapter vehicleLIstAdapter = new VehicleLIstAdapter(ctx, vehicleList);
                    //  ArrayAdapter<String> city_adapter = new ArrayAdapter<String>(mainActivity, R.layout.custom_sp_item_city,R.id.custom_city_tv, vehicleName);
                    vehicleSp.setAdapter(vehicleLIstAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if( progressDialog!= null && progressDialog.isShowing() )
                progressDialog.dismiss();
                Log.e("dkp error occurred", "Error");
                VolleyLog.d("Error", "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("error occurred", "TimeoutError");
                    // Toast.makeText(context, "TimeoutError", Toast.LENGTH_LONG).show();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                String[] tag = {"act_mode", "oid"};
                String[] value = {"myvehicle", oid};

                for (int i = 0; i < tag.length; i++)
                    params.put(tag[i], value[i]);

                return checkParams(params);
            }
        };

        postReq.setRetryPolicy(new DefaultRetryPolicy(
                8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(postReq);
    }

    private Map<String, String> checkParams(Map<String, String> map) {
        for (Map.Entry<String, String> pairs : map.entrySet()) {
            if (pairs.getValue() == null) {
                map.put(pairs.getKey(), "");
            }
        }
        return map;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public class GetVersionCode extends AsyncTask<Void, String, String> {
        @Override
        protected String doInBackground(Void... voids) {

            String newVersion = "";
            try {
                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + ctx.getPackageName() + "&hl=it")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div[itemprop=softwareVersion]")
                        .first()
                        .ownText();
                return newVersion;
            } catch (Exception e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(String onlineVersion) {
            super.onPostExecute(onlineVersion);
            if (onlineVersion != null && !onlineVersion.isEmpty()) {
                if (Float.valueOf(currentVersion) < Float.valueOf(onlineVersion)) {
                    ShowDialog((Activity) ctx, getString(R.string.version_upgrade_msg));

                }
                Log.d("update", "Current version " + currentVersion + "playstore version " + onlineVersion);
            }
        }

        void ShowDialog(Activity mMainActivity, String msg) {
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            final Dialog imageDialog = new Dialog(mMainActivity);
            imageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            imageDialog.setCancelable(false);
            imageDialog.setCanceledOnTouchOutside(false);
            imageDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            LayoutInflater inflater = (LayoutInflater) mMainActivity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.layout_update_dialoge, null);
            TextView mTextView = (TextView) layout.findViewById(R.id.id_network_ok);
            TextView mTextmsg = (TextView) layout.findViewById(R.id.id_network_not);
            mTextmsg.setText(msg);
            mTextView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    Intent in = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.xoocardriver&hl=en"));
                    startActivity(in);
                    imageDialog.dismiss();
                }
            });
            layout.setLayoutParams(param);
            imageDialog.setContentView(layout, param);
            imageDialog.show();
        }


    }

}
