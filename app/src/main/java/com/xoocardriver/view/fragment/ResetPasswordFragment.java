//package com.xoocardriver.view.fragment;
//
//import android.graphics.Typeface;
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.text.InputType;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.CheckBox;
//import android.widget.CompoundButton;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.android.volley.AuthFailureError;
//import com.android.volley.DefaultRetryPolicy;
//import com.android.volley.NetworkError;
//import com.android.volley.NoConnectionError;
//import com.android.volley.ParseError;
//import com.android.volley.Request;
//import com.android.volley.RequestQueue;
//import com.android.volley.Response;
//import com.android.volley.ServerError;
//import com.android.volley.TimeoutError;
//import com.android.volley.VolleyError;
//import com.android.volley.VolleyLog;
//import com.android.volley.toolbox.StringRequest;
//import com.android.volley.toolbox.Volley;
//
//import com.google.android.gms.analytics.HitBuilders;
//import com.google.android.gms.analytics.Tracker;
//import com.xoocardriver.GoogleAnalytic.AnalyticsApplication;
//import com.xoocardriver.R;
//import com.xoocardriver.helper.Constant;
//import com.xoocardriver.helper.Validator;
//import com.xoocardriver.model.Model;
//import com.xoocardriver.model.Registration;
//import com.xoocardriver.preference.PrefManager;
//import com.xoocardriver.view.activity.MainActivity;
//
//import org.json.JSONArray;
//import org.json.JSONObject;
//
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.Map;
//
//public class ResetPasswordFragment extends Fragment {
//
//    private MainActivity welcomeActivity;
//    private Button registerBtn;
//    private ImageView mbackIv;
//    private Button mresetBtn;
//    private EditText mNesPassTv;
//    private EditText mCPassTv;
//    private String npass;
//    private String cpass;
//
//    private String mobile;
//    private CheckBox showPasswordCB;
//    private Tracker mTracker;
//
//    public ResetPasswordFragment(MainActivity welcomeActivity,String mobile) {
//        this.welcomeActivity = welcomeActivity;
//        this.mobile=mobile;
//
//    }
//
//    public ResetPasswordFragment() {
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        View v = inflater.inflate(R.layout.fragment_reset_password, container, false);
//        initView(v);
//        listenerView();
//        AnalyticsApplication application = (AnalyticsApplication) welcomeActivity.getApplication();
//        mTracker = application.getDefaultTracker();
//        return v;
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        mTracker.setScreenName("Reset Password");
//        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
//    }
//
//
//    private void listenerView() {
//
//        showPasswordCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if(!isChecked) {
//                    mCPassTv.setInputType(129);
//                } else {
//                    mCPassTv.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
//                }
//            }
//        });
//
//        mresetBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                validate();
//            }
//        });
//
//        mbackIv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                welcomeActivity.getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout,new OtpFragment(mobile),"OtpFragment").commitAllowingStateLoss();
//            }
//        });
//    }
//
//    /*Initialization of view*/
//    private void initView(View v) {
//
//        showPasswordCB=(CheckBox)v.findViewById(R.id.reset_show_password_cb);
//        mresetBtn = (Button) v.findViewById(R.id.reset_in_submit_btn);
//        TextView mheaderTv = (TextView) v.findViewById(R.id.reset_in_header_tv);
//        mNesPassTv = (EditText) v.findViewById(R.id.reset_new_pass_edt);
//        mCPassTv = (EditText) v.findViewById(R.id.reset_cpass_edt);
//        mbackIv = (ImageView) v.findViewById(R.id.reset_in_back_iv);
//
//        PrefManager prefManager = new PrefManager(welcomeActivity);
//        prefManager.setOtpMobile(mobile);
//
//        Typeface face = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Regular.ttf");
//        mresetBtn.setTypeface(face);
//
//        mheaderTv.setTypeface(face);
//
//    }
//
//    public void getTextFromEditText() {
//        npass = mNesPassTv.getText().toString();
//        cpass = mCPassTv.getText().toString();
//    }
//
//    public void validate() {
//        getTextFromEditText();
//        if (Validator.mobValid(mNesPassTv, getContext())) {
//            if (Validator.validPassword(mCPassTv, getContext())) {
//                    getLoginRequest(welcomeActivity, Constant.RESET_PASS_POST_REQUEST_URL, npass,cpass);
//
//            } else {
//                mCPassTv.requestFocus();
//
//            }
//        } else {
//            mNesPassTv.requestFocus();
//
//        }
//    }
//
//        public void getLoginRequest (MainActivity context, String url, final String npass, final String cpass){
//            Log.d("reset:", " url = " + url + "npass = " + npass + "cpass = "+cpass);
//
//            RequestQueue rq = Volley.newRequestQueue(context);
//            StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
//                @Override
//                public void onResponse(String response) {
//                    Log.d("reset response ", "= " + response);
//                    try {
//
//
//                        JSONArray jsonArray = Model.getArray(response);
//                        Registration reg = new Registration();
//                        for (int i = 0; i < jsonArray.length(); i++) {
//                            JSONObject jsonObject = Model.getObject(jsonArray, i);
//                            if (jsonObject.has("Success")) {
//                                String status = jsonObject.getString("Success");
//                                if (status.equals("Success")) {
//                                    Toast.makeText(getActivity(), "Your password reset successfully", Toast.LENGTH_LONG).show();
//                                }
//                               else
//                                {
//                                }
//                            } else {
//                            }
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    VolleyLog.d("Error", "Error: " + error.getMessage());
//                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
//                        Log.d("error ocurred", "TimeoutError");
//                    } else if (error instanceof AuthFailureError) {
//                        Log.d("error ocurred", "AuthFailureError");
//
//                    } else if (error instanceof ServerError) {
//                        Log.d("error ocurred", "ServerError");
//
//                    } else if (error instanceof NetworkError) {
//                        Log.d("error ocurred", "NetworkError");
//
//                    } else if (error instanceof ParseError) {
//                        Log.d("error ocurred", "ParseError");
//
//                    }
//                }
//            }) {
//
//                @Override
//                protected Map<String, String> getParams() throws AuthFailureError {
//                    Map<String, String> params = new HashMap<>();
//
//                    Log.d(" dkp npass", ":" + npass);
//                    Log.d(" dkp cpass", ":" + cpass);
//                    String[] tag = {"act_mode","contactno","password"};
//                    String[] value = {"forgetpass",npass,cpass};
//
//                    for (int i = 0; i < tag.length; i++)
//                        params.put(tag[i], value[i]);
//                    return checkParams(params);
//                }
//            };
//            postReq.setRetryPolicy(new DefaultRetryPolicy(
//                    800000,
//                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//            rq.add(postReq);
//        }
//
//        private Map<String, String> checkParams (Map< String, String > map){
//            Iterator<Map.Entry<String, String>> it = map.entrySet().iterator();
//            while (it.hasNext()) {
//                Map.Entry<String, String> pairs = (Map.Entry<String, String>) it.next();
//                if (pairs.getValue() == null) {
//                    map.put(pairs.getKey(), "");
//                }
//            }
//            return map;
//    }
//}