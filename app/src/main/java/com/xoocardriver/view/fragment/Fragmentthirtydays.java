/*
package com.xoocardriver.view.fragment;


import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.xoocardriver.GoogleAnalytic.AnalyticsApplication;
import com.xoocardriver.R;
import com.xoocardriver.adapter.CabEarningAdapter;
import com.xoocardriver.adapter.Datewiselistadapter;
import com.xoocardriver.adapter.Grossamountadapter;
import com.xoocardriver.adapter.RideEarningAdaper;
import com.xoocardriver.adapter.Thirtydaysadapter;
import com.xoocardriver.adapter.TripListAdapter;
import com.xoocardriver.model.Datewaisedata;
import com.xoocardriver.model.Model;
import com.xoocardriver.model.MyRides;
import com.xoocardriver.model.Thirtydays;
import com.xoocardriver.model.VehicleItem;
import com.xoocardriver.preference.PrefManager;
import com.xoocardriver.view.activity.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;





public class Fragmentthirtydays extends Fragment {

    MainActivity mainActivity;

    private RecyclerView recyclerView;
    private PrefManager prefManager;
    private ProgressDialog progressDialog;
    private ArrayList<Thirtydays> list_arr;


    String date;

    private ProgressBar progressBar;
TextView datetext;

    private Tracker mTracker;

    public Fragmentthirtydays(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        MainActivity.toolbarText.setText("Day summary");
    }

    public Fragmentthirtydays() {
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activitythirtydays, container, false);


        try {
            progressDialog = new ProgressDialog(mainActivity);
            progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            progressDialog.show();
            progressDialog.setContentView(R.layout.progressbar);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd ");
        Calendar cal = Calendar.getInstance();

        cal.add(Calendar.DAY_OF_YEAR, -30);
        System.out.println(sdf.format(cal.getTime()));

// loop adding one day in each iteration
        String m = null;
        for(int i = 0; i< 30; i++){
            cal.add(Calendar.DAY_OF_YEAR, 1);
            System.out.println(sdf.format(cal.getTime()));
            //Log.e("sdf.format(sdf.format(cal.getTime()",sdf.format(cal.getTime()));
            if (i==0)
            {
                m=sdf.format(cal.getTime());
            }
        }
        String n=  sdf.format(cal.getTime());
        date = m +" - "+n;
        Log.d("mmmmmdd",m);
        Log.d("nnnnndd",n);
        prefManager = new PrefManager(mainActivity);
        String driver_id = prefManager.getKeyUserId();
        gettripdata(mainActivity,"",driver_id,n,m);
        int start_limit = 0;
       // getgrossdata(mainActivity, driver_id, date_from);
        start_limit = 0;
        AnalyticsApplication application = (AnalyticsApplication) mainActivity.getApplication();
        mTracker = application.getDefaultTracker();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        try {
            Log.d("onStart ", " url = " + "call on start");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mTracker.setScreenName("Trip list");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    public void initView(View view) {

        datetext =(TextView)view.findViewById(R.id.datetextview);
        recyclerView = (RecyclerView) view.findViewById(R.id.trip_list_rl);
       // recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mainActivity);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setLayoutManager(linearLayoutManager);

    }
    public void gettripdata(MainActivity context, String s1, final String driver_id, final String n, final String m) {
        final RequestQueue rq = Volley.newRequestQueue(context);
        final StringRequest postReq = new StringRequest(Request.Method.POST, "http://xoocar.com/api/mobileapp/datewisedetails/format/json", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("dkp0000 response ", "= " + response);
                progressDialog.dismiss();
                try {
                    JSONObject json=new JSONObject(response);
                    JSONArray jsonArray = json.getJSONArray("app_ride_details_datewise");
                    list_arr=new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = Model.getObject(jsonArray, i);
                        Thirtydays list = new Thirtydays();
                        list.setCancelledride(jsonObject.getString("Canceled_ride"));
                        list.setCompleteride(jsonObject.getString("Completed_ride"));
                        list.setDate(jsonObject.getString("datee"));
                        list.setShcedule(jsonObject.getString("Scheduled"));
                        list.setDriverid(jsonObject.getString("driver_id"));
                        list.setTotalprice(jsonObject.getString("total_price"));
                        list.setTotalride(jsonObject.getString("total_ride"));
                        list_arr.add(list);
                    }

                    Thirtydaysadapter gram=new Thirtydaysadapter(mainActivity,list_arr);
                    recyclerView.setAdapter(gram);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.d("dkp 1 error ocurred", "TimeoutError");
                VolleyLog.d("Error", "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.d("error ocurred", "TimeoutError");
                    // Toast.makeText(context, "TimeoutError", Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    Log.d("error ocurred", "AuthFailureError");
                    //  Toast.makeText(context, "AuthFailureError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    Log.d("error ocurred", "ServerError");
                    // Toast.makeText(context, "ServerError", Toast.LENGTH_LONG).show();
                } else if (error instanceof NetworkError) {
                    Log.d("error ocurred", "NetworkError");
                    // Toast.makeText(context, "NetworkError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    Log.d("error ocurred", "ParseError");
                    //Toast.makeText(context, "ParseError", Toast.LENGTH_LONG).show();
                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                String[] tag = {"driver_id", "date_max","date_min"};
                String[] value = {driver_id,n,m};
                Log.d("date_max",n);
                Log.d("date_min",m);



                for (int i = 0; i < tag.length; i++)
                    params.put(tag[i], value[i]);
                return checkParams(params);

            }
        };
        postReq.setRetryPolicy(new DefaultRetryPolicy(
                800000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(postReq);
    }
    private Map<String, String> checkParams(Map<String, String> map) {
        Iterator<Map.Entry<String, String>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> pairs = (Map.Entry<String, String>) it.next();
            if (pairs.getValue() == null) {
                map.put(pairs.getKey(), "");
            }
        }
        return map;
    }
}

*/
