//package com.xoocardriver.view.fragment;
//
//import android.app.ProgressDialog;
//import android.content.Context;
//import android.graphics.Typeface;
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.text.InputType;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.CheckBox;
//import android.widget.CompoundButton;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.android.volley.AuthFailureError;
//import com.android.volley.DefaultRetryPolicy;
//import com.android.volley.NoConnectionError;
//import com.android.volley.Request;
//import com.android.volley.RequestQueue;
//import com.android.volley.Response;
//import com.android.volley.TimeoutError;
//import com.android.volley.VolleyError;
//import com.android.volley.VolleyLog;
//import com.android.volley.toolbox.StringRequest;
//import com.android.volley.toolbox.Volley;
//import com.xoocardriver.R;
//import com.xoocardriver.helper.Constant;
//import com.xoocardriver.helper.Validator;
//import com.xoocardriver.preference.PrefManager;
//
//import org.json.JSONObject;
//
//import java.util.HashMap;
//import java.util.Map;
//
//public class ChangePassword extends Fragment {
//
//    private Button mSubmitBtn;
//    private ImageView mbackIv;
//    private PrefManager prefManager;
//    private String device_id;
//    private EditText currentPassTv;
//    private EditText newPassTv;
//    private EditText confirmPassTv;
//    private String cu_pass;
//    private String new_pass;
//    private String confirm_pass;
//    private ProgressDialog progressDialog;
//    private ImageView homeLogoIv;
//    private CheckBox showPasswordCB;
//    private Context ctx;
//
//    public ChangePassword() {
//    }
//
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        ctx=context;
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        View v = inflater.inflate(R.layout.fragment_change_password, container, false);
//        initView(v);
//        listenerView();
//        return v;
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//    }
//
//    /*Make click listener for view*/
//    private void listenerView() {
//        showPasswordCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (!isChecked) {
//                    newPassTv.setInputType(129);
//                    currentPassTv.setInputType(129);
//                    confirmPassTv.setInputType(129);
//                } else {
//                    newPassTv.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
//                    currentPassTv.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
//                    confirmPassTv.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
//                }
//            }
//        });
//
//
//        mSubmitBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                validate();
//            }
//        });
//
//        homeLogoIv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                try {
//                    String status = prefManager.getDriverOnlineOfflineStatus();
//                    String driver_id = prefManager.getKeyUserId();
//                    Log.d("dkp status", ":" + status);
//                    if (status != null && !status.isEmpty()) {
//                        if (status.equals("0")) {
//                            getFragmentManager().beginTransaction().add(R.id.frame_layout, new Home(), "home").commitAllowingStateLoss();
//                        } else {
//                            getFragmentManager().beginTransaction().add(R.id.frame_layout, new OfflineOnline(), "OfflineOnline").commitAllowingStateLoss();
//                        }
//                    } else {
//                        getFragmentManager().beginTransaction().add(R.id.frame_layout, new Home(), "home").commitAllowingStateLoss();
//                    }
//                } catch (Throwable e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//
//        mbackIv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (getFragmentManager().getBackStackEntryCount() == 0) {
//                } else {
//                    getFragmentManager().popBackStack();
//                }
//            }
//        });
//    }
//
//    /*Initialization of view*/
//    private void initView(View v) {
//        showPasswordCB = (CheckBox) v.findViewById(R.id.change_pass_show_password_cb);
//        mSubmitBtn = (Button) v.findViewById(R.id.change_pass_submit_btn);
//        currentPassTv = (EditText) v.findViewById(R.id.change_pass_current_tv);
//        newPassTv = (EditText) v.findViewById(R.id.change_pass_new_tv);
//        confirmPassTv = (EditText) v.findViewById(R.id.change_pass_confirm_tv);
//        mbackIv = (ImageView) v.findViewById(R.id.change_pass_iv);
//        TextView mheaderTv = (TextView) v.findViewById(R.id.change_pass_in_header_tv);
//        homeLogoIv = (ImageView) v.findViewById(R.id.change_pass_home_logo_iv);
//        prefManager = new PrefManager(getActivity());
//        device_id = prefManager.getKeyUserId();
//
//        Typeface face = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Regular.ttf");
//        mSubmitBtn.setTypeface(face);
//        currentPassTv.setTypeface(face);
//        newPassTv.setTypeface(face);
//        confirmPassTv.setTypeface(face);
//        mheaderTv.setTypeface(face);
//    }
//
//    public void getTextFromEditText() {
//
//        cu_pass = currentPassTv.getText().toString();
//        new_pass = newPassTv.getText().toString();
//        confirm_pass = confirmPassTv.getText().toString();
//
//    }
//
//    public void validate() {
//        getTextFromEditText();
//        if (Validator.validPassword(currentPassTv, getActivity())) {
//            if (Validator.validPassword(newPassTv, getActivity())) {
//                if (Validator.validPassword(confirmPassTv, getActivity())) {
//                    if (new_pass.equals(confirm_pass)) {
//                        try {
//                            progressDialog = new ProgressDialog(ctx);
//                            progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
//                            progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
//                            progressDialog.show();
//                            progressDialog.setContentView(R.layout.progressbar);
//                        } catch (NullPointerException e) {
//                            e.printStackTrace();
//                        }
//                        getLoginRequest(ctx, Constant.CHANGE_PASSWORD_POST_REQUEST_URL, cu_pass, new_pass, confirm_pass);
//                    }else {
//                        confirmPassTv.requestFocus();
//                        confirmPassTv.setError("Both passwords do not match");
//                    }
//                } else {
//                    confirmPassTv.requestFocus();
//                    //Toast.makeText(main, "Password should be of at least 6 characters", Toast.LENGTH_LONG).show();
//                }
//            } else {
//                newPassTv.requestFocus();
//                //Toast.makeText(main, "Password should be of at least 6 characters", Toast.LENGTH_LONG).show();
//            }
//        } else {
//            currentPassTv.requestFocus();
//            //Toast.makeText(main, "Password should be of at least 6 characters", Toast.LENGTH_LONG).show();
//        }
//    }
//
//    public void getLoginRequest(Context context, String url, final String cur_pass, final String new_pass, final String cun_pass) {
//        Log.d("change pass:", " url = " + url + " username = " + " cur_pass" + cur_pass + "new_pass = " + new_pass + "cun_pass = " + cun_pass + "device id= " + device_id);
//
//        RequestQueue rq = Volley.newRequestQueue(context);
//        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.d("change pass response ", "= " + response);
//                try {
//                    progressDialog.dismiss();
//                    JSONObject jsonObject = new JSONObject(response);
//                    if (jsonObject.has("response")) {
//                        String status = jsonObject.getString("response");
//                        if (status.equals("1")) {
//                            Toast.makeText(getActivity(), "Change password successfully", Toast.LENGTH_LONG).show();
//                        } else {
//                            Toast.makeText(getActivity(), "Change not password successfully", Toast.LENGTH_LONG).show();
//                        }
//                    } else {
//                        Toast.makeText(getActivity(), "Change not password successfully", Toast.LENGTH_LONG).show();
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                progressDialog.dismiss();
//                VolleyLog.d("Error", "Error: " + error.getMessage());
//                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
//                    Log.d("error ocurred", "TimeoutError");
//                    // Toast.makeText(context, "TimeoutError", Toast.LENGTH_LONG).show();
//                }
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> params = new HashMap<>();
//                String[] tag = {"userid", "oldpwd", "newpwd"};
//                String[] value = {device_id, cur_pass, new_pass};
//                for (int i = 0; i < tag.length; i++)
//                    params.put(tag[i], value[i]);
//                return checkParams(params);
//            }
//        };
//        postReq.setRetryPolicy(new DefaultRetryPolicy(
//                800000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        rq.add(postReq);
//    }
//
//    private Map<String, String> checkParams(Map<String, String> map) {
//        for (Map.Entry<String, String> pairs : map.entrySet()) {
//            if (pairs.getValue() == null) {
//                map.put(pairs.getKey(), "");
//            }
//        }
//        return map;
//    }
//}
