/*
package com.xoocardriver.view.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.Tracker;
import com.xoocardriver.GoogleAnalytic.AnalyticsApplication;
import com.xoocardriver.R;
import com.xoocardriver.preference.PrefManager;
import com.xoocardriver.view.activity.MainActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.xoocardriver.view.activity.MainActivity.mainActivity;

public class Dashboard extends Fragment {
    private MainActivity maiActivity;
    private Button registerBtn, mforgotBtn;
    private TextView complete,cancel,currentdate,dateformat,booking,bookincounted,wallet,cash,total,completrs,cancelrs,tsrs,bookingrs,bookingctdrs,cashpaymentrs,ters,bpmrs;
 private ProgressDialog progressDialog;
    java.util.Calendar myCalendar;
    ImageView arrowimg;
    private EditText mPassTv, mMobileTv;
    private ImageView clnderimg,img1;
    private String e_date = "";
    private String mobile;
    private PrefManager prefManager;
    private Tracker mTracker;

    public Dashboard(MainActivity mainActivity) {
        this.maiActivity = mainActivity;
        MainActivity.toolbarText.setText("Dashboard");
    }
    public Dashboard() {
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
         View view = inflater.inflate(R.layout.fragmentdashboard, container, false);

        initView(view);
        listenerView();
       // initView(view);
        java.util.Calendar c = java.util.Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String todayDate = df.format(c.getTime());

        try {
            progressDialog = new ProgressDialog(maiActivity);
            progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            progressDialog.show();
            progressDialog.setContentView(R.layout.progressbar);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        //  String driver_id = prefManager.getKeyUserId();
        //getEarnigApi(maiActivity, Constant.EARNING_DETAILS_POST_REQUEST, driver_id, todayDate);
        AnalyticsApplication application = (AnalyticsApplication) maiActivity.getApplication();
        mTracker = application.getDefaultTracker();
        return view;
    }
       // listenerView();
    private void initView(View v) {
        complete = (TextView) v.findViewById(R.id.completedtextview);
        cancel = (TextView) v.findViewById(R.id.cancelridetext);
        currentdate = (TextView) v.findViewById(R.id.currentdatetext);
        dateformat = (TextView) v.findViewById(R.id.dateformattext);
        booking = (TextView) v.findViewById(R.id.booking);
        bookincounted = (TextView) v.findViewById(R.id.bookingcounted);
        wallet = (TextView) v.findViewById(R.id.wallettext);
        cash = (TextView) v.findViewById(R.id.cashpaymenttext);
        total = (TextView) v.findViewById(R.id.totalearning);
        completrs = (TextView) v.findViewById(R.id.numerictext);
        cancelrs = (TextView) v.findViewById(R.id.numerictextcancel);
        tsrs = (TextView) v.findViewById(R.id.rupeetext);
        bookingrs = (TextView) v.findViewById(R.id.bookingrupees);
        bookingctdrs = (TextView) v.findViewById(R.id.bookingcountedrupees);
        cashpaymentrs = (TextView) v.findViewById(R.id.cashpaymentrupees);
        ters = (TextView) v.findViewById(R.id.totalearningrupees);
        bpmrs = (TextView) v.findViewById(R.id.bankpaymentrupeetext);
        clnderimg = (ImageView) v.findViewById(R.id.imageviewsummary);
       // arrowimg =(ImageView) v.findViewById(R.id.arrowsignimage);
        Typeface face = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Regular.ttf");
        complete.setTypeface(face);
        cancel.setTypeface(face);
        currentdate.setTypeface(face);
        dateformat.setTypeface(face);
        booking.setTypeface(face);
        bookincounted.setTypeface(face);
    }
    final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            myCalendar.set(java.util.Calendar.YEAR,year);
            myCalendar.set(java.util.Calendar.MONTH,monthOfYear);
            myCalendar.set(java.util.Calendar.DAY_OF_MONTH,dayOfMonth);
            updateEnd();
        }
    };
    public void listenerView() {
        clnderimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(maiActivity,date, myCalendar.get(java.util.Calendar.YEAR), myCalendar.get(java.util.Calendar.MONTH), myCalendar.get(java.util.Calendar.DAY_OF_MONTH)).show();
            }
        });
        currentdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog dialog = new Dialog(getActivity());

// Set the title
                dialog.setTitle("Please Select one ");

// inflate the layout
                dialog.setContentView(R.layout.activity_popup);

// Set the dialog text -- this is better done in the XML
                TextView text = (TextView)dialog.findViewById(R.id.currentdatetext);
                text.setText("CHECK DEATILS FOR");

                TextView textView =(TextView)dialog.findViewById(R.id.todaytesxt);
                textView.setText("Today");
                TextView textView1 =(TextView) dialog.findViewById(R.id.dateformattext);
                textView1.setText("Yesterday");

                TextView textView2 =(TextView) dialog.findViewById(R.id.last7daydstext);
                textView2.setText("Last 7 days");

                TextView textView3 =(TextView) dialog.findViewById(R.id.last30daydstext);
                textView3.setText("Last 30 days");
                TextView textView4 =(TextView) dialog.findViewById(R.id.customtext);
                textView4.setText("Custom");

// Display the dialog
                dialog.show();


            }

        });

        wallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



            }
        });
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            myCalendar.set(java.util.Calendar.YEAR, year);
            myCalendar.set(java.util.Calendar.MONTH, monthOfYear);
            myCalendar.set(java.util.Calendar.DAY_OF_MONTH, dayOfMonth);
            updateEnd();
        }
    };
}

    private LayoutInflater getLayoutInflater() {
        return null;
    }

    private void updateEnd() {
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        e_date = sdf.format(myCalendar.getTime());

        java.util.Calendar c = java.util.Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String todayDate = df.format(c.getTime());
        Log.d("current_date", ":" + todayDate);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date fdate = null;
        try {
            fdate = dateFormat.parse(e_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date cdate = null;
        try {
            cdate = dateFormat.parse(todayDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (fdate.before(cdate) || fdate.equals(cdate)) {
           // clnderimg.setI(sdf.format(myCalendar.getTime()));
            try {
                progressDialog = new ProgressDialog(maiActivity);
                progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
                progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                progressDialog.show();
                progressDialog.setContentView(R.layout.progressbar);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            String driver_id = prefManager.getKeyUserId();
           // getEarnigApi(mainActivity, Constant.EARNING_DETAILS_POST_REQUEST, driver_id, e_date);
        } else {
            Toast.makeText(mainActivity, "Please select before or equal to current date", Toast.LENGTH_SHORT).show();
        }

    }


    }





*/
