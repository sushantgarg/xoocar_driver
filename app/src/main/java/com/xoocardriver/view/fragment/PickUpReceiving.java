package com.xoocardriver.view.fragment;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.xoocardriver.R;
import com.xoocardriver.RetroFit.CancelRideScheduledFields;
import com.xoocardriver.RetroFit.CancelRideScheduledRequestData;
import com.xoocardriver.RetroFit.CancelRideScheduledResponceFields;
import com.xoocardriver.RetroFit.RequestInterface;
import com.xoocardriver.helper.Constant;
import com.xoocardriver.helper.MyApplication;
import com.xoocardriver.map.GeocodingLocation;
import com.xoocardriver.map.MapDirectionsParser;
import com.xoocardriver.preference.PrefManager;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by elite on 20/12/16.
 */

public class PickUpReceiving extends AppCompatActivity implements   OnMapReadyCallback, View.OnClickListener{
    int click = 0;
    private String rideid;
    private String c_first_name;
    private String c_source;
    private String c_destination;
    private GoogleMap googleMap;
    private LinearLayout callPhoneLL;
    private LinearLayout cancelLL;
    private LinearLayout shareLL;
    private LinearLayout navigate;
    private LinearLayout locateCustomer;
    private double latitude = 0, longitude = 0;
    private ArrayList<LatLng> points = null;
    private Polyline mPolyline = null;
    private LatLng source_LatLong;
    private LatLng driver_LatLong;
    private String city_id;
    private PrefManager prefManager;
    private ProgressDialog progressDialog;
    private String contact;
    private String first_name;
    private String last_name;
    private Context ctx;
    private Dialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_pick_up_receiving);

        ctx=PickUpReceiving.this;

        TextView pick_up_text = (TextView) findViewById(R.id.pickup_receiving_source_h_tv);
        TextView pickupTv = (TextView)     findViewById(R.id.pickup_receiving_source_tv);
        TextView dropTv = (TextView)       findViewById(R.id.pickup_receiving_destination_h_tv);
        TextView drop_up_text = (TextView) findViewById(R.id.pickup_receiving_destination_tv);

        prefManager = new PrefManager(ctx);

        FragmentManager fragmentManager = getSupportFragmentManager();

        SupportMapFragment mMapFragment = (SupportMapFragment) fragmentManager.findFragmentById(R.id.pickup_receiving_location_map);

        if ( mMapFragment == null ) {
            mMapFragment = SupportMapFragment.newInstance();
            fragmentManager.beginTransaction().replace(R.id.pickup_receiving_location_map, mMapFragment).commit();
        }

        mMapFragment.getMapAsync(this);

        Intent i=getIntent();

        rideid =        i.getStringExtra("rideid") ;
        String c_id = i.getStringExtra("c_id");
        c_first_name =  i.getStringExtra("c_first_name");
        c_source =      i.getStringExtra("c_source");
        c_destination = i.getStringExtra("c_destination");
        String c_profile_pic = i.getStringExtra("c_profile_pic");
        String d_latt = i.getStringExtra("d_latt");
        String d_long = i.getStringExtra("d_long");
        city_id =       i.getStringExtra("city_id");
        contact =       i.getStringExtra("contact");
        first_name =    i.getStringExtra("first_name");
        last_name =     i.getStringExtra("last_name");
        String c_lat = i.getStringExtra("c_lat");
        String c_lng = i.getStringExtra("c_lng");

        callPhoneLL = (LinearLayout) findViewById(R.id.pickup_arring_phone_call_ll);
        cancelLL = (LinearLayout)    findViewById(R.id.pickup_arring_cancel_ride_ll);
        shareLL = (LinearLayout)     findViewById(R.id.pickup_arring_share_ll);
        navigate = (LinearLayout)    findViewById(R.id.navigate);
        locateCustomer = (LinearLayout) findViewById(R.id.locateCustomer);
        TextView callTv = (TextView)    findViewById(R.id.pickup_arring_call_driver_dd_tv);
        TextView cancelTv = (TextView)  findViewById(R.id.pickup_arring_cancel_ride_dd_tv);
        Typeface face = Typeface.createFromAsset(ctx.getAssets(), "fonts/OpenSans-Regular.ttf");
        pick_up_text.setTypeface(face);
        pickupTv.setTypeface(face);
        dropTv.setTypeface(face);
        drop_up_text.setTypeface(face);
        callTv.setTypeface(face);
        cancelTv.setTypeface(face);
        pickupTv.setText(c_source);
        drop_up_text.setText(c_destination);

        listenerView();

        try {
            assert c_source != null;
            source_LatLong = new LatLng(Double.parseDouble(c_lat),Double.parseDouble(c_lng));
            latitude=source_LatLong.latitude;
            longitude=source_LatLong.longitude;

            if (googleMap != null) {

                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(latitude, longitude)).zoom(17)
                        .build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    public void listenerView() {

        navigate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                driver_LatLong = new LatLng(prefManager.getCurrentLat(), prefManager.getCurrentLng());
                if (source_LatLong != null) {
                    final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?saddr=" + driver_LatLong.latitude + "," + driver_LatLong.longitude + "&daddr=" + source_LatLong.latitude + "," + source_LatLong.longitude));
                    intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                    startActivity(intent);
                } else {
                    Toast.makeText(ctx, "Customer location not available", Toast.LENGTH_SHORT).show();
                }
            }
        });

        locateCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    String vehicle_id = prefManager.getVehicleId();
                    progressDialog = new ProgressDialog(ctx);
                    progressDialog.setMessage("loading...");
                    progressDialog.show();

                    pickupApi(ctx, Constant.PICKUP_POST_REQUEST_URL, city_id, vehicle_id, rideid);
            }
        });

        cancelLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showStopCastingDialog();
            }
        });

        callPhoneLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phone = contact;
                try {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                    startActivity(intent);
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }
        });

        shareLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                byte[] data = new byte[0];
                try {
                    Log.d("dkp share", ":link:" + rideid);
                    data = rideid.getBytes("UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                String base64 = Base64.encodeToString(data, Base64.DEFAULT);
                String share_link_base64 = Constant.SHARE_LINK + base64;

                Log.d("dkp share", ":link:" + share_link_base64);
                share_link_base64 = share_link_base64.replace("==", "");

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, first_name + " " + last_name + " is on ride with Xoocar. Follow his ride " + share_link_base64);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });

    }

    private void addMarks1( GoogleMap googleMap, String pickup, String drop ) {
        try {
            googleMap.clear();
            driver_LatLong = new LatLng(latitude, longitude);
            String driver = GeocodingLocation.getCompleteAddressString(ctx, latitude, longitude);
            MarkerOptions markerOptions = new MarkerOptions();
            MarkerOptions markerOptions2 = new MarkerOptions();
            markerOptions.position(source_LatLong);
            markerOptions.title("Pickup");
            markerOptions.snippet(pickup);
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.green_location_old));
            googleMap.addMarker(markerOptions);

            markerOptions2.position(driver_LatLong);
            markerOptions2.title("Driver");
            markerOptions2.snippet(driver);
            markerOptions2.icon(BitmapDescriptorFactory.fromResource(R.drawable.driver_black));
            googleMap.addMarker(markerOptions2);

            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(source_LatLong, 17);
            googleMap.animateCamera(cameraUpdate);
            traceMe();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void pickupApi(final Context context, String url, final String city_id, final String vehicle_id, final String rideid) {
        RequestQueue rq = Volley.newRequestQueue(context);
        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("pickup  response ", "= " + response);
                if(progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                    progressDialog.cancel();
                }
                prefManager.setRideIdCheck(rideid);
                prefManager.setRideId(rideid);

                Intent i=new Intent(context,StartTrip.class);
                i.putExtra("rideid",rideid);
                i.putExtra("c_first_name",c_first_name);
                i.putExtra("c_source",c_source);
                i.putExtra("c_destination",c_destination);
                startActivity(i);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                VolleyLog.d("Error", "Error: " + error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                String[] tag = {"city_id", "cat_id", "ride_id"};
                String[] value = {city_id, vehicle_id, rideid};

                for (int i = 0; i < tag.length; i++)
                    params.put(tag[i], value[i]);
                return checkParams(params);
            }
        };

        postReq.setRetryPolicy(new DefaultRetryPolicy(
                800000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(postReq);
    }

    private Map<String, String> checkParams(Map<String, String> map) {
        for (Map.Entry<String, String> pairs : map.entrySet()) {
            if (pairs.getValue() == null) {
                map.put(pairs.getKey(), "");
            }
        }
        return map;
    }

    private void traceMe() {
        String srcParam = "";
        String destParam = "";
        // if (MainActivity.key == 1) {
        try {
            srcParam = source_LatLong.latitude + "," + source_LatLong.longitude;
            destParam = driver_LatLong.latitude + "," + driver_LatLong.longitude;
        } catch (Exception e) {
            e.printStackTrace();
        }

        String url = "https://maps.googleapis.com/maps/api/directions/json?origin=" + srcParam
                + "&destination=" + destParam + "&sensor=false&units=metric&mode=driving&key="
                + Constant.google_direction_purchage_key;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("dkp lat long", ":" + response);
                        MapDirectionsParser parser = new MapDirectionsParser(ctx);
                        List<List<HashMap<String, String>>> routes = parser.parse(response);

                        for (int i = 0; i < routes.size(); i++) {
                            points = new ArrayList<LatLng>();
                            // lineOptions = new PolylineOptions();
                            // Fetching i-th route
                            List<HashMap<String, String>> path = routes.get(i);
                            // Fetching all the points in i-th route
                            for (int j = 0; j < path.size(); j++) {
                                HashMap<String, String> point = path.get(j);
                                double lat = Double.parseDouble(point.get("lat"));
                                double lng = Double.parseDouble(point.get("lng"));
                                LatLng position = new LatLng(lat, lng);
                                points.add(position);
                            }
                        }
                        drawPoints(points, googleMap);
                        //  PD.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                        }
                    }
                });
        MyApplication.getInstance().addToRequestQueue(jsonObjectRequest, "jreq");
    }

    private void drawPoints(ArrayList<LatLng> points, GoogleMap mMaps) {
        if (points == null) {
            return;
        }
        PolylineOptions polylineOpt = new PolylineOptions();
        for (LatLng latlng : points) {
            polylineOpt.add(latlng);
        }
        polylineOpt.color(Color.BLACK);
        if (mPolyline != null) {
            mPolyline.remove();
            mPolyline = null;
        }
        if (mMaps != null) {
            mPolyline = mMaps.addPolyline(polylineOpt);
        }
        if (mPolyline != null)
            mPolyline.setWidth(7);
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onClick(View v) {
    }

    public void addCustomerMarker() {

            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(latitude, longitude)).zoom(17)
                    .build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            addMarks1(googleMap, c_source, c_destination);

            googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    marker.hideInfoWindow();
                    return true;
                }
            });
        }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        try {
            this.googleMap = googleMap;

            googleMap.getUiSettings().setZoomControlsEnabled(false);
            googleMap.getUiSettings().setMyLocationButtonEnabled(false);
            googleMap.getUiSettings().setCompassEnabled(true);
            googleMap.getUiSettings().setRotateGesturesEnabled(true);
            googleMap.getUiSettings().setZoomGesturesEnabled(true);
            googleMap.getUiSettings().setMapToolbarEnabled(true);

            if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            googleMap.setMyLocationEnabled(true);

            addCustomerMarker();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void rejectRide(String url, final String ride_id) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);

        Call<CancelRideScheduledResponceFields> call = request.CancelRideScheduledFields("api/processapi/cancelled_schedule_bydriver/format/json/"
                , new CancelRideScheduledFields( new CancelRideScheduledRequestData(prefManager.getKeyUserId(),ride_id,"I have changed my mind")));

        call.enqueue(new Callback<CancelRideScheduledResponceFields>() {
            @Override
            public void onResponse(Call<CancelRideScheduledResponceFields> call, retrofit2.Response<CancelRideScheduledResponceFields> response) {

                    prefManager.setRideIdCheck("0");
                    prefManager.setLastCancelledRideId(ride_id);
                    Intent i=new Intent(PickUpReceiving.this,OfflineOnline.class);
                    startActivity(i);
                    if(dialog != null && dialog.isShowing()) dialog.dismiss();

            }

            @Override
            public void onFailure(Call<CancelRideScheduledResponceFields> call, Throwable t) {
                prefManager.setRideIdCheck("0");
                prefManager.setLastCancelledRideId(ride_id);
                Intent i = new Intent(PickUpReceiving.this, OfflineOnline.class);
                startActivity(i);
                if (dialog != null && dialog.isShowing()) dialog.dismiss();
            }
        });

    }

    private void showStopCastingDialog() {

        dialog = new Dialog(ctx);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_cancel_reson);

        final RadioButton rb=((RadioButton) dialog.findViewById(R.id.cancelReasonCb));

        dialog.findViewById(R.id.book_confirm_custom_cancel_tv).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if ( rb.isChecked()){
//                            Todo :call cancel ride
                            rejectRide(Constant.REJECT_RIDE,prefManager.getRideIdCheck());
                        } else {
                            Toast.makeText(ctx, "Check reason first", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

        dialog.show();
    }

    @Override
    public void onBackPressed() {

    }
}
