//package com.xoocardriver.view.fragment;
//
//import android.content.Context;
//import android.graphics.Typeface;
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.android.volley.AuthFailureError;
//import com.android.volley.DefaultRetryPolicy;
//import com.android.volley.NetworkError;
//import com.android.volley.NoConnectionError;
//import com.android.volley.ParseError;
//import com.android.volley.Request;
//import com.android.volley.RequestQueue;
//import com.android.volley.Response;
//import com.android.volley.ServerError;
//import com.android.volley.TimeoutError;
//import com.android.volley.VolleyError;
//import com.android.volley.VolleyLog;
//import com.android.volley.toolbox.StringRequest;
//import com.android.volley.toolbox.Volley;
//
//import com.google.android.gms.analytics.HitBuilders;
//import com.google.android.gms.analytics.Tracker;
//import com.xoocardriver.GoogleAnalytic.AnalyticsApplication;
//import com.xoocardriver.R;
//import com.xoocardriver.helper.Constant;
//import com.xoocardriver.helper.Validator;
//import com.xoocardriver.preference.PrefManager;
//import com.xoocardriver.view.activity.MainActivity;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.Map;
//
//public class ForgotPasswordFragment extends Fragment {
////    private MainActivity maiActivity;
//    private Button  mforgotBtn;
//
//    private EditText mMobileTv;
//    private ImageView mbackIv;
//    private String mobile;
//    private PrefManager prefManager;
//    private Context ctx;
//
//    public ForgotPasswordFragment() {
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        View v = inflater.inflate(R.layout.fragment_forgot_password, container, false);
//        initView(v);
//        listenerView();
//        return v;
//    }
//
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        ctx=context;
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//    }
//
//    /*Make click listener for view*/
//    private void listenerView() {
//
//        mforgotBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                validate();
//            }
//        });
//        mbackIv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                getFragmentManager().beginTransaction().replace(R.id.frame_layout, new SignIn(), "sign_in").commitAllowingStateLoss();
//            }
//        });
//    }
//    /*Initialization of view*/
//    private void initView(View v) {
//        mforgotBtn = (Button) v.findViewById(R.id.forgot_in_submit_btn);
//        TextView mheaderTv = (TextView) v.findViewById(R.id.forgot_in_header_tv);
//        mMobileTv = (EditText) v.findViewById(R.id.forgot_in_email_edt);
//        mbackIv = (ImageView) v.findViewById(R.id.forgot_in_back_iv);
//        prefManager=new PrefManager(ctx);
//        Typeface face = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Regular.ttf");
//        mforgotBtn.setTypeface(face);
//        mMobileTv.setTypeface(face);
//        mheaderTv.setTypeface(face);
//    }
//    public void getTextFromEditText()
//    {
//        mobile=mMobileTv.getText().toString();
//    }
//    public void validate()
//    {
//        getTextFromEditText();
//        if (Validator.mobValid(mMobileTv, getContext())) {
//                getLoginRequest(ctx, Constant.NEW_CHECK_OTP_POST_REQUEST_URL,mobile);
//        } else {
//            mMobileTv.requestFocus();
//            //  mMobileTv.setError("Please enter valid mobile");//9719039102
//        }
//    }
//    public void getLoginRequest(Context context, String url, final String mobile) {
//        Log.d("forgot pass:", " url = " + url + "mobile = " + mobile + "device id= ");
//
//        RequestQueue rq = Volley.newRequestQueue(context);
//        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.d("forgot response ", "= " + response);
//                try {
//                    JSONObject jsonObject=new JSONObject(response);
//                    if (jsonObject.has("otp"))
//                    {
//                        prefManager.setOtp(jsonObject.getString("otp"));
//                        getFragmentManager().beginTransaction().replace(R.id.frame_layout,new OtpFragment(mobile),"OtpFragment").commitAllowingStateLoss();
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                VolleyLog.d("Error", "Error: " + error.getMessage());
//                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
//                    Log.d("error ocurred", "TimeoutError");
//                    // Toast.makeText(context, "TimeoutError", Toast.LENGTH_LONG).show();
//                } else if (error instanceof AuthFailureError) {
//                    Log.d("error ocurred", "AuthFailureError");
//                    //  Toast.makeText(context, "AuthFailureError", Toast.LENGTH_LONG).show();
//                } else if (error instanceof ServerError) {
//                    Log.d("error ocurred", "ServerError");
//                    // Toast.makeText(context, "ServerError", Toast.LENGTH_LONG).show();
//                } else if (error instanceof NetworkError) {
//                    Log.d("error ocurred", "NetworkError");
//                    // Toast.makeText(context, "NetworkError", Toast.LENGTH_LONG).show();
//                } else if (error instanceof ParseError) {
//                    Log.d("error ocurred", "ParseError");
//                    //Toast.makeText(context, "ParseError", Toast.LENGTH_LONG).show();
//                }
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> params = new HashMap<>();
//                Log.d(" dkp mobile",":"+ mobile);
//                String[] tag = {"contactno"};
//                String[] value = {mobile};
//                for (int i = 0; i < tag.length; i++)
//                    params.put(tag[i], value[i]);
//                return checkParams(params);
//            }
//        };
//        postReq.setRetryPolicy(new DefaultRetryPolicy(
//                800000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        rq.add(postReq);
//    }
//    private Map<String, String> checkParams(Map<String, String> map) {
//        for (Map.Entry<String, String> pairs : map.entrySet()) {
//            if (pairs.getValue() == null) {
//                map.put(pairs.getKey(), "");
//            }
//        }
//        return map;
//    }
//}