package com.xoocardriver.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.xoocardriver.R;
import com.xoocardriver.preference.PrefManager;


public class CancelRideFragment extends AppCompatActivity {
    public PrefManager prefManager;
    private Typeface face;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_cancel_ride);
        Context ctx = CancelRideFragment.this;

        prefManager = new PrefManager(ctx);
        TextView mheaderTv = (TextView)findViewById(R.id.cancel_in_header_tv);
        TextView msgTv = (TextView) findViewById(R.id.cancelled_message_tv);
        ImageView img = findViewById(R.id.cancelImg);
        mheaderTv.setTypeface(face);
        msgTv.setTypeface(face);

        Intent i = getIntent();
        String type = i.getStringExtra("type");

        if (type.equals("cancel")) {
            img.setImageResource(R.drawable.cance_ride_bg_icon);
            msgTv.setText("Your ride has been cancelled by the customer.");
        } else {
            img.setImageResource(R.drawable.missed_ride_svg);
            msgTv.setText(R.string.picked_by_someone);
        }

        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                String status = prefManager.getDriverOnlineOfflineStatus();
                Log.d("dkp status", ":" + status);
                if (status != null && !status.isEmpty()) {
                    if (status.equals("0")) {
                        Intent i=new Intent(CancelRideFragment.this,Home.class);
                        startActivity(i);
                    } else {
                        Intent i=new Intent(CancelRideFragment.this,OfflineOnline.class);
                        startActivity(i);
                    }
                } else {
                    Intent i=new Intent(CancelRideFragment.this,Home.class);
                    startActivity(i);
                }
            }
        }, 3000);

        face = Typeface.createFromAsset(ctx.getAssets(), "fonts/OpenSans-Regular.ttf");
    }


    @Override
    public void onResume() {
        super.onResume();
        prefManager.setRideIdCheck("0");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent i=new Intent(CancelRideFragment.this,OfflineOnline.class);
        startActivity(i);
        finish();
    }

    /*Make click listener for view*/


}