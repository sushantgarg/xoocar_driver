package com.xoocardriver.view.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.xoocardriver.R;
import com.xoocardriver.adapter.GetAllContactsAdapter;
import com.xoocardriver.helper.Constant;
import com.xoocardriver.model.Contact;
import com.xoocardriver.preference.PrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class EmergencyContactFragment extends AppCompatActivity {
    private Button addContactBtn;
    public static String contact_name="0";
    public static String contact_no="0";
    private ProgressDialog progressDialog;
    private PrefManager prefManager;
    private RecyclerView emergencyRv;
    public static LinearLayout notfoundTv;
    private ArrayList<Contact> contact_list;
    static final int PICK_CONTACT=12;
    private Context ctx;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_emergency_contact);
        ctx=EmergencyContactFragment.this;

        notfoundTv=(LinearLayout)findViewById(R.id.emergency_not_found_contact_ll);
        TextView tvBottomA = (TextView) findViewById(R.id.tv_bottom_a);
        TextView tvBottomB = (TextView) findViewById(R.id.tv_bottom_b);
        TextView tvBottomC = (TextView) findViewById(R.id.tv_bottom_c);
        TextView tvBottomD = (TextView) findViewById(R.id.tv_bottom_d);
        emergencyRv=(RecyclerView)findViewById(R.id.emergency_contact_rv) ;
        emergencyRv.setLayoutManager(new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false));
        addContactBtn=(Button)findViewById(R.id.emerg_contact_in_submit_btn);
        try {
            prefManager=new PrefManager(ctx);
        } catch (Throwable e) {
            e.printStackTrace();
        }

        Typeface face = Typeface.createFromAsset(ctx.getAssets(), "fonts/OpenSans-Regular.ttf");
        addContactBtn.setTypeface(face);
        tvBottomA.setTypeface(face);
        tvBottomB.setTypeface(face);
        tvBottomC.setTypeface(face);
        tvBottomD.setTypeface(face);

        setinitView();
        try {
            progressDialog = new ProgressDialog(ctx);
            progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            progressDialog.show();
            progressDialog.setContentView(R.layout.progressbar);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        getContact(ctx, Constant.GET_EMERGENCY_CONTACT_URL);

    }

    @Override
    public void onResume() {
        super.onResume();

        if (!contact_name.equals("0")||!contact_no.equals("0"))
        {
            try {
                progressDialog = new ProgressDialog(ctx);
                progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
                progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                progressDialog.show();
                progressDialog.setContentView(R.layout.progressbar);
            } catch (NullPointerException ignored) {
            }
            addContact(ctx, Constant.ADD_EMERGENCY_CONTACT_URL,contact_name,contact_no);
            contact_name="0";
            contact_no="0";
        }

    }

    private void setinitView() {
        addContactBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                startActivityForResult(intent, PICK_CONTACT);
            }
        });

        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
            }
        };

    }

    public void addContact(Context context, String url, final String c_name, final String c_no) {
        Log.d("Add contact:", " url = " + url+":c_name:"+c_name+":c_no:"+c_no);

        RequestQueue rq = Volley.newRequestQueue(context);
        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.d("add contact response ", "= " + response);
                try {
                    JSONArray jsonArray=new JSONArray(response);
                    JSONObject jsonObject=jsonArray.getJSONObject(0);
                    if (jsonObject.has("success"))
                    {
                        Toast.makeText(ctx,"Add contact successfully", Toast.LENGTH_LONG).show();
                        getContact(ctx, Constant.GET_EMERGENCY_CONTACT_URL);
                    }
                    else if (jsonObject.has("Allready exist"))
                    {
                        Toast.makeText(ctx,"Sorry, Contact Allready exist", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                VolleyLog.d("Error", "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.d("error ocurred", "TimeoutError");
                    // Toast.makeText(context, "TimeoutError", Toast.LENGTH_LONG).show();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                String uid= prefManager.getKeyUserId();
                Log.d(" dkp uid", ":" + uid);
                String[] tag = {"uid", "contact", "name"};
                String[] value = {uid,c_no,c_name};

                for (int i = 0; i < tag.length; i++)
                    params.put(tag[i], value[i]);
                return checkParams(params);
            }
        };
        postReq.setRetryPolicy(new DefaultRetryPolicy(
                800000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(postReq);
    }

    public void getContact(Context context, String url) {
        Log.d("get contact:", " url = " + url);

        RequestQueue rq = Volley.newRequestQueue(context);
        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("get contact response ", "= " + response);
                progressDialog.dismiss();

                try {
                    JSONArray jsonArray=new JSONArray(response);
                    contact_list=new ArrayList<>();
                    for (int i=0;i<jsonArray.length();i++)
                    {
                        Contact contact=new Contact();
                        JSONObject jsonObject=jsonArray.getJSONObject(i);
                        contact.setId(jsonObject.getString("id"));
                        contact.setUid(jsonObject.getString("uid"));
                        contact.setName(jsonObject.getString("name"));
                        contact.setContactNo(jsonObject.getString("contact_no"));
                        contact.setRStatus(jsonObject.getString("r_status"));
                        contact_list.add(contact);
                    }

                    GetAllContactsAdapter contactAdapter = new GetAllContactsAdapter(ctx,contact_list);
                    emergencyRv.setAdapter(contactAdapter);
                    notfoundTv.setVisibility(View.GONE);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                notfoundTv.setVisibility(View.VISIBLE);
                VolleyLog.d("Error", "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.d("error ocurred", "TimeoutError");
                    // Toast.makeText(context, "TimeoutError", Toast.LENGTH_LONG).show();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                String uid= prefManager.getKeyUserId();
                Log.d(" dkp uid", ":" + uid);
                String[] tag = {"uid", "act_mode"};
                String[] value = {uid,"getemergencycontact"};

                for (int i = 0; i < tag.length; i++)
                    params.put(tag[i], value[i]);
                return checkParams(params);
            }
        };
        postReq.setRetryPolicy(new DefaultRetryPolicy(
                800000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(postReq);
    }

    private Map<String, String> checkParams(Map<String, String> map) {
        for (Map.Entry<String, String> pairs : map.entrySet()) {
            if (pairs.getValue() == null) {
                map.put(pairs.getKey(), "");
            }
        }
        return map;
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        Log.d("dkp contact"," reqCode :"+reqCode);
        switch (reqCode) {
            case (PICK_CONTACT) :
                if (resultCode == Activity.RESULT_OK) {

                    Uri contactData = data.getData();
                    Cursor c = managedQuery(contactData, null, null, null, null);
                    if (c.moveToFirst()) {

                        int columnIndex_number = c
                                .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                         contact_no = c.getString(columnIndex_number);
                         contact_name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                        Log.d("dkp contact"," reqCode hasPhone1:"+contact_no);
                        Log.d("dkp contact"," reqCode name:"+contact_name);
                    }
                }
                break;
        }
    }
}


