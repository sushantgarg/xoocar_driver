/*
package com.xoocardriver.view.fragment;


import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.xoocardriver.GoogleAnalytic.AnalyticsApplication;
import com.xoocardriver.R;
import com.xoocardriver.adapter.Grossamountadapter;
import com.xoocardriver.adapter.TripListAdapter;
import com.xoocardriver.model.Grossamount;
import com.xoocardriver.model.Model;
import com.xoocardriver.model.VehicleItem;
import com.xoocardriver.preference.PrefManager;
import com.xoocardriver.view.activity.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class Grossamountfragment extends Fragment {
    MainActivity mainActivity;
    private ArrayList<VehicleItem> vehicleItemArrayList;
    private RecyclerView recyclerView;
    private TripListAdapter triplistAdapter;
    private PrefManager prefManager;
    private ProgressDialog progressDialog;
    private ArrayList<Grossamount> list_arr;
    private Tracker mTracker;

    public Grossamountfragment(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        MainActivity.toolbarText.setText("Day summary");
    }

    public Grossamountfragment() {
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmentgrossamount, container, false);
        initView(view);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String date_from = df.format(c.getTime());
        Log.d("current_date", ":" + date_from);

        try {
        progressDialog = new ProgressDialog(mainActivity);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        progressDialog.show();
        progressDialog.setContentView(R.layout.progressbar);
    } catch (NullPointerException e) {
        e.printStackTrace();
    }
    String driver_id = prefManager.getKeyUserId();
    getgrossdata(mainActivity,driver_id,date_from);
        int start_limit = 0;
        AnalyticsApplication application = (AnalyticsApplication) mainActivity.getApplication();
    mTracker = application.getDefaultTracker();
        return view;
}


    @Override
    public void onStart() {
        super.onStart();
        try {
            Log.d("onStart ", " url = " + "call on start");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mTracker.setScreenName("Trip list");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }
    public void initView(View view) {
        prefManager = new PrefManager(mainActivity);
        recyclerView = (RecyclerView) view.findViewById(R.id.trip_list_rl_grossAmount);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mainActivity);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setLayoutManager(linearLayoutManager);

    }

    public void getgrossdata(MainActivity context,  final String driver_id, final String date_from) {
        RequestQueue rq = Volley.newRequestQueue(context);
        StringRequest postReq = new StringRequest(Request.Method.POST, "http://xoocar.com/api/mobileapp/appdashboard/format/json", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("gross amount response ", "= " + response);
                progressDialog.dismiss();

                try {
                    JSONObject responseObject= Model.getObject(response);
                    JSONArray jsonArray = Model.getArray(responseObject, "app_ride_details");
                    list_arr = new ArrayList<>();
                    Log.d("app_ride_details", ""+jsonArray);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = Model.getObject(jsonArray, i);
                        Grossamount list = new Grossamount();
                        list.setRidedate(jsonObject.getString("ride_date"));
                        list.setBrn(jsonObject.getString("brn"));
                        list.setPrice(jsonObject.getString("price"));
                        list.setVehicle_mo(jsonObject.getString("v_licence_no"));
                      JSONObject ride_summary_obj=Model.getJsonObject(jsonObject, "ride_summary");
                        list.setTotal(ride_summary_obj.getString("total"));
                        list.setCouponprice(ride_summary_obj.getString("couponprice"));
                        list.setTolltax(ride_summary_obj.getString("tolltax"));
                        list.setBasefare(ride_summary_obj.getString("basefare"));
                        list.setRidecharge(ride_summary_obj.getString("ridecharge"));
                        list.setPerminutchargeaccordingly(ride_summary_obj.getString("perminutchargeaccordingly"));
                        list.setWaitcharge(ride_summary_obj.getString("waitcharge"));
                        list.setGst(ride_summary_obj.getString("Goods & Service Tax"));
                        list.setNet(ride_summary_obj.getString("net"));
                        list_arr.add(list);
                    }
                    Log.d("list_arr", String.valueOf(list_arr));
                    Grossamountadapter gra=new Grossamountadapter(mainActivity,list_arr);
                    recyclerView.setAdapter(gra);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.d("dkp 1 error ocurred", "TimeoutError");
                VolleyLog.d("Error", "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.d("error ocurred", "TimeoutError");
                    // Toast.makeText(context, "TimeoutError", Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    Log.d("error ocurred", "AuthFailureError");
                    //  Toast.makeText(context, "AuthFailureError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    Log.d("error ocurred", "ServerError");
                    // Toast.makeText(context, "ServerError", Toast.LENGTH_LONG).show();
                } else if (error instanceof NetworkError) {
                    Log.d("error ocurred", "NetworkError");
                    // Toast.makeText(context, "NetworkError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    Log.d("error ocurred", "ParseError");
                    //Toast.makeText(context, "ParseError", Toast.LENGTH_LONG).show();
                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                String[] tag = {"driver_id", "date_from"};
                String[] value = {driver_id, date_from};
                Log.e("driver_id",driver_id);
                Log.e("date_from",date_from);
                for (int i = 0; i < tag.length; i++)
                    params.put(tag[i], value[i]);
                return checkParams(params);
            }
        };
        postReq.setRetryPolicy(new DefaultRetryPolicy(
                800000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(postReq);
    }
    private Map<String, String> checkParams(Map<String, String> map) {
        Iterator<Map.Entry<String, String>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> pairs = (Map.Entry<String, String>) it.next();
            if (pairs.getValue() == null) {
                map.put(pairs.getKey(), "");
            }
        }
        return map;
    }
}
*/
