package com.xoocardriver.view.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.xoocardriver.R;
import com.xoocardriver.adapter.ViewListAdapter;
import com.xoocardriver.helper.CircularImageView;
import com.xoocardriver.helper.Constant;
import com.xoocardriver.helper.CustomVolleyRequest;
import com.xoocardriver.model.Model;
import com.xoocardriver.model.View_Document;
import com.xoocardriver.preference.PrefManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class ViewDocument extends AppCompatActivity {

    private ProgressDialog progressDialog;
    private RecyclerView recyclerView;
    private ArrayList<View_Document> list_arr;
    private ViewListAdapter doclistAdapter;
    private TextView not_found_tv;
    private View editProfileLL;
    private Context ctx;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_view_document);
        ctx=ViewDocument.this;

        not_found_tv=(TextView)findViewById(R.id.view_doucument_not_available_tv);
        TextView nameTv = (TextView) findViewById(R.id.view_document_name_tv);
        TextView headingTv = (TextView) findViewById(R.id.view_document_heading_tv);
        CircularImageView picIv = (CircularImageView) findViewById(R.id.view_document_iv);
        LinearLayout requestFocustLL = (LinearLayout) findViewById(R.id.collapsing_toolbar);

        editProfileLL=(LinearLayout)findViewById(R.id.view_doc_edit_profile_ll);

        PrefManager prefManager = new PrefManager(ctx);
        recyclerView = (RecyclerView) findViewById(R.id.view_document_rl);
        recyclerView.setLayoutManager(new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false));

        String image= prefManager.getProfilePic();
        String name= prefManager.getKeyUserName();
        nameTv.setText(name);
        ImageLoader im = CustomVolleyRequest.getInstance(ctx).getImageLoader();
        picIv.setImageUrl(Constant.PROFILE_IMAGE_URL +image, im);
        Typeface face = Typeface.createFromAsset(ctx.getAssets(), "fonts/OpenSans-Regular.ttf");
        nameTv.setTypeface(face);
        headingTv.setTypeface(face);
        not_found_tv.setTypeface(face);

        requestFocustLL.requestFocus();
        listenerView();

        try {
            progressDialog = new ProgressDialog(ctx);
            progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            progressDialog.show();
            progressDialog.setContentView(R.layout.progressbar);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        String driver_id = prefManager.getKeyUserId();
        String city_id = prefManager.getCity();
        requestFocustLL.requestFocus();
        getDocument(ctx, Constant.VIEW_DRIVER_DOC_POST_REQUEST_URL, driver_id);

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void listenerView() {

        editProfileLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(ctx,EditProfile.class);
                startActivity(i);
            }
        });
    }

    public void initView(View view) {

    }

    public void getDocument(Context context, String url, final String driver_id) {
        Log.d("VIEW document:", " url = " + url);

        RequestQueue rq = Volley.newRequestQueue(context);
        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("VIEW document response ", "= " + response);
                progressDialog.dismiss();
                if (!response.contains("Something Went Wrong")) {
                try {
                    JSONArray jsonArray = Model.getArray(response);
                    assert jsonArray != null;
                    list_arr=new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = Model.getObject(jsonArray, i);
                        View_Document list=new View_Document();
                        list.setDocImage(Model.getString(jsonObject, "doc_image"));
                        list.setNId(Model.getString(jsonObject, "n_Id"));
                        list.setDocNum(Model.getString(jsonObject, "doc_num"));
                        list.setExpiryDate(Model.getString(jsonObject, "expiry_date"));
                        list.setName(Model.getString(jsonObject, "name"));
                        list_arr.add(list);
                    }
                    doclistAdapter = new ViewListAdapter(ctx,list_arr);
                    recyclerView.setAdapter(doclistAdapter);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                }
                else
                {
                    not_found_tv.setVisibility(View.VISIBLE);
                    not_found_tv.setText("No Documents available");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.e("dkp error ocurred", "Error");
                VolleyLog.d("Error", "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("error ocurred", "TimeoutError");
                    // Toast.makeText(context, "TimeoutError", Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    Log.e("error ocurred", "AuthFailureError");
                    //  Toast.makeText(context, "AuthFailureError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    Log.e("error ocurred", "ServerError");
                    // Toast.makeText(context, "ServerError", Toast.LENGTH_LONG).show();
                } else if (error instanceof NetworkError) {
                    Log.e("error ocurred", "NetworkError");
                    // Toast.makeText(context, "NetworkError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    Log.e("error ocurred", "ParseError");
                    //Toast.makeText(context, "ParseError", Toast.LENGTH_LONG).show();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                String[] tag = {"driver_id"};
                String[] value = {driver_id};
                for (int i = 0; i < tag.length; i++)
                    params.put(tag[i], value[i]);
                return checkParams(params);
            }
        };

        postReq.setRetryPolicy(new DefaultRetryPolicy(
                800000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(postReq);
    }

    private Map<String, String> checkParams(Map<String, String> map) {
        Iterator<Map.Entry<String, String>> it = map.entrySet().iterator();
        while (it.hasNext()) {

            Map.Entry<String, String> pairs = (Map.Entry<String, String>) it.next();
            if (pairs.getValue() == null) {
                map.put(pairs.getKey(), "");
            }
        }
        return map;
    }
}