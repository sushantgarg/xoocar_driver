package com.xoocardriver.view.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.xoocardriver.Constants;
import com.xoocardriver.R;
import com.xoocardriver.helper.Constant;
import com.xoocardriver.helper.Validator;
import com.xoocardriver.preference.PrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class InviteFriends extends AppCompatActivity {
    private ProgressDialog progressDialog;
    private EditText mobileEdt;
    private PrefManager prefManager;
    private Button invitBtn;
    private String mobile="";
    private LinearLayout addContactRl;
    static final int PICK_CONTACT=12;
    private String selectedNumber;
    private Button shareBtn;
    private String share_amount;
    private Context ctx;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_invite);
        ctx=InviteFriends.this;

        mobileEdt = (EditText) findViewById(R.id.invite_fiends_vehicle_edt);
        addContactRl=(LinearLayout)findViewById(R.id.add_contact_from_phone_rl);
        invitBtn = (Button) findViewById(R.id.invite_friends_btn);
        shareBtn=(Button) findViewById(R.id.share_friends_btn);
        prefManager = new PrefManager(ctx);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
        mobileEdt.setTypeface(face);
        invitBtn.setTypeface(face);
        shareBtn.setTypeface(face);

        listenerView();
        getShareAmount(ctx,Constant.GET_SHARE_AMOUNT);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void getTextFromEditText()
    {
        mobile=mobileEdt.getText().toString();
    }

    public void listenerView() {

        shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String c_first_name=prefManager.getKeyUserName();
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,"Hurrey! "+c_first_name+" has referred you to \n" +
                        "XOOCAR. Get Rs. "+share_amount+" XOOCASH download now."+" https://goo.gl/SQG3kv");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });


        invitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTextFromEditText();
                if (Validator.mobValid(mobileEdt, ctx)) {
                    try {
                        progressDialog = new ProgressDialog(ctx);
                        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
                        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                        progressDialog.show();
                        progressDialog.setContentView(R.layout.progressbar);
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                    postInviteFriendsTrip( ctx,Constant.SHARE_REFER_CONTACT_POST_REQUEST_URL,mobile );
                } else {
                    mobileEdt.requestFocus();
                    //  mMobileTv.setError("Please enter valid mobile");
                }
            }
        });

        addContactRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                    startActivityForResult(contactPickerIntent, Constants.PICK_CONTACT);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void getShareAmount(Context context, String url) {
        Log.d("invite getShareAmount:", " url = " + url);

        RequestQueue rq = Volley.newRequestQueue(context);
        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
               // Log.d("invite getShareAmount response ", "= " + response);

                try {
                    JSONObject jsonObject=new JSONObject(response);
                    share_amount=jsonObject.getString("amount");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if( progressDialog != null && progressDialog.isShowing() )
                progressDialog.dismiss();
                Log.e("dkp error ocurred", "Error");
                VolleyLog.d("Error", "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("error ocurred", "TimeoutError");
                    // Toast.makeText(context, "TimeoutError", Toast.LENGTH_LONG).show();
                }
            }
        }) {
        };

        postReq.setRetryPolicy(new DefaultRetryPolicy(
                800000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(postReq);
    }


    public void postInviteFriendsTrip(Context context, String url, final String mobile) {
        Log.d("invite friend:", " url = " + url);

        RequestQueue rq = Volley.newRequestQueue(context);
        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            public String earning;

            @Override
            public void onResponse(String response) {
                Log.d("invite friend response ", "= " + response);
                mobileEdt.setText("");
                progressDialog.dismiss();
                Toast.makeText(ctx,"Share mobile no. successfully",Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.e("dkp error ocurred", "Error");
                VolleyLog.d("Error", "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("error ocurred", "TimeoutError");
                    // Toast.makeText(context, "TimeoutError", Toast.LENGTH_LONG).show();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                String reffer_id=prefManager.getKeyUserId();
                String[] tag = {"contactno", "reffer_id"};
                String[] value = {mobile,reffer_id};

                for (int i = 0; i < tag.length; i++)
                    params.put(tag[i], value[i]);

                return checkParams(params);
            }
        };

        postReq.setRetryPolicy(new DefaultRetryPolicy(
                800000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(postReq);
    }

    private Map<String, String> checkParams(Map<String, String> map) {
        for (Map.Entry<String, String> pairs : map.entrySet()) {
            if (pairs.getValue() == null) {
                map.put(pairs.getKey(), "");
            }
        }
        return map;
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {


        super.onActivityResult(reqCode, resultCode, data);

        switch (reqCode) {
            case (Constants.PICK_CONTACT):
                contactPicked(data);
        }
    }

    private void contactPicked(Intent data) {
        Cursor cursor = null;
        try {
            String phoneNo = null ;
            String name = null;
            // getData() method will have the Content Uri of the selected contact
            Uri uri = data.getData();
            //Query the content uri
            cursor = getContentResolver().query(uri, null, null, null, null);
            cursor.moveToFirst();
            // column index of the phone number
            int  phoneIndex =cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            // column index of the contact name
            phoneNo = cursor.getString(phoneIndex);
            phoneNo=phoneNo.replace(" ","");
            phoneNo=phoneNo.replace("+","");
            mobileEdt.setText(phoneNo);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
