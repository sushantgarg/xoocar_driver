package com.xoocardriver.view.fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.xoocardriver.R;
import com.xoocardriver.helper.Constant;
import com.xoocardriver.helper.Validator;
import com.xoocardriver.preference.PrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by elite on 19/12/16.
 */

public class SignIn extends AppCompatActivity {
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 15;
    private Button mloginBtn;
    private EditText mPassTv, mMobileTv;
    private String mobile;
    private PrefManager prefManager;
    private ProgressDialog progressDialog;
    private RequestQueue rq;
    private Context ctx;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_sign_in);
        ctx=SignIn.this;

        mloginBtn = (Button) findViewById(R.id.sign_in_submit_btn);
        TextView mforgotPasswordTv = (TextView) findViewById(R.id.sign_in_forgot_pass_edt);
        mPassTv = (EditText) findViewById(R.id.sign_in_password_edt);
        mMobileTv = (EditText) findViewById(R.id.sign_in_mob_no);
        rq = Volley.newRequestQueue(ctx);


        prefManager = new PrefManager(ctx);

        Typeface face = Typeface.createFromAsset( getAssets(), "fonts/OpenSans-Regular.ttf");
        mloginBtn.setTypeface(face);
        mforgotPasswordTv.setTypeface(face);
        mPassTv.setTypeface(face);
        mMobileTv.setTypeface(face);
        requestPermissions();
        listenerView();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void listenerView() {
        mloginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate();
            }
        });

    }

    public void getTextFromEditText() {
        mobile = mMobileTv.getText().toString();
        mPassTv.setText(mobile);
    }

    public void validate() {
        getTextFromEditText();
        if (Validator.mobValid(mMobileTv, ctx)) {
            if (Validator.validPassword(mPassTv, ctx)) {
//                if (network.isNetworkOnline(ctx)) {
                    Log.d("login param 0", ":" + " url");
                    try {
                        progressDialog = new ProgressDialog(ctx);
                        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
                        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                        progressDialog.show();
                        progressDialog.setContentView(R.layout.progressbar);
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                    Log.d("login param 1", ":" + " url");

                    otpRequest(ctx, Constant.NEW_CHECK_OTP_POST_REQUEST_URL, mobile);
            } else {
                mPassTv.requestFocus();
            }
        } else {
            mMobileTv.requestFocus();

        }
    }

    public void otpRequest(final Context context, String url, final String mobile) {
        Log.d("registration:", " url = " + url + "mobile = " + mobile);

        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();

                Log.d("otp response ", "= " + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if( jsonObject.optString("isRegistered").equals("1") ){
                        if ( jsonObject.has("otp") ) {
                            String otp = jsonObject.getString("otp");
                            prefManager.setOtp(otp);

                            Intent intent=new Intent(ctx,OtpFragment.class);
                            intent.putExtra("mobile",mobile);
                            startActivity(intent);

                        } else {
                            Toast.makeText(context, "Server Not Responding", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(context, "Opps! You are Not Register,call 18001239002 to Drive with XOOCAR", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                VolleyLog.d("Error", "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.d("error ocurred", "TimeoutError");
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                Log.d(" dkp mobile", ":" + mobile);
                String[] tag = {"contactno"};
                String[] value = {mobile};

                for (int i = 0; i < tag.length; i++)
                    params.put(tag[i], value[i]);

                return checkParams(params);
            }
        };
        postReq.setRetryPolicy(new DefaultRetryPolicy(
                8*1000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(postReq);
    }

    private Map<String, String> checkParams(Map<String, String> map) {
        for (Map.Entry<String, String> pairs : map.entrySet()) {
            if (pairs.getValue() == null) {
                map.put(pairs.getKey(), "");
            }
        }
        return map;
    }

    public void requestPermissions() {
        if (ContextCompat.checkSelfPermission(SignIn.this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(SignIn.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(SignIn.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION
                                ,Manifest.permission.INTERNET
                                ,Manifest.permission.CALL_PHONE
                                ,Manifest.permission.READ_CONTACTS
                                ,Manifest.permission.READ_SMS
                                ,Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    Toast.makeText(this, "XooCar will not be able to fetch your location", Toast.LENGTH_SHORT).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
            }

            // other 'case' lianes to check for other
            // permissions this app might request
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }
}
