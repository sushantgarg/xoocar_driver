package com.xoocardriver.view.fragment;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.xoocardriver.R;
import com.xoocardriver.RetroFit.GetEarning.GetEarningRequestData;
import com.xoocardriver.RetroFit.GetEarning.GetEarningRequestFields;
import com.xoocardriver.RetroFit.GetEarning.GetEarningResponceFields;
import com.xoocardriver.RetroFit.RequestInterface;
import com.xoocardriver.adapter.Detaillistadapter;
import com.xoocardriver.preference.PrefManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class TripEarning extends AppCompatActivity {

    static final int START_DATE = 1;
    static final int END_DATE = 2;
    Calendar myCalendar;
    LinearLayout fromDataLinear;
    int cur = 0;
    private PrefManager prefManager;
    private RecyclerView recyclerView;
    private TextView walletrs;
    private TextView totalEarningText;
    private TextView completrs;
    private TextView cancelrs;
    private TextView bookingrs;
    private TextView cashpaymentrs;
    //    private String date_from = "";
//    private Tracker mTracker;
    private TextView datetext, dateformattextTo;
    private TextView onlineTimeText, bankDeposit;
    private LinearLayout calenderimgTo;
    private String endDate;
    private String startDate;
    private Context ctx;
    final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            if (cur == START_DATE) {
                int month = monthOfYear + 1;
                startDate = year + "-" + month + "-" + dayOfMonth;
                datetext.setText(startDate);

                cur = END_DATE;
                Toast.makeText(ctx, "Select END date", Toast.LENGTH_SHORT).show();

                new DatePickerDialog(ctx, date, myCalendar.get(Calendar.YEAR)
                        , myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();

//              updateEnd();
            } else {
                int month = monthOfYear + 1;
                endDate = year + "-" + month + "-" + dayOfMonth;
                dateformattextTo.setText(endDate);
                getEarning();
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_earning);
        ctx = TripEarning.this;
        prefManager = new PrefManager(ctx);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ctx);
        recyclerView = (RecyclerView) findViewById(R.id.trip_earning_list_rl);
        recyclerView.setLayoutManager(linearLayoutManager);

        fromDataLinear = (LinearLayout) findViewById(R.id.calenderimg);
        calenderimgTo = (LinearLayout) findViewById(R.id.calenderimgTo);
        walletrs = (TextView) findViewById(R.id.walletrupeestext);
        datetext = (TextView) findViewById(R.id.dateformattext);
        dateformattextTo = (TextView) findViewById(R.id.dateformattextTo);
        completrs = (TextView) findViewById(R.id.numerictext);
        cancelrs = (TextView) findViewById(R.id.numerictextcancel);
        totalEarningText = (TextView) findViewById(R.id.totalEarningText);
        bookingrs = (TextView) findViewById(R.id.bookingrupees);
        onlineTimeText = (TextView) findViewById(R.id.loginHrText);
        bankDeposit = (TextView) findViewById(R.id.bankpaymentrupeetext);
        cashpaymentrs = (TextView) findViewById(R.id.cashpaymentrupees);
        myCalendar = Calendar.getInstance();

        listenerView();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        {
            cal.add(Calendar.DAY_OF_YEAR, 0);
            System.out.println(df.format(cal.getTime()));
            startDate = df.format(cal.getTime());
            endDate = df.format(cal.getTime());
        }

        datetext.setText(startDate);
        dateformattextTo.setText(startDate);

        getEarning();

    }

    public void listenerView() {

        fromDataLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                list_arr.clear();
                cur = START_DATE;
                new DatePickerDialog(ctx, date, myCalendar.get(Calendar.YEAR)
                        , myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });

        calenderimgTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                list_arr.clear();
                cur = END_DATE;
                new DatePickerDialog(ctx, date, myCalendar.get(Calendar.YEAR)
                        , myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    public void getEarning() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);
        Call<GetEarningResponceFields> call = request.getEarning("api/driverappapi/driverearning/format/json/"
                , new GetEarningRequestFields(new GetEarningRequestData("driverearning"
                        , prefManager.getKeyUserId()
                        , startDate
                        , endDate)));

        call.enqueue(new Callback<GetEarningResponceFields>() {
            @Override
            public void onResponse(Call<GetEarningResponceFields> call, retrofit2.Response<GetEarningResponceFields> response) {

                if (response.body() != null) {
                    response.body().getDataArray().getDriverridedetail();

                    bookingrs.setText(response.body().getDataArray().getEarning().getTotalbooking());
                    walletrs.setText(getString(R.string.rupees_symbol) + " " + response.body().getDataArray().getEarning().getWallet());
                    cashpaymentrs.setText(getString(R.string.rupees_symbol) + " " + response.body().getDataArray().getEarning().getCash());
                    totalEarningText.setText(getString(R.string.rupees_symbol) + " " + response.body().getDataArray().getEarning().getDriverearning());
                    bankDeposit.setText(getString(R.string.rupees_symbol) + " " + response.body().getDataArray().getEarning().getBankdiposit());
                    completrs.setText(response.body().getDataArray().getEarning().getTotalcomplt());
                    cancelrs.setText(response.body().getDataArray().getEarning().getTotalcancl());
                    onlineTimeText.setText(response.body().getDataArray().getEarning().getLoginHrs());

                    Detaillistadapter grass = new Detaillistadapter(ctx, response.body().getDataArray().getDriverridedetail());
                    recyclerView.setAdapter(grass);

                }
            }

            @Override
            public void onFailure(Call<GetEarningResponceFields> call, Throwable t) {
            }
        });
    }

}





