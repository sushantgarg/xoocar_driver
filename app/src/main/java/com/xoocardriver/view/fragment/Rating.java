package com.xoocardriver.view.fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.xoocardriver.R;
import com.xoocardriver.helper.CircularImageView;
import com.xoocardriver.helper.Constant;
import com.xoocardriver.helper.CustomVolleyRequest;
import com.xoocardriver.map.GeocodingLocation;
import com.xoocardriver.preference.PrefManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by elite on 21/12/16.
 */

public class Rating extends AppCompatActivity {
    private Context ctx;
    MapView mMapView;
    private GoogleMap googleMap;
    Typeface normal_fonts;
    String rating_point;
    public GeocodingLocation geoLocation;
    private ProgressDialog progressDialog;
    private PrefManager prefManager;
    private TextView nameTv;
    private ImageLoader im;
    private CircularImageView picIv;
    private RatingBar avgratingBar;
    private TextView setRatingTv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_rating);

        ctx=Rating.this;
        prefManager = new PrefManager(ctx);

        picIv = (CircularImageView) findViewById(R.id.rating_user_image_iv);
        nameTv = (TextView) findViewById(R.id.name);
        Button complete_rating = (Button) findViewById(R.id.complete_rating);
        RatingBar ratingBar = (RatingBar) findViewById(R.id.trip_ratingBar);
        avgratingBar = (RatingBar) findViewById(R.id.trip_ratingbar_small);
        setRatingTv = (TextView) findViewById(R.id.rating_set_tv);
        geoLocation = new GeocodingLocation();

        try {
            progressDialog = new ProgressDialog(ctx);
            progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            progressDialog.show();
            progressDialog.setContentView(R.layout.progressbar);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        String ride_id = prefManager.getRideId();
        getRatings(ctx, Constant.GET_RATINGS_POST_REQUEST_URL, ride_id);

        mMapView = (MapView) findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume();

        try {
            MapsInitializer.initialize(ctx);
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;
                if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    return;
                }
                googleMap.setMyLocationEnabled(true);
                String address = GeocodingLocation.getCompleteAddressString(ctx, prefManager.getCurrentLat(), prefManager.getCurrentLng());


                LatLng sydney = new LatLng(prefManager.getCurrentLat(), prefManager.getCurrentLng());
                googleMap.addMarker(new MarkerOptions().position(sydney).title("Driver Location").snippet(address).icon(BitmapDescriptorFactory.fromResource(R.drawable.green_location_old)));

                CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(12).build();

                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        });

        normal_fonts = Typeface.createFromAsset(ctx.getAssets(), "fonts/OpenSans-Regular.ttf");

        nameTv.setTypeface(normal_fonts);
        complete_rating.setTypeface(normal_fonts);

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                rating_point = String.valueOf(ratingBar.getRating());
                Log.d("rating value", "" + rating_point);
            }
        });

        complete_rating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rating_point != null) {
                    try {
                        progressDialog = new ProgressDialog(ctx);
                        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
                        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                        progressDialog.show();
                        progressDialog.setContentView(R.layout.progressbar);
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                    String ride_id = prefManager.getRideId();
                    Log.d("ride_id_check :11", " : = " + ride_id);
                    postRating(ctx, Constant.POST_RATINGS_POST_REQUEST_URL, ride_id, rating_point);
                } else {
                    Toast.makeText(ctx, "Please rate first...", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    public void getRatings(Context context, String url, final String ride_id) {
        Log.d("Rating get res:", " url = " + url);

        RequestQueue rq = Volley.newRequestQueue(context);
        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Rating get response ", "= " + response);
                progressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String name = jsonObject.getString("dname");
                    String rating = jsonObject.getString("rating");
                    String profile_pic = jsonObject.getString("profile_pic");

                    im = CustomVolleyRequest.getInstance(ctx).getImageLoader();
                    picIv.setImageUrl(Constant.PROFILE_IMAGE_URL + profile_pic, im);
                    nameTv.setText(name);
                    avgratingBar.setRating(1);
                    setRatingTv.setText(rating);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.e("dkp error ocurred", "Error");
                VolleyLog.d("Error", "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("error ocurred", "TimeoutError");
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                String[] tag = {"rideid", "usertype"};
                String[] value = {ride_id, "4"};

                for (int i = 0; i < tag.length; i++)
                    params.put(tag[i], value[i]);

                return checkParams(params);
            }
        };

        postReq.setRetryPolicy(new DefaultRetryPolicy(
                800000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(postReq);
    }

    private Map<String, String> checkParams(Map<String, String> map) {
        for (Map.Entry<String, String> pairs : map.entrySet()) {

            if (pairs.getValue() == null) {
                map.put(pairs.getKey(), "");
            }
        }
        return map;
    }

    public void postRating(Context context, String url, final String ride_id, final String rating_point) {

        RequestQueue rq = Volley.newRequestQueue(context);
        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Rating post response ", "= " + response);
                progressDialog.dismiss();
                try {
                    JSONArray jsonObject = new JSONArray(response);
                    JSONObject jsonObject1 = jsonObject.getJSONObject(0);
                    if (jsonObject1.has("rating_id")) {
                        Toast.makeText(ctx, "Rating post successfully", Toast.LENGTH_SHORT).show();
                        Intent i=new Intent(Rating.this,OfflineOnline.class);
                        startActivity(i);
                    } else {
                        Toast.makeText(ctx, "Rating not post successfully", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.e("dkp error ocurred", "Error");
                VolleyLog.d("Error", "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("error ocurred", "TimeoutError");
                    // Toast.makeText(context, "TimeoutError", Toast.LENGTH_LONG).show();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                String[] tag = {"ride_id", "rating", "usertype"};
                String[] value = {ride_id, rating_point, "4"};

                for (int i = 0; i < tag.length; i++)
                    params.put(tag[i], value[i]);

                return checkParams(params);
            }
        };

        postReq.setRetryPolicy(new DefaultRetryPolicy(
                800000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(postReq);
    }

    @Override
    public void onBackPressed() {

    }

}
