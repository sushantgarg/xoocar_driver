package com.xoocardriver.view.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.xoocardriver.R;


public class AboutUsFragment extends AppCompatActivity {
    private WebView webViewWv;
    private String url;
    private ProgressDialog progressDialog;
    private Context ctx;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_about_us);
        ctx=AboutUsFragment.this;

        Intent i=getIntent();
        url=i.getStringExtra("url");

        webViewWv = (WebView) findViewById(R.id.about_us_webView);

        listenerView();
    }

    private void listenerView() {
        try {
            progressDialog = new ProgressDialog(ctx);
            progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        webViewWv.getSettings().setJavaScriptEnabled(true);
        webViewWv.getSettings().setLoadWithOverviewMode(true);
        webViewWv.getSettings().setUseWideViewPort(true);
        webViewWv.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                try {
                    progressDialog.show();
                    progressDialog.setContentView(R.layout.progressbar);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, final String url) {
                progressDialog.dismiss();
            }
        });
        webViewWv.loadUrl(url);
    }

}