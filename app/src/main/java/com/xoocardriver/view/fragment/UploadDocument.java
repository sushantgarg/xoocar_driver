package com.xoocardriver.view.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.PersistableBundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.javiersantos.bottomdialogs.BottomDialog;
import com.theartofdev.edmodo.cropper.CropImage;
import com.xoocardriver.R;
import com.xoocardriver.adapter.DocLIstAdapter;
import com.xoocardriver.helper.CircularImageView;
import com.xoocardriver.helper.Constant;
import com.xoocardriver.helper.CustomVolleyRequest;
import com.xoocardriver.helper.SecondPageFragmentListenerUploadDocument;
import com.xoocardriver.helper.SetPreviewImageListnerUploadDocument;
import com.xoocardriver.model.Model;
import com.xoocardriver.preference.PrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class UploadDocument extends AppCompatActivity implements SetPreviewImageListnerUploadDocument, SecondPageFragmentListenerUploadDocument {

    private ProgressDialog progressDialog;
    private String driver_id;
    private Spinner documentSp;
    private LinearLayout uploadLL;
    private ArrayList<String> docId, docName;
    private String doc_id = "0";
    private static final int SELECT_PICTURE = 100;
    SecondPageFragmentListenerUploadDocument secondPageFragmentListener;
    SetPreviewImageListnerUploadDocument setPreviewImageListner;
    private RelativeLayout layout_image_click;
    private Bitmap imageBitmap=null;
    private Uri outPutfileUri;
    private int TAKE_PIC=1;
    private Context ctx;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_upload_document);
        ctx=UploadDocument.this;

        secondPageFragmentListener = this;
        setPreviewImageListner = this;
        TextView uploadLabelTv = (TextView) findViewById(R.id.upload_document_label_tv);
        documentSp = (Spinner) findViewById(R.id.upload_document_sp);
        TextView uploadTV = (TextView) findViewById(R.id.upload_document_upload_tv);
        TextView nameTv = (TextView) findViewById(R.id.upload_document_user_name_tv);
        uploadLL = (LinearLayout) findViewById(R.id.upload_document_ll);
        layout_image_click = (RelativeLayout) findViewById(R.id.layout_image_click);
        CircularImageView downImageIv = (CircularImageView) findViewById(R.id.set_down_upload_iv);
        PrefManager prefManager = new PrefManager(ctx);
        String image= prefManager.getProfilePic();
        String name= prefManager.getKeyUserName();
        nameTv.setText(name);
        ImageLoader im = CustomVolleyRequest.getInstance(ctx).getImageLoader();
        downImageIv.setImageUrl(Constant.PROFILE_IMAGE_URL +image, im);

        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
        uploadLabelTv.setTypeface(face);
        uploadTV.setTypeface(face);
        nameTv.setTypeface(face);

        listenerView();

        try {
            progressDialog = new ProgressDialog(ctx);
            progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            progressDialog.show();
            progressDialog.setContentView(R.layout.progressbar);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        driver_id = prefManager.getKeyUserId();
        getDocument(ctx, Constant.DOCUMENT_VIEW_POST_REQUEST_URL, "1");
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    public void initView(View view) {


    }

    public void setBottomDialog(){
        BottomDialog bottomDialog = new BottomDialog.Builder(ctx)
                .setTitle("Image Chooser !")
                .setContent("Choose image from Camera or Gallery")
                .setIcon(R.drawable.galery)
                .setPositiveText("Camera")
                .setNegativeText("Gallery")
                .setPositiveBackgroundColorResource(R.color.colorPrimary)
                .setPositiveTextColorResource(android.R.color.white)
                .setNegativeTextColor(R.color.keyfeature5)

                .onPositive(new BottomDialog.ButtonCallback() {
                    @Override
                    public void onClick(BottomDialog dialog) {
                        takeImageFromCamera();
                    }
                })
                .onNegative(new BottomDialog.ButtonCallback() {
                    @Override
                    public void onClick(BottomDialog dialog) {
                        chooseFromGallery();
                    }
                }).show();

    }

    public void listenerView() {
        uploadLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!doc_id.equals("0")) {
                 /*   DialogFragmentChooseImageUploadDocument dialogFragmentChooseImage = new DialogFragmentChooseImageUploadDocument(mainActivity, secondPageFragmentListener);
                    dialogFragmentChooseImage.show(mainActivity.getSupportFragmentManager(), "");*/
                    setBottomDialog();
                } else {
                    Toast.makeText(ctx, "Please Select document", Toast.LENGTH_SHORT).show();
                }
            }
        });

        layout_image_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(ctx,EditProfile.class);
                startActivity(i);
            }
        });

        documentSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                doc_id = docId.get(position);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public String getStringImage(Bitmap bmp)
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    public void postDocument(Context context, String url, final String driverid, final String city_id, final String doc_id, final String image) {
        Log.d("post upload doc:", " url = " + url + ":city_id : " + city_id + " driverid :" + driverid + " doc_id:" + doc_id + " image :" + image);

        RequestQueue rq = Volley.newRequestQueue(context);
        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("post upload doc ", "= " + response);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    if (jsonObject.has("response"))
                    {
                        if (jsonObject.getString("response").equals("1"))
                        {
                           Toast.makeText(ctx,"Document upload successfully",Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            Toast.makeText(ctx,"Document not upload successfully",Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.e("dkp error ocurred", "Error");
                VolleyLog.d("Error", "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("error ocurred", "TimeoutError");
                    // Toast.makeText(context, "TimeoutError", Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    Log.e("error ocurred", "AuthFailureError");
                    //  Toast.makeText(context, "AuthFailureError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    Log.e("error ocurred", "ServerError");
                    // Toast.makeText(context, "ServerError", Toast.LENGTH_LONG).show();
                } else if (error instanceof NetworkError) {
                    Log.e("error ocurred", "NetworkError");
                    // Toast.makeText(context, "NetworkError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    Log.e("error ocurred", "ParseError");
                    //Toast.makeText(context, "ParseError", Toast.LENGTH_LONG).show();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                String[] tag = {"city_id", "driver_id", "image", "doctype", "number", "expiry_date"};
                String[] value = {city_id, driverid, image, doc_id, "", ""};

                for (int i = 0; i < tag.length; i++)
                    params.put(tag[i], value[i]);

                return checkParams(params);
            }
        };

        postReq.setRetryPolicy(new DefaultRetryPolicy(
                800000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(postReq);
    }


    public void getDocument(Context context, String url, final String city_id) {
        Log.d("all document:", " url = " + url);

        RequestQueue rq = Volley.newRequestQueue(context);
        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("all document response ", "= " + response);
                progressDialog.dismiss();
                JSONArray jsonArray = Model.getArray(response);
                try {
                    docId = new ArrayList<>();
                    docName = new ArrayList<>();
                    docName.add("Select Document");
                    docId.add("0");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        docId.add(jsonObject.getString("id"));
                        docName.add(jsonObject.getString("name"));
                    }
                    Log.d("name vehicelId", "sizes :" + docId.size());
                    Log.d("name vehicleName", "sizes :" + docName.size());
                    DocLIstAdapter vehicleLIstAdapter = new DocLIstAdapter(ctx, docName);
                    documentSp.setAdapter(vehicleLIstAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.e("dkp error ocurred", "Error");
                VolleyLog.d("Error", "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("error ocurred", "TimeoutError");
                    // Toast.makeText(context, "TimeoutError", Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    Log.e("error ocurred", "AuthFailureError");
                    //  Toast.makeText(context, "AuthFailureError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ServerError) {
                    Log.e("error ocurred", "ServerError");
                    // Toast.makeText(context, "ServerError", Toast.LENGTH_LONG).show();
                } else if (error instanceof NetworkError) {
                    Log.e("error ocurred", "NetworkError");
                    // Toast.makeText(context, "NetworkError", Toast.LENGTH_LONG).show();
                } else if (error instanceof ParseError) {
                    Log.e("error ocurred", "ParseError");
                    //Toast.makeText(context, "ParseError", Toast.LENGTH_LONG).show();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                String[] tag = {"city_id"};
                String[] value = {city_id};

                for (int i = 0; i < tag.length; i++)
                    params.put(tag[i], value[i]);

                return checkParams(params);
            }
        };
        postReq.setRetryPolicy(new DefaultRetryPolicy(
                800000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(postReq);
    }

    private Map<String, String> checkParams(Map<String, String> map) {
        Iterator<Map.Entry<String, String>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> pairs = (Map.Entry<String, String>) it.next();
            if (pairs.getValue() == null) {
                map.put(pairs.getKey(), "");
            }
        }
        return map;
    }

    public void chooseFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
    }

    @Override
    public void callImageChooser(String choose_option) {
        if (choose_option.equals("camera")) {
            takeImageFromCamera();
        } else if (choose_option.equals("gallery")) {
            // chooseFromGallery();
        }
    }

    @Override
    public void setImage(Bitmap image) {
        Log.d("meet1", "meet1");
        imageBitmap=image;
        if (imageBitmap!=null)
        {
            String  image1=getStringImage(imageBitmap);
            try {
                progressDialog = new ProgressDialog(ctx);
                progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
                progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                progressDialog.show();
                progressDialog.setContentView(R.layout.progressbar);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            postDocument(ctx, Constant.ADD_DOCUMENT_POST_REQUEST_URL, driver_id, "1", doc_id, image1);
        }
        else
        {
            Toast.makeText(ctx, "Please Select Image", Toast.LENGTH_SHORT).show();
        };
    }

    public void takeImageFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = new File(Environment.getExternalStorageDirectory(), "Profile_Picture.jpg");
        outPutfileUri = Uri.fromFile(file);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, outPutfileUri);
        startActivityForResult(intent, TAKE_PIC);
    }

    public void choosefromgallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == TAKE_PIC && resultCode == RESULT_OK) {
            if (null != outPutfileUri) {
                crop_Method(outPutfileUri);
            }
        }

        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                Uri selectedImageUri = data.getData();
                if (null != selectedImageUri) {
                    crop_Method(selectedImageUri);
                }
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), resultUri);
                    if (bitmap != null) {
                        Drawable d = new BitmapDrawable(getResources(), bitmap);
                        imageBitmap=bitmap;
                        if (imageBitmap!=null)
                        {
                            String  image1=getStringImage(imageBitmap);
                            try {
                                progressDialog = new ProgressDialog(ctx);
                                progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
                                progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                                progressDialog.show();
                                progressDialog.setContentView(R.layout.progressbar);
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }
                            postDocument(ctx, Constant.ADD_DOCUMENT_POST_REQUEST_URL, driver_id, "1", doc_id, image1);
                        }
                        else
                        {
                            Toast.makeText(ctx, "Please Select Image", Toast.LENGTH_SHORT).show();
                        };
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    public void crop_Method(Uri imageUri) {
        CropImage.activity(imageUri).start(this);
    }
}
