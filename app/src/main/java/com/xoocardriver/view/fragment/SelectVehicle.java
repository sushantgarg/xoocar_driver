//package com.xoocardriver.view.fragment;
//
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//import android.support.v7.widget.DefaultItemAnimator;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//
//import com.google.android.gms.analytics.HitBuilders;
//import com.google.android.gms.analytics.Tracker;
//import com.xoocardriver.GoogleAnalytic.AnalyticsApplication;
//import com.xoocardriver.R;
//import com.xoocardriver.adapter.SelectVehicleAdapter;
//import com.xoocardriver.model.VehicleItem;
//import com.xoocardriver.view.activity.MainActivity;
//
//import java.util.ArrayList;
//
///**
// * Created by elite on 19/12/16.
// */
//
//public class SelectVehicle extends Fragment {
//    MainActivity mainActivity;
//    private Tracker mTracker;
//
//    public SelectVehicle(MainActivity mainActivity) {
//        this.mainActivity = mainActivity;
//    }
//
//    public SelectVehicle() {
//    }
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.fragment_select_vehicle, container, false);
//
//        initView(view);
//        AnalyticsApplication application = (AnalyticsApplication) mainActivity.getApplication();
//        mTracker = application.getDefaultTracker();
//        return view;
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        mTracker.setScreenName("Select vehicle");
//        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
//    }
//
//
//    public void initView(View view) {
//        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
//        recyclerView.setNestedScrollingEnabled(false);
//        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
//        recyclerView.setLayoutManager(mLayoutManager);
//        recyclerView.setItemAnimator(new DefaultItemAnimator());
//
//        ArrayList<VehicleItem> vehicleItemArrayList = new ArrayList<>();
//        for (int i = 0; i < 15; i++) {
//            VehicleItem vehicleItem = new VehicleItem();
//            vehicleItem.setVehicle_num("GHZE00197");
//            vehicleItem.setVehicle_model("MARUTI DL800");
//            vehicleItemArrayList.add(vehicleItem);
//        }
//        recyclerView.setAdapter(new SelectVehicleAdapter(mainActivity, vehicleItemArrayList));
//    }
//}
