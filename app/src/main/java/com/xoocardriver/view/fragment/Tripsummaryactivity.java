//package com.xoocardriver.view.fragment;
//
//import android.support.v4.app.Fragment;
//import android.app.ProgressDialog;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.android.volley.AuthFailureError;
//import com.android.volley.DefaultRetryPolicy;
//import com.android.volley.NetworkError;
//import com.android.volley.NoConnectionError;
//import com.android.volley.ParseError;
//import com.android.volley.Request;
//import com.android.volley.RequestQueue;
//import com.android.volley.Response;
//import com.android.volley.ServerError;
//import com.android.volley.TimeoutError;
//import com.android.volley.VolleyError;
//import com.android.volley.VolleyLog;
//import com.android.volley.toolbox.StringRequest;
//import com.android.volley.toolbox.Volley;
//import com.google.android.gms.analytics.Tracker;
//import com.xoocardriver.GoogleAnalytic.AnalyticsApplication;
//import com.xoocardriver.R;
//import com.xoocardriver.model.Model;
//import com.xoocardriver.preference.PrefManager;
//import com.xoocardriver.view.activity.MainActivity;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.Map;
//
//
//
//
//public class Tripsummaryactivity extends Fragment {
//
//   private TextView destinationtv;
//    private TextView sourcetv;
//    private TextView dropfrom;
//    private TextView dropaddress;
//    private TextView tripkm;
//    private TextView tripkmnum;
//    private TextView triptime;
//    private TextView triptimenumrioc;
//    private TextView ridefare;
//    private TextView numericridefare;
//    private TextView tollcharge;
//    private TextView numerictollcharge;
//    private TextView discount;
//    private TextView numericdiscount;
//    private TextView paymentmode;
//    private TextView numericpayment;
//    private TextView numericgross;
//    private TextView grossamount;
//    private TextView customerpaidamt;
//    private TextView numericcustomerpaid;
//    private TextView gsttext;
//    private TextView gstrupees;
//    private int start_limit;
//    ProgressDialog progressDialog;
//    private PrefManager prefManager;
//    private Tracker mTracker;
//    private String rideid;
//
//    public Tripsummaryactivity(MainActivity mainActivity,String rideid) {
//        this.maiActivity = mainActivity;
//        this.rideid = rideid;
//        Log.d("rideid111",rideid);
//        MainActivity.toolbarText.setText("Trip Summary");
//    }
//
//
//
//
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.activity_tripsummary, container, false);
//        //listenerView(view);
//        initview(view);
//        try {
//            progressDialog = new ProgressDialog(maiActivity);
//            progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
//            progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
//            progressDialog.show();
//            progressDialog.setContentView(R.layout.progressbar);
//        } catch (NullPointerException e) {
//            e.printStackTrace();
//        }
//
//       // String rideid = prefManager.getKeyUserId();
//        getridedeatil(maiActivity,"",rideid);
//        start_limit = 0;
//       AnalyticsApplication application = (AnalyticsApplication) maiActivity.getApplication();
//        mTracker = application.getDefaultTracker();
//        return view;}
//
//         public void initview(View view){
//           prefManager =new PrefManager(maiActivity);
//             destinationtv =(TextView)view.findViewById(R.id.pickup_receiving_source_h_tv);
//              sourcetv =(TextView)view.findViewById(R.id.pickup_receiving_source_tv);
//             dropfrom =(TextView)view.findViewById(R.id.pickup_receiving_destination_h_tv);
//            dropaddress =(TextView)view.findViewById(R.id.pickup_receiving_destination_tv);
//             tripkm=(TextView)view.findViewById(R.id.profile_user_email_label_tv);
//             tripkmnum =(TextView)view.findViewById(R.id.profile_user_email_tv);
//              triptime =(TextView)view.findViewById(R.id.triptimetextview);
//             triptimenumrioc=(TextView)view.findViewById(R.id.numerictriptime);
//             ridefare =(TextView)view.findViewById(R.id.ridefaretext);
//             numericridefare=(TextView)view.findViewById(R.id.numericridefare);
//             tollcharge=(TextView)view.findViewById(R.id.tollchargetextview);
//             numerictollcharge=(TextView)view.findViewById(R.id.numerictollcharge);
//             discount =(TextView)view.findViewById(R.id.discounttext);
//             numericdiscount =(TextView)view.findViewById(R.id.numericdiscount);
//             paymentmode =(TextView)view.findViewById(R.id.paymentmodetext);
//             numericpayment =(TextView)view.findViewById(R.id.numericpaymentmode);
//             grossamount =(TextView)view.findViewById(R.id.grossamounttext);
//             numericgross =(TextView)view.findViewById(R.id.numericgrossamount);
//             customerpaidamt=(TextView)view.findViewById(R.id.customerpaidtext);
//             numericcustomerpaid=(TextView)view.findViewById(R.id.numericcustomerpaid);
//            // goback =(ImageView)view.findViewById(R.id.gobutton);
//
//         }
//
//         public void getridedeatil(MainActivity context, String url, final String rideid){
//             RequestQueue rq = Volley.newRequestQueue(context);
//             StringRequest postReq = new StringRequest(Request.Method.POST, "http://xoocar.com/api/mobileapp/appridesingle/format/json", new Response.Listener<String>() {
//                 @Override
//                 public void onResponse(String response) {
//                     Log.d("aaaaaa  response", "= " + response);
//                     progressDialog.dismiss();
//                     try {
//                        JSONObject jsonObject =new JSONObject(response.toString());
//                        // Log.e("response",response);
//                        JSONObject mydata =jsonObject.getJSONObject("mydata");
//                         String source =mydata.getString("source");
//                         String destination =mydata.getString("destination");
//                         String rideid = mydata.getString("rideid");
//                         String total = mydata.getString("total");
//                             Log.d("totaldd",total);
//                             String basefare = mydata.getString("basefare");
//                             String ridecharge = mydata.getString("ridecharge");
//                             Log.d("ridecharge",ridecharge);
//                             String waitcharge = mydata.getString("waitcharge");
//                             String taxcharge = mydata.getString("taxcharge");
//                             String couponprice = mydata.getString("couponprice");
//                             Log.d("couponprice",couponprice);
//                             String conviencecharge = mydata.getString("conveniencecharge");
//                             String perminutchargeaccordingly = mydata.getString("perminutchargeaccordingly");
//                             String tolltax = mydata.getString("tolltax");
//                             String pdflink = mydata.getString("pdflink");
//                             String actualkms = mydata.getString("actual_kms");
//                             String actualtime = mydata.getString("actual_time");
//                             String paymode = mydata.getString("pay_mode");
//                             String net = mydata.getString("net");
//                             String good = mydata.getString("Goods & Service Tax");
//
//                        int net1=Integer.parseInt(mydata.getString("net"));
//                         int gst1 =Integer.parseInt(mydata.getString("Goods & Service Tax"));
//                         int toll1=Integer.parseInt(mydata.getString("tolltax"));
//                         int couponprice1 =Integer.parseInt(mydata.getString("couponprice"));
//                       int totalprice =net1+gst1+toll1;
//
//                         if (totalprice<couponprice1){
//                             numericdiscount.setText(""+totalprice);
//                         }else
//                         {
//                             numericdiscount.setText(couponprice);
//                         }
//                             sourcetv.setText(source);
//                          dropaddress.setText(destination);
//                             tripkmnum.setText(actualkms);
//                             triptimenumrioc.setText(actualtime);
//                             numericridefare.setText(ridecharge);
//                             numerictollcharge.setText(tolltax);
//                           numericcustomerpaid.setText(total);
//                             numericpayment.setText(paymode);
//                             numericgross.setText(total);
//
//                     } catch (JSONException e1) {
//                         e1.printStackTrace();
//                     }
//                 }
//             }, new Response.ErrorListener() {
//                 @Override
//                 public void onErrorResponse(VolleyError error) {
//                     progressDialog.dismiss();
//                     Log.d("dkp 1 error ocurred", "TimeoutError");
//                     VolleyLog.d("Error", "Error: " + error.getMessage());
//                     if (error instanceof TimeoutError || error instanceof NoConnectionError) {
//                         Log.d("error ocurred", "TimeoutError");
//                         // Toast.makeText(context, "TimeoutError", Toast.LENGTH_LONG).show();
//                     } else if (error instanceof AuthFailureError) {
//                         Log.d("error ocurred", "AuthFailureError");
//                         //  Toast.makeText(context, "AuthFailureError", Toast.LENGTH_LONG).show();
//                     } else if (error instanceof ServerError) {
//                         Log.d("error ocurred", "ServerError");
//                         // Toast.makeText(context, "ServerError", Toast.LENGTH_LONG).show();
//                     } else if (error instanceof NetworkError) {
//                         Log.d("error ocurred", "NetworkError");
//                         // Toast.makeText(context, "NetworkError", Toast.LENGTH_LONG).show();
//                     } else if (error instanceof ParseError) {
//                         Log.d("error ocurred", "ParseError");
//                         //Toast.makeText(context, "ParseError", Toast.LENGTH_LONG).show();
//                     }
//                 }
//             }) {
//                 @Override
//                 protected Map<String, String> getParams() throws AuthFailureError {
//                     Map<String, String> params = new HashMap<>();
//                     String[] tag = {"id"};
//                     String[] value = {rideid};
//                     Log.d("id91",rideid);
//                     for (int i = 0; i < tag.length; i++)
//                         params.put(tag[i], value[i]);
//                     return checkParams(params);
//                 }
//             };
//             postReq.setRetryPolicy(new DefaultRetryPolicy(
//                     800000,
//                     DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                     DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//             rq.add(postReq);
//         }
//    private Map<String, String> checkParams(Map<String, String> map) {
//        Iterator<Map.Entry<String, String>> it = map.entrySet().iterator();
//        while (it.hasNext()) {
//            Map.Entry<String, String> pairs = (Map.Entry<String, String>) it.next();
//            if (pairs.getValue() == null) {
//                map.put(pairs.getKey(), "");
//            }
//        }
//        return map;
//    }
//}
//
//
