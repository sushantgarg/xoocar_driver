package com.xoocardriver.view.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.xoocardriver.R;
import com.xoocardriver.RetroFit.RequestInterface;
import com.xoocardriver.RetroFit.TripDetailBillSummary.TripDetailRequestData;
import com.xoocardriver.RetroFit.TripDetailBillSummary.TripDetailRequestFields;
import com.xoocardriver.RetroFit.TripDetailBillSummary.TripDetailResponceData;
import com.xoocardriver.RetroFit.TripDetailBillSummary.TripDetailResponceFields;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by abc on 10-07-2017.
 */

public class Tripsummaryactivitycash extends AppCompatActivity {

    private TextView sourceaddress,destAddress,binNum,totalRideEarning,disTravelled,rideTime,customerBrkupTotalPaid,customerBrkupCashPaid
    ,customerBrkupWalletPaid,customerBrkupToll,customerBrkupGst,customerBrkupBill,rideEarningBrkUpFareAmt,rideEarningBrkUpOperatorBill
            ,rideEarningBrkUpTaxes,rideEarningBrkUpRideEarning,rideEarningBrkUpToll,rideEarningBrkUpCashCollected,rideEarningBrkUpXooToCollect;
    private String oid;
    private TextView coupon;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activitytripsummarycash);
        Intent i=getIntent();
        oid=i.getStringExtra("oid");

        sourceaddress= (TextView) findViewById(R.id.sourceAddressCashSummary);
        destAddress= (TextView) findViewById(R.id.dropAddressCashSummary);
        binNum= (TextView) findViewById(R.id.binNumCashSummary);

        disTravelled= (TextView) findViewById(R.id.distanceTravelledCashSummary);
        totalRideEarning= (TextView) findViewById(R.id.rideEarningCashSummary);
        customerBrkupTotalPaid= (TextView) findViewById(R.id.customerBillBreakUP_customerPaid);
        customerBrkupCashPaid= (TextView) findViewById(R.id.customerBillBrkupCashPaid);
        customerBrkupWalletPaid= (TextView) findViewById(R.id.customerBillBrkupWalletPaid);

        customerBrkupToll= (TextView) findViewById(R.id.customerBillBreakUP_toll);
        rideTime= (TextView) findViewById(R.id.rideTimeCashSummary);
        customerBrkupBill= (TextView) findViewById(R.id.customerBillBreakUP_customerBill);
        coupon= (TextView) findViewById(R.id.coupon);
        rideEarningBrkUpFareAmt= (TextView) findViewById(R.id.rideEarningBreakUp_fareAmount);
        rideEarningBrkUpOperatorBill= (TextView) findViewById(R.id.rideEarningBreakUp_operatorBill);

        rideEarningBrkUpTaxes= (TextView) findViewById(R.id.rideEarningBreakUp_taxes);
        customerBrkupGst= (TextView) findViewById(R.id.customerBillBreakUP_gst);
        rideEarningBrkUpRideEarning= (TextView) findViewById(R.id.rideEarningBreakUp_rideEarnings);
        rideEarningBrkUpToll= (TextView) findViewById(R.id.rideEarningBreakUp_toll);
        rideEarningBrkUpCashCollected= (TextView) findViewById(R.id.rideEarningBreakUp_cashCollected);
        rideEarningBrkUpXooToCollect= (TextView) findViewById(R.id.rideEarningBreakUp_xooToCollect);
        getRideDetail();
    }

    private void getRideDetail() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://velenesa.com/vele/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface request = retrofit.create(RequestInterface.class);

        Call<TripDetailResponceFields> call = request.getTripDetail("api/driverappapi/appridesingle/format/json/"
                , new TripDetailRequestFields(new TripDetailRequestData(oid)));

        call.enqueue(new Callback<TripDetailResponceFields>() {
            @Override
            public void onResponse(Call<TripDetailResponceFields> call, retrofit2.Response<TripDetailResponceFields> response) {

                if( response.body() != null ) {
                    TripDetailResponceData data= response.body().getDataArray().getDriverridedetail();
                    sourceaddress.setText( data.getSource() );
                    destAddress.setText(data.getDestination());
                    binNum.setText("BIN- "+data.getCrnNo());

                    disTravelled.setText(data.getActualKms() +" Km");
                    totalRideEarning.setText(getString(R.string.rupees_symbol)+" "+ data.getDriverearning());
                    customerBrkupTotalPaid.setText(getString(R.string.rupees_symbol)+" "+ data.getGrossAmt());
                    customerBrkupCashPaid.setText(getString(R.string.rupees_symbol)+" "+ data.getCashPaid());
                    customerBrkupWalletPaid.setText(getString(R.string.rupees_symbol)+" "+ data.getWalletPaid());
                    customerBrkupToll.setText(getString(R.string.rupees_symbol)+" "+ data.getTolltax());
                    rideTime.setText(data.getActualTime()+" min");
                    customerBrkupBill.setText(getString(R.string.rupees_symbol)+" "+ data.getGrossAmt());
                    coupon.setText(getString(R.string.rupees_symbol)+" "+ data.getCouponPrice());
                    rideEarningBrkUpFareAmt.setText(getString(R.string.rupees_symbol)+" "+ data.getGrossAmt());
                    rideEarningBrkUpOperatorBill.setText(getString(R.string.rupees_symbol)+" "+ data.getCrnNo());
                    rideEarningBrkUpTaxes.setText("- "+getString(R.string.rupees_symbol)+" "+ data.getTaxCharge());
                    customerBrkupGst.setText(getString(R.string.rupees_symbol)+" "+ data.getTaxCharge());
                    rideEarningBrkUpRideEarning.setText(getString(R.string.rupees_symbol)+" "+ data.getDriverearning());
                    rideEarningBrkUpToll.setText("+ "+getString(R.string.rupees_symbol)+" "+ data.getTolltax());
                    rideEarningBrkUpCashCollected.setText(getString(R.string.rupees_symbol)+" "+ data.getCashPaid());
                    rideEarningBrkUpXooToCollect.setText(getString(R.string.rupees_symbol)+" "+ data.getXootocollect());
                }
            }

            @Override
            public void onFailure(Call<TripDetailResponceFields> call, Throwable t) {
            }
        });
    }
}




