package com.xoocardriver.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.xoocardriver.R;
import com.xoocardriver.helper.Constant;
import com.xoocardriver.model.Model;
import com.xoocardriver.preference.PrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by elite on 21/12/16.
 */

public class TripSummary extends AppCompatActivity {
    private TextView source;
    private TextView destination;
    private TextView travel_time_value;
    private TextView distance_value;
    private TextView amount_value;
    private TextView payment_through_value;
    private TextView collect_cash_value;
    private RelativeLayout ok_layout;
    Typeface normal_fonts;
    private String total;
    private String c_source;
    private String c_destination;
    private String atual_time;
    private String distance_km;
    private String payment_mode;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_trip_summary);
        Context ctx = TripSummary.this;

        Intent i=getIntent();
        total = i.getStringExtra("total");
        c_source = i.getStringExtra("c_source");
        atual_time = i.getStringExtra("atual_time");
        distance_km = i.getStringExtra("distance_km");
        payment_mode = i.getStringExtra("payment_mode");
        c_destination = i.getStringExtra("c_destination");
        String couponprice = i.getStringExtra("couponprice");

        source = (TextView) findViewById(R.id.source);
        destination = (TextView) findViewById(R.id.destination);
        TextView travel_time_text = (TextView) findViewById(R.id.travel_time_text);
        travel_time_value = (TextView) findViewById(R.id.travel_time_value);
        TextView distance_text = (TextView) findViewById(R.id.distance_text);
        distance_value = (TextView) findViewById(R.id.distance_value);
        TextView amount_text = (TextView) findViewById(R.id.amount_text);
        amount_value = (TextView) findViewById(R.id.amount_value);
        TextView payment_through_text = (TextView) findViewById(R.id.payment_through_text);
        payment_through_value = (TextView) findViewById(R.id.payment_through_value);
        TextView collect_cash_text = (TextView) findViewById(R.id.collect_cash_text);
        collect_cash_value = (TextView) findViewById(R.id.collect_cash_value);
        TextView coupon_value = (TextView) findViewById(R.id.coupon_value);

        TextView coupon_text = (TextView) findViewById(R.id.coupon_text);
        TextView ok = (TextView) findViewById(R.id.ok);
        ok_layout = (RelativeLayout) findViewById(R.id.ok_layout);
        PrefManager prefManager = new PrefManager(ctx);
        String rideid = prefManager.getRideId();
        parrallelcompleteApi(ctx, Constant.COMPLETE_PARALLELPOST_REQUEST_URL, rideid);

        normal_fonts = Typeface.createFromAsset(ctx.getAssets(), "fonts/OpenSans-Regular.ttf");

        source.setText(c_source);
        destination.setText(c_destination);
        travel_time_value.setText(atual_time + " min");
        distance_value.setText(distance_km + " km");
        collect_cash_value.setText("₹ " + total);
        payment_through_value.setText(payment_mode);
        amount_value.setText("₹ " + total);
        coupon_value.setText("₹ " + couponprice);
        source.setTypeface(normal_fonts);
        destination.setTypeface(normal_fonts);
        travel_time_text.setTypeface(normal_fonts);
        travel_time_value.setTypeface(normal_fonts);
        distance_text.setTypeface(normal_fonts);
        distance_value.setTypeface(normal_fonts);
        amount_text.setTypeface(normal_fonts);
        amount_value.setTypeface(normal_fonts);
        payment_through_text.setTypeface(normal_fonts);
        payment_through_value.setTypeface(normal_fonts);
        collect_cash_text.setTypeface(normal_fonts);
        collect_cash_value.setTypeface(normal_fonts);
        coupon_text.setTypeface(normal_fonts);
        coupon_value.setTypeface(normal_fonts);
        ok.setTypeface(normal_fonts);

        listenerView();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void parrallelcompleteApi(Context context, String url, final String rideid) {
        Log.d("p complete flow:", " url = " + url + ": rideid :" + rideid);

        RequestQueue rq = Volley.newRequestQueue(context);
        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("p complete  response ", "= " + response);
                JSONObject jsonObject= null;
                try {

                    jsonObject = new JSONObject(response);
                     total= Model.getString(jsonObject,"total");
                     c_source=Model.getString(jsonObject,"source");
                     c_destination=Model.getString(jsonObject,"destination");
                     payment_mode=Model.getString(jsonObject,"pay_mode");

                    if (!c_source.equals("null") || !c_source.equals("") || !c_source.isEmpty()) {
                        source.setText(c_source);
                    } else {
                        source.setText("");
                    }

                    if (!c_destination.equals("null") || !c_destination.equals("") || !c_destination.isEmpty()) {
                        destination.setText(c_destination);
                    } else {
                        destination.setText("");
                    }
                    if (!atual_time.equals("null") || !atual_time.equals("") || !atual_time.isEmpty()) {
                        travel_time_value.setText(atual_time + " min");
                    } else {
                        travel_time_value.setText("0" + " min");
                    }
                    if (!distance_km.equals("null") || !distance_km.equals("") || !distance_km.isEmpty()) {
                        distance_value.setText(distance_km + " km");
                    } else {
                        distance_value.setText("0" + " km");
                    }

                    if (!total.equals("null") || !total.equals("") || !total.isEmpty()) {
                        collect_cash_value.setText("₹ " + total);
                    } else {
                        collect_cash_value.setText("₹ " + "0");
                    }
                    if (!payment_mode.equals("null") || !payment_mode.equals("") || !payment_mode.isEmpty()) {
                        payment_through_value.setText(payment_mode);
                    } else {
                        payment_through_value.setText("");
                    }
                    if (!total.equals("null") || !total.equals("") || !total.isEmpty()) {
                        amount_value.setText("₹ " + total);

                    } else {
                        amount_value.setText("₹ " + "0");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // progressDialog.dismiss();
                Log.e("dkp error ocurred", "Error");
                VolleyLog.d("Error", "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("error ocurred", "TimeoutError");
                    // Toast.makeText(context, "TimeoutError", Toast.LENGTH_LONG).show();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                String[] tag = {"rideid"};
                String[] value = {rideid};

                for (int i = 0; i < tag.length; i++)
                    params.put(tag[i], value[i]);
                return checkParams(params);
            }
        };

        postReq.setRetryPolicy(new DefaultRetryPolicy(
                800000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(postReq);
    }


    public void listenerView() {
        ok_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(TripSummary.this,CollectCash.class);
                i.putExtra("total",total);
                i.putExtra("check_payment",payment_mode);
                startActivity(i);
            }
        });
    }


    private Map<String, String> checkParams(Map<String, String> map) {
        for (Map.Entry<String, String> pairs : map.entrySet()) {
            if (pairs.getValue() == null) {
                map.put(pairs.getKey(), "");
            }
        }
        return map;
    }

    @Override
    public void onBackPressed() {
    }
}
