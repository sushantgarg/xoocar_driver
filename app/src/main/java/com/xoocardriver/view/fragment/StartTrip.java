package com.xoocardriver.view.fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.xoocardriver.CommonMethods;
import com.xoocardriver.R;
import com.xoocardriver.helper.Constant;
import com.xoocardriver.helper.MyApplication;
import com.xoocardriver.map.GeocodingLocation;
import com.xoocardriver.map.MapDirectionsParser;
import com.xoocardriver.preference.PrefManager;
import com.xoocardriver.service.LocationUpdaterGoogleClientService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by elite on 21/12/16.
 */

public class StartTrip extends AppCompatActivity implements  OnMapReadyCallback, View.OnClickListener {

    static final Integer LOCATION = 0x1;
    public ProgressDialog progressDialog;
    Typeface normal_fonts;
    private String brn_no = "";
    private LinearLayout start_trip_layout;
    private GoogleMap googleMap;
    private ArrayList<LatLng> points = null;
    private Polyline mPolyline = null;
    private EditText binEdt;
    private PrefManager prefManager;
    private String rideid;
    private String c_first_name;
    private String c_source;
    private String c_destination;
    private LatLng source_LatLong;
    private LatLng destination_LatLong;
    private TextView sosTv;
    private Context ctx;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_start_trip);

        ctx=StartTrip.this;
        prefManager = new PrefManager(ctx);

        Intent i=getIntent();
        rideid =        i.getStringExtra("rideid");
        c_first_name =  i.getStringExtra("c_first_name");
        c_source =      i.getStringExtra("c_source");
        c_destination = i.getStringExtra("c_destination");

        TextView pick_up_text = (TextView)       findViewById(R.id.start_trip_pickup_source_tv);
        TextView pick_up_label_text = (TextView) findViewById(R.id.start_trip_pickup_source_h_tv);
        TextView start_trip_text = (TextView)    findViewById(R.id.start_trip_pickup_start_trip_tv);
        TextView drop_text = (TextView)          findViewById(R.id.start_trip_pickup_destination_tv);
        TextView drop_lable_text = (TextView)    findViewById(R.id.start_trip_pickup_destunation_h_tv);
        TextView nameTv = (TextView)             findViewById(R.id.start_trip_pickup_name_tv);
        TextView pickTv = (TextView)             findViewById(R.id.start_trip_pickup_pickup_tv);

        binEdt = (EditText)                      findViewById(R.id.start_trip_pickup_bin_no_tv);


        start_trip_layout = (LinearLayout)        findViewById(R.id.start_trip_layout_ll);
        pick_up_text.setTypeface(normal_fonts);
        pick_up_label_text.setTypeface(normal_fonts);
        start_trip_text.setTypeface(normal_fonts);
        drop_text.setTypeface(normal_fonts);
        drop_lable_text.setTypeface(normal_fonts);
        nameTv.setTypeface(normal_fonts);
        binEdt.setTypeface(normal_fonts);
        binEdt.requestFocus();

        pick_up_text.setText(c_source);
        drop_text.setText(c_destination);
        nameTv.setText(c_first_name);

        normal_fonts = Typeface.createFromAsset(ctx.getAssets(), "fonts/OpenSans-Regular.ttf");

        FragmentManager fragmentManager = getSupportFragmentManager();

        SupportMapFragment mMapFragment = (SupportMapFragment) fragmentManager.findFragmentById(R.id.start_trip_location_map);

        if ( mMapFragment == null ) {
            mMapFragment = SupportMapFragment.newInstance();
            fragmentManager.beginTransaction().replace(R.id.start_trip_location_map, mMapFragment).commit();
        }

        mMapFragment.getMapAsync(this);

        listenerView();

        try {
            assert c_source != null;
            source_LatLong = GeocodingLocation.getLocationFromAddress(ctx, c_source);
            if (c_destination != null || !c_destination.equals("") || !c_destination.equals("null") || !c_destination.isEmpty()) {
                destination_LatLong = GeocodingLocation.getLocationFromAddress(ctx, c_destination);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }

        Typeface face = Typeface.createFromAsset(ctx.getAssets(), "fonts/OpenSans-Regular.ttf");
        pick_up_text.setTypeface(face);
        drop_text.setTypeface(face);
        pick_up_label_text.setTypeface(face);
        start_trip_text.setTypeface(face);
        drop_lable_text.setTypeface(face);
        nameTv.setTypeface(face);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void initView(View view) {
    }

    public void listenerView() {
        start_trip_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                brn_no = binEdt.getText().toString();
                if ( !brn_no.equals("") || !brn_no.isEmpty() ) {
                    String driver_id = prefManager.getKeyUserId();
                    try {
                        progressDialog = new ProgressDialog(ctx);
                        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
                        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                        progressDialog.show();
                        progressDialog.setContentView(R.layout.progressbar);
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                    sendToStartTrip(Constant.UPDATE_ROOT_LAT_LONG_POST_REQUEST_URL, "" + prefManager.getCurrentLat(), "" + prefManager.getCurrentLng(), rideid);
                    startTripRequest(ctx, Constant.START_TRIP_POST_REQUEST_URL, driver_id, rideid, brn_no);
                } else {
                    Toast.makeText(ctx, "Please Enter BRN number", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void sendToStartTrip(String url, final String lat, final String longi, final String ride_id) {

        RequestQueue rq = Volley.newRequestQueue(ctx);
        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("Start update latlong 1", "=start trip= " + response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error", "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.d("error ocurred", "TimeoutError");
                    // Toast.makeText(context, "TimeoutError", Toast.LENGTH_LONG).show();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                String driver_id = prefManager.getKeyUserId();
                Log.d("start location upadate", ":" + driver_id);
                String[] tag = {"ride_id", "d_id", "d_latt", "d_long"};
                String[] value = {ride_id, driver_id, lat, longi};

                for (int i = 0; i < tag.length; i++)
                    params.put(tag[i], value[i]);
//                return params;
                return checkParams(params);
            }
        };
        postReq.setRetryPolicy(new DefaultRetryPolicy(
                800000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(postReq);
    }

    public void startTripRequest(Context context, String url, final String driver_id, final String rideid, final String brn_no) {
        RequestQueue rq = Volley.newRequestQueue(context);
        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                if (!response.contains("Something Went Wrong")) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String c_first_name = jsonObject.getString("c_first_name");      ///9719039102
                        String c_source = jsonObject.getString("source");
                        String c_destination = jsonObject.getString("destination");
                        String c_id = jsonObject.getString("c_id");
                        String d_latt = jsonObject.getString("d_latt");
                        String d_long = jsonObject.getString("d_long");
                        String c_destLat=jsonObject.getString("c_dest_lat");
                        String c_destLng=jsonObject.getString("c_dest_lng");
                        String cLat=jsonObject.getString("c_latt");
                        String cLng=jsonObject.getString("c_long");
                        String city_id = jsonObject.getString("city");
                        String rideid = jsonObject.getString("rideid");

                        if (jsonObject.has("rideid")) {
                            prefManager.setRideIdCheck(rideid);
                            prefManager.setRideId(rideid);
                            prefManager.setIsRideRunning(true);

                            if( !CommonMethods.isServiceRunning(ctx,LocationUpdaterGoogleClientService.class))
                                ctx.startService(new Intent(ctx,LocationUpdaterGoogleClientService.class));


                            Intent i1=new Intent(StartTrip.this,CompleteTrip.class);

                            i1.putExtra("rideid",rideid);
                            i1.putExtra("c_first_name",c_first_name);
                            i1.putExtra("c_source",c_source);
                            i1.putExtra("c_destination",c_destination);
                            i1.putExtra("c_lat",cLat);
                            i1.putExtra("c_lng",cLng);
                            i1.putExtra("c_dest_lat",c_destLat);
                            i1.putExtra("c_dest_lng",c_destLng);
                            startActivity(i1);

                        } else {
                            Toast.makeText(ctx, "Server Not Responding", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(ctx, "BIN not Confirm", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.e("dkp error ocurred", "Error");
                VolleyLog.d("Error", "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("error ocurred", "TimeoutError");
                    // Toast.makeText(context, "TimeoutError", Toast.LENGTH_LONG).show();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                String[] tag = {"driverid", "rideid", "brn_no"};
                String[] value = {driver_id, rideid, brn_no};
                for (int i = 0; i < tag.length; i++)
                    params.put(tag[i], value[i]);
                return checkParams(params);
            }
        };

        postReq.setRetryPolicy(new DefaultRetryPolicy(
                800000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(postReq);
    }

    private Map<String, String> checkParams(Map<String, String> map) {
        for (Map.Entry<String, String> pairs : map.entrySet()) {
            if (pairs.getValue() == null) {
                map.put(pairs.getKey(), "");
            }
        }
        return map;
    }

    private void traceMe() {
        String srcParam = "";
        String destParam = "";
        try {
            srcParam = source_LatLong.latitude + "," + source_LatLong.longitude;
            destParam = destination_LatLong.latitude + "," + destination_LatLong.longitude;
        } catch (Throwable e) {
            e.printStackTrace();
        }
        Log.d("drop lat pp1", ":" + destParam);

        String url = "https://maps.googleapis.com/maps/api/directions/json?origin=" + srcParam + "&destination=" + destParam + "&sensor=false&units=metric&mode=driving&key=" + Constant.google_direction_purchage_key;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("dkp lat long", ":" + response);
                        MapDirectionsParser parser = new MapDirectionsParser(ctx);
                        List<List<HashMap<String, String>>> routes = parser.parse(response);

                        for (int i = 0; i < routes.size(); i++) {
                            points = new ArrayList<LatLng>();
                            List<HashMap<String, String>> path = routes.get(i);
                            Log.d("dkp lat long", ":" + path);
                            for (int j = 0; j < path.size(); j++) {
                                HashMap<String, String> point = path.get(j);
                                double lat = Double.parseDouble(point.get("lat"));
                                double lng = Double.parseDouble(point.get("lng"));
                                LatLng position = new LatLng(lat, lng);
                                points.add(position);
                            }
                        }
                        Log.d("dkp points lat long", ":" + points);
                        drawPoints(points, googleMap);
                        //  PD.dismiss();
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            Log.d("error ocurred", "TimeoutError");
                            Toast.makeText(ctx, "No internet connection", Toast.LENGTH_LONG).show();
                        }
                    }
                });
        MyApplication.getInstance().addToRequestQueue(jsonObjectRequest, "jreq");
    }

    private void addMarks1(GoogleMap googleMap, String pickup, String drop) {
        googleMap.clear();

        try {
           // Log.d("dkp destination lat long", ":" + destination_LatLong);
            LatLng driver_LatLong = new LatLng(prefManager.getCurrentLat(), prefManager.getCurrentLng());
            String driver = GeocodingLocation.getCompleteAddressString(ctx, prefManager.getCurrentLat(), prefManager.getCurrentLng());

            if (destination_LatLong != null) {
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(source_LatLong);
                markerOptions.title("Pickup");
                markerOptions.snippet(pickup);
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.green_location));
                googleMap.addMarker(markerOptions);
                markerOptions.position(destination_LatLong);
                markerOptions.title("Drop");
                markerOptions.snippet(drop);
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.red_location));
                googleMap.addMarker(markerOptions);
                markerOptions.position(driver_LatLong);
                markerOptions.title("Driver");
                markerOptions.snippet(driver);
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.driver_black));
                googleMap.addMarker(markerOptions);
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(source_LatLong, 17);
                googleMap.animateCamera(cameraUpdate);
                traceMe();

            } else {
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(source_LatLong);
                markerOptions.title("Pickup");
                markerOptions.snippet(pickup);
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.green_location));
                googleMap.addMarker(markerOptions);

                markerOptions.position(driver_LatLong);
                markerOptions.title("Driver");
                markerOptions.snippet(driver);
                markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.driver_black));
                googleMap.addMarker(markerOptions);

                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(source_LatLong, 17);
                googleMap.animateCamera(cameraUpdate);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }


    private void drawPoints(ArrayList<LatLng> points, GoogleMap mMaps) {
        if (points == null) {
            return;
        }
        ArrayList<LatLng> traceOfMe = points;
        PolylineOptions polylineOpt = new PolylineOptions();
        for (LatLng latlng : traceOfMe) {
            polylineOpt.add(latlng);
        }
        polylineOpt.color(Color.BLACK);
        if (mPolyline != null) {
            mPolyline.remove();
            mPolyline = null;
        }
        if (mMaps != null) {
            mPolyline = mMaps.addPolyline(polylineOpt);
        }
        if (mPolyline != null)
            mPolyline.setWidth(7);
        //mFooterLayout.setVisibility(View.GONE);
    }

    @Override
    public void onPause() {
        super.onPause();
        // mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        // mMapView.onLowMemory();
    }

    @Override
    public void onClick(View v) {

    }

    public void onConnect() {

                //place marker at current position
        double latitude = prefManager.getCurrentLat();
        double longitude = prefManager.getCurrentLng();
                if (googleMap != null)
                    googleMap.clear();
                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(latitude, longitude)).zoom(17)
                        .build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                addMarks1(googleMap, c_source, c_destination);

    }


    @Override
    public void onMapReady(final GoogleMap googleMap) {
        Log.d("onMapReady", ":" + "onMapReady");
        this.googleMap = googleMap;

        try {
            googleMap.getUiSettings().setZoomControlsEnabled(false);
            googleMap.getUiSettings().setMyLocationButtonEnabled(false);
            googleMap.getUiSettings().setCompassEnabled(true);
            googleMap.getUiSettings().setRotateGesturesEnabled(true);
            googleMap.getUiSettings().setZoomGesturesEnabled(true);

            if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            googleMap.setMyLocationEnabled(true);
            onConnect();

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}

