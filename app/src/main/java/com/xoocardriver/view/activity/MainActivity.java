/*
package com.xoocardriver.view.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.IntentCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.xoocardriver.CommonMethods;
import com.xoocardriver.FireBase.FirebaseManager;
import com.xoocardriver.GoogleAnalytic.AnalyticsApplication;
import com.xoocardriver.IncentiveActivity;
import com.xoocardriver.InviteDriver;
import com.xoocardriver.R;
import com.xoocardriver.Training;
import com.xoocardriver.delegates.FragmentToMainCommunicator;
import com.xoocardriver.helper.CircularImageView;
import com.xoocardriver.helper.Constant;
import com.xoocardriver.helper.CustomVolleyRequest;
import com.xoocardriver.model.Model;
import com.xoocardriver.preference.PrefManager;
import com.xoocardriver.service.LocationUpdaterGoogleClientService;
import com.xoocardriver.view.fragment.AboutUsFragment;
import com.xoocardriver.view.fragment.CompleteTrip;
import com.xoocardriver.view.fragment.EmergencyContactFragment;
import com.xoocardriver.view.fragment.Home;
import com.xoocardriver.view.fragment.InviteFriends;
import com.xoocardriver.view.fragment.OfflineOnline;
import com.xoocardriver.view.fragment.PickUpReceiving;
import com.xoocardriver.view.fragment.Profile;
import com.xoocardriver.view.fragment.RideRequest;
import com.xoocardriver.view.fragment.SignIn;
import com.xoocardriver.view.fragment.TripEarning;
import com.xoocardriver.view.fragment.TripList;
import com.xoocardriver.view.fragment.UploadDocument;
import com.xoocardriver.view.fragment.ViewDocument;
import com.xoocardriver.view.fragment.Waybill;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,FragmentToMainCommunicator {
    public static Toolbar toolbar;
    public static TextView toolbarText;
    private PrefManager prefManager;
    public static DrawerLayout drawer;
    private LinearLayout document_tab, document_sub_category, about; //  taking about tab for testing purpose when driver accept notification
    private ImageView expand_or_collapse;
    private int click_time = 0;       // how and hide document sub tag

    private LinearLayout helpLL;
    private LinearLayout earningLL, profileLL;
    private LinearLayout currentTripLL;
    private LinearLayout viewdocumentLL;
    private LinearLayout uploadDocumentLL,incentivelayout;
    public static TextView menuProfileNameTv;
    public static TextView menuProfileEmailTv;
    public static CircularImageView menuProfilPicIv;
    private String driver_id;
    private LinearLayout logoutLL;
    private LinearLayout waybillLL;
//    public static MainActivity mainActivity;
    private ImageLoader im;

    private GoogleApiClient client;
    private LinearLayout invite_fireds_tab;
    private LinearLayout homeLL;
    private ProgressDialog progressDialog;
    private LinearLayout emerContactLL;
    private ImageView tootbarIconLogoIv;
    private Tracker mTracker;
    private LinearLayout termConditionLL;
    private LinearLayout policyLL;
    private FragmentManager fManager ;
    private boolean isRingerActive;
    private static final int WRITE_REQUEST_CODE = 1;
    private LinearLayout menu_invite_driver_ll,menu_training_ll;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context=MainActivity.this;

        initView();
        checkAndRequestPermissions();

        fManager = getSupportFragmentManager();

        // askForPermission1(Manifest.permission.ACCESS_FINE_LOCATION, LOCATION, "oncreate");
        listenerView();
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
        Intent intent = new Intent(MainActivity.this, LocationUpdaterService.class);
        startService(intent);


        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        // starts a new session and notification to google analytics
        mTracker.send(new HitBuilders.ScreenViewBuilder()
                .setNewSession()
                .build());
    }

    protected void onSaveInstanceState(Bundle icicle) {
        icicle.putLong("param", 10000);
        super.onSaveInstanceState(icicle);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if(savedInstanceState!=null ) {
            PackageManager packageManager = MainActivity.this.getPackageManager();
            Intent intent = packageManager.getLaunchIntentForPackage(MainActivity.this.getPackageName());
            ComponentName componentName = intent.getComponent();
            Intent mainIntent = IntentCompat.makeRestartActivityTask(componentName);
            MainActivity.this.startActivity(mainIntent);
            System.exit(0);
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

        String ride_id = prefManager.getRideIdCheck();
        driver_id = prefManager.getKeyUserId();
        if ( driver_id == null || driver_id.isEmpty() || driver_id.equals("") ) {
            lockDrawer();
            FragmentTransaction fTransaction = fManager.beginTransaction();
            Fragment fragment = fManager.findFragmentByTag("signIn");
            // If fragment doesn't exist yet, create one
            if ( fragment == null ) {
                fTransaction.add(R.id.frame_layout, new SignIn(), "signIn");
            }
            else { // re-use the old fragment
                fTransaction.replace(R.id.frame_layout, fragment, "signIn");
            }
            fTransaction.commitAllowingStateLoss();
        } else {
            if(  ride_id.equals("0") ) {
                if( prefManager.getDriverOnlineOfflineStatus().equals("1") ) {
                    getSupportFragmentManager().beginTransaction().add(R.id.frame_layout, new OfflineOnline( ), "OfflineOnline").commitAllowingStateLoss();
                } else {
                    getSupportFragmentManager().beginTransaction().add(R.id.frame_layout, new Home(), "home").commitAllowingStateLoss();
                }
            } else {
                parrallelstartApi(context, Constant.START_PARALLELPOST_REQUEST_URL, ride_id);
            }
            getProfileDetails(context, Constant.GET_PROFILE_POST_REQUEST_URL, driver_id);
        }

        mTracker.setScreenName("Welcome Screen");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("aaaa","resume");
    }

    @Override
    public void onStart() {
        super.onStart();
        try {
            client.connect();
            AppIndex.AppIndexApi.start(client, getIndexApiAction());
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        try {
            AppIndex.AppIndexApi.end(client, getIndexApiAction());
            client.disconnect();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public void communicateToMain(String msg) {
        isRingerActive = msg.equals("close");
    }

    public void initView() {
        menuProfileNameTv = (TextView) findViewById(R.id.menu_profile_name_tv);
        menuProfileEmailTv = (TextView) findViewById(R.id.menu_profile_email_tv);
        menuProfilPicIv = (CircularImageView) findViewById(R.id.menu_profile_pic_iv);

        TextView homeTv;
        TextView profileTv;
        TextView documentTv;
        TextView viewDocumentTv;
        TextView addDocumentTv;
        TextView tripHistoryTv;
        TextView earningTv;
        TextView waybillTv;
        TextView inviteFriendsTv;
        TextView policyTv;
        TextView helpTv;
        TextView aboutTv;
        TextView settingsTv;
        TextView logoutTv;

        prefManager = new PrefManager(context);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbarText = (TextView) findViewById(R.id.toolbar_text);

        setSupportActionBar(toolbar);
        toolbar.setTitle("");
        toolbarText.setText("");

        emerContactLL = (LinearLayout) findViewById(R.id.menu_emergency_contact_ll);

        homeLL = (LinearLayout) findViewById(R.id.menu_home_ll);
        viewdocumentLL = (LinearLayout) findViewById(R.id.menu_view_document_ll);
        uploadDocumentLL = (LinearLayout) findViewById(R.id.menu_upload_document_ll);
        incentivelayout = (LinearLayout) findViewById(R.id.menu_incentive_ll);
        document_sub_category = (LinearLayout) findViewById(R.id.document_sub_category);
        expand_or_collapse = (ImageView) findViewById(R.id.expand_or_collapse);
        about = (LinearLayout) findViewById(R.id.about);
        profileLL = (LinearLayout) findViewById(R.id.menu_profile_ll);
        helpLL = (LinearLayout) findViewById(R.id.menu_help_ll);
        termConditionLL = (LinearLayout) findViewById(R.id.menu_term_condition_ll);

        earningLL = (LinearLayout) findViewById(R.id.menu_earnings_ll);
        currentTripLL = (LinearLayout) findViewById(R.id.menu_trip_history_ll);
        waybillLL = (LinearLayout) findViewById(R.id.menu_way_bill_ll);

        policyLL = (LinearLayout) findViewById(R.id.menu_policy_ll);
        logoutLL = (LinearLayout) findViewById(R.id.logout_ll);
        menu_invite_driver_ll = (LinearLayout) findViewById(R.id.menu_invite_driver_ll);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.mipmap.menu_icon, this.getTheme());
        toggle.setDrawerIndicatorEnabled(false);
        toggle.setHomeAsUpIndicator(drawable);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        TextView emerContactTv = (TextView) findViewById(R.id.menu_emergency_contact_tv);
        homeTv = (TextView) findViewById(R.id.menu_home_tv);
        profileTv = (TextView) findViewById(R.id.menu_profile_tv);
        documentTv = (TextView) findViewById(R.id.menu_document_tv);
        viewDocumentTv = (TextView) findViewById(R.id.menu_document_view_tv);
        addDocumentTv = (TextView) findViewById(R.id.menu_document_add_tv);
        tripHistoryTv = (TextView) findViewById(R.id.menu_trip_history_tv);
        earningTv = (TextView) findViewById(R.id.menu_earnings_tv);
        waybillTv = (TextView) findViewById(R.id.menu_way_bill_tv);
        inviteFriendsTv = (TextView) findViewById(R.id.menu_invite_friends_tv);
        policyTv = (TextView) findViewById(R.id.menu_policy_tv);
        helpTv = (TextView) findViewById(R.id.menu_help_tv);
        aboutTv = (TextView) findViewById(R.id.menu_about_tv);
        settingsTv = (TextView) findViewById(R.id.menu_setting_tv);
        logoutTv = (TextView) findViewById(R.id.menu_logout_tv);
        tootbarIconLogoIv = (ImageView) findViewById(R.id.toolbar_home_logo_iv);
        menu_training_ll = (LinearLayout) findViewById(R.id.menu_training_ll);

        Typeface face = Typeface.createFromAsset( getAssets(), "fonts/OpenSans-Regular.ttf");
        menuProfileEmailTv.setTypeface(face);
        toolbarText.setTypeface(face);
        menuProfileNameTv.setTypeface(face);
        homeTv.setTypeface(face);
        profileTv.setTypeface(face);
        documentTv.setTypeface(face);
        viewDocumentTv.setTypeface(face);
        addDocumentTv.setTypeface(face);
        tripHistoryTv.setTypeface(face);
        earningTv.setTypeface(face);
        waybillTv.setTypeface(face);
        inviteFriendsTv.setTypeface(face);
        policyTv.setTypeface(face);
        helpTv.setTypeface(face);
        aboutTv.setTypeface(face);
        settingsTv.setTypeface(face);
        logoutTv.setTypeface(face);
        emerContactTv.setTypeface(face);
        unLockDrawer();
    }

    public void listenerView() {
        tootbarIconLogoIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lockDrawer();
                String status = prefManager.getDriverOnlineOfflineStatus();
                String rideId = prefManager.getRideIdCheck();
                if(  rideId.equals("0")) {
                    if (status != null && !status.isEmpty()) {
                        if (status.equals("0")) {
                            getSupportFragmentManager().beginTransaction().add(R.id.frame_layout, new Home(), "home").commitAllowingStateLoss();
                        } else {
                            getSupportFragmentManager().beginTransaction().add(R.id.frame_layout, new OfflineOnline( ), "OfflineOnline").commitAllowingStateLoss();
                        }
                    } else {
                        getSupportFragmentManager().beginTransaction().add(R.id.frame_layout, new Home(), "home").commitAllowingStateLoss();
                    }
                } else {
                    Toast.makeText(MainActivity.this, "Not allowed during active ride", Toast.LENGTH_SHORT).show();
                }
            }
        });

        homeLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                unLockDrawer();
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
                String status = prefManager.getDriverOnlineOfflineStatus();
                if (status != null && !status.isEmpty()) {
                    if (status.equals("0")) {
                        getSupportFragmentManager().beginTransaction().add(R.id.frame_layout, new Home(), "home").commitAllowingStateLoss();
                    } else {
                        getSupportFragmentManager().beginTransaction().add(R.id.frame_layout, new OfflineOnline( ), "OfflineOnline").commitAllowingStateLoss();
                    }
                } else {
                    getSupportFragmentManager().beginTransaction().add(R.id.frame_layout, new Home(), "home").commitAllowingStateLoss();
                }
            }
        });

        invite_fireds_tab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                unLockDrawer();
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
                Intent i=new Intent(context,InviteFriends.class);
                startActivity(i);
            }
        });

        menu_invite_driver_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(MainActivity.this, InviteDriver.class);
                startActivity(i);
            }
        });


        document_tab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                unLockDrawer();
                if (click_time == 0) {
                    document_sub_category.setVisibility(View.VISIBLE);
                    click_time = 1;
                    expand_or_collapse.setImageResource(R.mipmap.collapse);
                } else {
                    document_sub_category.setVisibility(View.GONE);
                    click_time = 0;
                    expand_or_collapse.setImageResource(R.mipmap.expand);
                }
            }
        });

        profileLL.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                unLockDrawer();
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
               Intent i=new Intent(context,Profile.class);
                startActivity(i);
            }
        });

        emerContactLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                unLockDrawer();
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }

                Intent i=new Intent(context,EmergencyContactFragment.class);
                startActivity(i);
            }
        });

        incentivelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(MainActivity.this, IncentiveActivity.class);
                startActivity(i);
            }
        });

        uploadDocumentLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                unLockDrawer();
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
                Intent i=new Intent(context,UploadDocument.class);
                startActivity(i);
            }
        });

        viewdocumentLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                unLockDrawer();
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
                Intent i=new Intent(context,ViewDocument.class);
                startActivity(i);
            }
        });

        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                unLockDrawer();
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
                Intent i=new Intent(context,AboutUsFragment.class);
                i.putExtra("url","http://www.xoocar.com/webapp/webapp/aboutus");
                startActivity(i);

            }
        });
        policyLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                unLockDrawer();
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }

                Intent i=new Intent(context,AboutUsFragment.class);
                i.putExtra("url","http://www.xoocar.com/webapp/webapp/policies");
                startActivity(i);

            }
        });

        helpLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                unLockDrawer();
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
                Intent i=new Intent(context,AboutUsFragment.class);
                i.putExtra("url","http://www.xoocar.com/webapp/webapp/contactus");
                startActivity(i);

            }
        });

        termConditionLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                unLockDrawer();
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
                Intent i=new Intent(context,AboutUsFragment.class);
                i.putExtra("url","http://www.xoocar.com/webapp/webapp/terms");
                startActivity(i);

            }
        });

        currentTripLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                unLockDrawer();
                if (!isRingerActive) {
                    if (drawer.isDrawerOpen(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    }
                    Intent i=new Intent(context,TripList.class);
                    startActivity(i);
                } else {
                    Toast.makeText(MainActivity.this, "Not allowed during ringer", Toast.LENGTH_SHORT).show();
                }
            }
        });

        earningLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                unLockDrawer();
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
                Intent i=new Intent(context,TripEarning.class);
                startActivity(i);
//                getSupportFragmentManager().beginTransaction().
//                        replace(R.id.frame_layout, new TripEarning(), "TripEarning").commitAllowingStateLoss();
            }
        });

        waybillLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
                Intent i=new Intent(context,Waybill.class);
                startActivity(i);

            }
        });

        logoutLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }

                String ride_check = prefManager.getRideIdCheck();
                if (ride_check == null || ride_check.equals("0")) {
                    try {
                        progressDialog = new ProgressDialog(context);
                        progressDialog.setMessage("loading");
                        progressDialog.show();

                        driver_id = prefManager.getKeyUserId();
                        postDriverStatus(context, Constant.DRIVER_STATUS_POST_REQUEST_URL, driver_id, "0");
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(context, "Sorry! First complete trip", Toast.LENGTH_SHORT).show();
                }
            }
        });

        menu_training_ll.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i=new Intent( MainActivity.this, Training.class );
                startActivity(i);
            }
        });
    }

    public void getProfileDetails(final Context context, String url, final String did) {
        Log.d("get profile:", " url = " + url);
        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("get profile response ", "= " + response);
                JSONArray jsonArray = Model.getArray(response);
                try {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String first_name = Model.getString(jsonObject, "first_name");
                        String last_name = Model.getString(jsonObject, "last_name");
                        String user_email = Model.getString(jsonObject, "user_email");
                        String image = Model.getString(jsonObject, "profile_pic");
                        prefManager.setProfilePic(image);
                        prefManager.setKeyUserName(first_name + " " + last_name);
                        menuProfileNameTv.setText(first_name + " " + last_name);
                        menuProfileEmailTv.setText(user_email);
                        im = CustomVolleyRequest.getInstance(context).getImageLoader();
                        menuProfilPicIv.setImageUrl(Constant.PROFILE_IMAGE_URL + image, im);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("dkp error ocurred", "Error");
                VolleyLog.d("Error", "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("error ocurred", "TimeoutError");
                    // Toast.makeText(context, "TimeoutError", Toast.LENGTH_LONG).show();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                String[] tag = {"driver_id"};
                String[] value = {did};
                for (int i = 0; i < tag.length; i++)
                    params.put(tag[i], value[i]);
                return checkParams(params);
            }
        };
        postReq.setRetryPolicy(new DefaultRetryPolicy(
                800000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        CustomVolleyRequest.getInstance(context).addToRequestQueue(postReq, "profile_request");
    }

    private Map<String, String> checkParams(Map<String, String> map) {
        for (Map.Entry<String, String> pairs : map.entrySet()) {
            if (pairs.getValue() == null) {
                map.put(pairs.getKey(), "");
            }
        }
        return map;
    }

    private void lockDrawer() {
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    public static void unLockDrawer() {
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    public void postDriverStatus(Context context, String url, final String driverid, final String status) {
        Log.d("all vehicle:", " url = " + url + " driverid :" + driverid + " status:" + status);
        RequestQueue rq = Volley.newRequestQueue(context);
        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("post driver status ", "= " + response);
                if( progressDialog != null && progressDialog.isShowing() )
                progressDialog.dismiss();
                JSONArray jsonArray = Model.getArray(response);
                try {
                    assert jsonArray != null;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String d_current_status = jsonObject.getString("d_current_status");
                        if ( d_current_status.equals("0") ) {
                            prefManager.setDriverOnlineOfflineStatus("0");

                            FirebaseManager.getInstance().removeDriver(prefManager.getKeyUserId());
                            prefManager.clearSession();
                            prefManager.setKeyUserId("");
                            // MyApplication.clearApplicationData(mainActivity);
                            drawer.getDrawerLockMode(Gravity.LEFT);
                            lockDrawer();
                            if(  CommonMethods.isServiceRunning(MainActivity.this,LocationUpdaterGoogleClientService.class))
                                stopService(new Intent(MainActivity.this,LocationUpdaterGoogleClientService.class));
                            getSupportFragmentManager().beginTransaction().
                                    replace(R.id.frame_layout, new SignIn(), "sign_in").commitAllowingStateLoss();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if( progressDialog != null && progressDialog.isShowing() )
                progressDialog.dismiss();
                Log.e("dkp error ocurred", "Error");
                VolleyLog.d("Error", "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("error ocurred", "TimeoutError");
                    // Toast.makeText(context, "TimeoutError", Toast.LENGTH_LONG).show();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                String[] tag = {"act_mode", "d_id", "status"};
                String[] value = {"driver_status", driverid, status};

                for (int i = 0; i < tag.length; i++)
                    params.put(tag[i], value[i]);
                return checkParams(params);
            }
        };
        postReq.setRetryPolicy(new DefaultRetryPolicy(
                800000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(postReq);
    }


    public void parrallelstartApi(final Context context, String url, final String rideid) {
        RequestQueue rq = Volley.newRequestQueue(context);
        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String c_first_name = jsonObject.getString("c_first_name");      ///9719039102
                    String c_source = jsonObject.getString("source");
                    String c_destination = jsonObject.getString("destination");
                    String c_id = jsonObject.getString("c_id");
                    String d_latt = jsonObject.getString("d_latt");
                    String d_long = jsonObject.getString("d_long");
                    String c_destLat=jsonObject.getString("c_dest_lat");
                    String c_destLng=jsonObject.getString("c_dest_lng");
                    String cLat=jsonObject.getString("c_latt");
                    String cLng=jsonObject.getString("c_long");
                    String city_id = jsonObject.getString("city");
                    String rideid = jsonObject.getString("rideid");
                    String booking_status = jsonObject.getString("booking_status");

                    switch (booking_status) {
                        case "Pending":
//                        call timer
                            if( ! isFinishing()) {

                                Intent i=new Intent(MainActivity.this,RideRequest.class);
                                i.putExtra("source_address",c_source);
                                i.putExtra("detination_address",c_destination);
                                i.putExtra("cLat",cLat);
                                i.putExtra("cLng",cLng);
                                i.putExtra("dLat",c_destLat);
                                i.putExtra("dLng",c_destLng);
                                startActivity(i);
                            }

                            break;
                        case "Schedule":
//                        call pickup fragment
                            String first_name = jsonObject.getString( "first_name");
                            String last_name = jsonObject.getString( "last_name");
                            String profile_pic = jsonObject.getString("c_profilepic");
                            String c_contact = jsonObject.getString("c_contact");

                            if( ! isFinishing()) {

                                Intent i=new Intent(context,PickUpReceiving.class);

                                i.putExtra("rideid",rideid);
                                i.putExtra("c_id",c_id);
                                i.putExtra("c_first_name",c_first_name);
                                i.putExtra("c_destination",c_destination);
                                i.putExtra("c_profile_pic",profile_pic);
                                i.putExtra("d_latt",d_latt);
                                i.putExtra("d_long",d_long);
                                i.putExtra("city_id",city_id);
                                i.putExtra("contact",c_contact);
                                i.putExtra("first_name",first_name);
                                i.putExtra("c_source",c_source);
                                i.putExtra("last_name",last_name);
                                i.putExtra("c_lat",cLat);
                                i.putExtra("c_lng",cLng);
                                i.putExtra("c_dest_lat",c_destLat);
                                i.putExtra("c_dest_lng",c_destLng);
                                startActivity(i);
                            }
                            break;
                        case "Running":
                            if( ! isFinishing()) {

                                Intent i1=new Intent(MainActivity.this,CompleteTrip.class);

                                i1.putExtra("rideid",rideid);
                                i1.putExtra("c_first_name",c_first_name);
                                i1.putExtra("c_source",c_source);
                                i1.putExtra("c_destination",c_destination);
                                i1.putExtra("c_lat",cLat);
                                i1.putExtra("c_lng",cLng);
                                i1.putExtra("c_dest_lat",c_destLat);
                                i1.putExtra("c_dest_lng",c_destLng);
                                startActivity(i1);

                            }
                            break;
                        case "Cancelled":
                            if( ! isFinishing()) {
                                Intent i=new Intent(context,Home.class);
                                startActivity(i);
                            }
                            break;
                        default:
                            prefManager.setRideIdCheck("0");
                            if( ! isFinishing()) {
                                Intent i=new Intent(context,OfflineOnline.class);
                                startActivity(i);
                            }
                            break;
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("aaaa","error");
                prefManager.setRideIdCheck("0");
                Intent i=new Intent(context,OfflineOnline.class);
                startActivity(i);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                String[] tag = {"rideid"};//866
                String[] value = {rideid};

                for (int i = 0; i < tag.length; i++)
                    params.put(tag[i], value[i]);
                return checkParams(params);
            }
        };
        postReq.setRetryPolicy(new DefaultRetryPolicy(
                7*1000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rq.add(postReq);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Main Page") // TODO: Define a title for the content shown.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case WRITE_REQUEST_CODE: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_CONTACTS, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.RECEIVE_SMS, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user

                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions
                    if (perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED) {
                        //   Log.d(TAG, "sms & location services permission granted");
                        // process the normal flow
                        //else any one or both the permissions are not granted
                    } else {
                        // Log.d(TAG, "Some permissions are not granted ask again ");
                        //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                        // shouldShowRequestPermissionRationale will return true
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION) || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_CONTACTS)
                                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)
                                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.RECEIVE_SMS)) {
                            showDialogOK("Permission required for this app",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkAndRequestPermissions();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    finish();
                                                    // proceed with logic by disabling the related features or quit the app.
                                                    break;
                                            }
                                        }
                                    });
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            //  Toast.makeText(this, "Go to settings and enable permissions", Toast.LENGTH_LONG).show();
                            //                            //proceed with logic by disabling the related features or quit the app.
                        }
                    }
                }
            }
        }

    }

    private boolean checkAndRequestPermissions() {
        // String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.READ_CONTACTS,Manifest.permission.CAMERA};

        int WRITE_EXTERNAL_STORAGE1 = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int READ_CONTACTS1 = ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CONTACTS);
        int CAMERA1 = ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA);
        int locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int RECEIVE_SMS = ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS);

        List<String> listPermissionsNeeded = new ArrayList<>();

        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }

        if (WRITE_EXTERNAL_STORAGE1 != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (READ_CONTACTS1 != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_CONTACTS);
        }

        if (CAMERA1 != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }

        if (RECEIVE_SMS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.RECEIVE_SMS);
        }


        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), WRITE_REQUEST_CODE);
            return false;
        }
        return true;
    }


    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

//    public void parrallelpickupApi(MainActivity context, String url, final String rideid) {
//        Log.d("p pickup flow:", " url = " + url + ": rideid :" + rideid);
//        RequestQueue rq = Volley.newRequestQueue(context);
//        StringRequest postReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.d("p pickup  response ", "= " + response);
//                if(progressDialog != null && progressDialog.isShowing())
//                progressDialog.dismiss();
//                try {
//                    JSONObject jsonObject = new JSONObject(response);
//                    String c_first_name = jsonObject.getString("c_first_name");
//                    String first_name = Model.getString(jsonObject, "first_name");
//                    String last_name = Model.getString(jsonObject, "last_name");
//                    String c_source = jsonObject.getString("source");
//                    String c_destination = jsonObject.getString("destination");
//                    String c_id = jsonObject.getString("c_id");
//                    String d_latt = jsonObject.getString("d_latt");
//                    String d_long = jsonObject.getString("d_long");
//                    String city_id = jsonObject.getString("city");
//                    String rideid = jsonObject.getString("rideid");
//                    String profile_pic = jsonObject.getString("c_profilepic");
//                    String c_contact = Model.getString(jsonObject, "c_contact");
//
//                    getSupportFragmentManager()().beginTransaction().
//                            replace(R.id.frame_layout, new PickUpReceiving(mainActivity, rideid, c_id, c_first_name, c_source, c_destination, profile_pic, d_latt, d_long, city_id, c_contact, first_name, last_name), "pick_up_receivinhg").commitAllowingStateLoss();
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                progressDialog.dismiss();
//                Log.e("dkp error ocurred", "Error");
//                VolleyLog.d("Error", "Error: " + error.getMessage());
//                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
//                    Log.e("error ocurred", "TimeoutError");
//                    // Toast.makeText(context, "TimeoutError", Toast.LENGTH_LONG).show();
//                } else if (error instanceof AuthFailureError) {
//                    Log.e("error ocurred", "AuthFailureError");
//                    //  Toast.makeText(context, "AuthFailureError", Toast.LENGTH_LONG).show();
//                } else if (error instanceof ServerError) {
//                    Log.e("error ocurred", "ServerError");
//                    // Toast.makeText(context, "ServerError", Toast.LENGTH_LONG).show();
//                } else if (error instanceof NetworkError) {
//                    Log.e("error ocurred", "NetworkError");
//                    // Toast.makeText(context, "NetworkError", Toast.LENGTH_LONG).show();
//                } else if (error instanceof ParseError) {
//                    Log.e("error ocurred", "ParseError");
//                }
//                prefManager.setRideIdCheck("0");
//                Toast.makeText(MainActivity.this, "Ride ended by user", Toast.LENGTH_LONG).show();
//                getSupportFragmentManager().beginTransaction().add(R.id.frame_layout, new OfflineOnline(mainActivity, driver_id), "OfflineOnline").commitAllowingStateLoss();
//
//
//
//
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> params = new HashMap<>();
//
//                String[] tag = {"rideid"};
//                String[] value = {rideid};
//
//                for (int i = 0; i < tag.length; i++)
//                    params.put(tag[i], value[i]);
//                return checkParams(params);
//            }
//        };
//
//        postReq.setRetryPolicy(new DefaultRetryPolicy(
//                800000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        rq.add(postReq);
//    }

}
*/
