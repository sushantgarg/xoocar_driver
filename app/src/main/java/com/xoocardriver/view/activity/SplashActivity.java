package com.xoocardriver.view.activity;

/**
 * Created by elite on 22/12/16.
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.xoocardriver.GoogleAnalytic.AnalyticsApplication;
import com.xoocardriver.R;
import com.xoocardriver.preference.PrefManager;
import com.xoocardriver.view.fragment.Home;
import com.xoocardriver.view.fragment.SignIn;

public class SplashActivity extends Activity {
    private Tracker mTracker;
    private PrefManager prefManager;
    //    private static int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        prefManager=new PrefManager(SplashActivity.this);
//        chatAnonymousAuth();

        if (prefManager.getKeyUserId() == null) {
            Intent intent = new Intent(SplashActivity.this, SignIn.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(SplashActivity.this, Home.class);
            startActivity(intent);
        }

        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
    }

    @Override
    public void onResume() {
        super.onResume();
        mTracker.setScreenName("Splash Screen");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    private void chatAnonymousAuth() {
        FirebaseAuth mAuth;
        mAuth = FirebaseAuth.getInstance();
        mAuth.signInAnonymously()
                .addOnCompleteListener(SplashActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if ( task.isSuccessful() ) {
                            if( prefManager.getKeyUserId() == null ) {
                                Intent intent = new Intent(SplashActivity.this, SignIn.class);
                                startActivity(intent);
                            }else{
                                Intent intent = new Intent(SplashActivity.this, Home.class);
                                startActivity(intent);
                            }
                        } else {
                            Toast.makeText(SplashActivity.this, "No internet conection", Toast.LENGTH_SHORT).show();
                            chatAnonymousAuth();
                        }
                    }
                });
    }
}
