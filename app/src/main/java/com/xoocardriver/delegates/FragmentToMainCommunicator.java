package com.xoocardriver.delegates;

/**
 * Created by sushant on 7/30/17.
 */

public interface FragmentToMainCommunicator {

    public void communicateToMain(String msg);
}
